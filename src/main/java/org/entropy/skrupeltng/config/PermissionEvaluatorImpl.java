package org.entropy.skrupeltng.config;

import java.io.Serializable;
import java.util.Optional;

import org.entropy.skrupeltng.modules.ingame.database.Player;
import org.entropy.skrupeltng.modules.ingame.database.PlayerRepository;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
public class PermissionEvaluatorImpl implements PermissionEvaluator {

	@Autowired
	private UserDetailServiceImpl userService;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private StarbaseRepository starbaseRepository;

	@Autowired
	private ShipRepository shipRepository;

	@Value("${skr.disablePermissionChecks:false}")
	private boolean disablePermissionChecks;

	@Override
	public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
		if (disablePermissionChecks) {
			return true;
		}

		if (permission.equals("game")) {
			return checkGame(targetDomainObject);
		}

		if (permission.equals("planet")) {
			return checkPlanet(targetDomainObject);
		}

		if (permission.equals("starbase")) {
			return checkStarbase(targetDomainObject);
		}

		if (permission.equals("ship")) {
			return checkShip(targetDomainObject);
		}

		if (permission.equals("turnNotDonePlanet")) {
			return checkTurnNotDonePlanet(targetDomainObject);
		}

		if (permission.equals("turnNotDoneShip")) {
			return checkTurnNotDoneShip(targetDomainObject);
		}

		if (permission.equals("turnNotDoneStarbase")) {
			return checkTurnNotDoneStarbase(targetDomainObject);
		}

		return false;
	}

	public boolean checkGame(Object targetDomainObject) {
		Optional<Player> optional = playerRepository.findByGameIdAndLoginId((long)targetDomainObject, userService.getLoginId());
		return optional.isPresent();
	}

	public boolean checkPlanet(Object targetDomainObject) {
		return planetRepository.loginOwnsPlanet((long)targetDomainObject, userService.getLoginId());
	}

	public boolean checkStarbase(Object targetDomainObject) {
		return starbaseRepository.loginOwnsStarbase((long)targetDomainObject, userService.getLoginId());
	}

	public boolean checkShip(Object targetDomainObject) {
		return shipRepository.loginOwnsShip((long)targetDomainObject, userService.getLoginId());
	}

	public boolean checkTurnNotDonePlanet(Object targetDomainObject) {
		return playerRepository.playersTurnNotDoneForPlanet((long)targetDomainObject, userService.getLoginId());
	}

	public boolean checkTurnNotDoneShip(Object targetDomainObject) {
		return playerRepository.playersTurnNotDoneForShip((long)targetDomainObject, userService.getLoginId());
	}

	public boolean checkTurnNotDoneStarbase(Object targetDomainObject) {
		return playerRepository.playersTurnNotDoneForStarbase((long)targetDomainObject, userService.getLoginId());
	}

	@Override
	public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
		return false;
	}
}