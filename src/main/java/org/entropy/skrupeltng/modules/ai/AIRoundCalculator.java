package org.entropy.skrupeltng.modules.ai;

import org.entropy.skrupeltng.modules.ingame.database.Player;

public interface AIRoundCalculator {

	void calculateRound(Player player);
}