package org.entropy.skrupeltng.modules.ai.easy;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.entropy.skrupeltng.modules.ai.AbstractAIRoundCalculator;
import org.entropy.skrupeltng.modules.ingame.database.Player;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.entropy.skrupeltng.modules.ingame.modules.ship.TotalGoodCalculator;
import org.entropy.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportRequest;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.controller.StarbaseProductionRequest;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.controller.StarbaseShipConstructionRequest;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbaseHullStock;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbasePropulsionStock;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbaseWeaponStock;
import org.entropy.skrupeltng.modules.ingame.service.CoordHelper;
import org.entropy.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;
import org.entropy.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.entropy.skrupeltng.modules.masterdata.database.WeaponTemplate;
import org.entropy.skrupeltng.modules.masterdata.service.MasterDataConstants;
import org.entropy.skrupeltng.modules.masterdata.service.StarbaseUpgradeType;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component("EASY")
public class AIEasyRoundCalculator extends AbstractAIRoundCalculator {

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void calculateRound(Player player) {
		processShips(player);
		processPlanets(player);
		processStarbases(player);
	}

	private void processPlanets(Player player) {
		List<Planet> planets = planetRepository.findByPlayerId(player.getId());

		for (Planet planet : planets) {
			int factoriesToBeBuild = planet.retrieveMaxFactories() - planet.getFactories();

			if (factoriesToBeBuild > 0) {
				planet = planetService.buildFactoriesWithoutPermissionCheck(planet.getId(), factoriesToBeBuild);
			}

			int minesToBeBuild = planet.retrieveMaxMines() - planet.getMines();

			if (minesToBeBuild > 0) {
				planet = planetService.buildMinesWithoutPermissionCheck(planet.getId(), minesToBeBuild);
			}
		}
	}

	private void processStarbases(Player player) {
		List<Starbase> starbases = starbaseRepository.findByPlayerId(player.getId());

		for (Starbase starbase : starbases) {
			starbase = upgradeStarbase(starbase);
			buildShip(starbase, player.getId());
		}
	}

	private Starbase upgradeStarbase(Starbase starbase) {
		if (starbase.getHullLevel() < 6 || starbase.getPropulsionLevel() < 6) {
			upgradeStarbase(starbase, 6, StarbaseUpgradeType.HULL, StarbaseUpgradeType.PROPULSION);

			if (starbase.getEnergyLevel() == 0) {
				upgradeStarbase(starbase, 1, StarbaseUpgradeType.ENERGY);
			}

			if (starbase.getProjectileLevel() == 0) {
				upgradeStarbase(starbase, 1, StarbaseUpgradeType.PROJECTILE);
			}
		} else {
			upgradeStarbase(starbase, null, StarbaseUpgradeType.values());
		}

		return starbaseRepository.save(starbase);
	}

	private void upgradeStarbase(Starbase starbase, Integer max, StarbaseUpgradeType... types) {
		Planet planet = starbase.getPlanet();

		if (max == null) {
			max = 10;
		}

		for (StarbaseUpgradeType type : types) {
			int techLevel = starbase.retrieveTechLevel(type);

			for (int i = techLevel + 1; i <= max; i++) {
				if (!upgradeStarbase(starbase, type, i)) {
					break;
				}
			}
		}

		planetRepository.save(planet);
	}

	private void buildShip(Starbase starbase, long playerId) {
		buildHull(starbase, playerId);

		if (!starbase.getHullStocks().isEmpty()) {
			StarbaseHullStock hullStock = starbase.getHullStocks().get(0);
			ShipTemplate shipTemplate = hullStock.getShipTemplate();

			Optional<StarbasePropulsionStock> propulsionStockResult = getPropulsionStock(starbase, shipTemplate);

			if (propulsionStockResult.isPresent()) {
				Long energyStockId = null;
				Long projectileStockId = null;

				if (shipTemplate.getEnergyWeaponsCount() > 0) {
					Optional<StarbaseWeaponStock> energyStock = getWeaponStock(starbase, shipTemplate.getEnergyWeaponsCount(), StarbaseUpgradeType.ENERGY);

					if (!energyStock.isPresent()) {
						return;
					}

					energyStockId = energyStock.get().getId();
				}

				if (shipTemplate.getProjectileWeaponsCount() > 0) {
					Optional<StarbaseWeaponStock> projectileStock = getWeaponStock(starbase, shipTemplate.getProjectileWeaponsCount(), StarbaseUpgradeType.PROJECTILE);

					if (!projectileStock.isPresent()) {
						return;
					}

					projectileStockId = projectileStock.get().getId();
				}

				StarbaseShipConstructionRequest request = new StarbaseShipConstructionRequest();
				request.setName(AI_FREIGHTER_NAME);
				request.setHullStockId(hullStock.getId());
				request.setPropulsionSystemStockId(propulsionStockResult.get().getId());
				request.setEnergyStockId(energyStockId);
				request.setProjectileStockId(projectileStockId);
				starbaseService.addShipConstructionJobWithoutPermissionCheck(starbase.getId(), request);
			}
		}

		starbaseRepository.save(starbase);
	}

	private void buildHull(Starbase starbase, long playerId) {
		if (starbase.getHullStocks().isEmpty()) {
			List<Ship> ships = shipRepository.findByPlayerId(playerId);

			boolean buildFreighter = false;

			if (!ships.isEmpty()) {
				long freighterCount = ships.stream().filter(s -> s.getName().contains(AI_FREIGHTER_NAME)).count();

				long planetCount = planetRepository.getPlanetCountByPlayerId(playerId);

				if (freighterCount < Math.max(planetCount / 3, 1)) {
					buildFreighter = true;
				}
			} else {
				buildFreighter = true;
			}

			if (buildFreighter) {
				Optional<ShipTemplate> match = getBestMatchingShipTemplate(starbase, s -> s.getStorageSpace() > 50, SHIP_STORAGE_COMPARATOR);

				if (match.isPresent()) {
					StarbaseProductionRequest request = new StarbaseProductionRequest();
					request.setQuantity(1);
					request.setTemplateId(match.get().getId());
					request.setType(StarbaseUpgradeType.HULL.name());
					starbaseService.produceWithoutPermissionCheck(request, starbase.getId());
				}
			}
		}
	}

	private Optional<StarbasePropulsionStock> getPropulsionStock(Starbase starbase, ShipTemplate template) {
		int propulsionSystemsCount = template.getPropulsionSystemsCount();

		Optional<StarbasePropulsionStock> propulsionStockResult = getPropulsionSystemStock(starbase, propulsionSystemsCount);

		if (!propulsionStockResult.isPresent()) {
			Optional<PropulsionSystemTemplate> propulsionSystemResult = propulsionSystemTemplateRepository.findAll().stream()
					.filter(p -> p.getTechLevel() <= starbase.getPropulsionLevel() && canBeProduced(starbase, p))
					.sorted(PRODUCABLE_TECHLEVEL_COMPARATOR)
					.findFirst();

			if (propulsionSystemResult.isPresent()) {
				PropulsionSystemTemplate propulsionSystemTemplate = propulsionSystemResult.get();

				if (propulsionSystemTemplate.getTechLevel() >= starbase.getProjectileLevel() - 3) {
					StarbaseProductionRequest request = new StarbaseProductionRequest();
					request.setTemplateId(propulsionSystemTemplate.getName());
					request.setType(StarbaseUpgradeType.PROPULSION.name());
					request.setQuantity(propulsionSystemsCount);
					starbaseService.produceWithoutPermissionCheck(request, starbase.getId());

					propulsionStockResult = getPropulsionSystemStock(starbase, propulsionSystemsCount);
				}
			}
		}

		return propulsionStockResult;
	}

	private Optional<StarbaseWeaponStock> getWeaponStock(Starbase starbase, int weaponCount, StarbaseUpgradeType type) {
		boolean projectiles = type == StarbaseUpgradeType.PROJECTILE;

		Optional<StarbaseWeaponStock> weaponStockResult = getWeaponStock(starbase, weaponCount, projectiles);

		if (!weaponStockResult.isPresent()) {
			Optional<WeaponTemplate> weaponTemplateResult = weaponTemplateRepository.findAll().stream()
					.filter(p -> p.getTechLevel() <= starbase.getPropulsionLevel() && canBeProduced(starbase, p))
					.sorted(PRODUCABLE_TECHLEVEL_COMPARATOR)
					.findFirst();

			if (weaponTemplateResult.isPresent()) {
				WeaponTemplate weaponTemplate = weaponTemplateResult.get();

				if (weaponTemplate.getTechLevel() >= starbase.getProjectileLevel() - 3) {
					StarbaseProductionRequest request = new StarbaseProductionRequest();
					request.setTemplateId(weaponTemplate.getName());
					request.setType(type.name());
					request.setQuantity(weaponCount);
					starbaseService.produceWithoutPermissionCheck(request, starbase.getId());

					weaponStockResult = getWeaponStock(starbase, weaponCount, projectiles);
				}
			}
		}

		return weaponStockResult;
	}

	private void processShips(Player player) {
		final long playerId = player.getId();

		List<Ship> allPlayerShips = shipRepository.findByPlayerId(playerId);
		List<Ship> movingShips = shipRepository.getMovingShipsByPlayerId(playerId);

		List<Planet> allPlanets = planetRepository.findByGameId(player.getGame().getId());

		List<Planet> targetPlanets = allPlanets.stream()
				.filter(p -> (p.getPlayer() == null || p.getPlayer().getId() != playerId) && !movingShips.stream().anyMatch(s -> s.getDestinationX() == p.getX() && s.getDestinationY() == p.getY()))
				.collect(Collectors.toList());

		List<Planet> highPopulationPlanets = allPlanets.stream()
				.filter(p -> p.getPlayer() != null && p.getPlayer().getId() == playerId && p.getColonists() > 20000)
				.collect(Collectors.toList());

		for (Ship ship : allPlayerShips) {
			ShipTemplate shipTemplate = ship.getShipTemplate();
			Planet planet = ship.getPlanet();

			if (planet != null) {
				long planetId = planet.getId();
				final ShipTransportRequest transportRequest = new ShipTransportRequest(ship);

				boolean raidResources = true;

				if (planet.getPlayer() == null || planet.getPlayer().getId() != playerId) {
					if (ship.getColonists() >= 1500) {
						transportRequest.setColonists(ship.getColonists() - 1500);

						float ratio = (float)transportRequest.getColonists() / ship.getColonists();
						transportRequest.setMoney(ship.getMoney() - (int)Math.floor(ship.getMoney() * ratio));
						transportRequest.setSupplies(ship.getSupplies() - (int)Math.floor(ship.getSupplies() * ratio));

						if (transportRequest.getSupplies() < 1 && ship.getSupplies() > 1) {
							transportRequest.setSupplies(0);
						}
					}
				} else {
					int targetColonists = (int)Math.min((shipTemplate.getStorageSpace() / 3f * 2f) * MasterDataConstants.COLONIST_STORAGE_FACTOR, 6000);

					if (targetColonists > ship.getColonists() && planet.getPlayer() != null && planet.getPlayer().getId() == playerId && planet.getColonists() > 20000) {
						raidResources = false;

						transportRequest.setColonists(targetColonists - ship.getColonists());

						int targetSupplies = (int)Math.min(shipTemplate.getStorageSpace() / 6f * 1f, 40);

						if (targetSupplies > ship.getSupplies()) {
							transportRequest.setSupplies(Math.min(planet.getSupplies(), targetSupplies));
						}

						if (500 > ship.getMoney()) {
							transportRequest.setMoney(Math.min(planet.getMoney(), 500));
						}

						transportRequest.setMineral1(0);
						transportRequest.setMineral2(0);
						transportRequest.setMineral3(0);
					}
				}

				if (raidResources) {
					transportRequest.setFuel(Math.min(shipTemplate.getFuelCapacity(), ship.getFuel() + planet.getFuel()));
					int emptySpace = shipTemplate.getStorageSpace() - TotalGoodCalculator.retrieveSum(transportRequest);

					if (emptySpace > 0) {
						float quantity = emptySpace / 3f;

						if (quantity >= 1f) {
							float checkSum = emptySpace;
							int quantityInt = (int)Math.floor(quantity);

							checkSum -= quantityInt;
							transportRequest.setMineral1(Math.min(planet.getMineral1(), quantityInt));

							checkSum -= quantityInt;
							transportRequest.setMineral2(Math.min(planet.getMineral2(), quantityInt));

							transportRequest.setMineral3(Math.min(planet.getMineral3(), (int)checkSum));
						} else {
							transportRequest.setMineral3(Math.min(planet.getMineral3(), emptySpace));
						}
					}
				}

				final Ship s = ship;
				Collection<Planet> planets = transportRequest.getColonists() >= 1500 ? targetPlanets : highPopulationPlanets;

				Optional<Planet> nearestPlanet = planets.stream()
						.filter(p -> p.getId() != planetId)
						.min((a, b) -> Double.compare(CoordHelper.getDistance(s, a), CoordHelper.getDistance(s, b)));

				if (nearestPlanet.isPresent()) {
					Planet target = nearestPlanet.get();
					int bestTravelSpeed = ship.getPropulsionSystemTemplate().getWarpSpeed();

					if (planet != null) {
						double distance = CoordHelper.getDistance(ship, target);
						double duration = masterDataService.calculateTravelDuration(bestTravelSpeed, distance);
						int fuelConsumption = masterDataService.calculateFuelConsumptionPerMonth(ship, bestTravelSpeed, distance, duration);
						int targetFuel = Math.min(shipTemplate.getFuelCapacity(), fuelConsumption * 10);

						if (ship.getFuel() < targetFuel) {
							int transportedFuel = Math.min(planet.getFuel(), targetFuel - ship.getFuel());
							transportRequest.setFuel(ship.getFuel() + transportedFuel);
						}
					}

					targetPlanets.remove(target);

					ship.setDestinationX(target.getX());
					ship.setDestinationY(target.getY());
					ship.setTravelSpeed(bestTravelSpeed);

					ship = shipRepository.save(ship);
				}

				shipService.transportWithoutPermissionCheck(transportRequest, ship, planet);
			}
		}
	}
}