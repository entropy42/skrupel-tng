package org.entropy.skrupeltng.modules.ai;

public enum AILevel {

	EASY, MEDIUM, HARD
}