package org.entropy.skrupeltng.modules.ai;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.entropy.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.entropy.skrupeltng.modules.ingame.modules.planet.service.PlanetService;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.entropy.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbasePropulsionStock;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbaseWeaponStock;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.service.StarbaseService;
import org.entropy.skrupeltng.modules.masterdata.StarbaseProducable;
import org.entropy.skrupeltng.modules.masterdata.database.ProducableTechLevelComparator;
import org.entropy.skrupeltng.modules.masterdata.database.PropulsionSystemTemplateRepository;
import org.entropy.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.entropy.skrupeltng.modules.masterdata.database.ShipTemplateStorageSpaceComparator;
import org.entropy.skrupeltng.modules.masterdata.database.WeaponTemplateRepository;
import org.entropy.skrupeltng.modules.masterdata.service.MasterDataService;
import org.entropy.skrupeltng.modules.masterdata.service.StarbaseUpgradeType;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractAIRoundCalculator implements AIRoundCalculator {

	protected static final Comparator<ShipTemplate> SHIP_STORAGE_COMPARATOR = new ShipTemplateStorageSpaceComparator().reversed();
	protected static final Comparator<StarbaseProducable> PRODUCABLE_TECHLEVEL_COMPARATOR = new ProducableTechLevelComparator().reversed();

	protected final String AI_FREIGHTER_NAME = "Freighter";

	@Autowired
	protected PlanetRepository planetRepository;

	@Autowired
	protected StarbaseRepository starbaseRepository;

	@Autowired
	protected ShipRepository shipRepository;

	@Autowired
	protected MasterDataService masterDataService;

	@Autowired
	protected PlanetService planetService;

	@Autowired
	protected StarbaseService starbaseService;

	@Autowired
	protected ShipService shipService;

	@Autowired
	protected PropulsionSystemTemplateRepository propulsionSystemTemplateRepository;

	@Autowired
	protected WeaponTemplateRepository weaponTemplateRepository;

	protected boolean upgradeStarbase(Starbase starbase, StarbaseUpgradeType type, int targetLevel) {
		Planet planet = starbase.getPlanet();

		int costs = masterDataService.calculateStarbaseUpgradeMoney(type, targetLevel - 1, targetLevel);

		if (costs < planet.getMoney()) {
			planet.spendMoney(costs);
			starbase.upgrade(type);

			return true;
		}

		return false;
	}

	protected Stream<ShipTemplate> getPossibleShipTemplates(Starbase starbase, List<ShipTemplate> shipTemplates) {
		return shipTemplates.stream()
				.filter(s -> s.getTechLevel() <= starbase.getHullLevel())
				.sorted();
	}

	protected boolean canBeProduced(Starbase starbase, StarbaseProducable producable) {
		Planet planet = starbase.getPlanet();

		if (producable.getCostMoney() > planet.getMoney()) {
			return false;
		}

		if (producable.getCostMineral1() > planet.getMineral1()) {
			return false;
		}

		if (producable.getCostMineral2() > planet.getMineral2()) {
			return false;
		}

		if (producable.getCostMineral3() > planet.getMineral3()) {
			return false;
		}

		return true;
	}

	protected Optional<ShipTemplate> getBestMatchingShipTemplate(Starbase starbase, Predicate<ShipTemplate> predicate, Comparator<ShipTemplate> comparator) {
		List<ShipTemplate> templates = starbase.getPlanet().getPlayer().getRace().getShips();
		templates = getPossibleShipTemplates(starbase, templates).collect(Collectors.toList());

		Map<Integer, ShipTemplate> bestPerTechLevel = new HashMap<>(starbase.getHullLevel());

		for (int i = templates.size() - 1; i >= 0; i--) {
			ShipTemplate template = templates.get(i);

			if (predicate.test(template)) {
				ShipTemplate existing = bestPerTechLevel.get(template.getTechLevel());

				if (existing == null || comparator.compare(template, existing) < 0) {
					bestPerTechLevel.put(template.getTechLevel(), template);
				}
			}
		}

		templates = bestPerTechLevel.values().stream().filter(s -> canBeProduced(starbase, s)).sorted().collect(Collectors.toList());

		if (templates.isEmpty()) {
			return Optional.empty();
		}

		templates.sort(comparator);

		return Optional.of(templates.get(0));
	}

	protected Optional<StarbasePropulsionStock> getPropulsionSystemStock(Starbase starbase, int stock) {
		return starbase.getPropulsionStocks().stream()
				.filter(p -> p.getStock() >= stock)
				.max((a, b) -> PRODUCABLE_TECHLEVEL_COMPARATOR.compare(a.getPropulsionSystemTemplate(), b.getPropulsionSystemTemplate()));
	}

	protected Optional<StarbaseWeaponStock> getWeaponStock(Starbase starbase, int stock, boolean projectiles) {
		return starbase.getWeaponStocks().stream()
				.filter(p -> p.getStock() >= stock && p.getWeaponTemplate().isUsesProjectiles() == projectiles)
				.max((a, b) -> PRODUCABLE_TECHLEVEL_COMPARATOR.compare(a.getWeaponTemplate(), b.getWeaponTemplate()));
	}
}