package org.entropy.skrupeltng.modules.ingame.database;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.entropy.skrupeltng.modules.RepositoryCustomBase;
import org.entropy.skrupeltng.modules.ingame.modules.overview.service.PlayerSummaryEntry;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public class PlayerRepositoryImpl extends RepositoryCustomBase implements PlayerRepositoryCustom {

	@Override
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void setupTurnValues(long gameId) {
		String sql = "UPDATE player SET overview_viewed = false, turn_finished = ai_level IS NOT NULL WHERE game_id = :gameId";
		Map<String, Object> params = new HashMap<>(1);
		params.put("gameId", gameId);
		jdbcTemplate.update(sql, params);
	}

	@Override
	public List<PlayerSummaryEntry> getSummaries(long gameId) {
		String sql = "" +
				"SELECT \n" +
				"	p.id as \"playerId\", \n" +
				"	l.username as \"name\", \n" +
				"	r.name as \"raceName\", \n" +
				"	r.folder_name as \"raceFolder\", \n" +
				"	COUNT(sb.id) as \"starbaseCount\", \n" +
				"	COUNT(pl.id) as \"planetCount\", \n" +
				"	COUNT(s.id) as \"shipCount\" \n" +
				"FROM \n" +
				"	player p \n" +
				"	INNER JOIN login l \n" +
				"		ON l.id = p.login_id AND p.game_id = :gameId \n" +
				"	INNER JOIN race r \n" +
				"		ON r.name = p.race_id \n" +
				"	LEFT OUTER JOIN planet pl \n" +
				"		ON pl.player_id = p.id \n" +
				"	LEFT OUTER JOIN starbase sb \n" +
				"		ON sb.id = pl.starbase_id \n" +
				"	LEFT OUTER JOIN ship s \n" +
				"		ON s.player_id = p.id \n" +
				"GROUP BY \n" +
				"	p.id, \n" +
				"	l.username, \n" +
				"	r.name, \n" +
				"	r.folder_name";

		Map<String, Object> params = new HashMap<>(1);
		params.put("gameId", gameId);

		RowMapper<PlayerSummaryEntry> rowMapper = new BeanPropertyRowMapper<>(PlayerSummaryEntry.class);
		List<PlayerSummaryEntry> list = jdbcTemplate.query(sql, params, rowMapper);

		List<PlayerSummaryEntry> sorted = list.stream().sorted((a, b) -> Integer.compare(b.getStarbaseCount(), a.getStarbaseCount())).collect(Collectors.toList());
		int i = 1;
		int lastValue = sorted.get(0).getStarbaseCount();
		for (PlayerSummaryEntry playerSummaryEntry : sorted) {
			if (lastValue > playerSummaryEntry.getStarbaseCount()) {
				i++;
			}

			playerSummaryEntry.setStarbaseRank(i);
			lastValue = playerSummaryEntry.getStarbaseCount();
		}

		sorted = list.stream().sorted((a, b) -> Integer.compare(b.getPlanetCount(), a.getPlanetCount())).collect(Collectors.toList());
		i = 1;
		lastValue = sorted.get(0).getPlanetCount();
		for (PlayerSummaryEntry playerSummaryEntry : sorted) {
			if (lastValue > playerSummaryEntry.getPlanetCount()) {
				i++;
			}

			playerSummaryEntry.setPlanetRank(i);
			lastValue = playerSummaryEntry.getPlanetCount();
		}

		sorted = list.stream().sorted((a, b) -> Integer.compare(b.getShipCount(), a.getShipCount())).collect(Collectors.toList());
		i = 1;
		lastValue = sorted.get(0).getShipCount();
		for (PlayerSummaryEntry playerSummaryEntry : sorted) {
			if (lastValue > playerSummaryEntry.getShipCount()) {
				i++;
			}

			playerSummaryEntry.setShipRank(i);
			lastValue = playerSummaryEntry.getShipCount();
		}

		sorted = list.stream().sorted((a, b) -> Integer.compare(b.getStarbaseCount() + b.getPlanetCount() + b.getShipCount(), a.getStarbaseCount() + a.getPlanetCount() + a.getShipCount())).collect(Collectors.toList());
		i = 1;
		PlayerSummaryEntry first = sorted.get(0);
		lastValue = first.getStarbaseCount() + first.getPlanetCount() + first.getShipCount();
		for (PlayerSummaryEntry playerSummaryEntry : sorted) {
			int newValue = playerSummaryEntry.getStarbaseCount() + playerSummaryEntry.getPlanetCount() + playerSummaryEntry.getShipCount();

			if (lastValue > newValue) {
				i++;
			}
			playerSummaryEntry.setRank(i);
			lastValue = newValue;
		}

		list.sort((a, b) -> Integer.compare(a.getRank(), b.getRank()));

		return list;
	}

	@Override
	public boolean playersTurnNotDoneForPlanet(long planetId, long loginId) {
		String sql = "" +
				"SELECT \n" +
				"	pl.turn_finished \n" +
				"FROM \n" +
				"	player pl \n" +
				"	INNER JOIN planet p \n" +
				"		ON p.id = :planetId AND p.player_id = pl.id AND pl.login_id = :loginId";

		Map<String, Object> params = new HashMap<>(2);
		params.put("planetId", planetId);
		params.put("loginId", loginId);

		List<Boolean> results = jdbcTemplate.queryForList(sql, params, Boolean.class);

		if (results.size() == 1) {
			return !results.get(0);
		}

		return false;
	}

	@Override
	public boolean playersTurnNotDoneForShip(long shipId, long loginId) {
		String sql = "" +
				"SELECT \n" +
				"	pl.turn_finished \n" +
				"FROM \n" +
				"	player pl \n" +
				"	INNER JOIN ship s \n" +
				"		ON s.id = :shipId AND s.player_id = pl.id AND pl.login_id = :loginId";

		Map<String, Object> params = new HashMap<>(2);
		params.put("shipId", shipId);
		params.put("loginId", loginId);

		List<Boolean> results = jdbcTemplate.queryForList(sql, params, Boolean.class);

		if (results.size() == 1) {
			return !results.get(0);
		}

		return false;
	}

	@Override
	public boolean playersTurnNotDoneForStarbase(long starbaseId, long loginId) {
		String sql = "" +
				"SELECT \n" +
				"	pl.turn_finished \n" +
				"FROM \n" +
				"	planet p \n" +
				"	INNER JOIN player pl \n" +
				"		ON p.starbase_id = :starbaseId AND p.player_id = pl.id AND pl.login_id = :loginId";

		Map<String, Object> params = new HashMap<>(2);
		params.put("starbaseId", starbaseId);
		params.put("loginId", loginId);

		List<Boolean> results = jdbcTemplate.queryForList(sql, params, Boolean.class);

		if (results.size() == 1) {
			return !results.get(0);
		}

		return false;
	}
}