package org.entropy.skrupeltng.modules.ingame.modules.starbase.database;

public interface StarbaseRepositoryCustom {

	boolean loginOwnsStarbase(long starbaseId, long loginId);
}