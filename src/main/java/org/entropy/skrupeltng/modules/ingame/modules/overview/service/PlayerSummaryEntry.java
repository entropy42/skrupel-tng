package org.entropy.skrupeltng.modules.ingame.modules.overview.service;

import java.io.Serializable;

public class PlayerSummaryEntry implements Serializable {

	private static final long serialVersionUID = 6595428821755282629L;

	private long playerId;
	private String name;
	private String raceName;
	private String raceFolder;
	private int starbaseCount;
	private int planetCount;
	private int shipCount;
	private int starbaseRank;
	private int planetRank;
	private int shipRank;
	private int rank;

	public long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRaceName() {
		return raceName;
	}

	public void setRaceName(String raceName) {
		this.raceName = raceName;
	}

	public String getRaceFolder() {
		return raceFolder;
	}

	public void setRaceFolder(String raceFolder) {
		this.raceFolder = raceFolder;
	}

	public int getStarbaseCount() {
		return starbaseCount;
	}

	public void setStarbaseCount(int starbaseCount) {
		this.starbaseCount = starbaseCount;
	}

	public int getPlanetCount() {
		return planetCount;
	}

	public void setPlanetCount(int planetCount) {
		this.planetCount = planetCount;
	}

	public int getShipCount() {
		return shipCount;
	}

	public void setShipCount(int shipCount) {
		this.shipCount = shipCount;
	}

	public int getStarbaseRank() {
		return starbaseRank;
	}

	public void setStarbaseRank(int starbaseRank) {
		this.starbaseRank = starbaseRank;
	}

	public int getPlanetRank() {
		return planetRank;
	}

	public void setPlanetRank(int planetRank) {
		this.planetRank = planetRank;
	}

	public int getShipRank() {
		return shipRank;
	}

	public void setShipRank(int shipRank) {
		this.shipRank = shipRank;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}
}