package org.entropy.skrupeltng.modules.ingame.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.entropy.skrupeltng.modules.ingame.CoordinateImpl;
import org.entropy.skrupeltng.modules.ingame.controller.PlanetEntry;
import org.entropy.skrupeltng.modules.ingame.database.FogOfWarType;
import org.entropy.skrupeltng.modules.ingame.database.Game;
import org.entropy.skrupeltng.modules.ingame.database.GameRepository;
import org.entropy.skrupeltng.modules.ingame.database.Player;
import org.entropy.skrupeltng.modules.ingame.database.PlayerRepository;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetScanLog;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetScanLogRepository;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.entropy.skrupeltng.modules.ingame.service.round.RoundCalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class IngameService {

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private RoundCalculationService roundCalculationService;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private PlayerPlanetScanLogRepository playerPlanetScanLogRepository;

	@Value("${skr.visibilityRadius:125}")
	private int visibilityRadius;

	public Optional<Game> getGame(long id) {
		return gameRepository.findById(id);
	}

	public long getPlayerId(long loginId, long gameId) {
		return playerRepository.getIdByLoginIdAndGameId(loginId, gameId);
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	public List<PlanetEntry> getPlanets(long gameId, long loginId, List<CoordinateImpl> visibilityCoordinates) {
		List<PlanetEntry> planets = planetRepository.getPlanetsForGalaxy(gameId);
		FogOfWarType fogOfWarType = gameRepository.getOne(gameId).getFogOfWarType();

		if (fogOfWarType != FogOfWarType.NONE) {
			long playerId = getPlayerId(loginId, gameId);

			Map<Long, PlayerPlanetScanLog> scanLogMap = new HashMap<>();

			if (fogOfWarType == FogOfWarType.VISITED || fogOfWarType == FogOfWarType.LONG_RANGE_SENSORS) {
				List<PlayerPlanetScanLog> planetIdsByPlayerId = playerPlanetScanLogRepository.findByPlayerId(playerId);

				for (PlayerPlanetScanLog log : planetIdsByPlayerId) {
					scanLogMap.put(log.getPlanet().getId(), log);
				}
			}

			List<PlanetEntry> visiblePlanets = new ArrayList<>(planets.size());

			for (PlanetEntry planet : planets) {
				long planetId = planet.getId();
				boolean directlyVisible = visibilityCoordinates.stream().anyMatch(c -> CoordHelper.getDistance(c, planet) <= visibilityRadius);

				PlayerPlanetScanLog scanLogEntry = scanLogMap.get(planetId);

				if (scanLogEntry != null || directlyVisible) {
					visiblePlanets.add(planet);

					if (directlyVisible && planet.getShipPlayerColor() != null) {
						planet.setShips(shipRepository.findByGameIdAndPlanetId(gameId, planetId));
					}

					if (!directlyVisible && scanLogEntry != null) {
						planet.setPlayerColor(scanLogEntry.getLastPlayerColor());

						if (!scanLogEntry.isHadStarbase()) {
							planet.setStarbaseId(null);
							planet.setStarbaseName(null);
						}
						if (scanLogEntry.getLastPlayerColor() == null) {
							planet.setPlayerId(null);
						}
					}
				}
			}

			return visiblePlanets;
		}

		return planets;
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	public List<CoordinateImpl> getVisibilityCoordinates(long gameId, long loginId) {
		Player player = playerRepository.findByGameIdAndLoginId(gameId, loginId).get();
		return gameRepository.getVisibilityCoordinates(player.getId());
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public boolean finishTurn(long gameId, long loginId) {
		Optional<Player> playerResult = playerRepository.findByGameIdAndLoginId(gameId, loginId);

		if (playerResult.isPresent()) {
			Player player = playerResult.get();
			player.setTurnFinished(true);
			player = playerRepository.save(player);

			return allPlayersHaveFinishedTheirTurns(player.getGame());
		}

		throw new IllegalArgumentException("Login " + loginId + " does not take part in game " + gameId + "!");
	}

	private boolean allPlayersHaveFinishedTheirTurns(Game game) {
		long notFinishedCount = game.getPlayers().stream().filter(p -> !p.isTurnFinished()).count();
		return notFinishedCount == 0L;
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public void calculateRound(long gameId) {
		if (allPlayersHaveFinishedTheirTurns(getGame(gameId).get())) {
			roundCalculationService.calculateRound(gameId);
		} else {
			throw new IllegalArgumentException("Not all players of game " + gameId + " have finished their turns!");
		}
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	public List<Ship> getShips(long gameId, long loginId, List<CoordinateImpl> visibilityCoordinates) {
		List<Ship> ships = shipRepository.findByGameId(gameId);

		return ships.stream()
				.filter(s -> visibilityCoordinates.stream().anyMatch(c -> CoordHelper.getDistance(c, s) <= visibilityRadius))
				.collect(Collectors.toList());
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void overviewViewed(long gameId, long loginId) {
		Optional<Player> playerResult = playerRepository.findByGameIdAndLoginId(gameId, loginId);

		if (playerResult.isPresent()) {
			Player player = playerResult.get();
			player.setOverviewViewed(true);
			playerRepository.save(player);
		} else {
			throw new IllegalArgumentException("Login " + loginId + " does not take part in game " + gameId + "!");
		}
	}
}