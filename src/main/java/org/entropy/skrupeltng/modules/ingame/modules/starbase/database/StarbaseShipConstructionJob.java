package org.entropy.skrupeltng.modules.ingame.modules.starbase.database;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.entropy.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;
import org.entropy.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.entropy.skrupeltng.modules.masterdata.database.WeaponTemplate;

@Entity
@Table(name = "starbase_ship_construction_job")
public class StarbaseShipConstructionJob implements Serializable {

	private static final long serialVersionUID = -1727871799782125060L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "ship_name")
	private String shipName;

	@ManyToOne(fetch = FetchType.EAGER)
	private Starbase starbase;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ship_template_id")
	private ShipTemplate shipTemplate;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "propulsion_system_template_name")
	private PropulsionSystemTemplate propulsionSystemTemplate;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "energy_weapon_template_name")
	private WeaponTemplate energyWeaponTemplate;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "projectile_weapon_template_name")
	private WeaponTemplate projectileWeaponTemplate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getShipName() {
		return shipName;
	}

	public void setShipName(String shipName) {
		this.shipName = shipName;
	}

	public Starbase getStarbase() {
		return starbase;
	}

	public void setStarbase(Starbase starbase) {
		this.starbase = starbase;
	}

	public ShipTemplate getShipTemplate() {
		return shipTemplate;
	}

	public void setShipTemplate(ShipTemplate shipTemplate) {
		this.shipTemplate = shipTemplate;
	}

	public PropulsionSystemTemplate getPropulsionSystemTemplate() {
		return propulsionSystemTemplate;
	}

	public void setPropulsionSystemTemplate(PropulsionSystemTemplate propulsionSystemTemplate) {
		this.propulsionSystemTemplate = propulsionSystemTemplate;
	}

	public WeaponTemplate getEnergyWeaponTemplate() {
		return energyWeaponTemplate;
	}

	public void setEnergyWeaponTemplate(WeaponTemplate energyWeaponTemplate) {
		this.energyWeaponTemplate = energyWeaponTemplate;
	}

	public WeaponTemplate getProjectileWeaponTemplate() {
		return projectileWeaponTemplate;
	}

	public void setProjectileWeaponTemplate(WeaponTemplate projectileWeaponTemplate) {
		this.projectileWeaponTemplate = projectileWeaponTemplate;
	}
}