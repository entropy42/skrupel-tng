package org.entropy.skrupeltng.modules.ingame.service.round;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.entropy.skrupeltng.modules.ingame.CoordinateImpl;
import org.entropy.skrupeltng.modules.ingame.database.FogOfWarType;
import org.entropy.skrupeltng.modules.ingame.database.Game;
import org.entropy.skrupeltng.modules.ingame.database.GameRepository;
import org.entropy.skrupeltng.modules.ingame.database.Player;
import org.entropy.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.entropy.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetScanLog;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetScanLogRepository;
import org.entropy.skrupeltng.modules.ingame.modules.planet.service.PlanetService;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.entropy.skrupeltng.modules.ingame.service.CoordHelper;
import org.entropy.skrupeltng.modules.masterdata.database.Race;
import org.entropy.skrupeltng.modules.masterdata.service.MasterDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class PlanetRoundCalculator {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private StarbaseRepository starbaseRepository;

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private PlayerPlanetScanLogRepository playerPlanetScanLogRepository;

	@Autowired
	private NewsService newsService;

	@Autowired
	private PlanetService planetService;

	@Value("${skr.visibilityRadius:125}")
	private int visibilityRadius;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processNewColonies(long gameId) {
		log.debug("Processing new colonies...");

		List<Planet> newColonies = planetRepository.findNewColonies(gameId);

		for (Planet planet : newColonies) {
			planet.setPlayer(planet.getNewPlayer());

			planet.setNewPlayer(null);
			planet.setColonists(planet.getNewColonists());
			planet.setNewColonists(0);

			planet.setLightGroundUnits(planet.getNewLightGroundUnits());
			planet.setNewLightGroundUnits(0);

			planet.setHeavyGroundUnits(planet.getNewHeavyGroundUnits());
			planet.setNewHeavyGroundUnits(0);

			planet = planetRepository.save(planet);

			newsService.add(planet.getPlayer(), NewsEntryConstants.news_entry_new_colony, planet.createFullImagePath(), planet.getName());
		}

		log.debug("New colonies processed.");
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processOwnedPlanets(long gameId) {
		log.debug("Processing owned planets...");

		List<Planet> ownedPlanets = planetRepository.findOwnedPlanetsByGameId(gameId);

		for (Planet planet : ownedPlanets) {
			if (planet.getColonists() < 1000) {
				recudeColonistsAndGroundUnits(planet);
			} else if (planet.getColonists() < 10000000) {
				increaseColonistsAndMoney(planet);
			}

			if (planet.getPlayer() != null) {
				processFactories(planet);
				processMines(planet);

				if (planet.isAutoBuildFactories()) {
					autoBuildFactories(planet);
				}

				if (planet.isAutoBuildMines()) {
					autoBuildMines(planet);
				}

				if (planet.isAutoBuildPlanetaryDefense()) {
					autobuildPlanetaryDefense(planet);
				}

				StarbaseType starbaseType = planet.getStarbaseUnderConstructionType();

				if (starbaseType != null) {
					Starbase starbase = new Starbase();
					starbase.setType(starbaseType);
					starbase.setName(planet.getStarbaseUnderConstructionName());
					starbase = starbaseRepository.save(starbase);

					planet.setStarbase(starbase);
					planet.setStarbaseUnderConstructionType(null);
					planet.setStarbaseUnderConstructionName(null);

					newsService.add(planet.getPlayer(), NewsEntryConstants.news_entry_starbase_constructed, starbase.createFullImagePath(), starbase.getName());
				}
			}

			planetRepository.save(planet);
		}

		log.debug("Owned planets processed.");
	}

	private void recudeColonistsAndGroundUnits(Planet planet) {
		planet.setColonists(planet.getColonists() - 50 - MasterDataService.RANDOM.nextInt(150));

		if (planet.getColonists() <= 0) {
			planet.setColonists(0);
			boolean ownerLosesPlanet = false;

			int allGroundUnits = planet.getLightGroundUnits() + planet.getHeavyGroundUnits();

			if (allGroundUnits == 0 || planet.getSupplies() == 0) {
				ownerLosesPlanet = true;
			} else {
				int suppliesConsumingGroundUnits = Math.round(allGroundUnits * 0.15f);

				if (suppliesConsumingGroundUnits < 15) {
					suppliesConsumingGroundUnits = 15;
				}

				if (suppliesConsumingGroundUnits > planet.getSupplies()) {
					int groundUnitsWithoutSupplies = suppliesConsumingGroundUnits - planet.getSupplies();
					planet.setSupplies(0);

					int groundUnitsToBeRemoved = Math.round(groundUnitsWithoutSupplies * 0.15f);
					allGroundUnits -= groundUnitsToBeRemoved;

					if (allGroundUnits <= 0) {
						ownerLosesPlanet = true;
					} else {
						if (groundUnitsToBeRemoved <= planet.getHeavyGroundUnits()) {
							planet.setHeavyGroundUnits(planet.getHeavyGroundUnits() - groundUnitsToBeRemoved);
						} else {
							groundUnitsToBeRemoved -= planet.getHeavyGroundUnits();
							planet.setHeavyGroundUnits(0);
							planet.setLightGroundUnits(planet.getLightGroundUnits() - groundUnitsToBeRemoved);
						}
					}
				} else {
					planet.setSupplies(suppliesConsumingGroundUnits);
				}
			}

			if (ownerLosesPlanet) {
				planet.setSupplies(0);
				planet.setAutoBuildMines(false);
				planet.setAutoBuildFactories(false);
				planet.setAutoBuildPlanetaryDefense(false);
				planet.setPlayer(null);
				planet.setLog(null);
			}
		}
	}

	private void increaseColonistsAndMoney(Planet planet) {
		int temperatureDiff = 0;
		Race race = planet.getPlayer().getRace();

		if (race.getPreferredTemperature() != 0) {
			temperatureDiff = Math.abs(planet.getTemperature() - race.getPreferredTemperature());
		}

		if (temperatureDiff <= 30) {
			float growth = 0.1745f - (temperatureDiff * 0.004886666666666f);
			// TODO native species and orbital systems
			planet.setColonists(planet.getColonists() + (Math.round(planet.getColonists() / 10 * growth)));
		}

		int newMoney = Math.round(planet.getColonists() * 0.008f * race.getTaxRate());
		planet.setMoney(planet.getMoney() + newMoney);
	}

	private void processMines(Planet planet) {
		int mines = planet.getMines();

		if (mines > 0) {
			int totalResources = planet.getUntappedFuel() + planet.getUntappedMineral1() + planet.getUntappedMineral2() + planet.getUntappedMineral3();
			// TODO native species and orbital systems

			if (totalResources > 0) {
				int minedFuel = mineResource(planet, totalResources, mines, planet.getUntappedFuel(), planet.getNecessaryMinesForOneFuel());
				planet.mineFuel(minedFuel);

				int minedMineral1 = mineResource(planet, totalResources, mines, planet.getUntappedMineral1(), planet.getNecessaryMinesForOneMineral1());
				planet.mineMineral1(minedMineral1);

				int minedMineral2 = mineResource(planet, totalResources, mines, planet.getUntappedMineral2(), planet.getNecessaryMinesForOneMineral2());
				planet.mineMineral2(minedMineral2);

				int minedMineral3 = mineResource(planet, totalResources, mines, planet.getUntappedMineral3(), planet.getNecessaryMinesForOneMineral3());
				planet.mineMineral3(minedMineral3);
			}
		}
	}

	private int mineResource(Planet planet, int totalResources, int mines, int untappedQuantity, int neededMinesPerUnit) {
		float usedMines = untappedQuantity * mines * planet.getPlayer().getRace().getMineProductionRate() / totalResources;
		int minedResource = Math.min(untappedQuantity, (int)Math.floor(usedMines / neededMinesPerUnit));
		return minedResource;
	}

	private void processFactories(Planet planet) {
		int factories = planet.getFactories();

		if (factories > 0) {
			// TODO native species and orbital systems

			planet.spendSupplies(-Math.round(factories * planet.getPlayer().getRace().getFactoryProductionRate()));
		}
	}

	private void autoBuildFactories(Planet planet) {
		int max = planet.retrieveMaxFactories();
		int add = max - planet.getFactories();
		planetService.buildFactoriesWithoutPermissionCheck(planet.getId(), add);
	}

	private void autoBuildMines(Planet planet) {
		int max = planet.retrieveMaxMines();
		int add = max - planet.getMines();
		planetService.buildMinesWithoutPermissionCheck(planet.getId(), add);
	}

	private void autobuildPlanetaryDefense(Planet planet) {
		int max = planet.retrieveMaxPlanetaryDefense();
		int add = max - planet.getPlanetaryDefense();
		planetService.buildPlanetaryDefenseWithoutPermissionCheck(planet.getId(), add);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void logVisitedPlanets(long gameId) {
		Game game = gameRepository.getOne(gameId);
		FogOfWarType fogOfWarType = game.getFogOfWarType();

		int radius = 0;

		if (fogOfWarType == FogOfWarType.LONG_RANGE_SENSORS) {
			radius = 250;
		} else if (fogOfWarType == FogOfWarType.VISITED) {
			radius = 150;
		}

		if (radius > 0) {
			int r = radius;

			List<Player> players = game.getPlayers();
			Set<Planet> planets = game.getPlanets();

			for (Player player : players) {
				long playerId = player.getId();
				List<CoordinateImpl> visibilityCoordinates = gameRepository.getVisibilityCoordinates(playerId);

				for (Planet planet : planets) {
					for (CoordinateImpl coords : visibilityCoordinates) {
						double distance = CoordHelper.getDistance(coords, planet);

						if (distance <= r) {
							Optional<PlayerPlanetScanLog> result = playerPlanetScanLogRepository.findByPlayerIdAndPlanetId(playerId, planet.getId());

							PlayerPlanetScanLog log = null;

							if (!result.isPresent()) {
								log = new PlayerPlanetScanLog();
								log.setPlayer(player);
								log.setPlanet(planet);
							} else {
								log = result.get();
							}

							if (distance <= visibilityRadius) {
								log.setHadStarbase(planet.getStarbase() != null);
								Player planetOwner = planet.getPlayer();
								log.setLastPlayerColor(planetOwner != null ? planetOwner.getColor() : null);
							}

							playerPlanetScanLogRepository.save(log);
							break;
						}
					}
				}
			}
		}
	}
}