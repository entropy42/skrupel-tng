package org.entropy.skrupeltng.modules.ingame.modules.planet.service;

import java.util.List;

import org.entropy.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PlanetService {

	private PlanetRepository planetRepository;

	@PreAuthorize("hasPermission(#planetId, 'planet')")
	public Planet getPlanet(long planetId) {
		return planetRepository.getOne(planetId);
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	public List<Planet> getPlanetsOfLogin(long gameId, long loginId) {
		return planetRepository.findByGameIdAndLoginId(gameId, loginId);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@PreAuthorize("hasPermission(#planetId, 'planet') and hasPermission(#planetId, 'turnNotDonePlanet')")
	public void toggleAutoBuildMines(long planetId) {
		Planet planet = planetRepository.getOne(planetId);
		planet.setAutoBuildMines(!planet.isAutoBuildMines());
		planetRepository.save(planet);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@PreAuthorize("hasPermission(#planetId, 'planet') and hasPermission(#planetId, 'turnNotDonePlanet')")
	public void toggleAutoBuildFactories(long planetId) {
		Planet planet = planetRepository.getOne(planetId);
		planet.setAutoBuildFactories(!planet.isAutoBuildFactories());
		planetRepository.save(planet);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@PreAuthorize("hasPermission(#planetId, 'planet') and hasPermission(#planetId, 'turnNotDonePlanet')")
	public void toggleAutoSellSupplies(long planetId) {
		Planet planet = planetRepository.getOne(planetId);
		planet.setAutoSellSupplies(!planet.isAutoSellSupplies());
		planetRepository.save(planet);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@PreAuthorize("hasPermission(#planetId, 'planet') and hasPermission(#planetId, 'turnNotDonePlanet')")
	public void toggleAutoBuildPlanetaryDefense(long planetId) {
		Planet planet = planetRepository.getOne(planetId);
		planet.setAutoBuildPlanetaryDefense(!planet.isAutoBuildPlanetaryDefense());
		planetRepository.save(planet);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@PreAuthorize("hasPermission(#planetId, 'planet') and hasPermission(#planetId, 'turnNotDonePlanet')")
	public Planet buildMines(long planetId, int quantity) {
		return buildMinesWithoutPermissionCheck(planetId, quantity);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Planet buildMinesWithoutPermissionCheck(long planetId, int quantity) {
		Planet planet = planetRepository.getOne(planetId);
		int moneyCosts = quantity * 4;

		if (planet.retrieveMaxMines() < planet.getMines() + quantity || planet.getSupplies() < quantity || moneyCosts > planet.getMoney()) {
			throw new RuntimeException("Cannot build " + quantity + " mines on planet " + planetId + "!");
		}

		planet.setMines(planet.getMines() + quantity);
		planet.setSupplies(planet.getSupplies() - quantity);
		planet.spendMoney(moneyCosts);
		return planetRepository.save(planet);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@PreAuthorize("hasPermission(#planetId, 'planet') and hasPermission(#planetId, 'turnNotDonePlanet')")
	public Planet buildFactories(long planetId, int quantity) {
		return buildFactoriesWithoutPermissionCheck(planetId, quantity);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Planet buildFactoriesWithoutPermissionCheck(long planetId, int quantity) {
		Planet planet = planetRepository.getOne(planetId);
		int moneyCosts = quantity * 3;

		if (planet.retrieveMaxFactories() < planet.getFactories() + quantity || planet.getSupplies() < quantity || moneyCosts > planet.getMoney()) {
			throw new RuntimeException("Cannot build " + quantity + " factories on planet " + planetId + "!");
		}

		planet.setFactories(planet.getFactories() + quantity);
		planet.setSupplies(planet.getSupplies() - quantity);
		planet.spendMoney(moneyCosts);
		return planetRepository.save(planet);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@PreAuthorize("hasPermission(#planetId, 'planet') and hasPermission(#planetId, 'turnNotDonePlanet')")
	public Planet sellSupplies(long planetId, int quantity) {
		Planet planet = planetRepository.getOne(planetId);

		if (planet.getSupplies() < quantity) {
			throw new RuntimeException("Cannot sell " + quantity + " supplies on planet " + planetId + "!");
		}

		planet.setSupplies(planet.getSupplies() - quantity);
		planet.spendMoney(-quantity);
		return planetRepository.save(planet);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@PreAuthorize("hasPermission(#planetId, 'planet') and hasPermission(#planetId, 'turnNotDonePlanet')")
	public Planet buildPlanetaryDefense(long planetId, int quantity) {
		return buildPlanetaryDefenseWithoutPermissionCheck(planetId, quantity);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Planet buildPlanetaryDefenseWithoutPermissionCheck(long planetId, int quantity) {
		Planet planet = planetRepository.getOne(planetId);
		int moneyCosts = quantity * 10;

		if (planet.retrieveMaxPlanetaryDefense() < planet.getPlanetaryDefense() + quantity || planet.getSupplies() < quantity || moneyCosts > planet.getMoney()) {
			throw new RuntimeException("Cannot build " + quantity + " planetary defense on planet " + planetId + "!");
		}

		planet.setPlanetaryDefense(planet.getPlanetaryDefense() + quantity);
		planet.setSupplies(planet.getSupplies() - quantity);
		planet.spendMoney(moneyCosts);
		return planetRepository.save(planet);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@PreAuthorize("hasPermission(#planetId, 'planet') and hasPermission(#planetId, 'turnNotDonePlanet')")
	public void constructStarbase(long planetId, StarbaseType type, String name) {
		Planet planet = planetRepository.getOne(planetId);

		if (planet.getStarbaseUnderConstructionType() != null) {
			throw new IllegalArgumentException("Planet " + planetId + " is already constructing a starbase!");
		}

		if (planet.getStarbase() != null) {
			throw new IllegalArgumentException("Planet " + planetId + " already has a starbase!");
		}

		if (!planet.canConstructStarbase(type)) {
			throw new IllegalArgumentException("Planet " + planetId + " has not the resources for a starbase of type " + type + "!");
		}

		planet.setStarbaseUnderConstructionType(type);
		planet.setStarbaseUnderConstructionName(name);
		planet.spendMoney(type.getCostMoney());
		planet.spendSupplies(type.getCostSupplies());
		planet.spendFuel(type.getCostFuel());
		planet.spendMineral1(type.getCostMineral1());
		planet.spendMineral2(type.getCostMineral2());
		planet.spendMineral3(type.getCostMineral3());
		planetRepository.save(planet);
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	@PreAuthorize("hasPermission(#planetId, 'planet')")
	public String getLogbook(long planetId) {
		Planet planet = planetRepository.getOne(planetId);
		return planet.getLog();
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@PreAuthorize("hasPermission(#planetId, 'planet')")
	public void changeLogbook(long planetId, String logbook) {
		Planet planet = planetRepository.getOne(planetId);
		planet.setLog(logbook);
		planetRepository.save(planet);
	}

	@Autowired
	public void setPlanetRepository(PlanetRepository planetRepository) {
		this.planetRepository = planetRepository;
	}
}