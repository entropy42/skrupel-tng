package org.entropy.skrupeltng.modules.ingame.modules.planet.database;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.entropy.skrupeltng.modules.dashboard.ResourceDensity;
import org.entropy.skrupeltng.modules.ingame.Coordinate;
import org.entropy.skrupeltng.modules.ingame.database.Game;
import org.entropy.skrupeltng.modules.ingame.database.Player;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;

@Entity
@Table(name = "planet")
public class Planet implements Serializable, Coordinate, Comparable<Planet> {

	private static final long serialVersionUID = 7057191250871893345L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private Game game;

	private String name;

	private int x;

	private int y;

	private String type;

	private int temperature;

	private String image;

	@ManyToOne(fetch = FetchType.LAZY)
	private Player player;

	private String log;

	private int money;

	private int fuel;

	private int mineral1;

	private int mineral2;

	private int mineral3;

	@OneToOne(fetch = FetchType.LAZY)
	private Starbase starbase;

	private int colonists;

	private int supplies;

	private int mines;

	@Column(name = "auto_build_mines")
	private boolean autoBuildMines;

	private int factories;

	@Column(name = "auto_build_factories")
	private boolean autoBuildFactories;

	@Column(name = "auto_sell_supplies")
	private boolean autoSellSupplies;

	@Column(name = "planetary_defense")
	private int planetaryDefense;

	@Column(name = "auto_build_planetary_defense")
	private boolean autoBuildPlanetaryDefense;

	@Column(name = "light_ground_units")
	private int lightGroundUnits;

	@Column(name = "heavy_ground_units")
	private int heavyGroundUnits;

	@Column(name = "untapped_fuel")
	private int untappedFuel;

	@Column(name = "untapped_mineral1")
	private int untappedMineral1;

	@Column(name = "untapped_mineral2")
	private int untappedMineral2;

	@Column(name = "untapped_mineral3")
	private int untappedMineral3;

	@Column(name = "necessary_mines_for_one_fuel")
	private int necessaryMinesForOneFuel;

	@Column(name = "necessary_mines_for_one_mineral1")
	private int necessaryMinesForOneMineral1;

	@Column(name = "necessary_mines_for_one_mineral2")
	private int necessaryMinesForOneMineral2;

	@Column(name = "necessary_mines_for_one_mineral3")
	private int necessaryMinesForOneMineral3;

	@Column(name = "new_colonists")
	private int newColonists;

	@Column(name = "new_light_ground_units")
	private int newLightGroundUnits;

	@Column(name = "new_heavy_ground_units")
	private int newHeavyGroundUnits;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "new_player_id")
	private Player newPlayer;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "native_species_id")
	private NativeSpecies nativeSpecies;

	@Column(name = "native_species_count")
	private int nativeSpeciesCount;

	@Enumerated(EnumType.STRING)
	@Column(name = "starbase_under_construction_type")
	private StarbaseType starbaseUnderConstructionType;

	@Column(name = "starbase_under_construction_name")
	private String starbaseUnderConstructionName;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "planet")
	private List<OrbitalSystem> orbitalSystems;

	@Override
	public String toString() {
		return "Planet [id=" + id + ", name=" + name + ", x=" + x + ", y=" + y + "]";
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	@Override
	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getTemperature() {
		return temperature;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	public int getFuel() {
		return fuel;
	}

	public void setFuel(int fuel) {
		this.fuel = fuel;
	}

	public int getMineral1() {
		return mineral1;
	}

	public void setMineral1(int mineral1) {
		this.mineral1 = mineral1;
	}

	public int getMineral2() {
		return mineral2;
	}

	public void setMineral2(int mineral2) {
		this.mineral2 = mineral2;
	}

	public int getMineral3() {
		return mineral3;
	}

	public void setMineral3(int mineral3) {
		this.mineral3 = mineral3;
	}

	public Starbase getStarbase() {
		return starbase;
	}

	public void setStarbase(Starbase starbase) {
		this.starbase = starbase;
	}

	public int getColonists() {
		return colonists;
	}

	public void setColonists(int colonists) {
		this.colonists = colonists;
	}

	public int getSupplies() {
		return supplies;
	}

	public void setSupplies(int supplies) {
		this.supplies = supplies;
	}

	public int getMines() {
		return mines;
	}

	public void setMines(int mines) {
		int maxMines = retrieveMaxMines();

		if (maxMines < mines) {
			throw new IllegalArgumentException("mines cannot be higher than " + maxMines);
		}

		this.mines = mines;
	}

	public boolean isAutoBuildMines() {
		return autoBuildMines;
	}

	public void setAutoBuildMines(boolean autoBuildMines) {
		this.autoBuildMines = autoBuildMines;
	}

	public int getFactories() {
		return factories;
	}

	public void setFactories(int factories) {
		int maxFactories = retrieveMaxFactories();

		if (maxFactories < factories) {
			throw new IllegalArgumentException("factories cannot be higher than " + maxFactories);
		}

		this.factories = factories;
	}

	public boolean isAutoBuildFactories() {
		return autoBuildFactories;
	}

	public void setAutoBuildFactories(boolean autoBuildFactories) {
		this.autoBuildFactories = autoBuildFactories;
	}

	public boolean isAutoSellSupplies() {
		return autoSellSupplies;
	}

	public void setAutoSellSupplies(boolean autoSellSupplies) {
		this.autoSellSupplies = autoSellSupplies;
	}

	public int getPlanetaryDefense() {
		return planetaryDefense;
	}

	public void setPlanetaryDefense(int planetaryDefense) {
		int maxPlanetaryDefense = retrieveMaxPlanetaryDefense();

		if (maxPlanetaryDefense < planetaryDefense) {
			throw new IllegalArgumentException("planetaryDefense cannot be higher than " + maxPlanetaryDefense);
		}

		this.planetaryDefense = planetaryDefense;
	}

	public boolean isAutoBuildPlanetaryDefense() {
		return autoBuildPlanetaryDefense;
	}

	public void setAutoBuildPlanetaryDefense(boolean autoBuildPlanetaryDefense) {
		this.autoBuildPlanetaryDefense = autoBuildPlanetaryDefense;
	}

	public int getLightGroundUnits() {
		return lightGroundUnits;
	}

	public void setLightGroundUnits(int lightGroundUnits) {
		this.lightGroundUnits = lightGroundUnits;
	}

	public int getHeavyGroundUnits() {
		return heavyGroundUnits;
	}

	public void setHeavyGroundUnits(int heavyGroundUnits) {
		this.heavyGroundUnits = heavyGroundUnits;
	}

	public int getUntappedFuel() {
		return untappedFuel;
	}

	public void setUntappedFuel(int untappedFuel) {
		this.untappedFuel = untappedFuel;
	}

	public int getUntappedMineral1() {
		return untappedMineral1;
	}

	public void setUntappedMineral1(int untappedMineral1) {
		this.untappedMineral1 = untappedMineral1;
	}

	public int getUntappedMineral2() {
		return untappedMineral2;
	}

	public void setUntappedMineral2(int untappedMineral2) {
		this.untappedMineral2 = untappedMineral2;
	}

	public int getUntappedMineral3() {
		return untappedMineral3;
	}

	public void setUntappedMineral3(int untappedMineral3) {
		this.untappedMineral3 = untappedMineral3;
	}

	public int getNecessaryMinesForOneFuel() {
		return necessaryMinesForOneFuel;
	}

	public void setNecessaryMinesForOneFuel(int necessaryMinesForOneFuel) {
		this.necessaryMinesForOneFuel = necessaryMinesForOneFuel;
	}

	public int getNecessaryMinesForOneMineral1() {
		return necessaryMinesForOneMineral1;
	}

	public void setNecessaryMinesForOneMineral1(int necessaryMinesForOneMineral1) {
		this.necessaryMinesForOneMineral1 = necessaryMinesForOneMineral1;
	}

	public int getNecessaryMinesForOneMineral2() {
		return necessaryMinesForOneMineral2;
	}

	public void setNecessaryMinesForOneMineral2(int necessaryMinesForOneMineral2) {
		this.necessaryMinesForOneMineral2 = necessaryMinesForOneMineral2;
	}

	public int getNecessaryMinesForOneMineral3() {
		return necessaryMinesForOneMineral3;
	}

	public void setNecessaryMinesForOneMineral3(int necessaryMinesForOneMineral3) {
		this.necessaryMinesForOneMineral3 = necessaryMinesForOneMineral3;
	}

	public int getNewColonists() {
		return newColonists;
	}

	public void setNewColonists(int newColonists) {
		this.newColonists = newColonists;
	}

	public int getNewLightGroundUnits() {
		return newLightGroundUnits;
	}

	public void setNewLightGroundUnits(int newLightGroundUnits) {
		this.newLightGroundUnits = newLightGroundUnits;
	}

	public int getNewHeavyGroundUnits() {
		return newHeavyGroundUnits;
	}

	public void setNewHeavyGroundUnits(int newHeavyGroundUnits) {
		this.newHeavyGroundUnits = newHeavyGroundUnits;
	}

	public Player getNewPlayer() {
		return newPlayer;
	}

	public void setNewPlayer(Player newPlayer) {
		this.newPlayer = newPlayer;
	}

	public NativeSpecies getNativeSpecies() {
		return nativeSpecies;
	}

	public void setNativeSpecies(NativeSpecies nativeSpecies) {
		this.nativeSpecies = nativeSpecies;
	}

	public int getNativeSpeciesCount() {
		return nativeSpeciesCount;
	}

	public void setNativeSpeciesCount(int nativeSpeciesCount) {
		this.nativeSpeciesCount = nativeSpeciesCount;
	}

	public List<OrbitalSystem> getOrbitalSystems() {
		return orbitalSystems;
	}

	public void setOrbitalSystems(List<OrbitalSystem> orbitalSystems) {
		this.orbitalSystems = orbitalSystems;
	}

	public StarbaseType getStarbaseUnderConstructionType() {
		return starbaseUnderConstructionType;
	}

	public void setStarbaseUnderConstructionType(StarbaseType starbaseUnderConstructionType) {
		this.starbaseUnderConstructionType = starbaseUnderConstructionType;
	}

	public String getStarbaseUnderConstructionName() {
		return starbaseUnderConstructionName;
	}

	public void setStarbaseUnderConstructionName(String starbaseUnderConstructionName) {
		this.starbaseUnderConstructionName = starbaseUnderConstructionName;
	}

	public void spendMoney(int amount) {
		if (money < amount) {
			throw new IllegalArgumentException("Too much money was tried to be spent!");
		}

		money -= amount;
	}

	public void spendFuel(int quantity) {
		if (fuel < quantity) {
			throw new IllegalArgumentException("Too much of fuel was tried to be spent!");
		}

		fuel -= quantity;
	}

	public void spendMineral1(int quantity) {
		if (mineral1 < quantity) {
			throw new IllegalArgumentException("Too much of mineral1 was tried to be spent!");
		}

		mineral1 -= quantity;
	}

	public void spendMineral2(int quantity) {
		if (mineral2 < quantity) {
			throw new IllegalArgumentException("Too much of mineral2 was tried to be spent!");
		}

		mineral2 -= quantity;
	}

	public void spendMineral3(int quantity) {
		if (mineral3 < quantity) {
			throw new IllegalArgumentException("Too much of mineral3 was tried to be spent!");
		}

		mineral3 -= quantity;
	}

	public void spendSupplies(int quantity) {
		if (supplies < quantity) {
			throw new IllegalArgumentException("Too much of supplies was tried to be spent!");
		}

		supplies -= quantity;
	}

	public void mineFuel(int quantity) {
		if (untappedFuel < quantity) {
			throw new IllegalArgumentException("Too much fuel was tried to be mined!");
		}

		fuel += quantity;
		untappedFuel -= quantity;
	}

	public void mineMineral1(int quantity) {
		if (untappedMineral1 < quantity) {
			throw new IllegalArgumentException("Too much of mineral1 was tried to be mined!");
		}

		mineral1 += quantity;
		untappedMineral1 -= quantity;
	}

	public void mineMineral2(int quantity) {
		if (untappedMineral2 < quantity) {
			throw new IllegalArgumentException("Too much of mineral2 was tried to be mined!");
		}

		mineral2 += quantity;
		untappedMineral2 -= quantity;
	}

	public void mineMineral3(int quantity) {
		if (untappedMineral3 < quantity) {
			throw new IllegalArgumentException("Too much of mineral3 was tried to be mined!");
		}

		mineral3 += quantity;
		untappedMineral3 -= quantity;
	}

	public boolean canConstructStarbase(StarbaseType type) {
		if (money < type.getCostMoney()) {
			return false;
		}

		if (supplies < type.getCostSupplies()) {
			return false;
		}

		if (fuel < type.getCostFuel()) {
			return false;
		}

		if (mineral1 < type.getCostMineral1()) {
			return false;
		}

		if (mineral2 < type.getCostMineral2()) {
			return false;
		}

		if (mineral3 < type.getCostMineral3()) {
			return false;
		}

		return true;
	}

	public String createFullImagePath() {
		return "/images/planets/" + image + ".jpg";
	}

	public int retrieveTotalPlanetaryDefense() {
		return 100 + planetaryDefense + (starbase != null ? starbase.getDefense() : 0);
	}

	public int retrieveEnergyWeaponCount() {
		int starbaseDefense = (starbase != null ? starbase.getDefense() : 0);
		long count = Math.round(Math.sqrt((planetaryDefense + starbaseDefense) / 3.0));
		return count > 10L ? 10 : (int)count;
	}

	public int retrieveEnergyWeaponDamage() {
		long damage = Math.round(Math.sqrt(planetaryDefense / 2));
		return damage > 10L ? 10 : (int)damage;
	}

	public int retrieveHangarCapacity() {
		long hangarCapacity = Math.round(Math.sqrt(planetaryDefense));

		if (starbase != null) {
			hangarCapacity += 5;
		}

		return hangarCapacity > 10L ? 10 : (int)hangarCapacity;
	}

	public int retrieveUntappedFuelDensityPercentage() {
		return (int)(100 * (float)untappedFuel / ResourceDensity.EXTREM.getMax());
	}

	public int retrieveUntappedMineral1DensityPercentage() {
		return (int)(100 * (float)untappedMineral1 / ResourceDensity.EXTREM.getMax());
	}

	public int retrieveUntappedMineral2DensityPercentage() {
		return (int)(100 * (float)untappedMineral2 / ResourceDensity.EXTREM.getMax());
	}

	public int retrieveUntappedMineral3DensityPercentage() {
		return (int)(100 * (float)untappedMineral3 / ResourceDensity.EXTREM.getMax());
	}

	private int retrieveMaxBuildings(int moneyFactor, int max, int current) {
		int colonistsFactor = (int)Math.floor(colonists / 100f <= 50f ? colonists / 100f : 50f + Math.sqrt(colonists / 100f));
		int temp = Math.min(Math.min(colonistsFactor, current + supplies), current + (money / moneyFactor));
		return Math.min(max, temp);
	}

	public int retrieveMaxMines() {
		// TODO orbital systems
		return retrieveMaxBuildings(4, 400, mines);
	}

	public int retrieveMaxFactories() {
		// TODO orbital systems
		return retrieveMaxBuildings(3, 200, factories);
	}

	public int retrieveMaxPlanetaryDefense() {
		// TODO orbital systems
		return retrieveMaxBuildings(10, 300, planetaryDefense);
	}

	@Override
	public int compareTo(Planet o) {
		return Long.compare(id, o.id);
	}
}