package org.entropy.skrupeltng.modules.ingame.modules.starbase.controller;

import java.io.Serializable;

public class StarbaseUpgradeLevel implements Serializable {

	private static final long serialVersionUID = 3856535024612520855L;

	private int number;
	private int money;

	public StarbaseUpgradeLevel() {

	}

	public StarbaseUpgradeLevel(int number, int money) {
		this.number = number;
		this.money = money;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}
}