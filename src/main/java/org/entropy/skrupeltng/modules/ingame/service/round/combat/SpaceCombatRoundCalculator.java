package org.entropy.skrupeltng.modules.ingame.service.round.combat;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.entropy.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class SpaceCombatRoundCalculator extends AbstractShipCombatRoundCalculator {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processCombat(long gameId) {
		log.debug("Processing space combat...");

		List<Ship> ships = shipRepository.findByGameId(gameId);

		Set<ShipDestructionData> destroyedShips = new HashSet<>(ships.size());
		Set<String> processedShipPairs = new HashSet<>();

		for (Ship ship : ships) {
			processCombat(gameId, destroyedShips, ship, processedShipPairs);
		}

		deleteDestroyedShips(destroyedShips);

		log.debug("Space combat processed.");
	}

	protected void processCombat(long gameId, Set<ShipDestructionData> destroyedShips, Ship ship, Set<String> processedShipPairs) {
		int x = ship.getX();
		int y = ship.getY();
		long playerId = ship.getPlayer().getId();

		List<Ship> enemyShips = shipRepository.findEnemyShipsOnSamePosition(gameId, playerId, x, y);

		if (enemyShips.isEmpty()) {
			return;
		}

		ShipDamage shipDamage = new ShipDamage(ship);

		for (Ship enemyShip : enemyShips) {
			String pairId = ship.getId() + "_" + enemyShip.getId();

			if (processedShipPairs.add(pairId)) {
				processCombat(ship, shipDamage, enemyShip);

				if (ship.getDamage() >= 100) {
					ShipDestructionData destructionData = new ShipDestructionData();
					destructionData.setShip(ship);
					destructionData.setDestroyingShipName(enemyShip.getName());
					destructionData.setDestroyingPlayer(enemyShip.getPlayer());
					destroyedShips.add(destructionData);
				} else {
					shipRepository.save(ship);
				}

				if (enemyShip.getDamage() >= 100) {
					ShipDestructionData destructionData = new ShipDestructionData();
					destructionData.setShip(enemyShip);
					destructionData.setDestroyingShipName(ship.getName());
					destructionData.setDestroyingPlayer(ship.getPlayer());
					destroyedShips.add(destructionData);
				} else {
					shipRepository.save(enemyShip);
				}
			}
		}
	}

	private void processCombat(Ship ship, ShipDamage shipDamage, Ship enemyShip) {
		ShipDamage enemyShipDamage = new ShipDamage(enemyShip);

		while (ship.getDamage() < 100 && enemyShip.getDamage() < 100 && ship.getCrew() > 0 && enemyShip.getCrew() > 0) {
			for (int round = 1; round <= 10; round++) {
				if (ship.getDamage() < 100 && enemyShip.getDamage() < 100 && ship.getCrew() > 0 && enemyShip.getCrew() > 0) {
					shipDamage.damageEnemyShip(enemyShip, round);
					enemyShipDamage.damageEnemyShip(ship, round);
				}
			}
		}
	}
}