package org.entropy.skrupeltng.modules.ingame.modules.planet.controller;

import org.entropy.skrupeltng.modules.AbstractController;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.entropy.skrupeltng.modules.ingame.modules.planet.service.PlanetService;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/ingame/planet")
public class PlanetController extends AbstractController {

	@Autowired
	private PlanetService planetService;

	@GetMapping("")
	public String getPlanet(@RequestParam long planetId, Model model) {
		Planet planet = planetService.getPlanet(planetId);
		model.addAttribute("planet", planet);
		model.addAttribute("turnDone", turnDoneForPlanet(planetId));
		return "ingame/planet/planet::content";
	}

	@GetMapping("/details")
	public String getDetails(@RequestParam long planetId, Model model) {
		Planet planet = planetService.getPlanet(planetId);
		model.addAttribute("planet", planet);
		return "ingame/planet/details::content";
	}

	@GetMapping("/mines")
	public String getMines(@RequestParam long planetId, Model model) {
		Planet planet = planetService.getPlanet(planetId);
		model.addAttribute("planet", planet);
		model.addAttribute("maxMines", planet.retrieveMaxMines());
		return "ingame/planet/mines::content";
	}

	@GetMapping("/factories")
	public String getFactories(@RequestParam long planetId, Model model) {
		Planet planet = planetService.getPlanet(planetId);
		model.addAttribute("planet", planet);
		model.addAttribute("maxFactories", planet.retrieveMaxFactories());
		return "ingame/planet/factories::content";
	}

	@GetMapping("/planetary-defense")
	public String getPlanetaryDefense(@RequestParam long planetId, Model model) {
		Planet planet = planetService.getPlanet(planetId);
		model.addAttribute("planet", planet);
		model.addAttribute("maxPlanetaryDefense", planet.retrieveMaxPlanetaryDefense());
		return "ingame/planet/planetary-defense::content";
	}

	@PostMapping("/mines")
	@ResponseBody
	public void buildMines(@RequestParam long planetId, @RequestParam int quantity) {
		planetService.buildMines(planetId, quantity);
	}

	@PostMapping("/factories")
	@ResponseBody
	public void buildFactories(@RequestParam long planetId, @RequestParam int quantity) {
		planetService.buildFactories(planetId, quantity);
	}

	@PostMapping("/sell-supplies")
	@ResponseBody
	public void sellSupplies(@RequestParam long planetId, @RequestParam int quantity) {
		planetService.sellSupplies(planetId, quantity);
	}

	@PostMapping("/planetary-defense")
	@ResponseBody
	public void buildPlanetaryDefense(@RequestParam long planetId, @RequestParam int quantity) {
		planetService.buildPlanetaryDefense(planetId, quantity);
	}

	@PostMapping("/automated-mines")
	@ResponseBody
	public void toggleAutoBuildMines(@RequestParam long planetId) {
		planetService.toggleAutoBuildMines(planetId);
	}

	@PostMapping("/automated-factories")
	@ResponseBody
	public void toggleAutoBuildFactories(@RequestParam long planetId) {
		planetService.toggleAutoBuildFactories(planetId);
	}

	@PostMapping("/automated-sell-supplies")
	@ResponseBody
	public void toggleAutoSellSupplies(@RequestParam long planetId) {
		planetService.toggleAutoSellSupplies(planetId);
	}

	@PostMapping("/automated-planetary-defense")
	@ResponseBody
	public void toggleAutoBuildPlanetaryDefense(@RequestParam long planetId) {
		planetService.toggleAutoBuildPlanetaryDefense(planetId);
	}

	@GetMapping("/starbase-construction")
	public String getStarbaseConstruction(@RequestParam long planetId, Model model) {
		Planet planet = planetService.getPlanet(planetId);

		if (planet.getStarbaseUnderConstructionType() == null) {
			model.addAttribute("planet", planet);
			model.addAttribute("starbaseTypes", StarbaseType.values());

			return "ingame/planet/starbase-construction::content";
		}

		return "ingame/planet/starbase-construction::under-construction";
	}

	@GetMapping("/starbase-construction-name-select")
	public String constructStarbase(@RequestParam long planetId, @RequestParam String type, Model model) {
		model.addAttribute("type", type);
		return "ingame/planet/starbase-construction::name-select";
	}

	@PostMapping("/starbase")
	public String constructStarbase(@RequestParam long planetId, @RequestParam String name, @RequestParam String type) {
		planetService.constructStarbase(planetId, StarbaseType.valueOf(type), name);
		return "ingame/planet/starbase-construction::order-successfull";
	}

	@GetMapping("/logbook")
	public String getLogbook(@RequestParam long planetId, Model model) {
		model.addAttribute("logbook", planetService.getLogbook(planetId));
		return "ingame/logbook::content";
	}

	@PostMapping("/logbook")
	@ResponseBody
	public void changeLogbook(@RequestParam long planetId, @RequestParam String logbook) {
		planetService.changeLogbook(planetId, logbook);
	}
}