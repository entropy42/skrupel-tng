package org.entropy.skrupeltng.modules.ingame.database;

public enum StableWormholeConfig {

	//@formatter:off
	NONE,
	ONE_RANDOM,
	TWO_RANDOM,
	THREE_RANDOM,
	FOUR_RANDOM,
	FIVE_RANDOM,
	ONE_NORTH_SOUTH,
	TWO_NORTH_SOUTH,
	ONE_WEST_EAST,
	TWO_WEST_EAST,
	ONE_NORTH_WEST_EAST_SOUTH,
	ONE_NORTHWEST_SOUTHEAST,
	ONE_NORTHEAST_SOUTHWEST,
	TWO_DIAGONAL
	//@formatter:on
}