package org.entropy.skrupeltng.modules.ingame.service.round;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.entropy.skrupeltng.modules.ai.AIRoundCalculator;
import org.entropy.skrupeltng.modules.ingame.database.GameRepository;
import org.entropy.skrupeltng.modules.ingame.database.Player;
import org.entropy.skrupeltng.modules.ingame.database.PlayerRepository;
import org.entropy.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.entropy.skrupeltng.modules.ingame.service.round.combat.OrbitalCombatRoundCalculator;
import org.entropy.skrupeltng.modules.ingame.service.round.combat.SpaceCombatRoundCalculator;
import org.entropy.skrupeltng.modules.ingame.service.round.losecondition.LoseConditionCalculator;
import org.entropy.skrupeltng.modules.ingame.service.round.wincondition.WinConditionCalculator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RoundCalculationService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private PlanetRoundCalculator planetRoundCalculator;

	@Autowired
	private StarbaseShipConstructionRoundCalculator starbaseShipConstructionRoundCalculator;

	@Autowired
	private ShipRoundCalculator shipRoundCalculator;

	@Autowired
	private OrbitalCombatRoundCalculator orbitalCombatRoundCalculator;

	@Autowired
	private SpaceCombatRoundCalculator spaceCombatRoundCalculator;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private NewsService newsService;

	@Autowired
	private LoseConditionCalculator loseConditionCalculator;

	@Autowired
	private WinConditionCalculator winConditionCalculator;

	@Autowired
	private Map<String, AIRoundCalculator> aiRoundCalculators;

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public void calculateRound(long gameId) {
		log.debug("Starting round calculation...");

		newsService.cleanNewsEntries(gameId);
		planetRoundCalculator.processNewColonies(gameId);
		planetRoundCalculator.processOwnedPlanets(gameId);

		shipRoundCalculator.processShips(gameId);
		shipRoundCalculator.updateDestinations(gameId);
		orbitalCombatRoundCalculator.processCombat(gameId);
		spaceCombatRoundCalculator.processCombat(gameId);

		starbaseShipConstructionRoundCalculator.processShipConstructionJobs(gameId);
		planetRoundCalculator.logVisitedPlanets(gameId);

		computeAIRounds(gameId);

		loseConditionCalculator.checkLoseCondition(gameId);
		winConditionCalculator.checkWinCondition(gameId);

		gameRepository.increaseRound(gameId);

		log.debug("Round calculation finished.");
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void computeAIRounds(long gameId) {
		log.debug("Computing AI rounds...");

		List<Player> players = playerRepository.findAIPlayers(gameId);

		ExecutorService executor = Executors.newSingleThreadExecutor();

		for (Player player : players) {
			try {
				Future<?> future = executor.submit(() -> calculateAIRound(player));
				future.get();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		executor.shutdown();
		log.debug("AI rounds computed.");

		log.debug("Setting up turn values...");
		playerRepository.setupTurnValues(gameId);
		log.debug("Turn values set up.");
	}

	private void calculateAIRound(Player player) {
		SecurityContext context = SecurityContextHolder.getContext();
		String aiLevel = player.getAiLevel().name();
		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(aiLevel, aiLevel);
		context.setAuthentication(authentication);

		AIRoundCalculator roundCalculator = aiRoundCalculators.get(aiLevel);
		roundCalculator.calculateRound(player);
	}
}