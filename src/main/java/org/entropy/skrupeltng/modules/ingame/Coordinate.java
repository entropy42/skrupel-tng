package org.entropy.skrupeltng.modules.ingame;

public interface Coordinate {

	int getX();

	int getY();
}