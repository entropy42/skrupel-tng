package org.entropy.skrupeltng.modules.ingame.database;

import java.util.List;

import org.entropy.skrupeltng.modules.dashboard.GameListResult;
import org.entropy.skrupeltng.modules.dashboard.GameSearchParameters;
import org.entropy.skrupeltng.modules.ingame.CoordinateImpl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface GameRepositoryCustom {

	Page<GameListResult> searchGames(GameSearchParameters params, Pageable page);

	List<CoordinateImpl> getVisibilityCoordinates(long playerId);
}