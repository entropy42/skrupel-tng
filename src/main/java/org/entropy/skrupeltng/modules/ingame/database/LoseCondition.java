package org.entropy.skrupeltng.modules.ingame.database;

public enum LoseCondition {

	LOSE_HOME_PLANET, LOSE_STATIONS, LOSE_PLANETS, LOSE_PLANETS_AND_FLEET
}