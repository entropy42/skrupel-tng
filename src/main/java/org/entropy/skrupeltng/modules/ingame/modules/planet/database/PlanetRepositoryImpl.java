package org.entropy.skrupeltng.modules.ingame.modules.planet.database;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.entropy.skrupeltng.modules.RepositoryCustomBase;
import org.entropy.skrupeltng.modules.ingame.controller.PlanetEntry;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

public class PlanetRepositoryImpl extends RepositoryCustomBase implements PlanetRepositoryCustom {

	@Override
	public List<PlanetEntry> getPlanetsForGalaxy(long gameId) {
		String sql = "" +
				"SELECT \n" +
				"	pl.id, \n" +
				"	pl.name, \n" +
				"	pl.x, \n" +
				"	pl.y, \n" +
				"	pl.type, \n" +
				"	p.color as \"playerColor\", \n" +
				"	pl.player_id as \"playerId\", \n" +
				"	sb.id as \"starbaseId\", \n" +
				"	sb.name as \"starbaseName\", \n" +
				"	sb.log as \"starbaseLog\", \n" +
				"	ps.color as \"shipPlayerColor\", \n" +
				"	pl.colonists, \n" +
				"	pl.money, \n" +
				"	pl.supplies, \n" +
				"	pl.fuel, \n" +
				"	pl.mineral1, \n" +
				"	pl.mineral2, \n" +
				"	pl.mineral3, \n" +
				"	pl.untapped_fuel as \"untappedFuel\", \n" +
				"	pl.untapped_mineral1 as \"untappedMineral1\", \n" +
				"	pl.untapped_mineral2 as \"untappedMineral2\", \n" +
				"	pl.untapped_mineral3 as \"untappedMineral3\", \n" +
				"	pl.mines, \n" +
				"	pl.factories, \n" +
				"	pl.auto_build_mines as \"autoBuildMines\", \n" +
				"	pl.auto_build_factories as \"autoBuildFactories\", \n" +
				"	pl.log \n" +
				"FROM \n" +
				"	planet pl \n" +
				"	LEFT OUTER JOIN player p \n" +
				"		ON p.id = pl.player_id \n" +
				"	LEFT OUTER JOIN ship s \n" +
				"		ON s.planet_id = pl.id \n" +
				"	LEFT OUTER JOIN player ps \n" +
				"		ON ps.id = s.player_id \n" +
				"	LEFT OUTER JOIN starbase sb \n" +
				"		ON sb.id = pl.starbase_id \n" +
				"WHERE \n" +
				"	pl.game_id = :gameId \n" +
				"GROUP BY \n" +
				"	pl.id, \n" +
				"	pl.name, \n" +
				"	pl.x, \n" +
				"	pl.y, \n" +
				"	pl.type, \n" +
				"	p.color, \n" +
				"	pl.player_id, \n" +
				"	sb.id, \n" +
				"	sb.name, \n" +
				"	sb.log, \n" +
				"	ps.color, \n" +
				"	pl.colonists, \n" +
				"	pl.money, \n" +
				"	pl.supplies, \n" +
				"	pl.fuel, \n" +
				"	pl.mineral1, \n" +
				"	pl.mineral2, \n" +
				"	pl.mineral3, \n" +
				"	pl.untapped_fuel, \n" +
				"	pl.untapped_mineral1, \n" +
				"	pl.untapped_mineral2, \n" +
				"	pl.untapped_mineral3, \n" +
				"	pl.mines, \n" +
				"	pl.factories, \n" +
				"	pl.auto_build_mines, \n" +
				"	pl.auto_build_factories, \n" +
				"	pl.log";

		Map<String, Object> params = new HashMap<>(1);
		params.put("gameId", gameId);

		RowMapper<PlanetEntry> rowMapper = new BeanPropertyRowMapper<>(PlanetEntry.class);
		return jdbcTemplate.query(sql, params, rowMapper);
	}

	@Override
	public boolean loginOwnsPlanet(long planetId, long loginId) {
		String sql = "" +
				"SELECT \n" +
				"	p.login_id \n" +
				"FROM \n" +
				"	planet pl \n" +
				"	INNER JOIN player p \n" +
				"		ON p.id = pl.player_id \n" +
				"WHERE \n" +
				"	pl.id = :planetId";

		Map<String, Object> params = new HashMap<>(1);
		params.put("planetId", planetId);

		List<Long> results = jdbcTemplate.queryForList(sql, params, Long.class);
		return results.size() == 1 && results.get(0).longValue() == loginId;
	}
}