package org.entropy.skrupeltng.modules.ingame.service.round.combat;

import org.entropy.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.entropy.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.entropy.skrupeltng.modules.masterdata.database.WeaponTemplate;
import org.entropy.skrupeltng.modules.masterdata.service.MasterDataService;

public class ShipDamage {

	public static final int HANGAR_DAMAGE = 4;
	private static final float HANGAR_DAMAGE_CREW = 1.5F;
	private static final int[] ENERGY_WEAPON_DAMAGE_DATA = new int[] { 0, 3, 7, 10, 15, 12, 29, 35, 37, 18, 45 };
	private static final int[] ENERGY_WEAPON_DAMAGE_CREW_DATA = new int[] { 0, 1, 2, 2, 4, 16, 7, 8, 9, 33, 11 };
	private static final int[] PROJECTILE_WEAPON_DAMAGE_DATA = new int[] { 0, 5, 8, 10, 6, 15, 30, 35, 12, 48, 55 };
	private static final int[] PROJECTILE_WEAPON_DAMAGE_CREW_DATA = new int[] { 0, 1, 2, 2, 13, 6, 7, 8, 36, 12, 14 };

	private final Ship ship;

	private int energyWeaponDamage;
	private int energyWeaponDamageCrew;
	private int projectileWeaponDamage;
	private int projectileWeaponDamageCrew;

	public ShipDamage(Ship ship) {
		this.ship = ship;

		initEnergyWeaponDamage();
		initProjectileWeaponDamage();
	}

	private void initEnergyWeaponDamage() {
		WeaponTemplate template = ship.getEnergyWeaponTemplate();

		if (template != null) {
			int index = template.getTechLevel() - 1;
			energyWeaponDamage = ENERGY_WEAPON_DAMAGE_DATA[index];
			projectileWeaponDamage = ENERGY_WEAPON_DAMAGE_CREW_DATA[index];
		}
	}

	private void initProjectileWeaponDamage() {
		WeaponTemplate template = ship.getProjectileWeaponTemplate();

		if (template != null) {
			int index = template.getTechLevel() - 1;
			projectileWeaponDamage = PROJECTILE_WEAPON_DAMAGE_DATA[index];
			projectileWeaponDamageCrew = PROJECTILE_WEAPON_DAMAGE_CREW_DATA[index];
		}
	}

	public void damageEnemyShip(Ship enemyShip, int round) {
		enemyShip.receiveDamage(getEnergyWeaponDamage(round), getEnergyWeaponDamageCrew(round));
		enemyShip.receiveDamage(getProjectileWeaponDamage(round), getProjectileWeaponDamageCrew(round));
		enemyShip.receiveDamage(HANGAR_DAMAGE, HANGAR_DAMAGE_CREW);
	}

	private int getEnergyWeaponDamage(int round) {
		ShipTemplate shipTemplate = ship.getShipTemplate();

		if (shipTemplate.getEnergyWeaponsCount() >= round) {
			return energyWeaponDamage;
		}

		return 0;
	}

	private int getEnergyWeaponDamageCrew(int round) {
		ShipTemplate shipTemplate = ship.getShipTemplate();

		if (shipTemplate.getEnergyWeaponsCount() >= round) {
			return energyWeaponDamageCrew;
		}

		return 0;
	}

	private int getProjectileWeaponDamage(int round) {
		return getProjectileDamage(round, projectileWeaponDamage);
	}

	private int getProjectileWeaponDamageCrew(int round) {
		return getProjectileDamage(round, projectileWeaponDamageCrew);
	}

	private int getProjectileDamage(int round, int damage) {
		ShipTemplate shipTemplate = ship.getShipTemplate();

		if (shipTemplate.getProjectileWeaponsCount() >= round && ship.getProjectiles() > 0) {
			ship.spendProjectile();
			int ran = MasterDataService.RANDOM.nextInt(100);

			if (ran < (66 + (6 * ship.getExperience()))) {
				return damage;
			}
		}

		return 0;
	}

	private int getHangarDamage(int round) {
		ShipTemplate shipTemplate = ship.getShipTemplate();

		if (shipTemplate.getHangarCapacity() >= round) {
			return HANGAR_DAMAGE;
		}

		return 0;
	}

	public int calculateDamageToPlanet(int round) {
		int damage = getEnergyWeaponDamage(round);
		damage += getProjectileWeaponDamage(round);
		damage += getHangarDamage(round);
		return damage;
	}
}
