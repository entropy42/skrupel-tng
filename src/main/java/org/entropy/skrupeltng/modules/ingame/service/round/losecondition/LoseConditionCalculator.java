package org.entropy.skrupeltng.modules.ingame.service.round.losecondition;

import java.util.List;
import java.util.Map;

import org.entropy.skrupeltng.modules.ingame.database.Game;
import org.entropy.skrupeltng.modules.ingame.database.GameRepository;
import org.entropy.skrupeltng.modules.ingame.database.LoseCondition;
import org.entropy.skrupeltng.modules.ingame.database.Player;
import org.entropy.skrupeltng.modules.ingame.database.PlayerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class LoseConditionCalculator {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private Map<String, LoseConditionHandler> loseConditionHandlers;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void checkLoseCondition(long gameId) {
		log.debug("Processing lose condition...");

		Game game = gameRepository.getOne(gameId);

		LoseCondition loseCondition = game.getLoseCondition();
		LoseConditionHandler handler = loseConditionHandlers.get(loseCondition.name());

		List<Player> players = playerRepository.findByGameId(gameId);

		for (Player player : players) {
			if (handler.hasLost(player)) {
				player.setHasLost(true);
				playerRepository.save(player);
			}
		}

		log.debug("Lose condition processed.");
	}
}