package org.entropy.skrupeltng.modules.ingame.modules.starbase.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.entropy.skrupeltng.modules.AbstractController;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbaseHullStock;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbasePropulsionStock;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbaseWeaponStock;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.service.StarbaseService;
import org.entropy.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.entropy.skrupeltng.modules.masterdata.service.MasterDataService;
import org.entropy.skrupeltng.modules.masterdata.service.StarbaseProductionEntry;
import org.entropy.skrupeltng.modules.masterdata.service.StarbaseUpgradeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/ingame/starbase")
public class StarbaseController extends AbstractController {

	@Autowired
	private StarbaseService starbaseService;

	@Autowired
	private MasterDataService masterDataService;

	@GetMapping("")
	public String getStarbase(@RequestParam long starbaseId, Model model) {
		Starbase starbase = starbaseService.getStarbase(starbaseId);
		model.addAttribute("starbase", starbase);
		model.addAttribute("turnDone", turnDoneForStarbase(starbaseId));
		return "ingame/starbase/starbase::content";
	}

	@GetMapping("/upgrade")
	public String getUpgrades(@RequestParam long starbaseId, Model model) {
		prepareUpgradeModel(model, starbaseId);
		return "ingame/starbase/upgrade::content";
	}

	private void prepareUpgradeModel(Model model, long starbaseId) {
		Starbase starbase = starbaseService.getStarbase(starbaseId);
		model.addAttribute("starbase", starbase);

		int money = starbase.getPlanet().getMoney();
		model.addAttribute("hullCosts", masterDataService.getLevelCosts(starbase.getHullLevel(), StarbaseUpgradeType.HULL, money));
		model.addAttribute("propulsionCosts", masterDataService.getLevelCosts(starbase.getPropulsionLevel(), StarbaseUpgradeType.PROPULSION, money));
		model.addAttribute("energyCosts", masterDataService.getLevelCosts(starbase.getEnergyLevel(), StarbaseUpgradeType.ENERGY, money));
		model.addAttribute("projectileCosts", masterDataService.getLevelCosts(starbase.getProjectileLevel(), StarbaseUpgradeType.PROJECTILE, money));
	}

	@PostMapping("/upgrade")
	@ResponseBody
	public void upgrade(@RequestBody StarbaseUpgradeRequest request, @RequestParam long starbaseId) {
		starbaseService.upgradeStarbase(starbaseId, request);
	}

	@GetMapping("/production-hull")
	public String getHullProduction(@RequestParam long starbaseId, Model model) {
		return getProduction(starbaseId, model, StarbaseUpgradeType.HULL);
	}

	@GetMapping("/production-propulsion")
	public String getPropulsionProduction(@RequestParam long starbaseId, Model model) {
		return getProduction(starbaseId, model, StarbaseUpgradeType.PROPULSION);
	}

	@GetMapping("/production-energyweapon")
	public String getEnergyProduction(@RequestParam long starbaseId, Model model) {
		return getProduction(starbaseId, model, StarbaseUpgradeType.ENERGY);
	}

	@GetMapping("/production-projectileweapon")
	public String getProjectileProduction(@RequestParam long starbaseId, Model model) {
		return getProduction(starbaseId, model, StarbaseUpgradeType.PROJECTILE);
	}

	private String getProduction(long starbaseId, Model model, StarbaseUpgradeType type) {
		List<StarbaseProductionEntry> data = starbaseService.getStarbaseProductionData(starbaseId, type);
		model.addAttribute("data", data);
		return "ingame/starbase/production::content";
	}

	@PostMapping("/produce")
	@ResponseBody
	public void produce(@RequestBody StarbaseProductionRequest request, @RequestParam long starbaseId) {
		starbaseService.produce(request, starbaseId);
	}

	@GetMapping("/shipconstruction")
	public String getShipPConstruction(@RequestParam long starbaseId, Model model) {
		if (starbaseService.shipConstructionJobExists(starbaseId)) {
			return "ingame/starbase/shipconstruction::ship-job-exists";
		}

		Starbase starbase = starbaseService.getStarbase(starbaseId);
		model.addAttribute("starbase", starbase);

		return "ingame/starbase/shipconstruction::hull-selection";
	}

	@GetMapping("/shipconstruction-details")
	public String getShipPConstructionDetails(@RequestParam long starbaseId, @RequestParam long hullStockId, Model model) {
		Starbase starbase = starbaseService.getStarbase(starbaseId);
		model.addAttribute("starbase", starbase);

		Optional<StarbaseHullStock> stockResult = starbase.getHullStocks().stream().filter(stock -> stock.getId() == hullStockId).findFirst();

		if (stockResult.isPresent()) {
			StarbaseHullStock hullStock = stockResult.get();
			model.addAttribute("hullStock", hullStock);

			boolean shipCannotBeBuild = false;

			ShipTemplate shipTemplate = hullStock.getShipTemplate();

			int propulsionSystemsCount = shipTemplate.getPropulsionSystemsCount();
			List<StarbasePropulsionStock> propulsionSystems = starbase.getPropulsionStocks().stream().filter(s -> s.getStock() >= propulsionSystemsCount).collect(Collectors.toList());
			model.addAttribute("propulsionSystems", propulsionSystems);

			if (propulsionSystems.isEmpty()) {
				shipCannotBeBuild = true;
			}

			int energyWeaponsCount = shipTemplate.getEnergyWeaponsCount();

			if (energyWeaponsCount > 0) {
				List<StarbaseWeaponStock> energyWeapons = starbase.getWeaponStocks().stream().filter(s -> !s.getWeaponTemplate().isUsesProjectiles() && s.getStock() >= energyWeaponsCount).collect(Collectors.toList());
				model.addAttribute("energyWeapons", energyWeapons);

				if (energyWeapons.isEmpty()) {
					shipCannotBeBuild = true;
				}
			}

			int projectileWeaponsCount = shipTemplate.getProjectileWeaponsCount();

			if (projectileWeaponsCount > 0) {
				List<StarbaseWeaponStock> projectileWeapons = starbase.getWeaponStocks().stream().filter(s -> s.getWeaponTemplate().isUsesProjectiles() && s.getStock() >= projectileWeaponsCount).collect(Collectors.toList());
				model.addAttribute("projectileWeapons", projectileWeapons);

				if (projectileWeapons.isEmpty()) {
					shipCannotBeBuild = true;
				}
			}

			model.addAttribute("shipCannotBeBuild", shipCannotBeBuild);

			return "ingame/starbase/shipconstruction::content";
		}

		throw new IllegalArgumentException("hullStockId " + hullStockId + " not found!");
	}

	@PostMapping("/shipconstruction")
	public String addShipConstructionJob(@RequestParam long starbaseId, @RequestBody StarbaseShipConstructionRequest request) {
		starbaseService.addShipConstructionJob(starbaseId, request);
		return "ingame/starbase/shipconstruction::success";
	}

	@GetMapping("/logbook")
	public String getLogbook(@RequestParam long starbaseId, Model model) {
		model.addAttribute("logbook", starbaseService.getLogbook(starbaseId));
		return "ingame/logbook::content";
	}

	@PostMapping("/logbook")
	@ResponseBody
	public void changeLogbook(@RequestParam long starbaseId, @RequestParam String logbook) {
		starbaseService.changeLogbook(starbaseId, logbook);
	}
}