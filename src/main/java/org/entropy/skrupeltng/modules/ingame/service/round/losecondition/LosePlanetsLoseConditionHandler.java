package org.entropy.skrupeltng.modules.ingame.service.round.losecondition;

import org.entropy.skrupeltng.modules.ingame.database.Player;
import org.springframework.stereotype.Component;

@Component("LOSE_PLANETS")
public class LosePlanetsLoseConditionHandler implements LoseConditionHandler {

	@Override
	public boolean hasLost(Player player) {
		return player.getPlanets().size() == 0;
	}
}