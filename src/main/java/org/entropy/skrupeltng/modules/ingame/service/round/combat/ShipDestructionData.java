package org.entropy.skrupeltng.modules.ingame.service.round.combat;

import java.util.Objects;

import org.entropy.skrupeltng.modules.ingame.database.Player;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.Ship;

public class ShipDestructionData {

	private Ship ship;
	private String destroyingShipName;
	private String destroyingPlanetName;
	private Player destroyingPlayer;

	public Ship getShip() {
		return ship;
	}

	public void setShip(Ship ship) {
		this.ship = ship;
	}

	public String getDestroyingShipName() {
		return destroyingShipName;
	}

	public void setDestroyingShipName(String destroyingShipName) {
		this.destroyingShipName = destroyingShipName;
	}

	public String getDestroyingPlanetName() {
		return destroyingPlanetName;
	}

	public void setDestroyingPlanetName(String destroyingPlanetName) {
		this.destroyingPlanetName = destroyingPlanetName;
	}

	public Player getDestroyingPlayer() {
		return destroyingPlayer;
	}

	public void setDestroyingPlayer(Player destroyingPlayer) {
		this.destroyingPlayer = destroyingPlayer;
	}

	@Override
	public int hashCode() {
		return Objects.hash(destroyingPlanetName, destroyingPlayer, destroyingShipName, ship);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ShipDestructionData other = (ShipDestructionData)obj;
		return Objects.equals(destroyingPlanetName, other.destroyingPlanetName) && Objects.equals(destroyingPlayer, other.destroyingPlayer) && Objects.equals(destroyingShipName, other.destroyingShipName) && Objects.equals(ship, other.ship);
	}
}
