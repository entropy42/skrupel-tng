package org.entropy.skrupeltng.modules.ingame.modules.overview.database;

import java.util.HashMap;
import java.util.Map;

import org.entropy.skrupeltng.modules.RepositoryCustomBase;
import org.entropy.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.entropy.skrupeltng.modules.ingame.modules.overview.service.RoundSummary;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

public class NewsEntryRepositoryImpl extends RepositoryCustomBase implements NewsEntryRepositoryCustom {

	@Override
	public void deleteByGameId(long gameId) {
		String sql = "" +
				"DELETE FROM \n" +
				"	news_entry \n" +
				"WHERE \n" +
				"	game_id = :gameId \n" +
				"	AND delete = true";

		Map<String, Object> params = new HashMap<>(1);
		params.put("gameId", gameId);

		jdbcTemplate.update(sql, params);
	}

	@Override
	public RoundSummary getRoundSummary(long gameId) {
		String sql = "" +
				"SELECT \n" +
				"	g.round, \n" +
				"	g.round_date as \"date\", \n" +
				"	g.win_condition as \"winCondition\", \n" +
				"	(SELECT COUNT(id) FROM news_entry WHERE game_id = :gameId AND template = '" + NewsEntryConstants.news_entry_new_colony + "') as \"newColonies\", \n" +
				"	(SELECT COUNT(id) FROM news_entry WHERE game_id = :gameId AND template = '" + NewsEntryConstants.news_entry_ship_constructed + "') as \"newShips\", \n" +
				"	(SELECT COUNT(id) FROM news_entry WHERE game_id = :gameId AND template = '" + NewsEntryConstants.news_entry_starbase_constructed + "') as \"newStarbases\", \n" +
				"	(SELECT COUNT(id) FROM news_entry WHERE game_id = :gameId AND template = '" + NewsEntryConstants.news_entry_conquered_colony + "') as \"conqueredColonies\", \n" +
				"	(SELECT COUNT(id) FROM news_entry WHERE game_id = :gameId AND template = '" + NewsEntryConstants.news_entry_destroyed_ship_by_planet + "') as \"destroyedShipsByPlanets\", \n" +
				"	(SELECT COUNT(id) FROM news_entry WHERE game_id = :gameId AND template = '" + NewsEntryConstants.news_entry_destroyed_ship_by_ship + "') as \"destroyedShipsByShips\" \n" +
				"FROM \n" +
				"	game g \n" +
				"WHERE \n" +
				"	g.id = :gameId";

		Map<String, Object> params = new HashMap<>(1);
		params.put("gameId", gameId);

		RowMapper<RoundSummary> rowMapper = new BeanPropertyRowMapper<>(RoundSummary.class);
		return jdbcTemplate.queryForObject(sql, params, rowMapper);
	}
}