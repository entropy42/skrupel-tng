package org.entropy.skrupeltng.modules.ingame.modules.starbase.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.entropy.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.controller.StarbaseProductionRequest;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.controller.StarbaseShipConstructionRequest;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.controller.StarbaseUpgradeLevel;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.controller.StarbaseUpgradeRequest;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbaseHullStock;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbaseHullStockRepository;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbasePropulsionStock;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbasePropulsionStockRepository;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbaseShipConstructionJob;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbaseShipConstructionJobRepository;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbaseWeaponStock;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbaseWeaponStockRepository;
import org.entropy.skrupeltng.modules.masterdata.StarbaseProducable;
import org.entropy.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;
import org.entropy.skrupeltng.modules.masterdata.database.PropulsionSystemTemplateRepository;
import org.entropy.skrupeltng.modules.masterdata.database.Race;
import org.entropy.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.entropy.skrupeltng.modules.masterdata.database.WeaponTemplate;
import org.entropy.skrupeltng.modules.masterdata.database.WeaponTemplateRepository;
import org.entropy.skrupeltng.modules.masterdata.service.MasterDataService;
import org.entropy.skrupeltng.modules.masterdata.service.StarbaseProductionEntry;
import org.entropy.skrupeltng.modules.masterdata.service.StarbaseUpgradeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

@Service
public class StarbaseService {

	@Autowired
	private StarbaseRepository starbaseRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private MasterDataService masterDataService;

	@Autowired
	private StarbaseHullStockRepository starbaseHullStockRepository;

	@Autowired
	private StarbasePropulsionStockRepository starbasePropulsionStockRepository;

	@Autowired
	private StarbaseWeaponStockRepository starbaseWeaponStockRepository;

	@Autowired
	private WeaponTemplateRepository weaponTemplateRepository;

	@Autowired
	private PropulsionSystemTemplateRepository propulsionSystemTemplateRepository;

	@Autowired
	private StarbaseShipConstructionJobRepository starbaseShipConstructionJobRepository;

	@PreAuthorize("hasPermission(#starbaseId, 'starbase')")
	public Starbase getStarbase(long starbaseId) {
		return starbaseRepository.getOne(starbaseId);
	}

	@PreAuthorize("hasPermission(#starbaseId, 'starbase') and hasPermission(#starbaseId, 'turnNotDoneStarbase')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void upgradeStarbase(long starbaseId, StarbaseUpgradeRequest request) {
		int hullLevel = request.getHullLevel();
		int propulsionLevel = request.getPropulsionLevel();
		int energyLevel = request.getEnergyLevel();
		int projectileLevel = request.getProjectileLevel();

		Starbase starbase = starbaseRepository.getOne(starbaseId);
		Planet planet = starbase.getPlanet();

		if (hullLevel > 0) {
			int finalLevel = upgradeStarbaseProperty(planet, starbase.getHullLevel(), hullLevel, StarbaseUpgradeType.HULL);
			starbase.setHullLevel(finalLevel);
		}

		if (propulsionLevel > 0) {
			int finalLevel = upgradeStarbaseProperty(planet, starbase.getPropulsionLevel(), propulsionLevel, StarbaseUpgradeType.PROPULSION);
			starbase.setPropulsionLevel(finalLevel);
		}

		if (energyLevel > 0) {
			int finalLevel = upgradeStarbaseProperty(planet, starbase.getEnergyLevel(), energyLevel, StarbaseUpgradeType.ENERGY);
			starbase.setEnergyLevel(finalLevel);
		}

		if (projectileLevel > 0) {
			int finalLevel = upgradeStarbaseProperty(planet, starbase.getProjectileLevel(), projectileLevel, StarbaseUpgradeType.PROJECTILE);
			starbase.setProjectileLevel(finalLevel);
		}

		planetRepository.save(planet);
		starbaseRepository.save(starbase);
	}

	private int upgradeStarbaseProperty(Planet planet, int startLevel, int targetLevel, StarbaseUpgradeType type) {
		List<StarbaseUpgradeLevel> list = masterDataService.getLevelCosts(startLevel, type, planet.getMoney());

		int finalLevel = startLevel;
		int moneyToBeSubtracted = 0;

		for (int i = finalLevel + 1; i <= targetLevel; i++) {
			int index = i - startLevel - 1;

			if (index < list.size()) {
				StarbaseUpgradeLevel level = list.get(index);

				if (planet.getMoney() >= level.getMoney()) {
					finalLevel++;
					moneyToBeSubtracted = level.getMoney();
				} else {
					break;
				}
			} else {
				break;
			}
		}

		if (moneyToBeSubtracted > 0) {
			planet.setMoney(planet.getMoney() - moneyToBeSubtracted);
		}

		return finalLevel;
	}

	@PreAuthorize("hasPermission(#starbaseId, 'starbase') and hasPermission(#starbaseId, 'turnNotDoneStarbase')")
	public List<StarbaseProductionEntry> getStarbaseProductionData(long starbaseId, StarbaseUpgradeType type) {
		Starbase starbase = starbaseRepository.getOne(starbaseId);

		switch (type) {
			case HULL:
				return getHullProductionData(starbase);
			case PROPULSION:
				return getPropulsionProductionData(starbase);
			case ENERGY:
				return getWeaponProductionData(starbase, StarbaseUpgradeType.ENERGY);
			case PROJECTILE:
				return getWeaponProductionData(starbase, StarbaseUpgradeType.PROJECTILE);
		}

		return null;
	}

	private List<StarbaseProductionEntry> getHullProductionData(Starbase starbase) {
		Planet planet = starbase.getPlanet();
		Race race = planet.getPlayer().getRace();
		int techLevel = starbase.getHullLevel();

		List<ShipTemplate> ships = race.getShips();
		List<StarbaseProductionEntry> data = new ArrayList<>(ships.size());

		for (ShipTemplate ship : ships) {
			StarbaseProductionEntry entry = new StarbaseProductionEntry();
			data.add(entry);

			entry.setId(ship.getId());
			entry.setName(ship.getName());
			entry.setTechLevel(ship.getTechLevel());
			entry.setType(StarbaseUpgradeType.HULL);
			entry.setCostMoney(ship.getCostMoney());
			entry.setCostMineral1(ship.getCostMineral1());
			entry.setCostMineral2(ship.getCostMineral2());
			entry.setCostMineral3(ship.getCostMineral3());

			Optional<StarbaseHullStock> stock = starbaseHullStockRepository.findByShipTemplateIdAndStarbaseId(ship.getId(), starbase.getId());

			if (stock.isPresent()) {
				entry.setStock(stock.get().getStock());
			}

			if (techLevel >= ship.getTechLevel()) {
				entry.setProducableQuantity(calculateProducableQuantity(planet, entry));
			}
		}

		return data;
	}

	private List<StarbaseProductionEntry> getPropulsionProductionData(Starbase starbase) {
		Planet planet = starbase.getPlanet();
		List<PropulsionSystemTemplate> propulsionSystems = propulsionSystemTemplateRepository.findAll();
		int techLevel = starbase.getPropulsionLevel();

		List<StarbaseProductionEntry> data = new ArrayList<>(propulsionSystems.size());

		for (PropulsionSystemTemplate propulsionSystem : propulsionSystems) {
			StarbaseProductionEntry entry = new StarbaseProductionEntry();
			data.add(entry);

			entry.setId(propulsionSystem.getName());
			entry.setName(propulsionSystem.getName());
			entry.setTechLevel(propulsionSystem.getTechLevel());
			entry.setType(StarbaseUpgradeType.PROPULSION);
			entry.setCostMoney(propulsionSystem.getCostMoney());
			entry.setCostMineral1(propulsionSystem.getCostMineral1());
			entry.setCostMineral2(propulsionSystem.getCostMineral2());
			entry.setCostMineral3(propulsionSystem.getCostMineral3());

			Optional<StarbasePropulsionStock> stock = starbasePropulsionStockRepository.findByPropulsionSystemTemplateNameAndStarbaseId(propulsionSystem.getName(), starbase.getId());

			if (stock.isPresent()) {
				entry.setStock(stock.get().getStock());
			}

			if (techLevel >= propulsionSystem.getTechLevel()) {
				entry.setProducableQuantity(calculateProducableQuantity(planet, entry));
			}
		}

		return data;
	}

	private List<StarbaseProductionEntry> getWeaponProductionData(Starbase starbase, StarbaseUpgradeType type) {
		Planet planet = starbase.getPlanet();
		boolean usesProjectiles = type == StarbaseUpgradeType.PROJECTILE;
		List<WeaponTemplate> weapons = weaponTemplateRepository.findByUsesProjectiles(usesProjectiles);
		int techLevel = usesProjectiles ? starbase.getProjectileLevel() : starbase.getEnergyLevel();

		List<StarbaseProductionEntry> data = new ArrayList<>(weapons.size());

		for (WeaponTemplate weapon : weapons) {
			StarbaseProductionEntry entry = new StarbaseProductionEntry();
			data.add(entry);

			entry.setId(weapon.getName());
			entry.setName(weapon.getName());
			entry.setTechLevel(weapon.getTechLevel());
			entry.setType(type);
			entry.setCostMoney(weapon.getCostMoney());
			entry.setCostMineral1(weapon.getCostMineral1());
			entry.setCostMineral2(weapon.getCostMineral2());
			entry.setCostMineral3(weapon.getCostMineral3());

			Optional<StarbaseWeaponStock> stock = starbaseWeaponStockRepository.findByWeaponTemplateNameAndStarbaseId(weapon.getName(), starbase.getId());

			if (stock.isPresent()) {
				entry.setStock(stock.get().getStock());
			}

			if (techLevel >= weapon.getTechLevel()) {
				entry.setProducableQuantity(calculateProducableQuantity(planet, entry));
			}
		}

		return data;
	}

	private int calculateProducableQuantity(Planet planet, StarbaseProductionEntry entry) {
		int costMoney = entry.getCostMoney();
		int costMineral1 = entry.getCostMineral1();
		int costMineral2 = entry.getCostMineral2();
		int costMineral3 = entry.getCostMineral3();

		int quantityMoney = costMoney > 0 ? planet.getMoney() / costMoney : 10;
		int quantityMin1 = costMineral1 > 0 ? planet.getMineral1() / costMineral1 : 10;
		int quantityMin2 = costMineral2 > 0 ? planet.getMineral2() / costMineral2 : 10;
		int quantityMin3 = costMineral3 > 0 ? planet.getMineral3() / costMineral3 : 10;

		List<Integer> list = Lists.newArrayList(10, quantityMoney, quantityMin1, quantityMin2, quantityMin3);
		return list.stream().sorted().findFirst().get();
	}

	@PreAuthorize("hasPermission(#starbaseId, 'starbase') and hasPermission(#starbaseId, 'turnNotDoneStarbase')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void produce(StarbaseProductionRequest request, long starbaseId) {
		produceWithoutPermissionCheck(request, starbaseId);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void produceWithoutPermissionCheck(StarbaseProductionRequest request, long starbaseId) {
		StarbaseUpgradeType type = StarbaseUpgradeType.valueOf(request.getType());
		Starbase starbase = starbaseRepository.getOne(starbaseId);

		switch (type) {
			case HULL:
				produceHull(request, starbase);
				break;
			case PROPULSION:
				producePropulsionSystem(request, starbase);
				break;
			case ENERGY:
				produceWeaponSystem(request, starbase);
				break;
			case PROJECTILE:
				produceWeaponSystem(request, starbase);
				break;
		}
	}

	private void produceHull(StarbaseProductionRequest request, Starbase starbase) {
		String shipTemplateId = request.getTemplateId();
		List<ShipTemplate> ships = starbase.getPlanet().getPlayer().getRace().getShips();
		ShipTemplate shipTemplate = ships.stream().filter(s -> s.getId().equals(shipTemplateId)).findFirst().get();

		Optional<StarbaseHullStock> optional = starbaseHullStockRepository.findByShipTemplateIdAndStarbaseId(shipTemplateId, starbase.getId());
		StarbaseHullStock stock = optional.isPresent() ? optional.get() : new StarbaseHullStock();

		stock.setShipTemplate(shipTemplate);
		stock.setStarbase(starbase);
		stock.setStock(stock.getStock() + request.getQuantity());
		stock = starbaseHullStockRepository.save(stock);
		starbase.getHullStocks().add(stock);

		spendPlanetResources(starbase.getPlanet(), shipTemplate);
	}

	private void producePropulsionSystem(StarbaseProductionRequest request, Starbase starbase) {
		String templateId = request.getTemplateId();
		Optional<StarbasePropulsionStock> optional = starbasePropulsionStockRepository.findByPropulsionSystemTemplateNameAndStarbaseId(templateId, starbase.getId());
		StarbasePropulsionStock stock = optional.isPresent() ? optional.get() : new StarbasePropulsionStock();

		PropulsionSystemTemplate template = propulsionSystemTemplateRepository.getOne(templateId);
		stock.setPropulsionSystemTemplate(template);
		stock.setStarbase(starbase);
		stock.setStock(stock.getStock() + request.getQuantity());
		stock = starbasePropulsionStockRepository.save(stock);
		starbase.getPropulsionStocks().add(stock);

		spendPlanetResources(starbase.getPlanet(), template);
	}

	private void produceWeaponSystem(StarbaseProductionRequest request, Starbase starbase) {
		String templateId = request.getTemplateId();
		Optional<StarbaseWeaponStock> optional = starbaseWeaponStockRepository.findByWeaponTemplateNameAndStarbaseId(templateId, starbase.getId());
		StarbaseWeaponStock stock = optional.isPresent() ? optional.get() : new StarbaseWeaponStock();

		WeaponTemplate template = weaponTemplateRepository.getOne(templateId);
		stock.setWeaponTemplate(template);
		stock.setStarbase(starbase);
		stock.setStock(stock.getStock() + request.getQuantity());
		stock = starbaseWeaponStockRepository.save(stock);
		starbase.getWeaponStocks().add(stock);

		spendPlanetResources(starbase.getPlanet(), template);
	}

	private void spendPlanetResources(Planet planet, StarbaseProducable producable) {
		planet.spendMoney(producable.getCostMoney());
		planet.spendMineral1(producable.getCostMineral1());
		planet.spendMineral2(producable.getCostMineral2());
		planet.spendMineral3(producable.getCostMineral3());
	}

	@PreAuthorize("hasPermission(#starbaseId, 'starbase') and hasPermission(#starbaseId, 'turnNotDoneStarbase')")
	public boolean shipConstructionJobExists(long starbaseId) {
		Optional<StarbaseShipConstructionJob> existing = starbaseShipConstructionJobRepository.findByStarbaseId(starbaseId);
		return existing.isPresent();
	}

	@PreAuthorize("hasPermission(#starbaseId, 'starbase') and hasPermission(#starbaseId, 'turnNotDoneStarbase')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void addShipConstructionJob(long starbaseId, StarbaseShipConstructionRequest request) {
		addShipConstructionJobWithoutPermissionCheck(starbaseId, request);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void addShipConstructionJobWithoutPermissionCheck(long starbaseId, StarbaseShipConstructionRequest request) {
		if (shipConstructionJobExists(starbaseId)) {
			throw new IllegalArgumentException("Starbase " + starbaseId + " already has a ship construction job!");
		}

		Optional<StarbaseHullStock> hullResult = starbaseHullStockRepository.findById(request.getHullStockId());

		if (!hullResult.isPresent()) {
			throw new IllegalArgumentException("Hull stock " + request.getHullStockId() + " not found!");
		}

		StarbaseHullStock hullStock = hullResult.get();

		if (hullStock.getStock() <= 0) {
			throw new IllegalArgumentException("Hull stock " + request.getHullStockId() + " has no stock!");
		}

		ShipTemplate shipTemplate = hullStock.getShipTemplate();

		if (hullStock.getStarbase().getId() != starbaseId) {
			throw new IllegalArgumentException("Hull stock " + request.getHullStockId() + " does not belong to starbase " + starbaseId + "!");
		}

		hullStock.setStock(hullStock.getStock() - 1);

		if (hullStock.getStock() == 0) {
			starbaseHullStockRepository.delete(hullStock);
		}

		Optional<StarbasePropulsionStock> propulsionResult = starbasePropulsionStockRepository.findById(request.getPropulsionSystemStockId());

		if (!propulsionResult.isPresent()) {
			throw new IllegalArgumentException("Propulsion system stock " + request.getPropulsionSystemStockId() + " not found!");
		}

		StarbasePropulsionStock propulsionStock = propulsionResult.get();

		if (propulsionStock.getStock() < shipTemplate.getPropulsionSystemsCount()) {
			throw new IllegalArgumentException("Propulsion system stock " + request.getPropulsionSystemStockId() + " not enough for ship!");
		}

		if (propulsionStock.getStarbase().getId() != starbaseId) {
			throw new IllegalArgumentException("Propulsion system stock " + request.getPropulsionSystemStockId() + " does not belong to starbase " + starbaseId + "!");
		}

		propulsionStock.setStock(propulsionStock.getStock() - shipTemplate.getPropulsionSystemsCount());

		if (propulsionStock.getStock() == 0) {
			starbasePropulsionStockRepository.delete(propulsionStock);
		}

		StarbaseShipConstructionJob constructionJob = new StarbaseShipConstructionJob();
		constructionJob.setShipName(request.getName());
		constructionJob.setStarbase(starbaseRepository.getOne(starbaseId));
		constructionJob.setShipTemplate(shipTemplate);
		constructionJob.setPropulsionSystemTemplate(propulsionStock.getPropulsionSystemTemplate());

		if (shipTemplate.getEnergyWeaponsCount() > 0) {
			if (request.getEnergyStockId() == null) {
				throw new IllegalArgumentException("No energy weapon stock was given!");
			}

			Optional<StarbaseWeaponStock> energyResult = starbaseWeaponStockRepository.findById(request.getEnergyStockId());

			if (!energyResult.isPresent()) {
				throw new IllegalArgumentException("Energy weapon stock " + request.getEnergyStockId() + " not found!");
			}

			StarbaseWeaponStock energyStock = energyResult.get();

			if (energyStock.getStock() < shipTemplate.getEnergyWeaponsCount()) {
				throw new IllegalArgumentException("Energy weapon stock " + request.getEnergyStockId() + " not enough for ship!");
			}

			if (energyStock.getStarbase().getId() != starbaseId) {
				throw new IllegalArgumentException("Energy weapon stock " + request.getEnergyStockId() + " does not belong to starbase " + starbaseId + "!");
			}

			constructionJob.setEnergyWeaponTemplate(energyStock.getWeaponTemplate());

			energyStock.setStock(energyStock.getStock() - shipTemplate.getEnergyWeaponsCount());

			if (energyStock.getStock() == 0) {
				starbaseWeaponStockRepository.delete(energyStock);
			}
		}

		if (shipTemplate.getProjectileWeaponsCount() > 0) {
			if (request.getProjectileStockId() == null) {
				throw new IllegalArgumentException("No projectile weapon stock was given!");
			}

			Optional<StarbaseWeaponStock> projectileResult = starbaseWeaponStockRepository.findById(request.getProjectileStockId());

			if (!projectileResult.isPresent()) {
				throw new IllegalArgumentException("Projectile weapon stock " + request.getProjectileStockId() + " not found!");
			}

			StarbaseWeaponStock projectileStock = projectileResult.get();

			if (projectileStock.getStock() < shipTemplate.getProjectileWeaponsCount()) {
				throw new IllegalArgumentException("Projectile weapon stock " + request.getProjectileStockId() + " not enough for ship!");
			}

			if (projectileStock.getStarbase().getId() != starbaseId) {
				throw new IllegalArgumentException("Projectile weapon stock " + request.getProjectileStockId() + " does not belong to starbase " + starbaseId + "!");
			}

			constructionJob.setProjectileWeaponTemplate(projectileStock.getWeaponTemplate());

			projectileStock.setStock(projectileStock.getStock() - shipTemplate.getProjectileWeaponsCount());

			if (projectileStock.getStock() == 0) {
				starbaseWeaponStockRepository.delete(projectileStock);
			}
		}

		starbaseShipConstructionJobRepository.save(constructionJob);
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	public List<Starbase> getStarbasesOfLogin(long gameId, long loginId) {
		return starbaseRepository.findByGameIdAndLoginId(gameId, loginId);
	}

	@PreAuthorize("hasPermission(#starbaseId, 'starbase')")
	public String getLogbook(long starbaseId) {
		Starbase starbase = starbaseRepository.getOne(starbaseId);
		return starbase.getLog();
	}

	@PreAuthorize("hasPermission(#starbaseId, 'starbase')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void changeLogbook(long starbaseId, String logbook) {
		Starbase starbase = starbaseRepository.getOne(starbaseId);
		starbase.setLog(logbook);
		starbaseRepository.save(starbase);
	}
}