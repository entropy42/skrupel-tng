package org.entropy.skrupeltng.modules.ingame.controller;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import org.entropy.skrupeltng.modules.ingame.Coordinate;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.Ship;

public class PlanetEntry implements Serializable, Coordinate {

	private static final long serialVersionUID = -2215400594425014224L;

	private long id;
	private String name;
	private int x;
	private int y;
	private String type;

	private Long playerId;
	private String playerColor;

	private Long starbaseId;
	private String starbaseName;
	private String starbaseLog;

	private String shipPlayerColor;
	private List<Ship> ships;

	private int colonists;
	private int money;
	private int supplies;
	private int fuel;
	private int mineral1;
	private int mineral2;
	private int mineral3;
	private int untappedFuel;
	private int untappedMineral1;
	private int untappedMineral2;
	private int untappedMineral3;
	private int mines;
	private int factories;
	private boolean autoBuildMines;
	private boolean autoBuildFactories;
	private String log;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	@Override
	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(Long playerId) {
		this.playerId = playerId;
	}

	public String getPlayerColor() {
		return playerColor;
	}

	public void setPlayerColor(String playerColor) {
		this.playerColor = playerColor;
	}

	public Long getStarbaseId() {
		return starbaseId;
	}

	public void setStarbaseId(Long starbaseId) {
		this.starbaseId = starbaseId;
	}

	public String getStarbaseName() {
		return starbaseName;
	}

	public void setStarbaseName(String starbaseName) {
		this.starbaseName = starbaseName;
	}

	public String getStarbaseLog() {
		return starbaseLog;
	}

	public void setStarbaseLog(String starbaseLog) {
		this.starbaseLog = starbaseLog;
	}

	public String getShipPlayerColor() {
		return shipPlayerColor;
	}

	public void setShipPlayerColor(String shipPlayerColor) {
		this.shipPlayerColor = shipPlayerColor;
	}

	public List<Ship> getShips() {
		return ships;
	}

	public void setShips(List<Ship> ships) {
		this.ships = ships;
	}

	public int getColonists() {
		return colonists;
	}

	public void setColonists(int colonists) {
		this.colonists = colonists;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	public int getSupplies() {
		return supplies;
	}

	public void setSupplies(int supplies) {
		this.supplies = supplies;
	}

	public int getFuel() {
		return fuel;
	}

	public void setFuel(int fuel) {
		this.fuel = fuel;
	}

	public int getMineral1() {
		return mineral1;
	}

	public void setMineral1(int mineral1) {
		this.mineral1 = mineral1;
	}

	public int getMineral2() {
		return mineral2;
	}

	public void setMineral2(int mineral2) {
		this.mineral2 = mineral2;
	}

	public int getMineral3() {
		return mineral3;
	}

	public void setMineral3(int mineral3) {
		this.mineral3 = mineral3;
	}

	public int getUntappedFuel() {
		return untappedFuel;
	}

	public void setUntappedFuel(int untappedFuel) {
		this.untappedFuel = untappedFuel;
	}

	public int getUntappedMineral1() {
		return untappedMineral1;
	}

	public void setUntappedMineral1(int untappedMineral1) {
		this.untappedMineral1 = untappedMineral1;
	}

	public int getUntappedMineral2() {
		return untappedMineral2;
	}

	public void setUntappedMineral2(int untappedMineral2) {
		this.untappedMineral2 = untappedMineral2;
	}

	public int getUntappedMineral3() {
		return untappedMineral3;
	}

	public void setUntappedMineral3(int untappedMineral3) {
		this.untappedMineral3 = untappedMineral3;
	}

	public int getMines() {
		return mines;
	}

	public void setMines(int mines) {
		this.mines = mines;
	}

	public int getFactories() {
		return factories;
	}

	public void setFactories(int factories) {
		this.factories = factories;
	}

	public boolean isAutoBuildMines() {
		return autoBuildMines;
	}

	public void setAutoBuildMines(boolean autoBuildMines) {
		this.autoBuildMines = autoBuildMines;
	}

	public boolean isAutoBuildFactories() {
		return autoBuildFactories;
	}

	public void setAutoBuildFactories(boolean autoBuildFactories) {
		this.autoBuildFactories = autoBuildFactories;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public String retrieveShipNames() {
		List<String> names = ships.stream().map(Ship::getName).collect(Collectors.toList());
		return String.join(";", names);
	}
}