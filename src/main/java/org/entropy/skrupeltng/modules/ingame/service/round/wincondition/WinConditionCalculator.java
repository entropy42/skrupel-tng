package org.entropy.skrupeltng.modules.ingame.service.round.wincondition;

import java.util.Map;

import org.entropy.skrupeltng.modules.ingame.database.Game;
import org.entropy.skrupeltng.modules.ingame.database.GameRepository;
import org.entropy.skrupeltng.modules.ingame.database.WinCondition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class WinConditionCalculator {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private Map<String, WinConditionHandler> winConditionHandlers;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void checkWinCondition(long gameId) {
		log.debug("Processing win condition...");

		Game game = gameRepository.getOne(gameId);

		WinCondition winCondition = game.getWinCondition();
		WinConditionHandler handler = winConditionHandlers.get(winCondition.name());

		handler.checkWinCondition(gameId);

		log.debug("Win condition processed.");
	}
}