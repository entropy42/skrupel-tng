package org.entropy.skrupeltng.modules.ingame.modules.starbase.database;

public enum StarbaseType {

	SHIP_YARD(2, 328, 20, 21, 78, 210, 107),

	BATTLE_STATION(3, 522, 51, 83, 99, 250, 210),

	STAR_BASE(1, 855, 35, 70, 123, 418, 370),

	WAR_BASE(4, 3250, 270, 327, 520, 830, 920);

	private int typeId;
	private int costMoney;
	private int costSupplies;
	private int costFuel;
	private int costMineral1;
	private int costMineral2;
	private int costMineral3;

	private StarbaseType(int typeId, int costMoney, int costSupplies, int costFuel, int costMineral1, int costMineral2, int costMineral3) {
		this.typeId = typeId;
		this.costMoney = costMoney;
		this.costSupplies = costSupplies;
		this.costFuel = costFuel;
		this.costMineral1 = costMineral1;
		this.costMineral2 = costMineral2;
		this.costMineral3 = costMineral3;
	}

	public int getTypeId() {
		return typeId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	public int getCostMoney() {
		return costMoney;
	}

	public void setCostMoney(int costMoney) {
		this.costMoney = costMoney;
	}

	public int getCostSupplies() {
		return costSupplies;
	}

	public void setCostSupplies(int costSupplies) {
		this.costSupplies = costSupplies;
	}

	public int getCostFuel() {
		return costFuel;
	}

	public void setCostFuel(int costFuel) {
		this.costFuel = costFuel;
	}

	public int getCostMineral1() {
		return costMineral1;
	}

	public void setCostMineral1(int costMineral1) {
		this.costMineral1 = costMineral1;
	}

	public int getCostMineral2() {
		return costMineral2;
	}

	public void setCostMineral2(int costMineral2) {
		this.costMineral2 = costMineral2;
	}

	public int getCostMineral3() {
		return costMineral3;
	}

	public void setCostMineral3(int costMineral3) {
		this.costMineral3 = costMineral3;
	}
}