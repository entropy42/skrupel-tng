package org.entropy.skrupeltng.modules.ingame.service.round;

import java.util.List;

import org.entropy.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.entropy.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbaseShipConstructionJob;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbaseShipConstructionJobRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class StarbaseShipConstructionRoundCalculator {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private StarbaseShipConstructionJobRepository starbaseShipConstructionJobRepository;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private NewsService newsService;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processShipConstructionJobs(long gameId) {
		log.debug("Processing ship construction jobs...");

		List<StarbaseShipConstructionJob> jobs = starbaseShipConstructionJobRepository.findByGameId(gameId);

		for (StarbaseShipConstructionJob job : jobs) {
			Planet planet = job.getStarbase().getPlanet();

			Ship ship = new Ship();
			ship.setShield(100);
			ship.setCrew(job.getShipTemplate().getCrew());
			ship.setDestinationX(-1);
			ship.setDestinationY(-1);
			ship.setName(job.getShipName());
			ship.setPlayer(planet.getPlayer());
			ship.setShipTemplate(job.getShipTemplate());
			ship.setPropulsionSystemTemplate(job.getPropulsionSystemTemplate());
			ship.setEnergyWeaponTemplate(job.getEnergyWeaponTemplate());
			ship.setProjectileWeaponTemplate(job.getProjectileWeaponTemplate());
			ship.setX(planet.getX());
			ship.setY(planet.getY());
			ship.setPlanet(planet);

			ship = shipRepository.save(ship);

			newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_ship_constructed, ship.createFullImagePath(), ship.getName());
		}

		starbaseShipConstructionJobRepository.deleteAll(jobs);

		log.debug("Ship construction jobs processed.");
	}
}