package org.entropy.skrupeltng.modules.ingame.service.round.losecondition;

import org.entropy.skrupeltng.modules.ingame.database.Player;
import org.springframework.stereotype.Component;

@Component("LOSE_HOME_PLANET")
public class LoseHomePlanetLoseConditionHandler implements LoseConditionHandler {

	@Override
	public boolean hasLost(Player player) {
		Player homePlanetPlayer = player.getHomePlanet().getPlayer();
		return homePlanetPlayer == null || homePlanetPlayer.getId() != player.getId();
	}
}