package org.entropy.skrupeltng.modules.ingame.modules.ship.database;

import java.util.Comparator;

public class ShipStorageSpaceComparator implements Comparator<Ship> {

	@Override
	public int compare(Ship a, Ship b) {
		return Integer.compare(a.getShipTemplate().getStorageSpace(), b.getShipTemplate().getStorageSpace());
	}
}