package org.entropy.skrupeltng.modules.ingame.service.round.wincondition;

import java.util.List;

import org.entropy.skrupeltng.modules.ingame.database.Game;
import org.entropy.skrupeltng.modules.ingame.database.GameRepository;
import org.entropy.skrupeltng.modules.ingame.database.Player;
import org.entropy.skrupeltng.modules.ingame.database.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("SURVIVE")
public class SurviveWinConditionHandler implements WinConditionHandler {

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private GameRepository gameRepository;

	@Override
	public void checkWinCondition(long gameId) {
		List<Player> players = playerRepository.getNotLostPlayers(gameId);

		if (players.size() == 1) {
			Player winner = players.get(0);
			Game game = winner.getGame();
			game.setFinished(true);
			gameRepository.save(game);
		} else if (players.size() == 0) {
			// TODO
		}
	}
}