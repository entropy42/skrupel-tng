package org.entropy.skrupeltng.modules.ingame.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.entropy.skrupeltng.modules.RepositoryCustomBase;
import org.entropy.skrupeltng.modules.dashboard.GameListResult;
import org.entropy.skrupeltng.modules.dashboard.GameSearchParameters;
import org.entropy.skrupeltng.modules.ingame.CoordinateImpl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

public class GameRepositoryImpl extends RepositoryCustomBase implements GameRepositoryCustom {

	@Override
	public Page<GameListResult> searchGames(GameSearchParameters params, Pageable page) {
		long loginId = params.getCurrentLoginId();
		Boolean started = params.getStarted();
		Boolean onlyOwnGames = params.getOnlyOwnGames();

		String join = "LEFT OUTER";

		if (onlyOwnGames != null && onlyOwnGames) {
			join = "INNER";
		}

		String sql = "" +
				"SELECT \n" +
				"	g.id, \n" +
				"	g.name, \n" +
				"	g.created, \n" +
				"	g.player_count as \"playerCount\", \n" +
				"	g.started, \n" +
				"	g.finished, \n" +
				"	g.round, \n" +
				"	(p.id IS NOT NULL) as \"playerOfGame\" \n" +
				"FROM \n" +
				"	game g \n" +
				"	" + join + " JOIN player p \n" +
				"		ON p.game_id = g.id AND p.login_id = :loginId \n";

		Map<String, Object> parameters = new HashMap<>();
		parameters.put("loginId", loginId);

		List<String> wheres = new ArrayList<>();

		if (started != null) {
			wheres.add("g.started = :started");
			parameters.put("started", started);
		}

		if (!wheres.isEmpty()) {
			sql += "WHERE \n	" + StringUtils.join(wheres, " \n	AND ");
		}

		sql += " \nORDER BY \n	g.created DESC";

		RowMapper<GameListResult> rowMapper = new BeanPropertyRowMapper<>(GameListResult.class);
		return search(sql, parameters, page, rowMapper);
	}

	@Override
	public List<CoordinateImpl> getVisibilityCoordinates(long playerId) {
		String sql = "" +
				"SELECT \n" +
				"	p.x, \n" +
				"	p.y \n" +
				"FROM \n" +
				"	planet p \n" +
				"WHERE \n" +
				"	p.player_id = :playerId \n" +
				"" +
				"UNION \n" +
				"" +
				"SELECT \n" +
				"	s.x, \n" +
				"	s.y \n" +
				"FROM \n" +
				"	ship s \n" +
				"WHERE \n" +
				"	s.player_id = :playerId";

		Map<String, Object> params = new HashMap<>(1);
		params.put("playerId", playerId);

		RowMapper<CoordinateImpl> rowMapper = new BeanPropertyRowMapper<>(CoordinateImpl.class);
		return jdbcTemplate.query(sql, params, rowMapper);
	}
}