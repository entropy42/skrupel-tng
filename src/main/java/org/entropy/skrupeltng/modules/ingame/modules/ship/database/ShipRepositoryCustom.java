package org.entropy.skrupeltng.modules.ingame.modules.ship.database;

public interface ShipRepositoryCustom {

	boolean loginOwnsShip(long shipId, long loginId);
}