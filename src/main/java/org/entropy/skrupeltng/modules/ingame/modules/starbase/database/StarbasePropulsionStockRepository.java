package org.entropy.skrupeltng.modules.ingame.modules.starbase.database;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StarbasePropulsionStockRepository extends JpaRepository<StarbasePropulsionStock, Long> {

	Optional<StarbasePropulsionStock> findByPropulsionSystemTemplateNameAndStarbaseId(String templateName, long starbaseId);
}