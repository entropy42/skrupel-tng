package org.entropy.skrupeltng.modules.ingame.modules.starbase.database;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StarbaseHullStockRepository extends JpaRepository<StarbaseHullStock, Long> {

	Optional<StarbaseHullStock> findByShipTemplateIdAndStarbaseId(String templateId, long starbaseId);
}