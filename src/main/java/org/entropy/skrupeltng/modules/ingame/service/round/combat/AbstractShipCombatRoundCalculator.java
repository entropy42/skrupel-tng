package org.entropy.skrupeltng.modules.ingame.service.round.combat;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.entropy.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.entropy.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.springframework.beans.factory.annotation.Autowired;

public class AbstractShipCombatRoundCalculator {

	protected ShipRepository shipRepository;
	protected NewsService newsService;

	protected void deleteDestroyedShips(Set<ShipDestructionData> destroyedShips) {
		if (!destroyedShips.isEmpty()) {
			List<Long> shipIds = destroyedShips.stream().map(d -> d.getShip().getId()).collect(Collectors.toList());
			destroyedShips.stream().forEach(this::addDestroyedNewsEntry);
			shipRepository.clearDestinationShip(shipIds);
			shipRepository.deleteInBatch(destroyedShips.stream().map(ShipDestructionData::getShip).collect(Collectors.toList()));
		}
	}

	protected void addDestroyedNewsEntry(ShipDestructionData data) {
		Ship ship = data.getShip();
		String planetName = data.getDestroyingPlanetName();

		boolean byPlanet = planetName != null;
		String victimKey = byPlanet ? NewsEntryConstants.news_entry_destroyed_ship_by_planet : NewsEntryConstants.news_entry_destroyed_ship_by_ship;
		String destroyer = byPlanet ? planetName : data.getDestroyingShipName();
		newsService.add(ship.getPlayer(), victimKey, ship.createFullImagePath(), ship.getName(), destroyer);

		String destroyerKey = byPlanet ? NewsEntryConstants.news_entry_planet_defended_enemy_ship : NewsEntryConstants.news_entry_ship_destroyed_enemy_ship;
		newsService.add(data.getDestroyingPlayer(), destroyerKey, ship.createFullImagePath(), destroyer, ship.getName());
	}

	@Autowired
	public void setShipRepository(ShipRepository shipRepository) {
		this.shipRepository = shipRepository;
	}

	@Autowired
	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}
}