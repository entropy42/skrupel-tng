package org.entropy.skrupeltng.modules.ingame.modules.overview.service;

import java.io.Serializable;
import java.util.Date;

public class RoundSummary implements Serializable {

	private static final long serialVersionUID = -3464234632954604032L;

	private int round;
	private Date date;
	private String winCondition;
	private int newColonies;
	private int newShips;
	private int newStarbases;
	private int destroyedShipsByPlanets;
	private int destroyedShipsByShips;
	private int conqueredColonies;
	private int lostShips;

	public int getRound() {
		return round;
	}

	public void setRound(int round) {
		this.round = round;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getWinCondition() {
		return winCondition;
	}

	public void setWinCondition(String winCondition) {
		this.winCondition = winCondition;
	}

	public int getNewColonies() {
		return newColonies;
	}

	public void setNewColonies(int newColonies) {
		this.newColonies = newColonies;
	}

	public int getNewShips() {
		return newShips;
	}

	public void setNewShips(int newShips) {
		this.newShips = newShips;
	}

	public int getNewStarbases() {
		return newStarbases;
	}

	public void setNewStarbases(int newStarbases) {
		this.newStarbases = newStarbases;
	}

	public int getDestroyedShipsByPlanets() {
		return destroyedShipsByPlanets;
	}

	public void setDestroyedShipsByPlanets(int destroyedShipsByPlanets) {
		this.destroyedShipsByPlanets = destroyedShipsByPlanets;
	}

	public int getDestroyedShipsByShips() {
		return destroyedShipsByShips;
	}

	public void setDestroyedShipsByShips(int destroyedShipsByShips) {
		this.destroyedShipsByShips = destroyedShipsByShips;
	}

	public int getConqueredColonies() {
		return conqueredColonies;
	}

	public void setConqueredColonies(int conqueredColonies) {
		this.conqueredColonies = conqueredColonies;
	}

	public int getLostShips() {
		return lostShips;
	}

	public void setLostShips(int lostShips) {
		this.lostShips = lostShips;
	}
}