package org.entropy.skrupeltng.modules.ingame.controller;

import java.util.ArrayList;
import java.util.List;

import org.entropy.skrupeltng.modules.ingame.CoordinateImpl;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.Ship;

public class ShipCluster extends CoordinateImpl {

	private List<Ship> ships;

	public ShipCluster(int x, int y) {
		super(x, y);
		ships = new ArrayList<>();
	}

	public List<Ship> getShips() {
		return ships;
	}

	public void setShips(List<Ship> ships) {
		this.ships = ships;
	}
}