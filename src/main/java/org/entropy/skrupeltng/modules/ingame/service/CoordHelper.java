package org.entropy.skrupeltng.modules.ingame.service;

import org.entropy.skrupeltng.modules.ingame.Coordinate;

public class CoordHelper {

	public static double getDistance(Coordinate a, Coordinate b) {
		int diffX = a.getX() - b.getX();
		int diffY = a.getY() - b.getY();
		return getDistance(diffX, diffY);
	}

	public static double getDistance(int x1, int y1, int x2, int y2) {
		int diffX = x1 - x2;
		int diffY = y1 - y2;
		return getDistance(diffX, diffY);
	}

	public static double getDistance(int diffX, int diffY) {
		double distance = Math.sqrt((diffX * diffX) + (diffY * diffY));
		return distance;
	}
}