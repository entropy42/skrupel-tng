package org.entropy.skrupeltng.modules.ingame.modules.ship.database;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.entropy.skrupeltng.modules.ingame.Coordinate;
import org.entropy.skrupeltng.modules.ingame.database.Player;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.entropy.skrupeltng.modules.ingame.modules.ship.GoodsContainer;
import org.entropy.skrupeltng.modules.ingame.modules.ship.TotalGoodCalculator;
import org.entropy.skrupeltng.modules.masterdata.database.PropulsionSystemTemplate;
import org.entropy.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.entropy.skrupeltng.modules.masterdata.database.WeaponTemplate;

@Entity
@Table(name = "ship")
public class Ship implements Serializable, Coordinate, GoodsContainer {

	private static final int MAX_INFO_HEIGHT = 75;

	private static final long serialVersionUID = -3929826340145908777L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String name;

	private int x;

	private int y;

	@ManyToOne(fetch = FetchType.LAZY)
	private Player player;

	@ManyToOne(fetch = FetchType.LAZY)
	private Planet planet;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ship_template_id")
	private ShipTemplate shipTemplate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "propulsion_system_template_name")
	private PropulsionSystemTemplate propulsionSystemTemplate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "energy_weapon_template_name")
	private WeaponTemplate energyWeaponTemplate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "projectile_weapon_template_name")
	private WeaponTemplate projectileWeaponTemplate;

	private int aggressiveness;

	private int damage;

	private int shield;

	private int crew;

	private int fuel;

	private int colonists;

	private int money;

	private int mineral1;

	private int mineral2;

	private int mineral3;

	private int supplies;

	@Column(name = "light_ground_units")
	private int lightGroundUnits;

	@Column(name = "heavy_ground_units")
	private int heavyGroundUnits;

	private int projectiles;

	private int experience;

	@Column(name = "destination_x")
	private int destinationX;

	@Column(name = "destination_y")
	private int destinationY;

	@Column(name = "last_x")
	private int lastX;

	@Column(name = "last_y")
	private int lastY;

	@Column(name = "travel_speed")
	private int travelSpeed;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "destination_ship_id")
	private Ship destinationShip;

	private String log;

	@Override
	public String toString() {
		return "Ship [id=" + id + ", name=" + name + ", x=" + x + ", y=" + y + "]";
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	@Override
	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Planet getPlanet() {
		return planet;
	}

	public void setPlanet(Planet planet) {
		this.planet = planet;
	}

	public ShipTemplate getShipTemplate() {
		return shipTemplate;
	}

	public void setShipTemplate(ShipTemplate shipTemplate) {
		this.shipTemplate = shipTemplate;
	}

	public PropulsionSystemTemplate getPropulsionSystemTemplate() {
		return propulsionSystemTemplate;
	}

	public void setPropulsionSystemTemplate(PropulsionSystemTemplate propulsionSystemTemplate) {
		this.propulsionSystemTemplate = propulsionSystemTemplate;
	}

	public WeaponTemplate getEnergyWeaponTemplate() {
		return energyWeaponTemplate;
	}

	public void setEnergyWeaponTemplate(WeaponTemplate energyWeaponTemplate) {
		this.energyWeaponTemplate = energyWeaponTemplate;
	}

	public WeaponTemplate getProjectileWeaponTemplate() {
		return projectileWeaponTemplate;
	}

	public void setProjectileWeaponTemplate(WeaponTemplate projectileWeaponTemplate) {
		this.projectileWeaponTemplate = projectileWeaponTemplate;
	}

	public int getAggressiveness() {
		return aggressiveness;
	}

	public void setAggressiveness(int aggressiveness) {
		this.aggressiveness = aggressiveness;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public int getShield() {
		return shield;
	}

	public void setShield(int shield) {
		this.shield = shield;
	}

	public int getCrew() {
		return crew;
	}

	public void setCrew(int crew) {
		this.crew = crew;
	}

	public int getFuel() {
		return fuel;
	}

	public void setFuel(int fuel) {
		this.fuel = fuel;
	}

	@Override
	public int getColonists() {
		return colonists;
	}

	public void setColonists(int colonists) {
		this.colonists = colonists;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	@Override
	public int getMineral1() {
		return mineral1;
	}

	public void setMineral1(int mineral1) {
		this.mineral1 = mineral1;
	}

	@Override
	public int getMineral2() {
		return mineral2;
	}

	public void setMineral2(int mineral2) {
		this.mineral2 = mineral2;
	}

	@Override
	public int getMineral3() {
		return mineral3;
	}

	public void setMineral3(int mineral3) {
		this.mineral3 = mineral3;
	}

	@Override
	public int getSupplies() {
		return supplies;
	}

	public void setSupplies(int supplies) {
		this.supplies = supplies;
	}

	@Override
	public int getLightGroundUnits() {
		return lightGroundUnits;
	}

	public void setLightGroundUnits(int lightGroundUnits) {
		this.lightGroundUnits = lightGroundUnits;
	}

	@Override
	public int getHeavyGroundUnits() {
		return heavyGroundUnits;
	}

	public void setHeavyGroundUnits(int heavyGroundUnits) {
		this.heavyGroundUnits = heavyGroundUnits;
	}

	public int getProjectiles() {
		return projectiles;
	}

	public void setProjectiles(int projectiles) {
		this.projectiles = projectiles;
	}

	public int getExperience() {
		return experience;
	}

	public void setExperience(int experience) {
		this.experience = experience;
	}

	public int getDestinationX() {
		return destinationX;
	}

	public void setDestinationX(int destinationX) {
		this.destinationX = destinationX;
	}

	public int getDestinationY() {
		return destinationY;
	}

	public void setDestinationY(int destinationY) {
		this.destinationY = destinationY;
	}

	public int getTravelSpeed() {
		return travelSpeed;
	}

	public void setTravelSpeed(int travelSpeed) {
		this.travelSpeed = travelSpeed;
	}

	public Ship getDestinationShip() {
		return destinationShip;
	}

	public void setDestinationShip(Ship destinationShip) {
		this.destinationShip = destinationShip;
	}

	public int getLastX() {
		return lastX;
	}

	public void setLastX(int lastX) {
		this.lastX = lastX;
	}

	public int getLastY() {
		return lastY;
	}

	public void setLastY(int lastY) {
		this.lastY = lastY;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public String createFullImagePath() {
		return "/races/" + shipTemplate.getRace().getFolderName() + "/bilder_schiffe/" + shipTemplate.getImage();
	}

	public void receiveDamage(int damage) {
		receiveDamage(damage, 0);
	}

	public void receiveDamage(int damage, float damageToCrew) {
		if (damage <= 0) {
			return;
		}

		if (shield > 0) {
			int factor = (80 / shipTemplate.getMass()) + 1;
			shield -= damage * factor;

			if (shield < 0) {
				shield = 0;
			}
		} else {
			int factor = (80 / shipTemplate.getMass()) + 1;
			this.damage += damage * (factor * factor + 2);

			if (this.damage > 100) {
				this.damage = 100;
			} else if (damageToCrew > 0) {
				float crewDamage = damageToCrew * (factor * factor + 2);
				crew -= Math.floor(shipTemplate.getCrew() * crewDamage / 100);

				if (crew < 0) {
					crew = 0;
				}
			}
		}
	}

	public void spendProjectile() {
		projectiles--;
	}

	public int retrieveTotalGoods() {
		return TotalGoodCalculator.retrieveSum(this);
	}

	public int retrieveScannerDistance() {
		return 47;
	}

	public int retrieveShieldInfoHeight() {
		return (int)((shield / 100f) * MAX_INFO_HEIGHT);
	}

	public int retrieveDamageInfoHeight() {
		return (int)(((100f - damage) / 100) * MAX_INFO_HEIGHT);
	}

	public int retrieveFuelInfoHeight() {
		float fuelCapacity = shipTemplate.getFuelCapacity();
		return (int)((fuel / fuelCapacity) * MAX_INFO_HEIGHT);
	}

	public int retrieveCargoInfoHeight() {
		float storageSpace = shipTemplate.getStorageSpace();
		return (int)((retrieveTotalGoods() / storageSpace) * MAX_INFO_HEIGHT);
	}
}