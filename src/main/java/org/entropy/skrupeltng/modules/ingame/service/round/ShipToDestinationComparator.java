package org.entropy.skrupeltng.modules.ingame.service.round;

import java.util.Comparator;

import org.entropy.skrupeltng.modules.ingame.CoordinateImpl;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.entropy.skrupeltng.modules.ingame.service.CoordHelper;

public class ShipToDestinationComparator implements Comparator<Ship> {

	@Override
	public int compare(Ship o1, Ship o2) {
		double d1 = CoordHelper.getDistance(o1, new CoordinateImpl(o1.getDestinationX(), o1.getDestinationY()));
		double d2 = CoordHelper.getDistance(o2, new CoordinateImpl(o2.getDestinationX(), o2.getDestinationY()));
		return Double.compare(d1, d2);
	}
}