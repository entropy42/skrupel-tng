package org.entropy.skrupeltng.modules.ingame.service.round;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections4.ComparatorUtils;
import org.entropy.skrupeltng.modules.ingame.modules.overview.NewsEntryConstants;
import org.entropy.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.entropy.skrupeltng.modules.ingame.service.CoordHelper;
import org.entropy.skrupeltng.modules.masterdata.service.MasterDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class ShipRoundCalculator {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private MasterDataService masterDataService;

	@Autowired
	private NewsService newsService;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processShips(long gameId) {
		processMovingShips(gameId);
	}

	private void processMovingShips(long gameId) {
		log.debug("Processing moving ships...");

		List<Ship> ships = shipRepository.getMovingShips(gameId);

		Collections.sort(ships, ComparatorUtils.reversedComparator(new ShipToDestinationComparator()));

		for (Ship ship : ships) {
			int travelSpeed = ship.getTravelSpeed();
			int x = ship.getX();
			int y = ship.getY();
			int destinationX = ship.getDestinationX();
			int destinationY = ship.getDestinationY();
			Ship destinationShip = ship.getDestinationShip();

			if (destinationShip != null) {
				destinationX = destinationShip.getX();
				destinationY = destinationShip.getY();
				ship.setDestinationX(destinationX);
				ship.setDestinationY(destinationY);
			}

			double distance = CoordHelper.getDistance(x, y, destinationX, destinationY);
			double duration = masterDataService.calculateTravelDuration(travelSpeed, distance);

			int fuelConsumption = masterDataService.calculateFuelConsumptionPerMonth(ship, travelSpeed, distance, duration);

			if (fuelConsumption > ship.getFuel()) {
				// TODO
			} else {
				ship.setLastX(ship.getX());
				ship.setLastY(ship.getY());

				if (duration <= 1d) {
					ship.setX(destinationX);
					ship.setY(destinationY);

					if (destinationShip == null) {
						ship.setDestinationX(-1);
						ship.setDestinationY(-1);
						ship.setTravelSpeed(0);
					}
				} else {
					ship.setX(x + (int)Math.floor((destinationX - x) / duration));
					ship.setY(y + (int)Math.floor((destinationY - y) / duration));

					if (destinationShip != null && destinationShip.getTravelSpeed() == travelSpeed) {
						double dist = CoordHelper.getDistance(ship, destinationShip);

						if (dist <= 1d) {
							ship.setX(destinationX);
							ship.setY(destinationY);
						}
					}
				}

				ship.setPlanet(null);
				ship.setFuel(ship.getFuel() - fuelConsumption);
			}

			Optional<Planet> newPlanet = planetRepository.findByGameIdAndXAndY(gameId, ship.getX(), ship.getY());

			if (newPlanet.isPresent()) {
				Planet planet = newPlanet.get();
				ship.setPlanet(planet);

				if (planet.getPlayer() != null && planet.getPlayer().getId() != ship.getPlayer().getId()) {
					newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_ship_arrived_at_enemy_planet, ship.createFullImagePath(), ship.getName(), planet.getName());
				} else {
					newsService.add(ship.getPlayer(), NewsEntryConstants.news_entry_ship_arrived, ship.createFullImagePath(), ship.getName());
				}
			}

			shipRepository.save(ship);
		}

		log.debug("Moving ships processed.");
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void updateDestinations(long gameId) {
		log.debug("Updating ship destinations...");

		List<Ship> ships = shipRepository.getMovingShips(gameId);

		for (Ship ship : ships) {
			Ship destinationShip = ship.getDestinationShip();

			if (destinationShip != null) {
				ship.setDestinationX(destinationShip.getX());
				ship.setDestinationY(destinationShip.getY());
			}
		}

		log.debug("Ship destinations updated.");
	}
}