package org.entropy.skrupeltng.modules.ingame.modules.planet.database;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PlanetRepository extends JpaRepository<Planet, Long>, PlanetRepositoryCustom {

	@Query("SELECT p FROM Planet p WHERE p.game.id = ?1 AND p.newColonists > 0 AND p.newPlayer IS NOT NULL")
	List<Planet> findNewColonies(long gameId);

	@Query("SELECT p FROM Planet p WHERE p.game.id = ?1 AND p.player IS NOT NULL")
	List<Planet> findOwnedPlanetsByGameId(long gameId);

	Optional<Planet> findByGameIdAndXAndY(long gameId, int x, int y);

	@Query("SELECT " +
			"	p " +
			"FROM " +
			"	Planet p " +
			"	INNER JOIN p.player o " +
			"WHERE " +
			"	o.game.id = ?1 " +
			"	AND o.login.id = ?2")
	List<Planet> findByGameIdAndLoginId(long gameId, long loginId);

	List<Planet> findByGameId(long gameId);

	List<Planet> findByPlayerId(long playerId);

	@Query("SELECT COUNT(pl) FROM Planet pl WHERE pl.player.id = ?1")
	long getPlanetCountByPlayerId(long playerId);
}