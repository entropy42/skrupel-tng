package org.entropy.skrupeltng.modules.ingame.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public interface GameRepository extends JpaRepository<Game, Long>, GameRepositoryCustom {

	@Modifying
	@Query("UPDATE Game g SET g.round = round + 1 WHERE g.id = ?1")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	void increaseRound(long gameId);
}