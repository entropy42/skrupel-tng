package org.entropy.skrupeltng.modules.ingame.modules.ship.database;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.entropy.skrupeltng.modules.RepositoryCustomBase;

public class ShipRepositoryImpl extends RepositoryCustomBase implements ShipRepositoryCustom {

	@Override
	public boolean loginOwnsShip(long shipId, long loginId) {
		String sql = "" +
				"SELECT \n" +
				"	p.login_id \n" +
				"FROM \n" +
				"	ship s \n" +
				"	INNER JOIN player p \n" +
				"		ON p.id = s.player_id \n" +
				"WHERE \n" +
				"	s.id = :shipId";

		Map<String, Object> params = new HashMap<>(1);
		params.put("shipId", shipId);

		List<Long> results = jdbcTemplate.queryForList(sql, params, Long.class);
		return results.size() == 1 && results.get(0).longValue() == loginId;
	}
}