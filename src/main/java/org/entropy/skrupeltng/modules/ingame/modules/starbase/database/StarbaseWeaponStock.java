package org.entropy.skrupeltng.modules.ingame.modules.starbase.database;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.entropy.skrupeltng.modules.masterdata.database.WeaponTemplate;

@Entity
@Table(name = "starbase_weapon_stock")
public class StarbaseWeaponStock implements Serializable {

	private static final long serialVersionUID = 3456728125969702951L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private Starbase starbase;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "weapon_template_name")
	private WeaponTemplate weaponTemplate;

	private int stock;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Starbase getStarbase() {
		return starbase;
	}

	public void setStarbase(Starbase starbase) {
		this.starbase = starbase;
	}

	public WeaponTemplate getWeaponTemplate() {
		return weaponTemplate;
	}

	public void setWeaponTemplate(WeaponTemplate weaponTemplate) {
		this.weaponTemplate = weaponTemplate;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}
}