package org.entropy.skrupeltng.modules.ingame.modules.starbase.database;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.entropy.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.entropy.skrupeltng.modules.masterdata.service.StarbaseUpgradeType;

@Entity
@Table(name = "starbase")
public class Starbase implements Serializable {

	private static final long serialVersionUID = -7737821269504592068L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "starbase")
	private Planet planet;

	private String name;

	private String log;

	@Enumerated(EnumType.STRING)
	private StarbaseType type;

	private int defense;

	@Column(name = "hull_level")
	private int hullLevel;

	@Column(name = "propulsion_level")
	private int propulsionLevel;

	@Column(name = "energy_level")
	private int energyLevel;

	@Column(name = "projectile_level")
	private int projectileLevel;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "starbase")
	private List<StarbaseHullStock> hullStocks;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "starbase")
	private List<StarbasePropulsionStock> propulsionStocks;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "starbase")
	private List<StarbaseWeaponStock> weaponStocks;

	@Override
	public String toString() {
		return "Starbase [id=" + id + ", planet=" + planet + ", name=" + name + "]";
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Planet getPlanet() {
		return planet;
	}

	public void setPlanet(Planet planet) {
		this.planet = planet;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public StarbaseType getType() {
		return type;
	}

	public void setType(StarbaseType type) {
		this.type = type;
	}

	public int getDefense() {
		return defense;
	}

	public void setDefense(int defense) {
		this.defense = defense;
	}

	public int getHullLevel() {
		return hullLevel;
	}

	public void setHullLevel(int hullLevel) {
		this.hullLevel = hullLevel;
	}

	public int getPropulsionLevel() {
		return propulsionLevel;
	}

	public void setPropulsionLevel(int propulsionLevel) {
		this.propulsionLevel = propulsionLevel;
	}

	public int getEnergyLevel() {
		return energyLevel;
	}

	public void setEnergyLevel(int energyLevel) {
		this.energyLevel = energyLevel;
	}

	public int getProjectileLevel() {
		return projectileLevel;
	}

	public void setProjectileLevel(int projectileLevel) {
		this.projectileLevel = projectileLevel;
	}

	public List<StarbaseHullStock> getHullStocks() {
		return hullStocks;
	}

	public void setHullStocks(List<StarbaseHullStock> hullStocks) {
		this.hullStocks = hullStocks;
	}

	public List<StarbasePropulsionStock> getPropulsionStocks() {
		return propulsionStocks;
	}

	public void setPropulsionStocks(List<StarbasePropulsionStock> propulsionStocks) {
		this.propulsionStocks = propulsionStocks;
	}

	public List<StarbaseWeaponStock> getWeaponStocks() {
		return weaponStocks;
	}

	public void setWeaponStocks(List<StarbaseWeaponStock> weaponStocks) {
		this.weaponStocks = weaponStocks;
	}

	public int retrieveTechLevel(StarbaseUpgradeType type) {
		switch (type) {
			case HULL:
				return hullLevel;
			case PROPULSION:
				return propulsionLevel;
			case ENERGY:
				return energyLevel;
			case PROJECTILE:
				return projectileLevel;
		}

		return 0;
	}

	public void upgrade(StarbaseUpgradeType type) {
		switch (type) {
			case HULL:
				hullLevel++;
			case PROPULSION:
				propulsionLevel++;
			case ENERGY:
				energyLevel++;
			case PROJECTILE:
				projectileLevel++;
		}
	}

	public String createFullImagePath() {
		return "/races/" + planet.getPlayer().getRace().getFolderName() + "/bilder_basen/" + type.getTypeId() + ".jpg";
	}
}