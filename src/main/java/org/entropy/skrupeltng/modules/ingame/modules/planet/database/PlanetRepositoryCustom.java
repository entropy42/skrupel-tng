package org.entropy.skrupeltng.modules.ingame.modules.planet.database;

import java.util.List;

import org.entropy.skrupeltng.modules.ingame.controller.PlanetEntry;

public interface PlanetRepositoryCustom {

	List<PlanetEntry> getPlanetsForGalaxy(long gameId);

	boolean loginOwnsPlanet(long planetId, long loginId);
}