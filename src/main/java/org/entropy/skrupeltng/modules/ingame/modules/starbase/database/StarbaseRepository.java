package org.entropy.skrupeltng.modules.ingame.modules.starbase.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface StarbaseRepository extends JpaRepository<Starbase, Long>, StarbaseRepositoryCustom {

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Starbase s " +
			"	INNER JOIN s.planet pl " +
			"	INNER JOIN pl.player p " +
			"WHERE " +
			"	p.game.id = ?1 " +
			"	AND p.login.id = ?2")
	List<Starbase> findByGameIdAndLoginId(long gameId, long loginId);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Starbase s " +
			"	INNER JOIN FETCH s.planet pl " +
			"WHERE " +
			"	pl.player.id = ?1")
	List<Starbase> findByPlayerId(long playerId);
}