package org.entropy.skrupeltng.modules.ingame.modules.ship.database;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface ShipRepository extends JpaRepository<Ship, Long>, ShipRepositoryCustom {

	List<Ship> findByPlayerId(long playerId);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN s.player p " +
			"	INNER JOIN s.planet pl " +
			"WHERE" +
			"	p.game.id = ?1 " +
			"	AND pl.id = ?2")
	List<Ship> findByGameIdAndPlanetId(long gameId, long planetId);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN s.player p " +
			"WHERE " +
			"	p.game.id = ?1 " +
			"	AND p.login.id = ?2")
	List<Ship> findShipsByGameIdAndLoginId(long gameId, long loginId);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN s.player p " +
			"WHERE " +
			"	p.game.id = ?1 " +
			"	AND p.login.id = ?2 " +
			"	AND s.x = ?3 " +
			"	AND s.y = ?4")
	List<Ship> findShipsByGameIdAndLoginIdAndCoordinates(long gameId, long loginId, int x, int y);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN s.player p " +
			"WHERE " +
			"	p.game.id = ?1")
	List<Ship> findByGameId(long gameId);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN s.player p " +
			"WHERE " +
			"	p.game.id = ?1 " +
			"	AND s.travelSpeed > 0")
	List<Ship> getMovingShips(long gameId);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN s.player p " +
			"WHERE " +
			"	p.id = ?1 " +
			"	AND s.travelSpeed = 0")
	List<Ship> getNotMovingShipsByPlayerId(long playerId);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"WHERE " +
			"	s.player.id = ?1 " +
			"	AND s.travelSpeed > 0")
	List<Ship> getMovingShipsByPlayerId(long playerId);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN FETCH s.player sp " +
			"	INNER JOIN FETCH s.planet pl " +
			"	INNER JOIN FETCH pl.player p " +
			"	INNER JOIN FETCH s.shipTemplate st " +
			"	LEFT OUTER JOIN FETCH s.energyWeaponTemplate ewt " +
			"	LEFT OUTER JOIN FETCH s.projectileWeaponTemplate pwt " +
			"WHERE " +
			"	p.game.id = ?1" +
			"	AND p.id != sp.id " +
			"ORDER BY" +
			"	s.aggressiveness DESC")
	List<Ship> getShipsOnEnemyPlanet(long gameId);

	@Modifying
	@Query("UPDATE Ship s SET s.destinationShip = null WHERE s.destinationShip.id IN (?1)")
	void clearDestinationShip(List<Long> shipIds);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN FETCH s.player p " +
			"	INNER JOIN FETCH s.shipTemplate st " +
			"	LEFT OUTER JOIN FETCH s.energyWeaponTemplate ewt " +
			"	LEFT OUTER JOIN FETCH s.projectileWeaponTemplate pwt " +
			"WHERE " +
			"	p.game.id = ?1 " +
			"	AND p.id != ?2 " +
			"	AND s.x = ?3 " +
			"	AND s.y = ?4 " +
			"ORDER BY" +
			"	s.aggressiveness DESC")
	List<Ship> findEnemyShipsOnSamePosition(long gameId, long playerId, int x, int y);

	@Query("SELECT " +
			"	s " +
			"FROM " +
			"	Ship s " +
			"	INNER JOIN s.player pl " +
			"WHERE " +
			"	pl.game.id = ?1 " +
			"	AND s.x = ?2 " +
			"	AND s.y = ?3 " +
			"	AND s.name = ?4")
	Optional<Ship> findByGameIdAndXAndYAndName(long id, int x, int y, String name);
}