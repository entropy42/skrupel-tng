package org.entropy.skrupeltng.modules.ingame.modules.ship.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.entropy.skrupeltng.modules.AbstractController;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.entropy.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.entropy.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/ingame/ship")
public class ShipController extends AbstractController {

	@Autowired
	private ShipService shipService;

	@Autowired
	private MasterDataService masterDataService;

	@GetMapping("")
	public String getShip(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);
		model.addAttribute("ship", ship);
		model.addAttribute("turnDone", turnDoneForShip(shipId));
		return "ingame/ship/ship::content";
	}

	@GetMapping("/navigation")
	public String getNavigation(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);

		if (ship.getTravelSpeed() <= 0) {
			ship.setTravelSpeed(ship.getPropulsionSystemTemplate().getWarpSpeed());
		}

		model.addAttribute("ship", ship);

		int propulsionLevel = ship.getPropulsionSystemTemplate().getTechLevel();
		float[] fuelConsumptionData = masterDataService.getFuelConsumptionData(propulsionLevel);
		String fuel = StringUtils.join(fuelConsumptionData, ',');
		model.addAttribute("fuelConsumptionData", fuel);

		String destinationName = shipService.getDestinationName(shipId);
		model.addAttribute("destinationName", destinationName);

		return "ingame/ship/navigation::content";
	}

	@PostMapping("/navigation")
	@ResponseBody
	public ShipCourseSelectionResponse setCourse(@RequestParam long shipId, @RequestBody ShipCourseSelectionRequest request) {
		return shipService.setCourse(shipId, request);
	}

	@DeleteMapping("/navigation")
	@ResponseBody
	public void deleteCourse(@RequestParam long shipId) {
		shipService.deleteCourse(shipId);
	}

	@GetMapping("/transporter")
	public String getTransporter(@RequestParam long shipId, Model model) {
		Ship ship = shipService.getShip(shipId);

		Planet planet = ship.getPlanet();

		if (planet != null) {
			model.addAttribute("ship", ship);

			boolean isOwnPlanet = planet.getPlayer() != null && planet.getPlayer().getId() == ship.getPlayer().getId();
			model.addAttribute("isOwnPlanet", isOwnPlanet);

			return "ingame/ship/transporter::content";
		}

		return "ingame/ship/transporter::cannot-transport";
	}

	@PostMapping("/transporter")
	public String transport(@RequestParam long shipId, @RequestBody ShipTransportRequest request) {
		shipService.transport(shipId, request);
		return "ingame/ship/transporter::transport-successfull";
	}

	@GetMapping("/scanner")
	public String getScannerOverview(@RequestParam long shipId, Model model) {
		List<Ship> ships = shipService.getScannedShips(shipId);
		List<Planet> planets = shipService.getScannedPlanets(shipId);

		if (ships.isEmpty() && planets.isEmpty()) {
			return "ingame/ship/scanner::no-objects";
		}

		model.addAttribute("ships", ships);
		model.addAttribute("planets", planets);
		return "ingame/ship/scanner::content";
	}

	@GetMapping("/scanner/ship")
	public String getScannedShipDetails(@RequestParam long shipId, @RequestParam long scannedShipId, Model model) {
		Ship ship = shipService.getScannedShip(shipId, scannedShipId);
		model.addAttribute("ship", ship);
		return "ingame/ship/scanner::ship";
	}

	@GetMapping("/scanner/planet")
	public String getScannedPlanetDetails(@RequestParam long shipId, @RequestParam long scannedPlanetId, Model model) {
		Planet planet = shipService.getScannedPlanet(shipId, scannedPlanetId);
		model.addAttribute("planet", planet);
		return "ingame/ship/scanner::planet";
	}

	@GetMapping("/logbook")
	public String getLogbook(@RequestParam long shipId, Model model) {
		model.addAttribute("logbook", shipService.getLogbook(shipId));
		return "ingame/logbook::content";
	}

	@PostMapping("/logbook")
	@ResponseBody
	public void changeLogbook(@RequestParam long shipId, @RequestParam String logbook) {
		shipService.changeLogbook(shipId, logbook);
	}
}