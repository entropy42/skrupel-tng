package org.entropy.skrupeltng.modules.ingame.service.round.combat;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.entropy.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class OrbitalCombatRoundCalculator extends AbstractShipCombatRoundCalculator {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private PlanetRepository planetRepository;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void processCombat(long gameId) {
		log.debug("Processing orbital combat...");

		List<Ship> ships = shipRepository.getShipsOnEnemyPlanet(gameId);
		Set<ShipDestructionData> destroyedShips = new HashSet<>(ships.size());

		for (Ship ship : ships) {
			processShip(destroyedShips, ship);
		}

		deleteDestroyedShips(destroyedShips);

		log.debug("Orbital combat processed.");
	}

	protected void processShip(Set<ShipDestructionData> destroyedShips, Ship ship) {
		Planet planet = ship.getPlanet();
		Starbase starbase = planet.getStarbase();
		boolean isBattleStation = starbase != null && starbase.getType() == StarbaseType.BATTLE_STATION;

		ShipDamage shipDamage = new ShipDamage(ship);

		int planetEnergyWeapons = planet.retrieveEnergyWeaponCount();
		int totalPlanetaryDefense = planet.retrieveTotalPlanetaryDefense();
		int planetHangarCapacity = planet.retrieveHangarCapacity();

		int planetEnergyDamage = planet.retrieveEnergyWeaponDamage();

		int damageToPlanet = 0;

		while (ship.getDamage() < 100 && damageToPlanet < totalPlanetaryDefense && (planet.getPlanetaryDefense() >= 1 || isBattleStation)) {
			for (int round = 1; round <= 10; round++) {
				if (ship.getDamage() < 100 && damageToPlanet < totalPlanetaryDefense) {
					damageToPlanet += shipDamage.calculateDamageToPlanet(round);

					if (planetEnergyWeapons >= round) {
						ship.receiveDamage(planetEnergyDamage);
					}

					if (planetHangarCapacity >= round) {
						ship.receiveDamage(ShipDamage.HANGAR_DAMAGE);
					}
				}
			}
		}

		if (ship.getDamage() >= 100) {
			ShipDestructionData destructionData = new ShipDestructionData();
			destructionData.setShip(ship);
			destructionData.setDestroyingPlanetName(planet.getName());
			destructionData.setDestroyingPlayer(planet.getPlayer());
			destroyedShips.add(destructionData);
		} else {
			shipRepository.save(ship);
		}

		if ((ship.getDamage() < 100 && damageToPlanet >= totalPlanetaryDefense) || planet.getPlanetaryDefense() == 0) {
			planet.setPlayer(ship.getPlayer());
		}

		planetRepository.save(planet);
	}

	@Autowired
	public void setPlanetRepository(PlanetRepository planetRepository) {
		this.planetRepository = planetRepository;
	}
}