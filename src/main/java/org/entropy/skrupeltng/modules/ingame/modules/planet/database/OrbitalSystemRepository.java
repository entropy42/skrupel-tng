package org.entropy.skrupeltng.modules.ingame.modules.planet.database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

public interface OrbitalSystemRepository extends JpaRepository<OrbitalSystem, Long> {

	@Modifying
	void deleteByPlanetId(long planetId);
}