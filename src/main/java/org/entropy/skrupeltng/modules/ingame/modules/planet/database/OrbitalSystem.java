package org.entropy.skrupeltng.modules.ingame.modules.planet.database;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "orbital_system")
public class OrbitalSystem implements Serializable {

	private static final long serialVersionUID = -5005616637575582935L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	private Planet planet;

	private String type;

	public OrbitalSystem() {

	}

	public OrbitalSystem(Planet planet) {
		this.planet = planet;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Planet getPlanet() {
		return planet;
	}

	public void setPlanet(Planet planet) {
		this.planet = planet;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}