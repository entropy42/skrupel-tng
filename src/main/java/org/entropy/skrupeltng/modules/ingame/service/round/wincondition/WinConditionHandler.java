package org.entropy.skrupeltng.modules.ingame.service.round.wincondition;

public interface WinConditionHandler {

	void checkWinCondition(long gameId);
}