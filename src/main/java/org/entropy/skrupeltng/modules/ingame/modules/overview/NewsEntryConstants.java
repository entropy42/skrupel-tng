package org.entropy.skrupeltng.modules.ingame.modules.overview;

public interface NewsEntryConstants {

	String news_entry_ship_constructed = "news_entry_ship_constructed";
	String news_entry_ship_arrived = "news_entry_ship_arrived";
	String news_entry_ship_arrived_at_enemy_planet = "news_entry_ship_arrived_at_enemy_planet";
	String news_entry_starbase_constructed = "news_entry_starbase_constructed";
	String news_entry_new_colony = "news_entry_new_colony";
	String news_entry_conquered_colony = "news_entry_conquered_colony";
	String news_entry_destroyed_ship_by_ship = "news_entry_destroyed_ship_by_ship";
	String news_entry_destroyed_ship_by_planet = "news_entry_destroyed_ship_by_planet";
	String news_entry_ship_destroyed_enemy_ship = "news_entry_ship_destroyed_enemy_ship";
	String news_entry_planet_defended_enemy_ship = "news_entry_planet_defended_enemy_ship";
}