package org.entropy.skrupeltng.modules.ingame.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.entropy.skrupeltng.exceptions.ResourceNotFoundException;
import org.entropy.skrupeltng.modules.AbstractController;
import org.entropy.skrupeltng.modules.ingame.CoordinateImpl;
import org.entropy.skrupeltng.modules.ingame.database.Game;
import org.entropy.skrupeltng.modules.ingame.database.Player;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.entropy.skrupeltng.modules.ingame.modules.planet.service.PlanetService;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.entropy.skrupeltng.modules.ingame.modules.ship.service.ShipService;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.service.StarbaseService;
import org.entropy.skrupeltng.modules.ingame.service.IngameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/ingame")
public class IngameController extends AbstractController {

	@Autowired
	private IngameService ingameService;

	@Autowired
	private PlanetService planetService;

	@Autowired
	private StarbaseService starbaseService;

	@Autowired
	private ShipService shipService;

	@Value("${skr.disablePermissionChecks:false}")
	private boolean disablePermissionChecks;

	@GetMapping("/game")
	public String game(@RequestParam(required = true) long id, Model model) {
		Optional<Game> optional = ingameService.getGame(id);

		if (optional.isPresent()) {
			Game game = optional.get();
			long loginId = userDetailService.getLoginId();
			long playerId = ingameService.getPlayerId(loginId, id);
			Player player = game.getPlayers().stream().filter(p -> p.getLogin().getId() == loginId).findFirst().get();

			if (player.isHasLost() || game.isFinished()) {
				return "redirect:/dashboard/game?id=" + id;
			}

			model.addAttribute("game", game);
			model.addAttribute("playerId", playerId);

			model.addAttribute("playerColor", player.getColor());
			model.addAttribute("overviewViewed", player.isOverviewViewed());

			List<CoordinateImpl> visibilityCoordinates = ingameService.getVisibilityCoordinates(id, loginId);
			model.addAttribute("visibilityCoordinates", visibilityCoordinates);

			List<PlanetEntry> planets = ingameService.getPlanets(id, loginId, visibilityCoordinates);
			model.addAttribute("planets", planets);

			List<Ship> ships = ingameService.getShips(id, loginId, visibilityCoordinates);
			model.addAttribute("ships", ships);

			model.addAttribute("disablePermissionChecks", disablePermissionChecks);

			Map<String, ShipCluster> shipClusters = new HashMap<>();

			for (Ship ship : ships) {
				if (ship.getPlanet() == null) {
					String coord = ship.getX() + "_" + ship.getY();
					ShipCluster shipCluster = shipClusters.get(coord);

					if (shipCluster == null) {
						shipCluster = new ShipCluster(ship.getX(), ship.getY());
						shipClusters.put(coord, shipCluster);
					}

					shipCluster.getShips().add(ship);
				}
			}

			List<ShipCluster> clusters = shipClusters.values().stream().filter(c -> c.getShips().size() > 1).collect(Collectors.toList());
			model.addAttribute("shipClusters", clusters);

			List<Ship> singleShips = shipClusters.values().stream().filter(c -> c.getShips().size() == 1).flatMap(c -> c.getShips().stream()).collect(Collectors.toList());
			model.addAttribute("singleShips", singleShips);

			return "ingame/ingame";
		}

		throw new ResourceNotFoundException();
	}

	@PostMapping("/overview-viewed")
	@ResponseBody
	public void overviewViewed(@RequestParam(required = true) long gameId) {
		ingameService.overviewViewed(gameId, userDetailService.getLoginId());
	}

	@PostMapping("/turn")
	@ResponseBody
	public boolean finishTurn(@RequestParam(required = true) long gameId) {
		return ingameService.finishTurn(gameId, userDetailService.getLoginId());
	}

	@PostMapping("/round")
	@ResponseBody
	public void finishRound(@RequestParam(required = true) long gameId) {
		ingameService.calculateRound(gameId);
	}

	@GetMapping("/planet-selection")
	public String showColonies(@RequestParam long gameId, Model model) {
		List<Planet> planets = planetService.getPlanetsOfLogin(gameId, userDetailService.getLoginId());
		model.addAttribute("planets", planets);
		return "ingame/planet-selection";
	}

	@GetMapping("/starbase-selection")
	public String showStarbases(@RequestParam long gameId, Model model) {
		List<Starbase> starbases = starbaseService.getStarbasesOfLogin(gameId, userDetailService.getLoginId());
		model.addAttribute("starbases", starbases);
		return "ingame/starbase-selection";
	}

	@GetMapping("/ship-selection")
	public String showShips(@RequestParam long gameId, @RequestParam(required = false) Integer x, @RequestParam(required = false) Integer y, Model model) {
		List<Ship> ships = shipService.getShipsOfLogin(gameId, userDetailService.getLoginId(), x, y);
		model.addAttribute("ships", ships);
		return "ingame/ship-selection";
	}
}