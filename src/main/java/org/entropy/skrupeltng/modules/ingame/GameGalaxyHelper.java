package org.entropy.skrupeltng.modules.ingame;

import org.springframework.stereotype.Component;

@Component("galaxyHelper")
public class GameGalaxyHelper {

	public String getCoordinatesStyle(int x, int y) {
		x *= getZoom();
		y *= getZoom();

		return "left: " + x + "px; top: " + y + "px;";
	}

	public String getGalaxySizeStyle(int size) {
		size *= getZoom();

		return "width: " + size + "px; height: " + size + "px;";
	}

	public int getZoom() {
		return 1;
	}
}