package org.entropy.skrupeltng.modules.ingame.modules.ship.service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.entropy.skrupeltng.modules.ingame.database.Game;
import org.entropy.skrupeltng.modules.ingame.database.Player;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.entropy.skrupeltng.modules.ingame.modules.ship.TotalGoodCalculator;
import org.entropy.skrupeltng.modules.ingame.modules.ship.controller.ShipCourseSelectionRequest;
import org.entropy.skrupeltng.modules.ingame.modules.ship.controller.ShipCourseSelectionResponse;
import org.entropy.skrupeltng.modules.ingame.modules.ship.controller.ShipDestinationCycleException;
import org.entropy.skrupeltng.modules.ingame.modules.ship.controller.ShipTransportRequest;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.entropy.skrupeltng.modules.ingame.service.CoordHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ShipService {

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	public Ship getShip(long shipId) {
		return shipRepository.getOne(shipId);
	}

	@PreAuthorize("hasPermission(#gameId, 'game')")
	public List<Ship> getShipsOfLogin(long gameId, long loginId, Integer x, Integer y) {
		if (x == null || y == null) {
			return shipRepository.findShipsByGameIdAndLoginId(gameId, loginId);
		}

		return shipRepository.findShipsByGameIdAndLoginIdAndCoordinates(gameId, loginId, x, y);
	}

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	public String getDestinationName(long shipId) {
		Ship ship = shipRepository.getOne(shipId);

		Ship destinationShip = ship.getDestinationShip();

		if (destinationShip != null) {
			return destinationShip.getName();
		}

		int x = ship.getDestinationX();
		int y = ship.getDestinationY();
		long gameId = ship.getPlayer().getGame().getId();

		Optional<Planet> planetOpt = planetRepository.findByGameIdAndXAndY(gameId, x, y);

		if (planetOpt.isPresent()) {
			return planetOpt.get().getName();
		}

		return null;
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public ShipCourseSelectionResponse setCourse(long shipId, ShipCourseSelectionRequest request) {
		Ship ship = shipRepository.getOne(shipId);
		Player player = ship.getPlayer();
		Game game = player.getGame();

		int x = request.getX();
		int y = request.getY();
		int speed = request.getSpeed();
		String name = request.getTargetName();

		if (x < 0 || y < 0 || x > game.getGalaxySize() || y > game.getGalaxySize() || speed < 0 || speed > 9) {
			throw new IllegalArgumentException("Invalid ship course!");
		}

		ShipCourseSelectionResponse response = new ShipCourseSelectionResponse();

		try {
			Set<Long> visitedShipIds = new HashSet<>();
			visitDestinationShip(visitedShipIds, ship, ship.getPlayer().getId());

			ship.setDestinationX(x);
			ship.setDestinationY(y);
			ship.setTravelSpeed(speed);

			Optional<Ship> destinationShipOpt = shipRepository.findByGameIdAndXAndYAndName(game.getId(), x, y, name);

			if (destinationShipOpt.isPresent()) {
				ship.setDestinationShip(destinationShipOpt.get());
			} else {
				ship.setDestinationShip(null);
			}

			shipRepository.save(ship);
		} catch (ShipDestinationCycleException e) {
			response.setCycleDetected(true);
		}

		return response;
	}

	private void visitDestinationShip(Set<Long> visitedShipIds, Ship ship, long currentPlayerId) throws ShipDestinationCycleException {
		Ship destinationShip = ship.getDestinationShip();

		if (destinationShip != null) {
			if (visitedShipIds.add(destinationShip.getId())) {
				visitDestinationShip(visitedShipIds, destinationShip, currentPlayerId);
			} else if (destinationShip.getPlayer().getId() == currentPlayerId) {
				throw new ShipDestinationCycleException();
			}
		}
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void transport(long shipId, ShipTransportRequest request) {
		Ship ship = getShip(shipId);

		Planet planet = ship.getPlanet();

		transportWithoutPermissionCheck(request, ship, planet);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Pair<Ship, Planet> transportWithoutPermissionCheck(ShipTransportRequest request, Ship ship, Planet planet) {
		if (planet != null) {
			int fuel = request.getFuel();
			int mineral1 = request.getMineral1();
			int mineral2 = request.getMineral2();
			int mineral3 = request.getMineral3();
			int money = request.getMoney();
			int supplies = request.getSupplies();
			int colonists = request.getColonists();
			int lightGroundUnits = request.getLightGroundUnits();
			int heavyGroundUnits = request.getHeavyGroundUnits();

			int fuelCapacity = ship.getShipTemplate().getFuelCapacity();
			int storageSpace = ship.getShipTemplate().getStorageSpace();

			// Reset everything to the planet first so later checks are easier
			planet.setFuel(planet.getFuel() + ship.getFuel());
			planet.setMineral1(planet.getMineral1() + ship.getMineral1());
			planet.setMineral2(planet.getMineral2() + ship.getMineral2());
			planet.setMineral3(planet.getMineral3() + ship.getMineral3());
			planet.setMoney(planet.getMoney() + ship.getMoney());
			planet.setSupplies(planet.getSupplies() + ship.getSupplies());

			if (fuel < 0 || fuelCapacity < fuel || storageSpace < TotalGoodCalculator.retrieveSum(request)) {
				throw new IllegalArgumentException("Invalid change ship storage request!");
			}

			if (mineral1 < 0 || mineral1 > planet.getMineral1()) {
				throw new IllegalArgumentException("Invalid change ship storage request!");
			}

			if (mineral2 < 0 || mineral2 > planet.getMineral2()) {
				throw new IllegalArgumentException("Invalid change ship storage request!");
			}

			if (mineral3 < 0 || mineral3 > planet.getMineral3()) {
				throw new IllegalArgumentException("Invalid change ship storage request!");
			}

			if (money < 0 || money > planet.getMoney()) {
				throw new IllegalArgumentException("Invalid change ship storage request!");
			}

			if (supplies < 0 || supplies > planet.getSupplies()) {
				throw new IllegalArgumentException("Invalid change ship storage request!");
			}

			if (colonists < 0) {
				throw new IllegalArgumentException("Invalid change ship storage request!");
			}

			if (lightGroundUnits < 0) {
				throw new IllegalArgumentException("Invalid change ship storage request!");
			}

			if (heavyGroundUnits < 0) {
				throw new IllegalArgumentException("Invalid change ship storage request!");
			}

			ship.setFuel(fuel);
			planet.setFuel(planet.getFuel() - fuel);

			ship.setMineral1(mineral1);
			planet.setMineral1(planet.getMineral1() - mineral1);

			ship.setMineral2(mineral2);
			planet.setMineral2(planet.getMineral2() - mineral2);

			ship.setMineral3(mineral3);
			planet.setMineral3(planet.getMineral3() - mineral3);

			ship.setMoney(money);
			planet.setMoney(planet.getMoney() - money);

			ship.setSupplies(supplies);
			planet.setSupplies(planet.getSupplies() - supplies);

			if (planet.getPlayer() != null && planet.getPlayer().getId() == ship.getPlayer().getId()) {
				planet.setColonists(planet.getColonists() + ship.getColonists() - colonists);
				ship.setColonists(colonists);

				planet.setLightGroundUnits(planet.getLightGroundUnits() + ship.getLightGroundUnits() - lightGroundUnits);
				ship.setLightGroundUnits(lightGroundUnits);

				planet.setHeavyGroundUnits(planet.getHeavyGroundUnits() + ship.getHeavyGroundUnits() - heavyGroundUnits);
				ship.setHeavyGroundUnits(heavyGroundUnits);
			} else {
				planet.setNewPlayer(ship.getPlayer());

				planet.setNewColonists(planet.getNewColonists() + ship.getColonists() - colonists);
				ship.setColonists(colonists);

				planet.setNewLightGroundUnits(planet.getNewLightGroundUnits() + ship.getLightGroundUnits() - lightGroundUnits);
				ship.setLightGroundUnits(lightGroundUnits);

				planet.setNewHeavyGroundUnits(planet.getNewHeavyGroundUnits() + ship.getHeavyGroundUnits() - heavyGroundUnits);
				ship.setHeavyGroundUnits(heavyGroundUnits);
			}

			ship = shipRepository.save(ship);
			planet = planetRepository.save(planet);

			return Pair.of(ship, planet);
		} else {
			throw new IllegalArgumentException("Ship " + ship.getId() + " is not in a planet's orbit!");
		}
	}

	@PreAuthorize("hasPermission(#shipId, 'ship') and hasPermission(#shipId, 'turnNotDoneShip')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteCourse(long shipId) {
		Ship ship = shipRepository.getOne(shipId);
		ship.setDestinationShip(null);
		ship.setDestinationX(-1);
		ship.setDestinationY(-1);
		ship.setTravelSpeed(0);
		shipRepository.save(ship);
	}

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	public List<Ship> getScannedShips(long shipId) {
		Ship ship = shipRepository.getOne(shipId);
		Player player = ship.getPlayer();
		long playerId = player.getId();
		long gameId = player.getGame().getId();
		int scannerDist = ship.retrieveScannerDistance();

		List<Ship> ships = shipRepository.findByGameId(gameId);
		return ships.stream().filter(s -> s.getPlayer().getId() != playerId && CoordHelper.getDistance(s, ship) <= scannerDist).collect(Collectors.toList());
	}

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	public List<Planet> getScannedPlanets(long shipId) {
		Ship ship = shipRepository.getOne(shipId);
		Player player = ship.getPlayer();
		long playerId = player.getId();
		long gameId = player.getGame().getId();
		int scannerDist = ship.retrieveScannerDistance();

		List<Planet> planets = planetRepository.findByGameId(gameId);
		return planets.stream().filter(p -> (p.getPlayer() == null || p.getPlayer().getId() != playerId) && CoordHelper.getDistance(p, ship) <= scannerDist).collect(Collectors.toList());
	}

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	public Ship getScannedShip(long shipId, long scannedShipId) {
		Ship ship = shipRepository.getOne(scannedShipId);

		if (getScannedShips(shipId).contains(ship)) {
			return ship;
		}

		throw new IllegalArgumentException("Ship " + scannedShipId + " is not in scanner reach of ship " + shipId + "!");
	}

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	public Planet getScannedPlanet(long shipId, long scannedPlanetId) {
		Planet planet = planetRepository.getOne(scannedPlanetId);

		if (getScannedPlanets(shipId).contains(planet)) {
			return planet;
		}

		throw new IllegalArgumentException("Planet " + scannedPlanetId + " is not in scanner reach of ship " + shipId + "!");
	}

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	public String getLogbook(long shipId) {
		Ship ship = shipRepository.getOne(shipId);
		return ship.getLog();
	}

	@PreAuthorize("hasPermission(#shipId, 'ship')")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void changeLogbook(long shipId, String logbook) {
		Ship ship = shipRepository.getOne(shipId);
		ship.setLog(logbook);
		shipRepository.save(ship);
	}
}