package org.entropy.skrupeltng.modules.ingame.modules.planet.database;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "planet_type")
public class PlanetType implements Serializable {

	private static final long serialVersionUID = -4348251191802354190L;

	@Id
	private String id;

	@Column(name = "image_prefix")
	private String imagePrefix;

	@Column(name = "image_count")
	private int imageCount;

	@Column(name = "min_temperature")
	private int minTemperature;

	@Column(name = "max_temperature")
	private int maxTemperature;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getImagePrefix() {
		return imagePrefix;
	}

	public void setImagePrefix(String imagePrefix) {
		this.imagePrefix = imagePrefix;
	}

	public int getImageCount() {
		return imageCount;
	}

	public void setImageCount(int imageCount) {
		this.imageCount = imageCount;
	}

	public int getMinTemperature() {
		return minTemperature;
	}

	public void setMinTemperature(int minTemperature) {
		this.minTemperature = minTemperature;
	}

	public int getMaxTemperature() {
		return maxTemperature;
	}

	public void setMaxTemperature(int maxTemperature) {
		this.maxTemperature = maxTemperature;
	}
}