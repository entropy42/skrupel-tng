package org.entropy.skrupeltng.modules.ingame.modules.planet.database;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PlayerPlanetScanLogRepository extends JpaRepository<PlayerPlanetScanLog, Long> {

	List<PlayerPlanetScanLog> findByPlayerId(long playerId);

	Optional<PlayerPlanetScanLog> findByPlayerIdAndPlanetId(long playerId, long planetId);
}