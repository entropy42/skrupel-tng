package org.entropy.skrupeltng.modules.ingame.modules.starbase.controller;

import java.io.Serializable;

public class StarbaseProductionRequest implements Serializable {

	private static final long serialVersionUID = 1655265892575455549L;

	private String templateId;
	private int quantity;
	private String type;

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}