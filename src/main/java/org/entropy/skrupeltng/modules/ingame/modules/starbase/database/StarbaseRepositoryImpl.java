package org.entropy.skrupeltng.modules.ingame.modules.starbase.database;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.entropy.skrupeltng.modules.RepositoryCustomBase;

public class StarbaseRepositoryImpl extends RepositoryCustomBase implements StarbaseRepositoryCustom {

	@Override
	public boolean loginOwnsStarbase(long starbaseId, long loginId) {
		String sql = "" +
				"SELECT \n" +
				"	p.login_id \n" +
				"FROM \n" +
				"	starbase s \n" +
				"	INNER JOIN planet pl" +
				"		ON pl.starbase_id = s.id \n" +
				"	INNER JOIN player p \n" +
				"		ON p.id = pl.player_id \n" +
				"WHERE \n" +
				"	s.id = :starbaseId";

		Map<String, Object> params = new HashMap<>(1);
		params.put("starbaseId", starbaseId);

		List<Long> results = jdbcTemplate.queryForList(sql, params, Long.class);
		return results.size() == 1 && results.get(0).longValue() == loginId;
	}
}