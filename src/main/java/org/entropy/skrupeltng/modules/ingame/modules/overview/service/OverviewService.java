package org.entropy.skrupeltng.modules.ingame.modules.overview.service;

import java.util.List;

import org.entropy.skrupeltng.modules.ingame.database.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OverviewService {

	@Autowired
	private PlayerRepository playerRepository;

	public List<PlayerSummaryEntry> getPlayerSummaries(long gameId) {
		return playerRepository.getSummaries(gameId);
	}
}