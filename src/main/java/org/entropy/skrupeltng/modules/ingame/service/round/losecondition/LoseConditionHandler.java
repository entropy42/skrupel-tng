package org.entropy.skrupeltng.modules.ingame.service.round.losecondition;

import org.entropy.skrupeltng.modules.ingame.database.Player;

public interface LoseConditionHandler {

	boolean hasLost(Player player);
}