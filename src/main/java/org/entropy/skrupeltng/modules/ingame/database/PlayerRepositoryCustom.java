package org.entropy.skrupeltng.modules.ingame.database;

import java.util.List;

import org.entropy.skrupeltng.modules.ingame.modules.overview.service.PlayerSummaryEntry;

public interface PlayerRepositoryCustom {

	void setupTurnValues(long gameId);

	List<PlayerSummaryEntry> getSummaries(long gameId);

	boolean playersTurnNotDoneForPlanet(long planetId, long loginId);

	boolean playersTurnNotDoneForShip(long shipId, long loginId);

	boolean playersTurnNotDoneForStarbase(long starbaseId, long loginId);
}