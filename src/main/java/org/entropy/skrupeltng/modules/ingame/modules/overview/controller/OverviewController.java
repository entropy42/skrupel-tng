package org.entropy.skrupeltng.modules.ingame.modules.overview.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.entropy.skrupeltng.modules.AbstractController;
import org.entropy.skrupeltng.modules.ingame.modules.overview.database.NewsEntry;
import org.entropy.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.entropy.skrupeltng.modules.ingame.modules.overview.service.OverviewService;
import org.entropy.skrupeltng.modules.ingame.modules.overview.service.PlayerSummaryEntry;
import org.entropy.skrupeltng.modules.ingame.modules.overview.service.RoundSummary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/ingame/overview")
public class OverviewController extends AbstractController {

	@Autowired
	private NewsService newsService;

	@Autowired
	private OverviewService overviewService;

	@GetMapping("")
	public String getOverview(@RequestParam long gameId, Model model) {
		RoundSummary summary = newsService.getRoundSummary(gameId);
		model.addAttribute("summary", summary);

		List<PlayerSummaryEntry> playerSummaries = overviewService.getPlayerSummaries(gameId);
		model.addAttribute("players", playerSummaries);

		long loginId = userDetailService.getLoginId();
		List<NewsEntry> news = newsService.findNews(gameId, loginId);

		for (NewsEntry newsEntry : news) {
			String arguments = newsEntry.getArguments();

			Object[] args = null;

			if (StringUtils.isNotBlank(arguments)) {
				args = arguments.split(",");
			}

			String text = messageSource.getMessage(newsEntry.getTemplate(), args, newsEntry.getTemplate(), LocaleContextHolder.getLocale());
			newsEntry.setTemplate(text);
		}

		model.addAttribute("news", news);

		return "ingame/overview/overview::content";
	}

	@PostMapping("/toggle-news-delete")
	@ResponseBody
	public boolean toggleDeleteNews(@RequestParam long newsEntryId) {
		return newsService.toggleDeleteNews(newsEntryId);
	}
}