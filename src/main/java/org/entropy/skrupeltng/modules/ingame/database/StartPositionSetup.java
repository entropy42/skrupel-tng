package org.entropy.skrupeltng.modules.ingame.database;

public enum StartPositionSetup {

	EQUAL_DISTANCE, RANDOM
}