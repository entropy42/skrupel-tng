package org.entropy.skrupeltng.modules.masterdata.service;

public enum StarbaseUpgradeType {

	HULL, PROPULSION, ENERGY, PROJECTILE
}