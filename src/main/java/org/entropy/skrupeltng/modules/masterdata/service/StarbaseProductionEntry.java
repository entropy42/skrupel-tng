package org.entropy.skrupeltng.modules.masterdata.service;

import java.io.Serializable;

public class StarbaseProductionEntry implements Serializable {

	private static final long serialVersionUID = 7988159547082370970L;

	private String id;
	private String name;
	private int techLevel;
	private StarbaseUpgradeType type;
	private int costMoney;
	private int costMineral1;
	private int costMineral2;
	private int costMineral3;
	private int stock;
	private int producableQuantity;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTechLevel() {
		return techLevel;
	}

	public void setTechLevel(int techLevel) {
		this.techLevel = techLevel;
	}

	public StarbaseUpgradeType getType() {
		return type;
	}

	public void setType(StarbaseUpgradeType type) {
		this.type = type;
	}

	public int getCostMoney() {
		return costMoney;
	}

	public void setCostMoney(int costMoney) {
		this.costMoney = costMoney;
	}

	public int getCostMineral1() {
		return costMineral1;
	}

	public void setCostMineral1(int costMineral1) {
		this.costMineral1 = costMineral1;
	}

	public int getCostMineral2() {
		return costMineral2;
	}

	public void setCostMineral2(int costMineral2) {
		this.costMineral2 = costMineral2;
	}

	public int getCostMineral3() {
		return costMineral3;
	}

	public void setCostMineral3(int costMineral3) {
		this.costMineral3 = costMineral3;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public int getProducableQuantity() {
		return producableQuantity;
	}

	public void setProducableQuantity(int producableQuantity) {
		this.producableQuantity = producableQuantity;
	}
}