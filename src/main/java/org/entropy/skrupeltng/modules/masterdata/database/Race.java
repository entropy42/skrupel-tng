package org.entropy.skrupeltng.modules.masterdata.database;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "race")
public class Race implements Serializable {

	private static final long serialVersionUID = 8555363870878082042L;

	@Id
	private String name;

	@Column(name = "folder_name")
	private String folderName;

	private String description;

	@Column(name = "main_image")
	private String mainImage;

	@Column(name = "preferred_planet_type")
	private String preferredPlanetType;

	@Column(name = "preferred_temperature")
	private int preferredTemperature;

	@Column(name = "mine_production_rate")
	private float mineProductionRate;

	@Column(name = "factory_production_rate")
	private float factoryProductionRate;

	@Column(name = "tax_rate")
	private float taxRate;

	@Column(name = "ground_combat_attack_rate")
	private float groundCombatAttackRate;

	@Column(name = "ground_combat_defense_rate")
	private float groundCombatDefenseRate;

	@Column(name = "home_planet_name")
	private String homePlanetName;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "race")
	private List<ShipTemplate> ships;

	public Race() {

	}

	public Race(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMainImage() {
		return mainImage;
	}

	public void setMainImage(String mainImage) {
		this.mainImage = mainImage;
	}

	public String getPreferredPlanetType() {
		return preferredPlanetType;
	}

	public void setPreferredPlanetType(String preferredPlanetType) {
		this.preferredPlanetType = preferredPlanetType;
	}

	public int getPreferredTemperature() {
		return preferredTemperature;
	}

	public void setPreferredTemperature(int preferredTemperature) {
		this.preferredTemperature = preferredTemperature;
	}

	public float getMineProductionRate() {
		return mineProductionRate;
	}

	public void setMineProductionRate(float mineProductionRate) {
		this.mineProductionRate = mineProductionRate;
	}

	public float getFactoryProductionRate() {
		return factoryProductionRate;
	}

	public void setFactoryProductionRate(float factoryProductionRate) {
		this.factoryProductionRate = factoryProductionRate;
	}

	public float getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(float taxRate) {
		this.taxRate = taxRate;
	}

	public float getGroundCombatAttackRate() {
		return groundCombatAttackRate;
	}

	public void setGroundCombatAttackRate(float groundCombatAttackRate) {
		this.groundCombatAttackRate = groundCombatAttackRate;
	}

	public float getGroundCombatDefenseRate() {
		return groundCombatDefenseRate;
	}

	public void setGroundCombatDefenseRate(float groundCombatDefenseRate) {
		this.groundCombatDefenseRate = groundCombatDefenseRate;
	}

	public List<ShipTemplate> getShips() {
		return ships;
	}

	public void setShips(List<ShipTemplate> ships) {
		this.ships = ships;
	}

	public String getHomePlanetName() {
		return homePlanetName;
	}

	public void setHomePlanetName(String homePlanetName) {
		this.homePlanetName = homePlanetName;
	}
}