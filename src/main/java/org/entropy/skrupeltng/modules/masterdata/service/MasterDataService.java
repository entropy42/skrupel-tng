package org.entropy.skrupeltng.modules.masterdata.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.PlanetTypeRepository;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.controller.StarbaseUpgradeLevel;
import org.entropy.skrupeltng.modules.masterdata.database.Race;
import org.entropy.skrupeltng.modules.masterdata.database.RaceRepository;
import org.entropy.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.entropy.skrupeltng.modules.masterdata.database.ShipTemplateRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MasterDataService {

	public static final Random RANDOM = new Random();

	private static final String SHIP_DATA_FILE = "/schiffe.txt";

	private static final String RACE_DATA_FILE = "/daten.txt";

	private final Logger log = LoggerFactory.getLogger(getClass());

	private final Map<Integer, float[]> FUEL_CONSUMPTION = new HashMap<>(10);
	private final Map<StarbaseUpgradeType, List<Integer>> starbaseUpgradeCosts = new HashMap<>(StarbaseUpgradeType.values().length);
	private final int[] NECESSARY_MINES = new int[] { 10, 6, 4, 2, 1 };

	private final CSVFormat format = CSVFormat.newFormat(':');

	@Autowired
	private RaceRepository raceRepository;

	@Autowired
	private ShipTemplateRepository shipTemplateRepository;

	@Autowired
	private PlanetTypeRepository planetTypeRepository;

	@Value("${skr.randomness_seed:0}")
	private long randomnessSeed;

	@PostConstruct
	public void loadFiles() throws Exception {
		if (randomnessSeed != 0L) {
			RANDOM.setSeed(randomnessSeed);
		}

		loadDefaultRace();
		loadRaces();
		initStarbaseCosts();
		initFuelConsumptionData();
	}

	private void loadDefaultRace() throws IOException {
		String racefolder = "orion";
		String dir = "/static/races/";
		InputStream raceDataInputStream = getClass().getResourceAsStream(dir + racefolder + RACE_DATA_FILE);
		InputStream shipDataInputStream = getClass().getResourceAsStream(dir + racefolder + SHIP_DATA_FILE);
		addRace(racefolder, raceDataInputStream, shipDataInputStream);
	}

	private void loadRaces() throws Exception {
		String raceFolderName = "races";
		File racesFolder = new File(raceFolderName);

		if (!racesFolder.exists() || !racesFolder.isDirectory()) {
			return;
		}

		String[] folders = racesFolder.list((f, n) -> !n.startsWith("."));

		for (String racefolder : folders) {
			log.info("Updating race in folder " + racefolder + "...");

			InputStream raceDataInputStream = new FileInputStream(raceFolderName + "/" + racefolder + RACE_DATA_FILE);
			InputStream shipDataInputStream = new FileInputStream(raceFolderName + "/" + racefolder + SHIP_DATA_FILE);

			addRace(racefolder, raceDataInputStream, shipDataInputStream);
		}
	}

	private void addRace(String racefolder, InputStream raceDataInputStream, InputStream shipDataInputStream) throws IOException {
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(raceDataInputStream, StandardCharsets.UTF_8))) {
			List<String> lines = reader.lines().collect(Collectors.toList());

			String raceName = lines.get(0);

			Optional<Race> raceResult = raceRepository.findById(raceName);

			Race race = raceResult.isPresent() ? raceResult.get() : new Race(raceName);
			race.setFolderName(racefolder);

			String[] mainAttributes = lines.get(2).split(":");

			int preferredTemperature = Integer.valueOf(mainAttributes[0]) - 35;
			race.setPreferredTemperature(preferredTemperature);
			race.setTaxRate(Float.valueOf(mainAttributes[1]));
			race.setMineProductionRate(Float.valueOf(mainAttributes[2]));
			race.setGroundCombatAttackRate(Float.valueOf(mainAttributes[3]));
			race.setGroundCombatDefenseRate(Float.valueOf(mainAttributes[4]));
			race.setFactoryProductionRate(Float.valueOf(mainAttributes[5]));

			String planetTypeNumber = mainAttributes[6];
			String planetType = planetTypeRepository.getIdByImagePrefix(planetTypeNumber);
			race.setPreferredPlanetType(planetType);

			String homePlanetName = lines.get(5);
			race.setHomePlanetName(homePlanetName);

			race = raceRepository.save(race);

			CSVParser parser = format.parse(new InputStreamReader(shipDataInputStream, StandardCharsets.UTF_8));

			for (CSVRecord record : parser) {
				String id = race.getName().replace(" ", "_") + "_" + record.get(1);
				Optional<ShipTemplate> shipResult = shipTemplateRepository.findById(id);

				ShipTemplate ship = shipResult.isPresent() ? shipResult.get() : new ShipTemplate(id);

				ship.setName(record.get(0));
				ship.setRace(race);
				ship.setTechLevel(Integer.valueOf(record.get(2)));
				ship.setImage(record.get(3));
				ship.setCostMoney(Integer.valueOf(record.get(5)));
				ship.setCostMineral1(Integer.valueOf(record.get(6)));
				ship.setCostMineral2(Integer.valueOf(record.get(7)));
				ship.setCostMineral3(Integer.valueOf(record.get(8)));
				ship.setEnergyWeaponsCount(Integer.valueOf(record.get(9)));
				ship.setProjectileWeaponsCount(Integer.valueOf(record.get(10)));
				ship.setHangarCapacity(Integer.valueOf(record.get(11)));
				ship.setStorageSpace(Integer.valueOf(record.get(12)));
				ship.setFuelCapacity(Integer.valueOf(record.get(13)));
				ship.setPropulsionSystemsCount(Integer.valueOf(record.get(14)));
				ship.setCrew(Integer.valueOf(record.get(15)));
				ship.setMass(Integer.valueOf(record.get(16)));

				shipTemplateRepository.save(ship);
			}
		}
	}

	private void initStarbaseCosts() {
		List<Integer> starbaseHullUpgradeCosts = new ArrayList<>(10);
		List<Integer> starbasePropulsionUpgradeCosts = new ArrayList<>(10);
		List<Integer> starbaseWeaponUpgradeCosts = new ArrayList<>(10);

		starbaseUpgradeCosts.put(StarbaseUpgradeType.HULL, starbaseHullUpgradeCosts);
		starbaseUpgradeCosts.put(StarbaseUpgradeType.PROPULSION, starbasePropulsionUpgradeCosts);
		starbaseUpgradeCosts.put(StarbaseUpgradeType.ENERGY, starbaseWeaponUpgradeCosts);
		starbaseUpgradeCosts.put(StarbaseUpgradeType.PROJECTILE, starbaseWeaponUpgradeCosts);

		starbaseHullUpgradeCosts.add(100);
		starbaseHullUpgradeCosts.add(200);
		starbaseHullUpgradeCosts.add(300);
		starbaseHullUpgradeCosts.add(800);
		starbaseHullUpgradeCosts.add(1000);
		starbaseHullUpgradeCosts.add(1200);
		starbaseHullUpgradeCosts.add(2500);
		starbaseHullUpgradeCosts.add(5000);
		starbaseHullUpgradeCosts.add(7500);
		starbaseHullUpgradeCosts.add(10000);

		starbasePropulsionUpgradeCosts.add(100);
		starbasePropulsionUpgradeCosts.add(200);
		starbasePropulsionUpgradeCosts.add(300);
		starbasePropulsionUpgradeCosts.add(400);
		starbasePropulsionUpgradeCosts.add(500);
		starbasePropulsionUpgradeCosts.add(600);
		starbasePropulsionUpgradeCosts.add(700);
		starbasePropulsionUpgradeCosts.add(4000);
		starbasePropulsionUpgradeCosts.add(7000);
		starbasePropulsionUpgradeCosts.add(10000);

		starbaseWeaponUpgradeCosts.add(100);
		starbaseWeaponUpgradeCosts.add(400);
		starbaseWeaponUpgradeCosts.add(900);
		starbaseWeaponUpgradeCosts.add(1600);
		starbaseWeaponUpgradeCosts.add(2500);
		starbaseWeaponUpgradeCosts.add(3600);
		starbaseWeaponUpgradeCosts.add(4900);
		starbaseWeaponUpgradeCosts.add(6400);
		starbaseWeaponUpgradeCosts.add(8100);
		starbaseWeaponUpgradeCosts.add(10000);
	}

	private void initFuelConsumptionData() {
		FUEL_CONSUMPTION.put(1, new float[] { 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f, 0f });
		FUEL_CONSUMPTION.put(2, new float[] { 0f, 100f, 107.5f, 300f, 400f, 500f, 600f, 700f, 800f, 900f });
		FUEL_CONSUMPTION.put(3, new float[] { 0f, 100f, 106.25f, 107.78f, 337.5f, 500f, 600f, 700f, 800f, 900f });
		FUEL_CONSUMPTION.put(4, new float[] { 0f, 100f, 103.75f, 104.44f, 106.25f, 300f, 322.22f, 495.92f, 487.5f, 900f });
		FUEL_CONSUMPTION.put(5, new float[] { 0f, 100f, 103.75f, 104.44f, 106.25f, 104f, 291.67f, 291.84f, 366.41f, 900f });
		FUEL_CONSUMPTION.put(6, new float[] { 0f, 100f, 103.75f, 104.44f, 106.25f, 104f, 103.69f, 251.02f, 335.16f, 900f });
		FUEL_CONSUMPTION.put(7, new float[] { 0f, 100f, 103.75f, 104.44f, 106.25f, 104f, 103.69f, 108.16f, 303.91f, 529.63f });
		FUEL_CONSUMPTION.put(9, new float[] { 0f, 100f, 100f, 100f, 100f, 100f, 100f, 102.04f, 109.38f, 529.63f });
		FUEL_CONSUMPTION.put(10, new float[] { 0f, 100f, 100f, 100f, 100f, 100f, 100f, 100f, 100f, 100f });
	}

	public List<String> getAllRaceNames() {
		return raceRepository.getAllRaceNames();
	}

	public Race getRaceData(String race) {
		return raceRepository.getOne(race);
	}

	public List<StarbaseUpgradeLevel> getLevelCosts(int startLevel, StarbaseUpgradeType type, int planetMoney) {
		List<StarbaseUpgradeLevel> levelCosts = new ArrayList<>(10);

		for (int i = startLevel + 1; i <= 10; i++) {
			int money = calculateStarbaseUpgradeMoney(type, startLevel, i);

			if (money > 0 && money <= planetMoney) {
				StarbaseUpgradeLevel level = new StarbaseUpgradeLevel(i, money);
				levelCosts.add(level);
			} else {
				break;
			}
		}

		return levelCosts;
	}

	public int calculateStarbaseUpgradeMoney(StarbaseUpgradeType type, int startLevel, int endLevel) {
		List<Integer> list = starbaseUpgradeCosts.get(type);

		int costs = 0;

		for (int i = startLevel + 1; i <= endLevel; i++) {
			costs += list.get(i - 1);
		}

		return costs;
	}

	public float[] getFuelConsumptionData(int propulsionLevel) {
		return FUEL_CONSUMPTION.get(propulsionLevel);
	}

	public int calculateFuelConsumptionPerMonth(Ship ship, int travelSpeed, double distance, double duration) {
		float masterDataFuelConsumption = getFuelConsumptionData(ship.getPropulsionSystemTemplate().getTechLevel())[travelSpeed];
		float fuelConsumption = masterDataFuelConsumption;

		if (fuelConsumption > 0f) {
			if (duration <= 1d) {
				fuelConsumption = (float)Math.floor(distance * fuelConsumption * (ship.getShipTemplate().getMass() / 100000f));
			} else {
				fuelConsumption = (float)Math.floor(Math.pow(travelSpeed, 2) * fuelConsumption * (ship.getShipTemplate().getMass() / 100000f));
			}

			if (fuelConsumption == 0f) {
				fuelConsumption = 1f;
			}
		}

		return (int)fuelConsumption;
	}

	public double calculateTravelDuration(int travelSpeed, double distance) {
		return distance / (Math.pow(travelSpeed, 2));
	}

	public int getNecessaryMinesByIndex(int index) {
		return NECESSARY_MINES[index];
	}
}