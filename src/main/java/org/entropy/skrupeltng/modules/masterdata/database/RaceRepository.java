package org.entropy.skrupeltng.modules.masterdata.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface RaceRepository extends JpaRepository<Race, String> {

	@Query("SELECT r.id FROM Race r ORDER BY r.name ASC")
	List<String> getAllRaceNames();
}