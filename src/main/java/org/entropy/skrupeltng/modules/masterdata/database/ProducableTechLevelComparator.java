package org.entropy.skrupeltng.modules.masterdata.database;

import java.util.Comparator;

import org.entropy.skrupeltng.modules.masterdata.StarbaseProducable;

public class ProducableTechLevelComparator implements Comparator<StarbaseProducable> {

	@Override
	public int compare(StarbaseProducable a, StarbaseProducable b) {
		return Integer.compare(a.getTechLevel(), b.getTechLevel());
	}
}