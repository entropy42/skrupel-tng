package org.entropy.skrupeltng.modules.masterdata.database;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.entropy.skrupeltng.modules.masterdata.StarbaseProducable;

@Entity
@Table(name = "ship_template")
public class ShipTemplate implements Serializable, StarbaseProducable, Comparable<ShipTemplate> {

	private static final long serialVersionUID = 1381090807145835544L;

	@Id
	private String id;

	private String name;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "race_id")
	private Race race;

	@Column(name = "tech_level")
	private int techLevel;

	private String image;

	private int crew;

	private int mass;

	@Column(name = "fuel_capacity")
	private int fuelCapacity;

	@Column(name = "storage_space")
	private int storageSpace;

	@Column(name = "propulsion_systems_count")
	private int propulsionSystemsCount;

	@Column(name = "energy_weapons_count")
	private int energyWeaponsCount;

	@Column(name = "projectile_weapons_count")
	private int projectileWeaponsCount;

	@Column(name = "hangar_capacity")
	private int hangarCapacity;

	@Column(name = "cost_money")
	private int costMoney;

	@Column(name = "cost_mineral1")
	private int costMineral1;

	@Column(name = "cost_mineral2")
	private int costMineral2;

	@Column(name = "cost_mineral3")
	private int costMineral3;

	public ShipTemplate() {

	}

	public ShipTemplate(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Race getRace() {
		return race;
	}

	public void setRace(Race race) {
		this.race = race;
	}

	public int getTechLevel() {
		return techLevel;
	}

	public void setTechLevel(int techLevel) {
		this.techLevel = techLevel;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getCrew() {
		return crew;
	}

	public void setCrew(int crew) {
		this.crew = crew;
	}

	public int getMass() {
		return mass;
	}

	public void setMass(int mass) {
		this.mass = mass;
	}

	public int getFuelCapacity() {
		return fuelCapacity;
	}

	public void setFuelCapacity(int fuelCapacity) {
		this.fuelCapacity = fuelCapacity;
	}

	public int getStorageSpace() {
		return storageSpace;
	}

	public void setStorageSpace(int storageSpace) {
		this.storageSpace = storageSpace;
	}

	public int getPropulsionSystemsCount() {
		return propulsionSystemsCount;
	}

	public void setPropulsionSystemsCount(int propulsionSystemsCount) {
		this.propulsionSystemsCount = propulsionSystemsCount;
	}

	public int getEnergyWeaponsCount() {
		return energyWeaponsCount;
	}

	public void setEnergyWeaponsCount(int energyWeaponsCount) {
		this.energyWeaponsCount = energyWeaponsCount;
	}

	public int getProjectileWeaponsCount() {
		return projectileWeaponsCount;
	}

	public void setProjectileWeaponsCount(int projectileWeaponsCount) {
		this.projectileWeaponsCount = projectileWeaponsCount;
	}

	public int getHangarCapacity() {
		return hangarCapacity;
	}

	public void setHangarCapacity(int hangarCapacity) {
		this.hangarCapacity = hangarCapacity;
	}

	@Override
	public int getCostMoney() {
		return costMoney;
	}

	public void setCostMoney(int costMoney) {
		this.costMoney = costMoney;
	}

	@Override
	public int getCostMineral1() {
		return costMineral1;
	}

	public void setCostMineral1(int costMineral1) {
		this.costMineral1 = costMineral1;
	}

	@Override
	public int getCostMineral2() {
		return costMineral2;
	}

	public void setCostMineral2(int costMineral2) {
		this.costMineral2 = costMineral2;
	}

	@Override
	public int getCostMineral3() {
		return costMineral3;
	}

	public void setCostMineral3(int costMineral3) {
		this.costMineral3 = costMineral3;
	}

	@Override
	public int compareTo(ShipTemplate o) {
		return Integer.compare(techLevel, o.getTechLevel());
	}
}