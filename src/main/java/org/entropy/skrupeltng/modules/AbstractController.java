package org.entropy.skrupeltng.modules;

import org.entropy.skrupeltng.config.PermissionEvaluatorImpl;
import org.entropy.skrupeltng.config.UserDetailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

public class AbstractController {

	@Autowired
	protected UserDetailServiceImpl userDetailService;

	@Autowired
	protected MessageSource messageSource;

	@Autowired
	private PermissionEvaluatorImpl permissionEvaluator;

	protected Pageable createPageable(Integer page, Integer pageSize, String sortField, Boolean sortDirection) {
		if (page == null) {
			page = 0;
		}

		if (pageSize == null) {
			pageSize = 20;
		}

		Sort sort = null;

		if (sortField != null && sortDirection != null) {
			sort = new Sort(sortDirection ? Direction.ASC : Direction.DESC, sortField);
		}

		return PageRequest.of(page, pageSize, sort);
	}

	protected boolean turnDoneForPlanet(long planetId) {
		return !permissionEvaluator.checkTurnNotDonePlanet(planetId);
	}

	protected boolean turnDoneForShip(long shipId) {
		return !permissionEvaluator.checkTurnNotDoneShip(shipId);
	}

	protected boolean turnDoneForStarbase(long starbaseId) {
		return !permissionEvaluator.checkTurnNotDoneStarbase(starbaseId);
	}
}