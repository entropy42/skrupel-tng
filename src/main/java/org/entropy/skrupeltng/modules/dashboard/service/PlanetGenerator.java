package org.entropy.skrupeltng.modules.dashboard.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import org.entropy.skrupeltng.modules.dashboard.ResourceDensity;
import org.entropy.skrupeltng.modules.ingame.database.Game;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystem;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemRepository;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.PlanetNameRepository;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.PlanetType;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.PlanetTypeRepository;
import org.entropy.skrupeltng.modules.ingame.service.CoordHelper;
import org.entropy.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class PlanetGenerator {

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private PlanetNameRepository planetNameRepository;

	@Autowired
	private PlanetTypeRepository planetTypeRepository;

	@Autowired
	private OrbitalSystemRepository orbitalSystemRepository;

	@Autowired
	private MasterDataService masterDataService;

	@Value("${skr.galaxy_structure_file_size:50}")
	private int galaxyStructureFileSize;

	@Value("${skr.min_planet_distance:55}")
	private int minPlanetDistance;

	@Value("${skr.max_planet_iterations:35}")
	private int maxPlanetIterations;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void generatePlanets(Game game) {
		List<Planet> planets = new ArrayList<>(game.getPlanetCount());

		// We make a copy of the list since we don't want to manipulate the cached object
		List<String> planetNames = planetNameRepository.getAllPlanetNames();
		List<PlanetType> planetTypes = planetTypeRepository.findAll();

		ResourceDensity resourceDensity = game.getResourceDensity();
		int resourceDensityDiff = resourceDensity.getMax() - resourceDensity.getMin();

		GalaxyData galaxyData = getGalaxyStructureData(game.getGalaxyConfig().getId());
		List<PlanetContainer> containerList = galaxyData.getContainerList();

		int containerDimension = game.getGalaxySize() / galaxyStructureFileSize;

		Random random = MasterDataService.RANDOM;

		for (int i = 0; i < game.getPlanetCount(); i++) {
			Planet planet = new Planet();

			int iterations = 0;
			boolean tooManyIterations = false;

			while (true) {
				PlanetContainer container = containerList.get(random.nextInt(containerList.size()));
				int containerX = container.getX();
				int containerY = container.getY();

				planet.setX((containerDimension * containerX) + random.nextInt(containerDimension));
				planet.setY((containerDimension * containerY) + random.nextInt(containerDimension));

				boolean tooNear = false;

				if (containerDimension < 40) {
					for (Planet p : planets) {
						if (CoordHelper.getDistance(p, planet) < minPlanetDistance) {
							tooNear = true;
							break;
						}
					}
				} else {
					List<PlanetContainer> neighbors = container.getNeighbors();

					for (PlanetContainer neighbor : neighbors) {
						for (Planet p : neighbor) {
							if (CoordHelper.getDistance(p, planet) < minPlanetDistance) {
								tooNear = true;
								break;
							}
						}

						if (tooNear) {
							break;
						}
					}
				}

				if (!tooNear) {
					container.add(planet);
					break;
				} else {
					iterations++;

					if (iterations > maxPlanetIterations) {
						tooManyIterations = true;
						break;
					}
				}
			}

			if (!tooManyIterations) {
				planets.add(planet);
				planet.setGame(game);
				game.getPlanets().add(planet);

				String name = planetNames.get(random.nextInt(planetNames.size()));
				planet.setName(name);

				PlanetType planetType = planetTypes.get(random.nextInt(planetTypes.size()));
				planet.setType(planetType.getId());

				int temperature = planetType.getMinTemperature() + random.nextInt(planetType.getMaxTemperature());
				planet.setTemperature(temperature);

				String image = planetType.getImagePrefix() + "_" + (1 + random.nextInt(planetType.getImageCount()));
				planet.setImage(image);

				planet.setFuel(1 + random.nextInt(70));
				planet.setMineral1(1 + random.nextInt(70));
				planet.setMineral2(1 + random.nextInt(70));
				planet.setMineral3(1 + random.nextInt(70));

				planet.setUntappedFuel(resourceDensity.getMin() + random.nextInt(resourceDensityDiff));
				planet.setUntappedMineral1(resourceDensity.getMin() + random.nextInt(resourceDensityDiff));
				planet.setUntappedMineral2(resourceDensity.getMin() + random.nextInt(resourceDensityDiff));
				planet.setUntappedMineral3(resourceDensity.getMin() + random.nextInt(resourceDensityDiff));

				planet.setNecessaryMinesForOneFuel(masterDataService.getNecessaryMinesByIndex(random.nextInt(5)));
				planet.setNecessaryMinesForOneMineral1(masterDataService.getNecessaryMinesByIndex(random.nextInt(5)));
				planet.setNecessaryMinesForOneMineral2(masterDataService.getNecessaryMinesByIndex(random.nextInt(5)));
				planet.setNecessaryMinesForOneMineral3(masterDataService.getNecessaryMinesByIndex(random.nextInt(5)));
			}
		}

		List<Planet> savedPlanets = planetRepository.saveAll(planets);
		game.setPlanets(new HashSet<>(savedPlanets));

		List<OrbitalSystem> orbitalSystems = new ArrayList<>(savedPlanets.size() * 3);

		for (Planet planet : savedPlanets) {
			int count = 1 + random.nextInt(6);

			for (int i = 1; i <= count; i++) {
				orbitalSystems.add(new OrbitalSystem(planet));
			}
		}

		orbitalSystemRepository.saveAll(orbitalSystems);
	}

	private GalaxyData getGalaxyStructureData(String galaxyStructure) {
		try (InputStream stream = getClass().getResourceAsStream("/galaxies/" + galaxyStructure + ".txt"); BufferedReader reader = new BufferedReader(new InputStreamReader(stream, StandardCharsets.UTF_8))) {
			PlanetContainer[][] containers = new PlanetContainer[galaxyStructureFileSize][];
			List<PlanetContainer> containerList = new ArrayList<>(galaxyStructureFileSize * galaxyStructureFileSize);

			for (int y = 0; y < galaxyStructureFileSize; y++) {
				String line = reader.readLine();
				char[] charArray = line.toCharArray();

				PlanetContainer[] row = new PlanetContainer[galaxyStructureFileSize];
				containers[y] = row;

				for (int x = 0; x < galaxyStructureFileSize; x++) {
					if (charArray[x] == '1') {
						PlanetContainer container = new PlanetContainer(x, y);
						row[x] = container;
						containerList.add(container);
					}
				}
			}

			containerList.parallelStream().forEach(container -> {
				int x = container.getX();
				int y = container.getY();

				addNeighbors(container, containers, x + 1, y);
				addNeighbors(container, containers, x + 1, y + 1);
				addNeighbors(container, containers, x + 1, y - 1);
				addNeighbors(container, containers, x - 1, y);
				addNeighbors(container, containers, x - 1, y + 1);
				addNeighbors(container, containers, x - 1, y - 1);
				addNeighbors(container, containers, x, y + 1);
				addNeighbors(container, containers, x, y - 1);
			});

			return new GalaxyData(containers, containerList);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	private void addNeighbors(PlanetContainer center, PlanetContainer[][] containers, int x, int y) {
		if (x < 0 || y < 0 || x >= galaxyStructureFileSize || y >= galaxyStructureFileSize) {
			return;
		}

		PlanetContainer[] line = containers[y];

		if (line != null) {
			PlanetContainer container = line[x];

			if (container != null) {
				center.getNeighbors().add(container);
			}
		}
	}
}