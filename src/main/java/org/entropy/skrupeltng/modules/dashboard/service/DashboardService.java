package org.entropy.skrupeltng.modules.dashboard.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.entropy.skrupeltng.modules.ai.AILevel;
import org.entropy.skrupeltng.modules.dashboard.GameDetails;
import org.entropy.skrupeltng.modules.dashboard.GameDetailsMapper;
import org.entropy.skrupeltng.modules.dashboard.GameListResult;
import org.entropy.skrupeltng.modules.dashboard.GameMapper;
import org.entropy.skrupeltng.modules.dashboard.GameSearchParameters;
import org.entropy.skrupeltng.modules.dashboard.LoginSearchResultJSON;
import org.entropy.skrupeltng.modules.dashboard.NewGameRequest;
import org.entropy.skrupeltng.modules.dashboard.database.GalaxyConfigRepository;
import org.entropy.skrupeltng.modules.ingame.database.Game;
import org.entropy.skrupeltng.modules.ingame.database.GameRepository;
import org.entropy.skrupeltng.modules.ingame.database.Player;
import org.entropy.skrupeltng.modules.ingame.database.PlayerRepository;
import org.entropy.skrupeltng.modules.ingame.service.round.RoundCalculationService;
import org.entropy.skrupeltng.modules.login.database.Login;
import org.entropy.skrupeltng.modules.login.database.LoginRepository;
import org.entropy.skrupeltng.modules.masterdata.database.RaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DashboardService {

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private GalaxyConfigRepository galaxyConfigRepository;

	@Autowired
	private RaceRepository raceRepository;

	@Autowired
	private GameStartingHelper gameStartingHelper;

	@Autowired
	private RoundCalculationService roundCalculationService;

	@Autowired
	private GameMapper gameMapper;

	@Autowired
	private GameDetailsMapper gameDetailsMapper;

	public Page<GameListResult> searchGames(GameSearchParameters params, Pageable page) {
		return gameRepository.searchGames(params, page);
	}

	public List<String> getGalaxyConfigsIds() {
		return galaxyConfigRepository.getIds();
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public long createdNewGame(NewGameRequest request, long loginId) {
		Game game = gameMapper.newGameRequestToGame(request);
		game.setGalaxyConfig(galaxyConfigRepository.getOne(request.getGalaxyConfigId()));
		game.setPlasmaStormProbability(request.getPlasmaStormProbability() / 100.0f);

		Login creator = loginRepository.getOne(loginId);
		game.setCreator(creator);

		game = gameRepository.save(game);
		playerRepository.save(new Player(game, creator));

		return game.getId();
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void updateGame(GameDetails request, long loginId) {
		Optional<Game> gameOpt = gameRepository.findById(request.getId());

		if (!gameOpt.isPresent()) {
			return;
		}

		Game game = gameOpt.get();

		if (game.isStarted()) {
			throw new IllegalArgumentException("Game " + request.getId() + " has already started and cannot be changed anymore!");
		}

		gameDetailsMapper.updateGame(request, game);
		game.setGalaxyConfig(galaxyConfigRepository.getOne(request.getGalaxyConfigId()));
		gameRepository.save(game);
	}

	public Optional<Game> getGame(long id) {
		return gameRepository.findById(id);
	}

	public List<LoginSearchResultJSON> searchLogins(String name) {
		List<Login> logins = loginRepository.searchByUsername(name.toLowerCase() + "%");
		return logins.stream().map(l -> new LoginSearchResultJSON(l.getId(), l.getUsername())).collect(Collectors.toList());
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void selectRaceForLogin(long gameId, String race, long loginId) {
		Optional<Player> playerOptional = playerRepository.findByGameIdAndLoginId(gameId, loginId);

		if (playerOptional.isPresent()) {
			Player player = playerOptional.get();
			player.setRace(raceRepository.getOne(race));
			playerRepository.save(player);
		} else {
			throw new RuntimeException("Login " + loginId + " not found for game " + gameId + "!");
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void selectRaceForPlayer(String race, long playerId) {
		Optional<Player> playerOptional = playerRepository.findById(playerId);

		if (playerOptional.isPresent()) {
			Player player = playerOptional.get();
			player.setRace(raceRepository.getOne(race));
			playerRepository.save(player);
		} else {
			throw new RuntimeException("Player " + playerId + " not found!");
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public long addPlayer(long gameId, long loginId) throws GameFullException {
		Game game = gameRepository.getOne(gameId);

		List<Player> players = game.getPlayers();

		if (players != null && players.size() == game.getPlayerCount()) {
			throw new GameFullException(game.getPlayerCount());
		}

		Login login = loginRepository.getOne(loginId);
		Player player = new Player(game, login);

		try {
			AILevel aiLevel = AILevel.valueOf(login.getUsername());
			player.setAiLevel(aiLevel);
		} catch (Exception e) {

		}

		player = playerRepository.save(player);
		return player.getId();
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void removePlayerFromGame(long gameId, long playerId) {
		playerRepository.deleteById(playerId);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void startGame(long gameId) {
		gameStartingHelper.startGame(gameId);

		roundCalculationService.computeAIRounds(gameId);
	}
}