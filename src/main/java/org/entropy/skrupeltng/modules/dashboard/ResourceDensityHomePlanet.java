package org.entropy.skrupeltng.modules.dashboard;

public enum ResourceDensityHomePlanet {

	EXTREM(2000, 3000), HIGH(1500, 2500), MEDIUM(1000, 2000), LOW(500, 1000);

	private int min;
	private int max;

	private ResourceDensityHomePlanet(int min, int max) {
		this.min = min;
		this.max = max;
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}
}