package org.entropy.skrupeltng.modules.dashboard.database;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "galaxy_config")
public class GalaxyConfig implements Serializable {

	private static final long serialVersionUID = 1722146672966127363L;

	@Id
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}