package org.entropy.skrupeltng.modules.dashboard.service;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import org.entropy.skrupeltng.modules.dashboard.ResourceDensityHomePlanet;
import org.entropy.skrupeltng.modules.ingame.database.FogOfWarType;
import org.entropy.skrupeltng.modules.ingame.database.Game;
import org.entropy.skrupeltng.modules.ingame.database.GameRepository;
import org.entropy.skrupeltng.modules.ingame.database.HomePlanetSetup;
import org.entropy.skrupeltng.modules.ingame.database.Player;
import org.entropy.skrupeltng.modules.ingame.database.PlayerRepository;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystem;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.OrbitalSystemRepository;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.PlanetType;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.PlanetTypeRepository;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetScanLog;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.PlayerPlanetScanLogRepository;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbaseRepository;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.entropy.skrupeltng.modules.ingame.service.CoordHelper;
import org.entropy.skrupeltng.modules.masterdata.database.Race;
import org.entropy.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class GameStartingHelper {

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private StarbaseRepository starbaseRepository;

	@Autowired
	private PlanetTypeRepository planetTypeRepository;

	@Autowired
	private OrbitalSystemRepository orbitalSystemRepository;

	@Autowired
	private PlayerPlanetScanLogRepository playerPlanetScanLogRepository;

	@Autowired
	private MasterDataService masterDataService;

	@Autowired
	private PlanetGenerator planetGenerator;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void startGame(long gameId) {
		Game game = gameRepository.getOne(gameId);
		game.setStarted(true);

		planetGenerator.generatePlanets(game);

		assignStartPositions(game);
		assignPlayerColors(game);

		if (game.getFogOfWarType() == FogOfWarType.LONG_RANGE_SENSORS) {
			List<Planet> planets = planetRepository.findByGameId(gameId);
			List<Planet> ownedPlanets = planetRepository.findOwnedPlanetsByGameId(gameId);

			for (Planet ownedPlanet : ownedPlanets) {
				planets.stream()
						.filter(p -> CoordHelper.getDistance(p, ownedPlanet) <= 250)
						.forEach(p -> {
							PlayerPlanetScanLog log = new PlayerPlanetScanLog();
							log.setPlanet(p);
							log.setPlayer(ownedPlanet.getPlayer());
							playerPlanetScanLogRepository.save(log);
						});
			}
		}

		gameRepository.save(game);
	}

	private void assignStartPositions(Game game) {
		List<Player> players = new ArrayList<>(game.getPlayers());
		Collections.sort(players);
		List<Planet> playerPlanets = new ArrayList<>(players.size());
		List<Planet> planets = new ArrayList<>(game.getPlanets());
		Collections.sort(planets);

		AtomicInteger expectedPlayerDistance = new AtomicInteger(0);

		if (game.getPlayerCount() > 1) {
			expectedPlayerDistance.set(game.getGalaxySize() / (game.getPlayerCount() - 1));
		}

		Random random = MasterDataService.RANDOM;

		for (Player player : players) {
			if (playerPlanets.isEmpty()) {
				Planet planet = planets.get(random.nextInt(planets.size()));
				addOwnedPlanet(game, playerPlanets, player, planet);
			} else {
				while (true) {
					Planet planet = planets.get(random.nextInt(planets.size()));
					Point newPoint = new Point(planet.getX(), planet.getY());
					boolean hasNearPlanets = playerPlanets.stream().anyMatch(p -> new Point(p.getX(), p.getY()).distance(newPoint) < expectedPlayerDistance.get());

					if (!hasNearPlanets) {
						addOwnedPlanet(game, playerPlanets, player, planet);
						break;
					}

					expectedPlayerDistance.decrementAndGet();
				}
			}
		}
	}

	private void addOwnedPlanet(Game game, List<Planet> playerPlanets, Player player, Planet planet) {
		Random random = MasterDataService.RANDOM;

		planet.setPlayer(player);
		playerPlanets.add(planet);

		Race race = player.getRace();
		planet.setName(race.getHomePlanetName());
		planet.setType(race.getPreferredPlanetType());
		planet.setTemperature(race.getPreferredTemperature());

		PlanetType planetType = planetTypeRepository.getOne(planet.getType());
		String image = planetType.getImagePrefix() + "_" + (1 + random.nextInt(planetType.getImageCount()));
		planet.setImage(image);

		int colonists = random.nextInt(1000) + (random.nextInt(1000) * 5) + 50000;
		planet.setColonists(colonists);
		planet.setMoney(game.getInitMoney());
		planet.setSupplies(5);
		planet.setMines(5);
		planet.setFactories(5);
		planet.setPlanetaryDefense(5);
		planet.setFuel(50 + random.nextInt(20));
		planet.setMineral1(50 + random.nextInt(20));
		planet.setMineral2(50 + random.nextInt(20));
		planet.setMineral3(50 + random.nextInt(20));

		ResourceDensityHomePlanet density = game.getResourceDensityHomePlanet();

		int minResources = density.getMin();
		int resourceDiff = density.getMax() - minResources;
		planet.setUntappedFuel(minResources + random.nextInt(resourceDiff));
		planet.setUntappedMineral1(minResources + random.nextInt(resourceDiff));
		planet.setUntappedMineral2(minResources + random.nextInt(resourceDiff));
		planet.setUntappedMineral3(minResources + random.nextInt(resourceDiff));

		planet.setNecessaryMinesForOneFuel(masterDataService.getNecessaryMinesByIndex(3 + random.nextInt(2)));
		planet.setNecessaryMinesForOneMineral1(masterDataService.getNecessaryMinesByIndex(3 + random.nextInt(2)));
		planet.setNecessaryMinesForOneMineral2(masterDataService.getNecessaryMinesByIndex(3 + random.nextInt(2)));
		planet.setNecessaryMinesForOneMineral3(masterDataService.getNecessaryMinesByIndex(3 + random.nextInt(2)));

		HomePlanetSetup homePlanetSetup = game.getHomePlanetSetup();

		if (homePlanetSetup == HomePlanetSetup.STAR_BASE) {
			Starbase starbase = new Starbase();
			starbase.setName("Starbase 1");
			starbase.setType(StarbaseType.STAR_BASE);
			starbase = starbaseRepository.save(starbase);
			planet.setStarbase(starbase);
		}

		orbitalSystemRepository.deleteByPlanetId(planet.getId());

		List<OrbitalSystem> orbitalSystems = new ArrayList<>(4);
		orbitalSystems.add(new OrbitalSystem(planet));
		orbitalSystems.add(new OrbitalSystem(planet));
		orbitalSystems.add(new OrbitalSystem(planet));
		orbitalSystems.add(new OrbitalSystem(planet));
		orbitalSystemRepository.saveAll(orbitalSystems);

		planet = planetRepository.save(planet);
		player.setHomePlanet(planet);
		playerRepository.save(player);
	}

	private void assignPlayerColors(Game game) {
		List<Player> players = game.getPlayers();

		float interval = 1f / players.size();
		float hue = 0f;

		for (Player player : players) {
			Color cc = Color.getHSBColor(hue, 1f, 1f);
			String colorString = String.format("%02x%02x%02x", cc.getRed(), cc.getGreen(), cc.getBlue());
			player.setColor(colorString);
			hue += interval;
		}

		playerRepository.saveAll(players);
	}
}