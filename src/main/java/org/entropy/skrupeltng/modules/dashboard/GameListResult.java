package org.entropy.skrupeltng.modules.dashboard;

import java.io.Serializable;
import java.util.Date;

public class GameListResult implements Serializable {

	private static final long serialVersionUID = -6813910842222539718L;

	private long id;
	private String name;
	private Date created;
	private boolean started;
	private boolean finished;
	private int playerCount;
	private int round;
	private boolean playerOfGame;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public boolean isStarted() {
		return started;
	}

	public void setStarted(boolean started) {
		this.started = started;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public int getPlayerCount() {
		return playerCount;
	}

	public void setPlayerCount(int playerCount) {
		this.playerCount = playerCount;
	}

	public int getRound() {
		return round;
	}

	public void setRound(int round) {
		this.round = round;
	}

	public boolean isPlayerOfGame() {
		return playerOfGame;
	}

	public void setPlayerOfGame(boolean playerOfGame) {
		this.playerOfGame = playerOfGame;
	}
}