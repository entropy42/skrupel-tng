package org.entropy.skrupeltng.modules.dashboard;

import org.entropy.skrupeltng.modules.ingame.database.Game;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface GameMapper {

	GameMapper INSTANCE = Mappers.getMapper(GameMapper.class);

	@Mapping(source = "plasmaStormProbability", target = "plasmaStormProbability", ignore = true)
	Game newGameRequestToGame(NewGameRequest request);
}