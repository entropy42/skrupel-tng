package org.entropy.skrupeltng.modules.dashboard;

public class GameSearchParameters {

	private final Boolean started;
	private final Boolean onlyOwnGames;
	private final long currentLoginId;

	public GameSearchParameters(Boolean started, Boolean onlyOwnGames, long currentLoginId) {
		this.started = started;
		this.onlyOwnGames = onlyOwnGames;
		this.currentLoginId = currentLoginId;
	}

	public Boolean getStarted() {
		return started;
	}

	public Boolean getOnlyOwnGames() {
		return onlyOwnGames;
	}

	public long getCurrentLoginId() {
		return currentLoginId;
	}
}