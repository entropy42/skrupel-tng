package org.entropy.skrupeltng.modules.dashboard.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.entropy.skrupeltng.exceptions.ResourceNotFoundException;
import org.entropy.skrupeltng.modules.dashboard.GameDetails;
import org.entropy.skrupeltng.modules.dashboard.GameDetailsMapper;
import org.entropy.skrupeltng.modules.dashboard.GameListResult;
import org.entropy.skrupeltng.modules.dashboard.GameSearchParameters;
import org.entropy.skrupeltng.modules.dashboard.LoginSearchResultJSON;
import org.entropy.skrupeltng.modules.dashboard.NewGameRequest;
import org.entropy.skrupeltng.modules.dashboard.service.GameFullException;
import org.entropy.skrupeltng.modules.ingame.database.Game;
import org.entropy.skrupeltng.modules.ingame.service.IngameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping({ "/dashboard", "" })
public class DashboardController extends AbstractGameController {

	@Autowired
	private IngameService gameService;

	@Autowired
	private GameDetailsMapper gameDetailsMapper;

	@GetMapping({ "/existing-games", "" })
	public String getDashboard(
			@RequestParam(required = false) Boolean onlyOwnGames,
			@RequestParam(required = false) Boolean started,

			@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer pageSize,
			@RequestParam(required = false) String sortField,
			@RequestParam(required = false) Boolean sortDirection,
			Model model) {

		Pageable pageRequest = createPageable(page, pageSize, sortField, sortDirection);

		long playerId = userDetailService.getLoginId();

		GameSearchParameters params = new GameSearchParameters(started, onlyOwnGames, playerId);
		Page<GameListResult> results = dashboardService.searchGames(params, pageRequest);

		model.addAttribute("results", results);

		return "dashboard/existing-games";
	}

	@GetMapping("/new-game")
	public String getNewGame(Model model) {
		NewGameRequest request = NewGameRequest.createDefaultRequest();
		model.addAttribute("request", request);
		model.addAttribute("canEdit", true);

		prepareNewGameModel(model);
		return "dashboard/new-game";
	}

	@PostMapping("/new-game")
	public String createNewGame(@Valid @ModelAttribute("request") NewGameRequest request, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			prepareNewGameModel(model);
			model.addAttribute("canEdit", true);
			return "dashboard/new-game";
		}

		long gameId = dashboardService.createdNewGame(request, userDetailService.getLoginId());

		return "redirect:game?id=" + gameId;
	}

	@GetMapping("/game")
	public String getGame(@RequestParam(required = true) long id, Model model) {
		Optional<Game> optional = gameService.getGame(id);

		if (optional.isPresent()) {
			Game game = optional.get();
			GameDetails gameDetails = gameDetailsMapper.newGameToGameDetails(game);
			prepareNewGameModel(model);
			prepareGameDetailsModel(id, model, game, gameDetails);

			return "dashboard/game";
		}

		throw new ResourceNotFoundException();
	}

	@PostMapping("/game/race")
	@ResponseBody
	public void selectRace(@RequestParam(required = true) long gameId, @RequestParam(required = true) String race) {
		dashboardService.selectRaceForLogin(gameId, race, userDetailService.getLoginId());
	}

	@PutMapping("/game/race")
	@ResponseBody
	public void setRaceForPlayer(@RequestParam(required = true) String race, @RequestParam(required = true) long playerId) {
		dashboardService.selectRaceForPlayer(race, playerId);
	}

	@DeleteMapping("/game/player")
	@ResponseBody
	public void removePlayer(@RequestParam(required = true) long gameId, @RequestParam(required = true) long playerId) {
		dashboardService.removePlayerFromGame(gameId, playerId);
	}

	@PostMapping("/game/player")
	@ResponseBody
	public String joinGame(@RequestParam long gameId) {
		try {
			dashboardService.addPlayer(gameId, userDetailService.getLoginId());
			return null;
		} catch (GameFullException e) {
			return getGameFullMessage(e);
		}
	}

	@PutMapping("/game/player")
	@ResponseBody
	public String addPlayer(@RequestParam long gameId, @RequestParam long loginId) {
		try {
			dashboardService.addPlayer(gameId, loginId);
			return null;
		} catch (GameFullException e) {
			return getGameFullMessage(e);
		}
	}

	private String getGameFullMessage(GameFullException e) {
		return messageSource.getMessage("game_full", new Integer[] { Integer.valueOf(e.getMaxPlayerCount()) }, LocaleContextHolder.getLocale());
	}

	@PostMapping("/start-game")
	@ResponseBody
	public void startGame(@RequestParam long gameId) {
		dashboardService.startGame(gameId);
	}

	@GetMapping("/game/players")
	@ResponseBody
	public List<LoginSearchResultJSON> searchPlayers(@RequestParam String name) {
		return dashboardService.searchLogins(name);
	}
}