package org.entropy.skrupeltng.modules.dashboard.controller;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.entropy.skrupeltng.modules.AbstractController;
import org.entropy.skrupeltng.modules.dashboard.GameDetails;
import org.entropy.skrupeltng.modules.dashboard.ResourceDensity;
import org.entropy.skrupeltng.modules.dashboard.ResourceDensityHomePlanet;
import org.entropy.skrupeltng.modules.dashboard.service.DashboardService;
import org.entropy.skrupeltng.modules.ingame.database.FogOfWarType;
import org.entropy.skrupeltng.modules.ingame.database.Game;
import org.entropy.skrupeltng.modules.ingame.database.HomePlanetSetup;
import org.entropy.skrupeltng.modules.ingame.database.LoseCondition;
import org.entropy.skrupeltng.modules.ingame.database.Player;
import org.entropy.skrupeltng.modules.ingame.database.StableWormholeConfig;
import org.entropy.skrupeltng.modules.ingame.database.StartPositionSetup;
import org.entropy.skrupeltng.modules.ingame.database.WinCondition;
import org.entropy.skrupeltng.modules.ingame.modules.overview.service.OverviewService;
import org.entropy.skrupeltng.modules.ingame.modules.overview.service.PlayerSummaryEntry;
import org.entropy.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.ui.Model;

public class AbstractGameController extends AbstractController {

	@Autowired
	protected DashboardService dashboardService;

	@Autowired
	protected OverviewService overviewService;

	@Autowired
	protected MasterDataService masterDataService;

	protected void prepareNewGameModel(Model model) {
		model.addAttribute("resourceDensities", ResourceDensity.values());
		model.addAttribute("resourceDensityHomePlanets", ResourceDensityHomePlanet.values());
		model.addAttribute("winConditions", WinCondition.values());
		model.addAttribute("startPositionSetups", StartPositionSetup.values());
		model.addAttribute("homePlanetSetups", HomePlanetSetup.values());
		model.addAttribute("loseConditions", LoseCondition.values());
		model.addAttribute("galaxyConfigs", dashboardService.getGalaxyConfigsIds());
		model.addAttribute("stableWormholeConfigs", StableWormholeConfig.values());
		model.addAttribute("fogOfWarTypes", FogOfWarType.values());
	}

	protected void prepareGameDetailsModel(long id, Model model, Game game, GameDetails gameDetails) {
		gameDetails.setGalaxyConfigId(game.getGalaxyConfig().getId());

		List<Player> players = game.getPlayers();
		Collections.sort(players);

		model.addAttribute("game", gameDetails);
		model.addAttribute("races", masterDataService.getAllRaceNames());
		model.addAttribute("players", players);

		long currentLoginId = userDetailService.getLoginId();
		model.addAttribute("currentLoginId", currentLoginId);
		model.addAttribute("canEdit", !game.isStarted() && game.getCreator().getId() == currentLoginId);

		Optional<Player> currentPlayerOpt = players.stream().filter(p -> p.getLogin().getId() == currentLoginId).findAny();
		boolean hasLost = false;

		if (currentPlayerOpt.isPresent()) {
			hasLost = currentPlayerOpt.get().isHasLost();

			if (hasLost) {
				String loseText = messageSource.getMessage("lose_text_" + game.getLoseCondition().name(), null, LocaleContextHolder.getLocale());
				model.addAttribute("loseText", loseText);

				addPlayerSummaries(id, model);
			}
		}

		model.addAttribute("playerLost", hasLost);

		boolean showStartButton = game.getCreator().getId() == currentLoginId && !game.isStarted();
		model.addAttribute("showStartButton", showStartButton);

		boolean playerTakesPartInGame = game.getPlayers().stream().anyMatch(l -> l.getLogin().getId() == currentLoginId);
		model.addAttribute("showPlayerButton", game.isStarted() && playerTakesPartInGame);
		model.addAttribute("showJoinGameButton", !showStartButton && !game.isStarted() && !playerTakesPartInGame);

		model.addAttribute("gameFull", game.getPlayerCount() == game.getPlayers().size());

		if (game.isFinished()) {
			addPlayerSummaries(id, model);

			List<Player> winners = players.stream().filter(p -> !p.isHasLost()).collect(Collectors.toList());
			WinCondition winCondition = game.getWinCondition();

			Object[] args = null;

			if (winCondition == WinCondition.SURVIVE) {
				args = new Object[] { winners.get(0).getLogin().getUsername() };
			} else if (winCondition == WinCondition.DEATH_FOE || winCondition == WinCondition.TEAM_DEATH_FOE) {
				args = new Object[2];
			}

			String winnerText = messageSource.getMessage("game_finished_text_" + winCondition.name(), args, LocaleContextHolder.getLocale());
			model.addAttribute("winnerText", winnerText);
		}
	}

	private void addPlayerSummaries(long id, Model model) {
		List<PlayerSummaryEntry> playerSummaries = overviewService.getPlayerSummaries(id);
		model.addAttribute("playerSummaries", playerSummaries);
	}
}