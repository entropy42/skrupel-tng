package org.entropy.skrupeltng.modules.dashboard;

import java.io.Serializable;

public class LoginSearchResultJSON implements Serializable {

	private static final long serialVersionUID = 1343093528299157442L;

	private long id;
	private String playerName;

	public LoginSearchResultJSON() {

	}

	public LoginSearchResultJSON(long id, String playerName) {
		this.id = id;
		this.playerName = playerName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}
}