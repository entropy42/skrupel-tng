package org.entropy.skrupeltng.modules.dashboard.controller;

import javax.validation.Valid;

import org.entropy.skrupeltng.modules.dashboard.GameDetails;
import org.entropy.skrupeltng.modules.ingame.database.Game;
import org.entropy.skrupeltng.modules.ingame.service.IngameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UpdateGameController extends AbstractGameController {

	@Autowired
	private IngameService gameService;

	@Autowired
	private GameUpdateValidator gameUpdateValidator;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(gameUpdateValidator);
	}

	@PostMapping("/dashboard/game")
	public String updateGame(@Valid @ModelAttribute("game") GameDetails request, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			Game game = gameService.getGame(request.getId()).get();
			prepareNewGameModel(model);
			prepareGameDetailsModel(game.getId(), model, game, request);
			prepareNewGameModel(model);
			return "dashboard/game";
		}

		dashboardService.updateGame(request, userDetailService.getLoginId());

		return "redirect:game?id=" + request.getId() + "#options";
	}
}