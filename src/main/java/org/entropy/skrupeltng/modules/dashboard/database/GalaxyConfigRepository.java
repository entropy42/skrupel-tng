package org.entropy.skrupeltng.modules.dashboard.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface GalaxyConfigRepository extends JpaRepository<GalaxyConfig, String> {

	@Query("SELECT id FROM GalaxyConfig ORDER BY id ASC")
	List<String> getIds();
}