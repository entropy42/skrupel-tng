package org.entropy.skrupeltng.modules.dashboard;

import org.entropy.skrupeltng.modules.ingame.database.Game;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface GameDetailsMapper {

	GameDetailsMapper INSTANCE = Mappers.getMapper(GameDetailsMapper.class);

	@Mapping(source = "creator.username", target = "creator")
	@Mapping(source = "creator.id", target = "creatorId")
	GameDetails newGameToGameDetails(Game request);

	@Mapping(source = "creator", target = "creator", ignore = true)
	@Mapping(source = "id", target = "id", ignore = true)
	@Mapping(source = "started", target = "started", ignore = true)
	@Mapping(source = "finished", target = "finished", ignore = true)
	void updateGame(GameDetails details, @MappingTarget Game game);
}