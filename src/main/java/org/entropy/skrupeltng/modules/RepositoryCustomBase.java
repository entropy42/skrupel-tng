package org.entropy.skrupeltng.modules;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public class RepositoryCustomBase {

	@Autowired
	protected NamedParameterJdbcTemplate jdbcTemplate;

	protected String where(List<String> wheres) {
		if (!wheres.isEmpty()) {
			return "WHERE \n	" + StringUtils.join(wheres, " \n	AND ");
		}

		return "";
	}

	protected <T> Page<T> search(String sql, Map<String, Object> params, Pageable pageable, RowMapper<T> rowMapper) {
		List<T> page = jdbcTemplate.query(sql, params, rowMapper);

		int totalElements = page.size();

		if (page.size() > pageable.getPageSize()) {
			int start = (int)pageable.getOffset();
			int end = (int)(pageable.getOffset() + pageable.getPageSize());

			if (start >= page.size()) {
				start = 0;
			}

			if (end >= page.size()) {
				end = page.size() - 1;
			}

			page = page.subList(start, end);
		}

		return new PageImpl<>(page, pageable, totalElements);
	}
}