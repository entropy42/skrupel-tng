package org.entropy.skrupeltng.modules.login.service;

import org.entropy.skrupeltng.modules.ai.AILevel;
import org.entropy.skrupeltng.modules.login.InitSetupRequest;
import org.entropy.skrupeltng.modules.login.RegisterRequest;
import org.entropy.skrupeltng.modules.login.Roles;
import org.entropy.skrupeltng.modules.login.database.Login;
import org.entropy.skrupeltng.modules.login.database.LoginRepository;
import org.entropy.skrupeltng.modules.login.database.LoginRole;
import org.entropy.skrupeltng.modules.login.database.LoginRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class LoginService {

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private LoginRoleRepository loginRoleRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	public boolean noPlayerExist() {
		return loginRepository.count() == 0L;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public long createAdmin(InitSetupRequest request) {
		Login admin = new Login();
		admin.setUsername(request.getUsername());
		admin.setPassword(passwordEncoder.encode(request.getPassword()));
		admin = loginRepository.save(admin);

		loginRoleRepository.save(new LoginRole(admin, Roles.ADMIN));
		loginRoleRepository.save(new LoginRole(admin, Roles.PLAYER));

		createAILogin(AILevel.EASY);
		createAILogin(AILevel.MEDIUM);
		createAILogin(AILevel.HARD);

		return admin.getId();
	}

	private void createAILogin(AILevel level) {
		Login aiLogin = new Login();
		aiLogin.setUsername(level.name());
		aiLogin.setPassword(passwordEncoder.encode(level.name()));
		aiLogin = loginRepository.save(aiLogin);

		loginRoleRepository.save(new LoginRole(aiLogin, Roles.PLAYER));
		loginRoleRepository.save(new LoginRole(aiLogin, Roles.AI));
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void registerNewLogin(RegisterRequest request) {
		Login login = new Login();
		login.setUsername(request.getUsername());
		login.setPassword(passwordEncoder.encode(request.getPassword()));
		login = loginRepository.save(login);

		loginRoleRepository.save(new LoginRole(login, Roles.PLAYER));
	}
}