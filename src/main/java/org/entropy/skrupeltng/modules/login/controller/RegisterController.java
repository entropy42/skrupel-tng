package org.entropy.skrupeltng.modules.login.controller;

import javax.validation.Valid;

import org.entropy.skrupeltng.modules.AbstractController;
import org.entropy.skrupeltng.modules.login.RegisterRequest;
import org.entropy.skrupeltng.modules.login.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class RegisterController extends AbstractController {

	@Autowired
	private LoginService loginService;

	@Autowired
	private RegistrationValidator registrationValidator;

	@InitBinder
	protected void initBinder(final WebDataBinder binder) {
		binder.addValidators(registrationValidator);
	}

	@GetMapping("register")
	public String register(Model model) {
		if (loginService.noPlayerExist()) {
			return "redirect:init-setup";
		}

		model.addAttribute("request", new RegisterRequest());

		return "login/register";
	}

	@PostMapping("register")
	public String register(@Valid @ModelAttribute("request") RegisterRequest request, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			return "login/register";
		}

		loginService.registerNewLogin(request);

		return "redirect:dashboard";
	}
}