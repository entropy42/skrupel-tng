package org.entropy.skrupeltng.modules.login.database;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LoginRoleRepository extends JpaRepository<LoginRole, Long> {

}