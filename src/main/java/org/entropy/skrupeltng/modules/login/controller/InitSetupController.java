package org.entropy.skrupeltng.modules.login.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.entropy.skrupeltng.modules.AbstractController;
import org.entropy.skrupeltng.modules.ai.AILevel;
import org.entropy.skrupeltng.modules.login.InitSetupRequest;
import org.entropy.skrupeltng.modules.login.service.DebugGameCreator;
import org.entropy.skrupeltng.modules.login.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class InitSetupController extends AbstractController {

	@Value("${skr.debug:false}")
	private boolean debug;

	@Value("${skr.debugLoginCount:8}")
	private int debugLoginCount;

	@Autowired
	private LoginService loginService;

	@Autowired
	private DebugGameCreator debugGameCreator;

	@Autowired
	private RememberMeServices rememberMeService;

	@GetMapping("init-setup")
	public String initSetup(Model model) {
		if (!loginService.noPlayerExist()) {
			return "redirect:login";
		}

		model.addAttribute("request", new InitSetupRequest());

		return "login/init-setup";
	}

	@PostMapping("init-setup")
	public String initSetup(@Valid @ModelAttribute("request") InitSetupRequest request, BindingResult bindingResult, Model model, HttpServletRequest req, HttpServletResponse resp) {
		if (loginService.noPlayerExist()) {
			if (bindingResult.hasErrors()) {
				return "login/init-setup";
			}

			long adminLoginId = loginService.createAdmin(request);

			if (debug) {
				long gameId = debugGameCreator.createDebugGame(adminLoginId);
				debugGameCreator.setPlayerRaceForDebugGame(1L);

				for (int i = 1; i <= debugLoginCount; i++) {
					long playerId = debugGameCreator.addAIPlayerToDebugGame(gameId, AILevel.EASY);
					debugGameCreator.setPlayerRaceForDebugGame(playerId);
				}

				debugGameCreator.startDebugGame(gameId);
				debugGameCreator.addDebugShips(gameId);
			}

			UsernamePasswordAuthenticationToken authReq = new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword());
			SecurityContext sc = SecurityContextHolder.getContext();
			sc.setAuthentication(authReq);

			rememberMeService.loginSuccess(req, resp, authReq);

			return "redirect:dashboard";
		}

		return "redirect:login";
	}
}