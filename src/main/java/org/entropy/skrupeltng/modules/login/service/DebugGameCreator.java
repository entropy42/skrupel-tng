package org.entropy.skrupeltng.modules.login.service;

import java.util.List;

import org.entropy.skrupeltng.modules.ai.AILevel;
import org.entropy.skrupeltng.modules.dashboard.NewGameRequest;
import org.entropy.skrupeltng.modules.dashboard.service.DashboardService;
import org.entropy.skrupeltng.modules.dashboard.service.GameFullException;
import org.entropy.skrupeltng.modules.ingame.database.Player;
import org.entropy.skrupeltng.modules.ingame.database.PlayerRepository;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.entropy.skrupeltng.modules.login.database.Login;
import org.entropy.skrupeltng.modules.login.database.LoginRepository;
import org.entropy.skrupeltng.modules.masterdata.database.PropulsionSystemTemplateRepository;
import org.entropy.skrupeltng.modules.masterdata.database.ShipTemplateRepository;
import org.entropy.skrupeltng.modules.masterdata.service.MasterDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class DebugGameCreator {

	@Autowired
	private DashboardService dashboardService;

	@Autowired
	private MasterDataService masterDataService;

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private ShipTemplateRepository shipTemplateRepository;

	@Autowired
	private PropulsionSystemTemplateRepository propulsionSystemTemplateRepository;

	@Value("${skr.debugLoginCount:8}")
	private int debugLoginCount;

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public long createDebugGame(long adminLoginId) {
		NewGameRequest newGameRequest = NewGameRequest.createDefaultRequest();
		newGameRequest.setName("Debug");
		newGameRequest.setPlayerCount(debugLoginCount);
		return dashboardService.createdNewGame(newGameRequest, adminLoginId);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public long addAIPlayerToDebugGame(long gameId, AILevel level) {
		try {
			Login aiLogin = loginRepository.findByUsername(level.name());
			long playerId = dashboardService.addPlayer(gameId, aiLogin.getId());

			Player player = playerRepository.getOne(playerId);
			player.setAiLevel(level);
			player = playerRepository.save(player);

			return player.getId();
		} catch (GameFullException e) {
			e.printStackTrace();
			return 1l;
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void setPlayerRaceForDebugGame(long playerId) {
		List<String> races = masterDataService.getAllRaceNames();
		dashboardService.selectRaceForPlayer(races.get(MasterDataService.RANDOM.nextInt(races.size())), playerId);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void startDebugGame(long gameId) {
		dashboardService.startGame(gameId);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public void addDebugShips(long gameId) {
		List<Player> players = playerRepository.findByGameId(gameId);

		for (Player player : players) {
			Ship ship = new Ship();
			ship.setPlayer(player);
			ship.setShipTemplate(shipTemplateRepository.getOne("Orion_Konglomerat_1"));
			ship.setName("Debug ship");

			Planet planet = playerRepository.getHomePlanet(player.getId());
			ship.setPlanet(planet);
			ship.setX(planet.getX());
			ship.setY(planet.getY());
			ship.setDestinationX(-1);
			ship.setDestinationY(-1);
			ship.setTravelSpeed(0);
			ship.setShield(100);

			ship.setPropulsionSystemTemplate(propulsionSystemTemplateRepository.getOne("solarisplasmotan"));
			shipRepository.save(ship);
		}
	}
}