package org.entropy.skrupeltng.modules.login;

public interface Roles {

	String PLAYER = "ROLE_PLAYER";
	String ADMIN = "ROLE_ADMIN";
	String AI = "ROLE_AI";
}