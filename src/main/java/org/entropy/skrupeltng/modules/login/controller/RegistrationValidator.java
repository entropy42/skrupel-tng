package org.entropy.skrupeltng.modules.login.controller;

import org.entropy.skrupeltng.modules.login.RegisterRequest;
import org.entropy.skrupeltng.modules.login.database.Login;
import org.entropy.skrupeltng.modules.login.database.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class RegistrationValidator implements Validator {

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private LoginRepository playerRepository;

	@Override
	public boolean supports(Class<?> clazz) {
		return RegisterRequest.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		RegisterRequest request = (RegisterRequest)target;

		Login player = playerRepository.findByUsername(request.getUsername());

		if (player != null) {
			errors.rejectValue("username", null, messageSource.getMessage("username_already_exists", null, LocaleContextHolder.getLocale()));
		}

		if (request.getPassword() != null && !request.getPassword().equals(request.getPasswordRepeat())) {
			errors.rejectValue("passwordRepeat", null, messageSource.getMessage("passwords_must_match", null, LocaleContextHolder.getLocale()));
		}
	}
}