package org.entropy.skrupeltng.modules.login.database;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface LoginRepository extends JpaRepository<Login, Long> {

	Login findByUsername(String username);

	@Query("SELECT l FROM Login l WHERE LOWER(l.username) LIKE ?1")
	List<Login> searchByUsername(String name);
}