const ingame = new Ingame();

var shipNavigation = null;

function Ingame() {
	const me = this;

	this.resizeGalaxy = function() {
		skrupel.resize('#skr-game-galaxy-wrapper', [ '#header', '#skr-game-selection' ]);
	};

	$(window).resize(function() {
		me.resizeGalaxy();
	});

	const gameId = new URLSearchParams(window.location.search).get('id');
	
	this.showOverview = function() {
		$('#skr-ingame-overview-modal-wrapper').load('overview?gameId=' + gameId, function() {
			$('#skr-ingame-overview-modal').modal('show');
		});
	};
	
	this.finishTurn = function() {
		$.ajax({
			url : 'turn?gameId=' + gameId,
			type : 'POST',
			contentType : 'application/json',
			success : function(response) {
				if (response) {
					$('#skr-ingame-round-calculation-modal').modal('show');
					$.ajax({
						url : 'round?gameId=' + gameId,
						type : 'POST',
						contentType : 'application/json',
						success : function() {
							window.location.reload();
						}
					});
				}
			}
		});
	};
	
	this.selectedPage = null;
	this.selectedId = null;
	
	this.secondHashAlreadyLoaded = false;

	this.select = function(page, id, x, y, updateHash = true, ingamePageForRefresh) {
		if (!shipNavigation || !shipNavigation.courseModeActive) {
			me.selectedPage = page;
			
			if (page.indexOf('-selection') > 0) {
				let url = page + '?gameId=' + gameId;
				
				if (x && y) {
					url = url + '&x=' + x + '&y=' + y;
				}
				
				$('#skr-game-selection').load(url, function() {
					window.location.hash = page;
				});
			} else {			
				me.selectedId = id;
		
				$('#skr-game-selection').load(page + '?' + page + 'Id=' + id, function() {
					if (!ingamePageForRefresh) {
						const wrapper = $('#skr-game-galaxy-wrapper');
						wrapper.scrollLeft(x);
						wrapper.scrollTop(y - (wrapper.innerHeight()) / 2);
					}
					
					if (updateHash === true) {
						window.location.hash = me.getMainUrlHash();
					} else if(ingamePageForRefresh) {
						ingamePageForRefresh.show(ingame.getSecondUrlHash());
					}
				});
			}
		}
	};

	this.setSecondUrlHash = function(page) {
		window.location.hash = me.getMainUrlHash() + ';' + page;
	};

	this.getMainUrlHash = function() {
		return me.selectedPage + '=' + me.selectedId;
	};

	this.getSecondUrlHash = function() {
		const hash = window.location.hash;

		if (hash) {
			const parts = hash.replace('#', '').split(';');

			if (parts.length == 2) {
				return parts[1].split('=');
			}
		}

		return null;
	};
	
	this.refreshSelection = function(ingamePage) {
		me.select(me.selectedPage, me.selectedId, 0, 0, false, ingamePage);
	};
	
	this.drawShipTravelLine = function(shipId) {
		const destinationX = parseInt($('#skr-game-ship-destinationx-' + shipId).val());
		const destinationY = parseInt($('#skr-game-ship-destinationy-' + shipId).val());
		
		if (destinationX >= 0 || destinationY >= 0) {
			const x = parseInt($('#skr-game-ship-x-' + shipId).val());
			const y = parseInt($('#skr-game-ship-y-' + shipId).val());
			const color = '#' + $('#skr-game-playercolor').val();
			
			const elem = me.createLine(x, y, destinationX, destinationY, color);
			
			const wrapper = $('#skr-game-ship-' + shipId + ' .skr-game-ship-line-wrapper');
			wrapper.empty();
			wrapper.append(elem);
		} else {
			me.clearShipTravelLine(shipId);
		}
	};
	
	this.clearShipTravelLine = function(shipId) {
		const wrapper = $('#skr-game-ship-' + shipId + ' .skr-game-ship-line-wrapper');
		wrapper.empty();
	};

	this.createLine = function(x1, y1, x2, y2, color) {
		const a = x1 - x2;
		const b = y1 - y2;
		const c = Math.sqrt(a * a + b * b);
		
		const sx = (x1 + x2) / 2;
		const sy = (y1 + y2) / 2;
		
		const x = sx - c / 2;
		const y = sy;
		
		const alpha = Math.PI - Math.atan2(-b, a);

	    return me.createLineElement(x, y, c, alpha, color);
	};
	
	this.createLineElement = function(x, y, length, angle, color) {
	    const line = document.createElement("div");
	    const styles = 'border: 1px dotted ' + color + '; '
	               + 'width: ' + length + 'px; '
	               + 'height: 0px; '
	               + '-moz-transform: rotate(' + angle + 'rad); '
	               + '-webkit-transform: rotate(' + angle + 'rad); '
	               + '-o-transform: rotate(' + angle + 'rad); '  
	               + '-ms-transform: rotate(' + angle + 'rad); '  
	               + 'position: absolute; '
	               + 'top: ' + y + 'px; '
	               + 'left: ' + x + 'px; ';
	    
	    line.setAttribute('style', styles);  
	    return line;
	};
	
	this.toggleNewsDelete = function(newsEntryId) {
		$.ajax({
			url : 'overview/toggle-news-delete?newsEntryId=' + newsEntryId,
			type : 'POST',
			contentType : 'application/json',
			success : function(newValue) {
				const id = newValue ? '#skr-overview-will-be-deleted-text' : '#skr-overview-will-not-be-deleted-text';
				const text = $(id).val();
				$('#skr-overview-toggle-news-delete-button-' + newsEntryId).html(text);
			}
		});
	};
	
	$(document).on('mouseenter', '.skr-game-planet-mouse', function() {
		const planetId = $(this).attr('planetid');
		$('#skr-ingame-planet-details-' + planetId).show();
	});
	
	$(document).on('mouseleave', '.skr-game-planet-mouse', function() {
		const planetId = $(this).attr('planetid');
		$('#skr-ingame-planet-details-' + planetId).hide();
	});
	
	$(document).on('mouseenter', '.skr-game-ship-mouse', function() {
		const shipId = $(this).attr('shipid');
		$('#skr-ingame-ship-details-' + shipId).show();
	});
	
	$(document).on('mouseleave', '.skr-game-ship-mouse', function() {
		const shipId = $(this).attr('shipid');
		$('#skr-ingame-ship-details-' + shipId).hide();
	});
	
	$(document).on('mouseenter', '.skr-game-ships-mouse', function() {
		const planetId = $(this).attr('planetid');
		$('#skr-ingame-ships-details-' + planetId).show();
	});
	
	$(document).on('mouseleave', '.skr-game-ships-mouse', function() {
		const planetId = $(this).attr('planetid');
		$('#skr-ingame-ships-details-' + planetId).hide();
	});
	
	$(document).on('mouseenter', '.skr-game-starbase-mouse', function() {
		const starbaseId = $(this).attr('starbaseid');
		$('#skr-ingame-starbase-details-' + starbaseId).show();
	});
	
	$(document).on('mouseleave', '.skr-game-starbase-mouse', function() {
		const starbaseId = $(this).attr('starbaseid');
		$('#skr-ingame-starbase-details-' + starbaseId).hide();
	});
	
	$(document).on('mouseenter', '.skr-game-ship-cluster-mouse', function() {
		const coordId = $(this).attr('coordId');
		$('#skr-ingame-ships-details-' + coordId).show();
	});
	
	$(document).on('mouseleave', '.skr-game-ship-cluster-mouse', function() {
		const coordId = $(this).attr('coordId');
		$('#skr-ingame-ships-details-' + coordId).hide();
	});
}

function IngamePage(name) {
	const me = this;

	this.show = function(page) {
		const url = name + '/' + page + '?' + name + 'Id=' + ingame.selectedId;
		
		$('#skr-' + name + '-selection').load(url, function() {
			ingame.setSecondUrlHash(page);
		});
	};
	
	if(!ingame.secondHashAlreadyLoaded) {
		const value = ingame.getSecondUrlHash();
	
		if (value) {
			this.show(value);
			ingame.secondHashAlreadyLoaded = true;
		}
	}
};

$(document).ready(function() {
	const hash = window.location.hash;

	if (hash) {
		const parts = hash.replace('#', '').split(';');
		const first = parts[0].split('=');
		ingame.select(first[0], first[1], false);
	}
	
	$('.skr-game-ship').each(function() {
		const shipId = parseInt($(this).prop('id').replace('skr-game-ship-', ''));
		ingame.drawShipTravelLine(shipId);
	});
	
	ingame.resizeGalaxy();
	
	if ($('#skr-ingame-overview-viewed').val() === 'false') {
		ingame.showOverview();
		
		$.ajax({
			url : 'overview-viewed?gameId=' + new URLSearchParams(window.location.search).get('id'),
			type : 'POST',
			contentType : 'application/json'
		});
	}
});