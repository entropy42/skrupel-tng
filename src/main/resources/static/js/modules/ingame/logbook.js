if (!logbook) {
	var logbook = new Logbook();

	function Logbook() {

		const me = this;

		const id = ingame.selectedId;

		this.saveLogbook = function() {
			const logbook = $('#skr-ingame-logbook-input').val();
			
			const prefix = ingame.selectedPage;
			
			$.ajax({
				type : "POST",
				url : prefix + "/logbook?" + prefix + "Id=" + id + "&logbook=" + logbook,
			});
		};
	}
}