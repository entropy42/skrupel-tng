if (!starbaseProduction) {
	var starbaseProduction = new StarbaseProduction();

	function StarbaseProduction() {

		const me = this;

		const starbaseId = ingame.selectedId;

		this.produce = function(templateId, type) {
			const quantity = $('#skr-starbase-production-quantity-' + templateId).val();
			

			const request = {
				templateId: templateId,
				type : type,
				quantity : quantity
			};

			$.ajax({
				url : 'starbase/produce?starbaseId=' + starbaseId,
				type : 'POST',
				contentType : 'application/json',
				data : JSON.stringify(request),
				success : function() {
					ingame.refreshSelection(starbase);
				}
			});
		};
	}
}