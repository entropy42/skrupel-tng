if (!starbaseUpgrade) {
	var starbaseUpgrade = new StarbaseUpgrade();
	
	function StarbaseUpgrade() {

		const me = this;

		const starbaseId = ingame.selectedId;

		this.performUpgrade = function() {
			const request = {
				hullLevel : $('#skr-starbase-upgrade-hull-select').val(),
				propulsionLevel : $('#skr-starbase-upgrade-propulsion-select').val(),
				energyLevel : $('#skr-starbase-upgrade-energy-select').val(),
				projectileLevel : $('#skr-starbase-upgrade-projectile-select').val()
			};

			$.ajax({
				url : 'starbase/upgrade?starbaseId=' + starbaseId,
				type : 'POST',
				contentType : 'application/json',
				data : JSON.stringify(request),
				success : function() {
					ingame.refreshSelection(starbase);
				}
			});
		};
	}
}