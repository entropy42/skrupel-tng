if (!starbaseShipConstruction) {
	var starbaseShipConstruction = new StarbaseShipConstruction();

	function StarbaseShipConstruction() {

		const me = this;

		const starbaseId = ingame.selectedId;

		this.selectHull = function() {
			const stockId = $('#skr-starbase-shipconstruction-hull-select').val();

			$('#skr-starbase-selection').load('starbase/shipconstruction-details?hullStockId=' + stockId + '&starbaseId=' + starbaseId, function() {

			})
		};

		this.construct = function() {
			const name = $('#skr-starbase-shipconstruction-shipname-input').val();

			if (!name || name.length === 0) {
				$('#skr-starbase-shipconstruction-form').addClass('was-validated');
				$('#skr-starbase-shipconstruction-shipname-input').focus();
			} else {
				const hullId = $('#skr-starbase-shipconstruction-hull-id').val();
				const propulsionSystemId = $('#skr-starbase-shipconstruction-propulsion-select').val();
				const energyWeaponId = $('#skr-starbase-shipconstruction-energy-select').val();
				const projectileWeaponId = $('#skr-starbase-shipconstruction-projectile-select').val();

				const request = {
					name : name,
					hullStockId : hullId,
					propulsionSystemStockId : propulsionSystemId,
					energyStockId : energyWeaponId,
					projectileStockId : projectileWeaponId
				};

				$.ajax({
					url : 'starbase/shipconstruction?starbaseId=' + starbaseId,
					type : 'POST',
					contentType : 'application/json',
					data : JSON.stringify(request),
					success : function(response) {
						$('#skr-starbase-shipconstruction-content').replaceWith(response);
					}
				});
			}
		};
	}
}