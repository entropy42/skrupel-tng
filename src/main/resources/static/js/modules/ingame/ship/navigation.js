if (!shipNavigation) {
	var shipNavigation = new ShipNavigation();

	function ShipNavigation() {

		const me = this;

		this.courseModeActive = false;
		this.courseSaved = false;

		this.refreshData = function() {
			me.shipId = ingame.selectedId;

			me.mass = parseInt($('#skr-ship-courseselect-mass').val());
			me.shipName = parseInt($('#skr-ship-courseselect-shipname').val());
			me.shipX = parseInt($('#skr-ship-courseselect-shipx').val());
			me.shipY = parseInt($('#skr-ship-courseselect-shipy').val());

			me.originDestinationX = $('#skr-game-ship-destinationx-' + me.shipId).val();
			me.originDestinationY = $('#skr-game-ship-destinationy-' + me.shipId).val();

			me.destinationX = -1;
			me.destinationY = -1;

			me.fuelConsumptionData = $('#skr-ship-courseselect-fuelconsumptiondata').val().split(',');
		};

		me.refreshData();

		$(document).on('click', '#skr-ship-courseselect-checkbox', function() {
			me.courseModeActive = $(this).prop('checked');

			if (me.courseModeActive) {
				me.courseSaved = false;
			} else if (!me.courseSaved) {
				$('#skr-game-ship-destinationx-' + me.shipId).val(me.originDestinationX);
				$('#skr-game-ship-destinationy-' + me.shipId).val(me.originDestinationY);
				ingame.drawShipTravelLine(me.shipId);
				ingame.refreshSelection(ship);
			}

			const textSource = me.courseModeActive ? 'skr-ship-courseselect-active-text' : 'skr-ship-courseselect-inactive-text';
			const text = $('#' + textSource).val();
			$('#skr-ship-courseselect-checkbox-label').text(text);
		});

		$(document).on('click', '.skr-game-planet', function(event) {
			if (me.courseModeActive) {
				event.stopPropagation();

				const x = me.getCoord($(this).css('left'));
				const y = me.getCoord($(this).css('top'));
				const name = $(this).attr('name');

				me.selectDestination(x, y, name);
			}
		});

		$(document).on('click', '.ship-in-space', function(event) {
			if (me.courseModeActive) {
				event.stopPropagation();

				const x = me.getCoord($(this).parent().css('left'));
				const y = me.getCoord($(this).parent().css('top'));
				const name = $(this).attr('name');

				me.selectDestination(x, y, name);
			}
		});

		$(document).on('click', '.ship', function(event) {
			if (me.courseModeActive) {
				event.stopPropagation();

				const x = me.getCoord($(this).parent().css('left'));
				const y = me.getCoord($(this).parent().css('top'));
				let name = $(this).attr('name');

				const names = name.split(";");

				if (names.length > 1) {
					for ( let i in names) {
						const n = names[i];

						if (n != me.shipName) {
							name = n;
							break;
						}
					}
				}

				me.selectDestination(x, y, name);
			}
		});

		$(document).on('change', '#skr-ship-courseselect-speed-select', function() {
			me.updateFuelConsumption();
		});

		$(document).on('click', '.skr-ingame-ship-scan-circle-inner', function(event) {
			if (me.courseModeActive) {
				event.stopPropagation();
				const elem = $(this);
				const elemParent = elem.parent();

				const x = me.getCoord(elemParent.css('left')) + event.offsetX - (elem.width() / 2);
				const y = me.getCoord(elemParent.css('top')) + event.offsetY - (elem.height() / 2);
				const name = $('#skr-ship-courseselect-emptyspace-text').val();

				me.selectDestination(x, y, name);
			}
		});

		$(document).on('click', '.skr-ingame-ship-scan-circle', function(event) {
			if (me.courseModeActive) {
				event.stopPropagation();
				const elem = $(this);

				const x = me.getCoord(elem.css('left')) + event.offsetX;
				const y = me.getCoord(elem.css('top')) + event.offsetY;
				const name = $('#skr-ship-courseselect-emptyspace-text').val();

				me.selectDestination(x, y, name);
			}
		});

		$(document).on('click', '#skr-game-galaxy', function(event) {
			if (me.courseModeActive) {
				const elem = $(this);
				const x = event.offsetX;
				const y = event.offsetY;

				if (y <= elem.height() && x <= elem.width()) {
					const name = $('#skr-ship-courseselect-emptyspace-text').val();

					me.selectDestination(x, y, name);
				}
			}
		});

		this.getCoord = function(v) {
			return parseInt(v.replace('px', ''));
		};

		this.selectDestination = function(x, y, name) {
			if (me.courseModeActive) {
				me.destinationX = x;
				me.destinationY = y;

				$('#skr-ship-courseselect-coordinates').text(x + ' / ' + y);
				$('#skr-ship-courseselect-distance').text(me.calculateDistance());
				$('#skr-ship-courseselect-name').text(name);

				me.updateFuelConsumption();

				$('#skr-game-ship-destinationx-' + me.shipId).val(x);
				$('#skr-game-ship-destinationy-' + me.shipId).val(y);
				ingame.drawShipTravelLine(me.shipId);
			}
		};

		this.calculateDistance = function() {
			return Math.round(Math.sqrt(Math.pow(me.destinationX - me.shipX, 2) + Math.pow(me.destinationY - me.shipY, 2)));
		}

		this.getSelectedSpeed = function() {
			return parseInt($('#skr-ship-courseselect-speed-select').val());
		};

		this.getTravelMonths = function() {
			const distance = me.calculateDistance();
			const speed = me.getSelectedSpeed();
			return Math.ceil(distance / (speed * speed));
		};

		this.updateFuelConsumption = function() {
			const months = me.getTravelMonths();
			$('#skr-ship-courseselect-duration').text(months);

			const speed = me.getSelectedSpeed();
			const consumptionPerMonth = parseFloat(me.fuelConsumptionData[speed]);
			let consumption = 0;

			if (consumptionPerMonth > 0) {
				const distance = me.calculateDistance();
				consumption = consumptionPerMonth * (me.mass / 100000);

				if (months <= 1) {
					consumption = Math.floor(distance * consumption);
				} else {
					consumption = Math.floor(speed * speed * consumption);
				}

				if (consumption == 0) {
					consumption = 1;
				}
				
				consumption *= months;
			}

			$('#skr-ship-courseselect-fuelconsumption').text(consumption);
		};

		this.setCourse = function() {
			const fuel = parseInt($('#skr-ship-courseselect-fuel').val());
			const fuelConsumption = parseInt($('#skr-ship-courseselect-fuelconsumption').text());
			const targetName = $('#skr-ship-courseselect-name').text();

			if (fuelConsumption > fuel) {
				window.alert($('#skr-ship-courseselect-notenoughfuel-text').val());
			} else {
				const speed = me.getSelectedSpeed();

				const request = {
					x : me.destinationX,
					y : me.destinationY,
					speed : speed,
					targetName : targetName
				};

				$.ajax({
					url : 'ship/navigation?shipId=' + me.shipId,
					type : 'POST',
					contentType : 'application/json',
					data : JSON.stringify(request),
					success : function(response) {
						if (response.cycleDetected) {
							window.alert($('#skr-ship-courseselect-cycle-detected-text').val());
						} else {
							me.originDestinationX = me.destinationX;
							me.originDestinationY = me.destinationY;
							me.courseSaved = true;
							me.courseModeActive = false;
							ingame.refreshSelection(ship);
						}
					}
				});
			}
		};

		this.deleteCourse = function() {
			$.ajax({
				url : 'ship/navigation?shipId=' + me.shipId,
				type : 'DELETE',
				contentType : 'application/json',
				success : function() {
					me.originDestinationX = -1;
					me.originDestinationY = -1;
					me.courseModeActive = false;
					ingame.refreshSelection(ship);
					ingame.clearShipTravelLine(me.shipId);
				}
			});
		};
	}
}

shipNavigation.refreshData();

$(document).ready(function() {
	if (shipNavigation.getSelectedSpeed() > 0) {
		const x = $('#skr-ship-courseselect-destinationx').val();
		const y = $('#skr-ship-courseselect-destinationy').val();

		if (x >= 0 && y >= 0) {
			shipNavigation.destinationX = x;
			shipNavigation.destinationY = y;

			const name = $('#skr-ship-courseselect-destination-name').val();

			$('#skr-ship-courseselect-coordinates').text(x + ' / ' + y);
			$('#skr-ship-courseselect-distance').text(shipNavigation.calculateDistance());
			$('#skr-ship-courseselect-name').text(name);
			shipNavigation.updateFuelConsumption();
			$('#skr-ship-courseselect-delete-course').prop('disabled', false);
		}
	}
});