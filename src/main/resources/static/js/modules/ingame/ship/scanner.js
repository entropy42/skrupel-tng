if (!shipScanner) {
	var shipScanner = new ShipScanner();

	function ShipScanner() {

		const me = this;

		this.showPlanetDetails = function(planetId) {
			$.ajax({
				url : 'ship/scanner/planet?shipId=' + ingame.selectedId + '&scannedPlanetId=' + planetId,
				type : 'GET',
				success : function(response) {
					$('#skr-game-ship-scanner').replaceWith(response);
				}
			});
		};

		this.showShipDetails = function(shipId) {
			$.ajax({
				url : 'ship/scanner/ship?shipId=' + ingame.selectedId + '&scannedShipId=' + shipId,
				type : 'GET',
				success : function(response) {
					$('#skr-game-ship-scanner').replaceWith(response);
				}
			});
		};
	}
}