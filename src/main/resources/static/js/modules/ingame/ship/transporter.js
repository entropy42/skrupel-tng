if (!shipTransporter) {
	var shipTransporter = new ShipTransporter();

	function ShipTransporter() {

		const me = this;

		const shipId = ingame.selectedId;

		$(document).on('input', 'input[type=range]', function() {
			me.updateLabels($(this));
		});
		
		this.updateLabels = function(elem) {
			const id = elem.prop('id');
			const max = parseInt(elem.prop('max'));
			const value = parseInt(elem.val());

			$('#ship-label-' + id).text(max - value);
			$('#planet-label-' + id).text(value);
		};
		
		this.finishTransport = function() {
			const request = {
				fuel : $('#slider-fuel').prop('max') - $('#slider-fuel').val(),
				colonists : $('#slider-colonists').prop('max') - $('#slider-colonists').val(),
				money : $('#slider-money').prop('max') - $('#slider-money').val(),
				supplies : $('#slider-supplies').prop('max') - $('#slider-supplies').val(),
				mineral1 : $('#slider-mineral1').prop('max') - $('#slider-mineral1').val(),
				mineral2 : $('#slider-mineral2').prop('max') - $('#slider-mineral2').val(),
				mineral3 : $('#slider-mineral3').prop('max') - $('#slider-mineral3').val(),
				lightGroundUnits : $('#slider-light-ground-units').prop('max') - $('#slider-light-ground-units').val(),
				heavyGroundUnits : $('#slider-heavy-ground-units').prop('max') - $('#slider-heavy-ground-units').val()
			};

			const maxFuel = parseInt($('#skr-game-ship-transporter-fuelcapacity').text());
			const maxStorage = parseInt($('#skr-game-ship-transporter-storagespace').text());

			const sum = Math.round((request.colonists / 100) + request.supplies + request.mineral1 + request.mineral2 + request.mineral3 + (request.lightGroundUnits * 0.3)
					+ (request.heavyGroundUnits * 1.5));

			const fuelDiff = request.fuel - maxFuel;
			const storageDiff = sum - maxStorage;
			
			if (fuelDiff > 0) {
				let msg = $('#skr-game-ship-transporter-fuelmessage').val();
				msg = msg.replace('{0}', fuelDiff);
				window.alert(msg);
			} else if (storageDiff > 0) {
				let msg = $('#skr-game-ship-transporter-storagemessage').val();
				msg = msg.replace('{0}', storageDiff);
				window.alert(msg);
			} else {
				$.ajax({
					url : 'ship/transporter?shipId=' + shipId,
					type : 'POST',
					contentType : 'application/json',
					data : JSON.stringify(request),
					success : function(response) {
						$('#skr-game-ship-transporter').replaceWith(response);
					}
				});
			}
		};
	}
}