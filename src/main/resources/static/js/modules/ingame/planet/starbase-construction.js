if (!starbaseConstruction) {
	var starbaseConstruction = new StarbaseConstruction();

	function StarbaseConstruction() {

		const me = this;

		const planetId = ingame.selectedId;

		this.toNameSelect = function(starbaseType) {
			$.ajax({
				url : 'planet/starbase-construction-name-select?planetId=' + planetId + '&type=' + starbaseType,
				type : 'GET',
				success : function(response) {
					$('#skr-planet-starbasepconstruction-content').replaceWith(response);
				}
			});
		};

		this.construct = function() {
			const type = $('#skr-planet-starbaseconstruction-type').val();
			const name = $('#skr-planet-starbaseconstruction-name-input').val();
			
			$.ajax({
				url : 'planet/starbase?planetId=' + planetId + '&type=' + type + '&name=' + name,
				type : 'POST',
				success : function(response) {
					$('#skr-planet-starbasepconstruction-content').replaceWith(response);
				}
			});
		};
	}
}