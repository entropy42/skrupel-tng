if (!planetMines) {
	var planetMines = new PlanetMines();

	function PlanetMines() {

		const me = this;

		const planetId = ingame.selectedId;

		$(document).on('click', '#skr-planet-mines-automated-checkbox', function() {
			$.ajax({
				url : 'planet/automated-mines?planetId=' + planetId,
				type : 'POST',
				contentType : 'application/json',
				success : function() {
				}
			});
		});

		$(document).on('click', '#skr-planet-mines-build-mines', function() {
			const quantity = $('#skr-planet-mines-quantity-select').val();

			$.ajax({
				url : 'planet/mines?planetId=' + planetId + '&quantity=' + quantity,
				type : 'POST',
				contentType : 'application/json',
				success : function() {
					ingame.refreshSelection(planet);
				}
			});
		});
	}
}