if (!planetPlanetaryDefense) {
	var planetPlanetaryDefense = new PlanetPlanetaryDefense();

	function PlanetPlanetaryDefense() {

		const me = this;

		const planetId = ingame.selectedId;

		$(document).on('click', '#skr-planet-planetarydefense-automated-checkbox', function() {
			$.ajax({
				url : 'planet/automated-planetary-defense?planetId=' + planetId,
				type : 'POST',
				contentType : 'application/json',
				success : function() {
				}
			});
		});

		$(document).on('click', '#skr-planet-planetarydefense-build-planetarydefense', function() {
			const quantity = $('#skr-planet-planetarydefense-quantity-select').val();

			$.ajax({
				url : 'planet/planetary-defense?planetId=' + planetId + '&quantity=' + quantity,
				type : 'POST',
				contentType : 'application/json',
				success : function() {
					ingame.refreshSelection(planet);
				}
			});
		});
	}
}