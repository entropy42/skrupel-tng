if (!planetFactories) {
	var planetFactories = new PlanetFactories();

	function PlanetFactories() {

		const me = this;

		const planetId = ingame.selectedId;

		$(document).on('click', '#skr-planet-factories-automated-checkbox', function() {
			$.ajax({
				url : 'planet/automated-factories?planetId=' + planetId,
				type : 'POST',
				contentType : 'application/json',
				success : function() {
				}
			});
		});

		$(document).on('click', '#skr-planet-sell-supplies-automated-checkbox', function() {
			$.ajax({
				url : 'planet/automated-sell-supplies?planetId=' + planetId,
				type : 'POST',
				contentType : 'application/json',
				success : function() {
				}
			});
		});

		$(document).on('click', '#skr-planet-factories-build-factories', function() {
			const quantity = $('#skr-planet-factories-quantity-select').val();

			$.ajax({
				url : 'planet/factories?planetId=' + planetId + '&quantity=' + quantity,
				type : 'POST',
				contentType : 'application/json',
				success : function() {
					ingame.refreshSelection(planet);
				}
			});
		});

		$(document).on('click', '#skr-planet-factories-sell-supplies', function() {
			const quantity = $('#skr-planet-supplies-quantity-select').val();

			$.ajax({
				url : 'planet/sell-supplies?planetId=' + planetId + '&quantity=' + quantity,
				type : 'POST',
				contentType : 'application/json',
				success : function() {
					ingame.refreshSelection(planet);
				}
			});
		});
	}
}