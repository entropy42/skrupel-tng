$(document).ready(function() {
	let id = '#skr-dashboard-existing-games';

	if (window.location.pathname.indexOf('/dashboard/') >= 0) {
		id = window.location.pathname.replace('/dashboard/', '#skr-dashboard-');
	}

	$(id).addClass('active');
});