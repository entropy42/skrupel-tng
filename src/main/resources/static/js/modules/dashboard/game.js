const game = new Game();

function Game() {

	const me = this;

	const gameId = new URLSearchParams(window.location.search).get('id');
	const isAdmin = $('#skr-game-is-admin').val() === 'true';

	this.selectedPlayer = null;

	$(document).on('change', '.skr-game-race-select', function() {
		const race = $(this).val();

		if (race) {
			const aiLevel = $(this).prop('name');
			
			if (aiLevel) {
				const idParts = $(this).prop('id').split('-');
				const playerId = idParts[idParts.length - 1];

				$.ajax({
					type : "PUT",
					url : "game/race?&race=" + race + "&playerId=" + playerId,
					success : function() {
						window.location.reload();
					}
				});
			} else {
				$.ajax({
					type : "POST",
					url : "game/race?gameId=" + gameId + "&race=" + race,
					success : function() {
						window.location.reload();
					}
				});
			}
		}
	});

	this.removePlayer = function(playerId) {
		$.ajax({
			type : "DELETE",
			url : "game/player?gameId=" + gameId + "&playerId=" + playerId,
			success : function() {
				window.location.reload();
			}
		});
	};

	this.updateStartGameButton = function() {
		const button = $('#skr-start-game-button');

		if (button) {
			let allSet = true;

			$('.skr-game-race-select').each(function() {
				if (!$(this).val()) {
					allSet = false;
				}
			});

			button.prop('disabled', !allSet);
		}
	};

	$('#skr-game-player-search').typeahead({}, {
		source : function(query, sync, async) {
			$.ajax({
				url : 'game/players',
				type : 'GET',
				data : {
					name : query
				},
				success : function(data) {
					return async(data);
				}
			});
		},
		display : 'playerName'
	});

	$(document).on('click', '#skr-start-game-button', function() {
		$.ajax({
			url : 'start-game?gameId=' + gameId,
			type : 'POST',
			success : function() {
				window.location.href = '/ingame/game?id=' + gameId;
			}
		});
	});

	$(document).on('click', '#skr-join-game-button', function() {
		$.ajax({
			url : 'game/player?gameId=' + gameId,
			type : 'POST',
			success : function(errorMessage) {
				if (errorMessage) {
					window.alert(errorMessage);
				} else {
					window.location.reload();
				}
			}
		});
	});

	$(document).on('typeahead:selected', '#skr-game-player-search', function(e, value) {
		me.selectedPlayer = value;
	});

	$(document).on('click', '#skr-game-add-player', function() {
		$.ajax({
			url : 'game/player?gameId=' + gameId + '&loginId=' + me.selectedPlayer.id,
			type : 'PUT',
			success : function(errorMessage) {
				if (errorMessage) {
					window.alert(errorMessage);
				} else {
					window.location.reload();
				}
			}
		});
	});

	me.updateStartGameButton();
}

$(document).ready(function() {
	const parts = window.location.href.split('#');

	if (parts.length == 2) {
		const tabId = parts[1];

		$('#skr-game-tab-' + tabId).tab('show');

		setTimeout(function() {
			$(window).scrollTop(0);
		}, 100);
	} else if (window.location.href.indexOf('?id=') < 0) {
		$('#skr-game-tab-options').tab('show');

		setTimeout(function() {
			$(window).scrollTop(0);
		}, 100);
	}

	$(document).on('click', '.nav-link', function() {
		const tab = $(this).prop('id').replace('skr-game-tab-', '');

		let url = window.location.href;
		const parts = window.location.href.split('#');

		if (parts.length == 2) {
			url = parts[0];
		}

		window.history.pushState(null, null, "#" + tab);
	});
});
