const skrupel = new Skrupel();

function Skrupel() {

	const me = this;

	this.resize = function(content, subtractions) {
		setTimeout(function() {
			let sum = 0;

			for(elem of subtractions) {
				sum += $(elem).outerHeight();
			}

			const contentHeight = $(window).height() - sum + "px";
			$(content).height(contentHeight);
		}, 100);
	};
};