package org.entropy.skrupeltng.modules.ingame.modules.planet.service;

import static org.junit.Assert.assertEquals;

import org.entropy.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public class PlanetServiceTest {

	private PlanetService service;
	private PlanetRepository planetRepository;

	@Before
	public void setup() {
		service = Mockito.spy(new PlanetService());

		planetRepository = Mockito.mock(PlanetRepository.class);
		service.setPlanetRepository(planetRepository);

		Mockito.when(planetRepository.save(Mockito.any())).thenAnswer(new Answer<Planet>() {
			@Override
			public Planet answer(InvocationOnMock invocation) throws Throwable {
				return invocation.getArgumentAt(0, Planet.class);
			}
		});
	}

	@Test
	public void shouldBuildMines() {
		Planet planet = new Planet();
		planet.setColonists(1000);
		planet.setMoney(200);
		planet.setSupplies(40);
		planet.setMines(5);
		Mockito.when(planetRepository.getOne(1L)).thenReturn(planet);

		service.buildMines(1L, 5);

		assertEquals(10, planet.getMines());
	}

	@Test(expected = RuntimeException.class)
	public void shouldNotBuildMinesBecauseNotEnoughMoney() {
		Planet planet = new Planet();
		planet.setColonists(1000);
		planet.setMoney(8);
		planet.setSupplies(40);
		planet.setMines(5);
		Mockito.when(planetRepository.getOne(1L)).thenReturn(planet);

		service.buildMines(1L, 5);
	}

	@Test(expected = RuntimeException.class)
	public void shouldNotBuildMinesBecauseNotEnoughSupply() {
		Planet planet = new Planet();
		planet.setColonists(1000);
		planet.setMoney(20);
		planet.setSupplies(2);
		planet.setMines(5);
		Mockito.when(planetRepository.getOne(1L)).thenReturn(planet);

		service.buildMines(1L, 5);
	}

	@Test(expected = RuntimeException.class)
	public void shouldNotBuildMinesBecauseAllMinesBuild() {
		Planet planet = new Planet();
		planet.setColonists(1000);
		planet.setMoney(20);
		planet.setSupplies(2);
		planet.setMines(10);
		Mockito.when(planetRepository.getOne(1L)).thenReturn(planet);

		service.buildMines(1L, 5);
	}
}