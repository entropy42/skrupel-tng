package org.entropy.skrupeltng.modules.ingame.modules.overview.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.entropy.skrupeltng.AbstractDbTest;
import org.entropy.skrupeltng.modules.ingame.database.Player;
import org.entropy.skrupeltng.modules.ingame.database.PlayerRepository;
import org.entropy.skrupeltng.modules.ingame.database.WinCondition;
import org.entropy.skrupeltng.modules.ingame.modules.overview.service.RoundSummary;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class NewsEntryRepositoryDatabaseTest extends AbstractDbTest {

	@Autowired
	private NewsEntryRepository newsEntryRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Test
	public void shouldDeleteNewsEntries() {
		newsEntryRepository.deleteByGameId(1L);

		List<Player> players = playerRepository.findByGameId(1L);

		for (Player player : players) {
			List<NewsEntry> list = newsEntryRepository.findByGameIdAndLoginId(1L, player.getLogin().getId());
			assertEquals(0, list.size());
		}
	}

	@Test
	public void shouldReturnFilledSummary() {
		RoundSummary summary = newsEntryRepository.getRoundSummary(1L);
		assertNotNull(summary);

		assertEquals(0, summary.getConqueredColonies());
		assertEquals(0, summary.getDestroyedShipsByPlanets());
		assertEquals(0, summary.getDestroyedShipsByShips());
		assertEquals(0, summary.getLostShips());
		assertEquals(0, summary.getNewColonies());
		assertEquals(0, summary.getNewStarbases());
		assertEquals(WinCondition.SURVIVE.name(), summary.getWinCondition());
		assertTrue(summary.getNewShips() > 0);
	}

	@Test
	public void shouldReturnEmptySummary() {
		RoundSummary summary = newsEntryRepository.getRoundSummary(2L);
		assertNotNull(summary);

		assertEquals(0, summary.getConqueredColonies());
		assertEquals(0, summary.getDestroyedShipsByPlanets());
		assertEquals(0, summary.getDestroyedShipsByShips());
		assertEquals(0, summary.getLostShips());
		assertEquals(0, summary.getNewColonies());
		assertEquals(0, summary.getNewStarbases());
		assertEquals(0, summary.getNewShips());
		assertEquals(WinCondition.SURVIVE.name(), summary.getWinCondition());
	}
}