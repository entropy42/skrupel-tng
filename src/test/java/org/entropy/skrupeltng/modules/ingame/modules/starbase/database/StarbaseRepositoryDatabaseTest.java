package org.entropy.skrupeltng.modules.ingame.modules.starbase.database;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.entropy.skrupeltng.AbstractDbTest;
import org.entropy.skrupeltng.modules.ingame.database.Player;
import org.entropy.skrupeltng.modules.ingame.database.PlayerRepository;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class StarbaseRepositoryDatabaseTest extends AbstractDbTest {

	@Autowired
	private StarbaseRepository starbaseRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Test
	public void shouldAcceptHomePlanetStarbaseAsOwnedByPlayer() {
		Player player = playerRepository.getOne(1L);
		Planet homePlanet = player.getHomePlanet();
		boolean loginOwnsStarbase = starbaseRepository.loginOwnsStarbase(homePlanet.getStarbase().getId(), player.getLogin().getId());
		assertTrue(loginOwnsStarbase);
	}

	@Test
	public void shouldNotAcceptEnemyStarbaseAsOwnedByPlayer() {
		Player player = playerRepository.getOne(1L);
		Player other = playerRepository.getOne(2L);
		Planet otherHomePlanet = other.getHomePlanet();
		boolean loginOwnsStarbase = starbaseRepository.loginOwnsStarbase(otherHomePlanet.getStarbase().getId(), player.getLogin().getId());
		assertFalse(loginOwnsStarbase);
	}
}