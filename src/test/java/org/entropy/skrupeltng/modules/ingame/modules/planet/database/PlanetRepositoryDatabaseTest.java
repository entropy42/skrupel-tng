package org.entropy.skrupeltng.modules.ingame.modules.planet.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.entropy.skrupeltng.AbstractDbTest;
import org.entropy.skrupeltng.modules.ingame.controller.PlanetEntry;
import org.entropy.skrupeltng.modules.ingame.database.Player;
import org.entropy.skrupeltng.modules.ingame.database.PlayerRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class PlanetRepositoryDatabaseTest extends AbstractDbTest {

	@Autowired
	private PlanetRepository planetRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Test
	public void shouldReturnAllExistingPlanets() {
		int count = planetRepository.findByGameId(1L).size();
		List<PlanetEntry> planets = planetRepository.getPlanetsForGalaxy(1L);
		assertEquals(count, planets.size());

		for (PlanetEntry planet : planets) {
			if (planet.getPlayerId() != null) {
				assertNotNull(planet.getPlayerColor());
				assertNotNull(planet.getStarbaseId());
			} else {
				assertNull(planet.getPlayerColor());
				assertNull(planet.getStarbaseId());
			}
		}
	}

	@Test
	public void shouldReturnNoPlanets() {
		List<PlanetEntry> planets = planetRepository.getPlanetsForGalaxy(4L);
		assertEquals(0, planets.size());
	}

	@Test
	public void shouldAcceptHomePlanetAsOwnedByPlayer() {
		Player player = playerRepository.getOne(1L);
		Planet homePlanet = player.getHomePlanet();
		boolean loginOwnsPlanet = planetRepository.loginOwnsPlanet(homePlanet.getId(), player.getLogin().getId());
		assertTrue(loginOwnsPlanet);
	}

	@Test
	public void shouldNotAcceptEnemyPlanetAsOwnedByPlayer() {
		Player player = playerRepository.getOne(1L);
		Player other = playerRepository.getOne(2L);
		Planet otherHomePlanet = other.getHomePlanet();
		boolean loginOwnsPlanet = planetRepository.loginOwnsPlanet(otherHomePlanet.getId(), player.getLogin().getId());
		assertFalse(loginOwnsPlanet);
	}

	@Test
	public void shouldNotAcceptUninhabitedPlanetAsOwnedByPlayer() {
		Player player = playerRepository.getOne(1L);
		Planet uninhabitedPlayer = planetRepository.findAll().stream().filter(p -> p.getPlayer() == null).findFirst().get();
		boolean loginOwnsPlanet = planetRepository.loginOwnsPlanet(uninhabitedPlayer.getId(), player.getLogin().getId());
		assertFalse(loginOwnsPlanet);
	}
}