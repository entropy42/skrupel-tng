package org.entropy.skrupeltng.modules.ingame.database;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.entropy.skrupeltng.AbstractDbTest;
import org.entropy.skrupeltng.modules.dashboard.GameListResult;
import org.entropy.skrupeltng.modules.dashboard.GameSearchParameters;
import org.entropy.skrupeltng.modules.ingame.CoordinateImpl;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public class GameRepositoryDatabaseTest extends AbstractDbTest {

	@Autowired
	private GameRepository gameRepository;

	@Test
	public void shouldReturnAllGames() {
		GameSearchParameters params = new GameSearchParameters(null, null, 1L);
		Page<GameListResult> result = gameRepository.searchGames(params, PageRequest.of(0, 10));

		assertNotNull(result);
		assertEquals(3, result.getNumberOfElements());

		List<GameListResult> content = result.getContent();

		GameListResult gameListResult = content.get(0);
		assertEquals(3L, gameListResult.getId());
		assertEquals(true, gameListResult.isPlayerOfGame());

		gameListResult = content.get(1);
		assertEquals(2L, gameListResult.getId());
		assertEquals(true, gameListResult.isPlayerOfGame());

		gameListResult = content.get(2);
		assertEquals(1L, gameListResult.getId());
		assertEquals(true, gameListResult.isPlayerOfGame());
	}

	@Test
	public void shouldReturnAllStartedGames() {
		GameSearchParameters params = new GameSearchParameters(true, null, 1L);
		Page<GameListResult> result = gameRepository.searchGames(params, PageRequest.of(0, 10));

		assertNotNull(result);
		assertEquals(2, result.getNumberOfElements());

		List<GameListResult> content = result.getContent();
		assertEquals(2L, content.get(0).getId());
		assertEquals(1L, content.get(1).getId());
	}

	@Test
	public void shouldReturnAllGamesWithLoginOne() {
		GameSearchParameters params = new GameSearchParameters(null, true, 1L);
		Page<GameListResult> result = gameRepository.searchGames(params, PageRequest.of(0, 10));

		assertNotNull(result);
		assertEquals(3, result.getNumberOfElements());

		List<GameListResult> content = result.getContent();
		assertEquals(3L, content.get(0).getId());
		assertEquals(2L, content.get(1).getId());
		assertEquals(1L, content.get(2).getId());
	}

	@Test
	public void shouldReturnAllNotStartedGames() {
		GameSearchParameters params = new GameSearchParameters(false, null, 1L);
		Page<GameListResult> result = gameRepository.searchGames(params, PageRequest.of(0, 10));

		assertNotNull(result);
		assertEquals(1, result.getNumberOfElements());

		List<GameListResult> content = result.getContent();
		assertEquals(3L, content.get(0).getId());
	}

	@Test
	public void shouldReturnNoGames() {
		GameSearchParameters params = new GameSearchParameters(null, true, 3L);
		Page<GameListResult> result = gameRepository.searchGames(params, PageRequest.of(0, 10));

		assertNotNull(result);
		assertEquals(0, result.getNumberOfElements());
	}

	@Test
	public void shouldReturnDifferentVisibilityCoordinates() {
		List<CoordinateImpl> results1 = gameRepository.getVisibilityCoordinates(1L);
		assertNotNull(results1);
		assertTrue(results1.size() > 0);

		List<CoordinateImpl> results2 = gameRepository.getVisibilityCoordinates(2L);
		assertNotNull(results2);
		assertTrue(results2.size() > 0);

		assertNotEquals(results1, results2);
	}
}