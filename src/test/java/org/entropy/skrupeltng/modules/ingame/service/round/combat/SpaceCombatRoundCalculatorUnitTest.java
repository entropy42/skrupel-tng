package org.entropy.skrupeltng.modules.ingame.service.round.combat;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.entropy.skrupeltng.modules.ingame.database.Player;
import org.entropy.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class SpaceCombatRoundCalculatorUnitTest extends AbstractCombatRoundCalculatorTest {

	private SpaceCombatRoundCalculator calculator;
	private ShipRepository shipRepository;
	private NewsService newsService;

	@Before
	public void setup() {
		calculator = Mockito.spy(new SpaceCombatRoundCalculator());

		shipRepository = Mockito.mock(ShipRepository.class);
		calculator.setShipRepository(shipRepository);

		newsService = Mockito.mock(NewsService.class);
		calculator.setNewsService(newsService);
	}

	@Test
	public void shouldDestroyEnemyShip() {
		Set<ShipDestructionData> destroyedShips = new HashSet<>(1);

		Ship ship = createShip(100, 1000, 10, 10, 50);
		ship.setX(100);
		ship.setY(100);
		ship.setPlayer(new Player(2L));
		Ship enemyShip = createShip(100, 1000, 2, 2, 50);
		enemyShip.setPlayer(new Player(3L));

		Mockito.when(shipRepository.findEnemyShipsOnSamePosition(1L, 2L, 100, 100)).thenReturn(Arrays.asList(enemyShip));

		Set<String> processedShipPairs = new HashSet<>();
		calculator.processCombat(1L, destroyedShips, ship, processedShipPairs);

		assertEquals(1, destroyedShips.size());
		assertEquals(enemyShip, destroyedShips.iterator().next().getShip());
		Mockito.verify(shipRepository).save(ship);
		Mockito.verify(shipRepository, Mockito.never()).save(enemyShip);
	}

	@Test
	public void shouldBeDestroyedByEnemyShip() {
		Set<ShipDestructionData> destroyedShips = new HashSet<>(1);

		Ship ship = createShip(100, 1000, 10, 0, 50);
		ship.setX(100);
		ship.setY(100);
		ship.setPlayer(new Player(2L));
		Ship enemyShip = createShip(200, 2000, 10, 0, 200);

		Mockito.when(shipRepository.findEnemyShipsOnSamePosition(1L, 2L, 100, 100)).thenReturn(Arrays.asList(enemyShip));

		Set<String> processedShipPairs = new HashSet<>();
		calculator.processCombat(1L, destroyedShips, ship, processedShipPairs);

		assertEquals(1, destroyedShips.size());
		assertEquals(ship, destroyedShips.iterator().next().getShip());
		Mockito.verify(shipRepository, Mockito.never()).save(ship);
		Mockito.verify(shipRepository).save(enemyShip);
	}
}