package org.entropy.skrupeltng.modules.ingame.modules.planet.database;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.entropy.skrupeltng.AbstractDbTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class PlanetNameRepositoryDatabaseTest extends AbstractDbTest {

	@Autowired
	private PlanetNameRepository planetNameRepository;

	@Test
	public void shouldReturnAllPlanetNames() {
		List<String> names = planetNameRepository.getAllPlanetNames();
		assertEquals(400, names.size());
	}
}