package org.entropy.skrupeltng.modules.ingame.modules.ship.database;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.entropy.skrupeltng.AbstractDbTest;
import org.entropy.skrupeltng.modules.ingame.database.Player;
import org.entropy.skrupeltng.modules.ingame.database.PlayerRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ShipRepositoryDatabaseTest extends AbstractDbTest {

	@Autowired
	private ShipRepository shipRepository;

	@Autowired
	private PlayerRepository playerRepository;

	@Test
	public void shouldAcceptOwnedShipAsOwnedByPlayer() {
		List<Ship> ships = shipRepository.findByGameId(1L);
		assertTrue(!ships.isEmpty());

		Ship ship = ships.get(0);

		boolean loginOwnsShip = shipRepository.loginOwnsShip(ship.getId(), ship.getPlayer().getLogin().getId());
		assertTrue(loginOwnsShip);
	}

	@Test
	public void shouldNotAcceptEnemyShipAsOwnedByPlayer() {
		List<Ship> ships = shipRepository.findByGameId(1L);
		assertTrue(!ships.isEmpty());

		Ship ship = ships.get(0);
		long shipLoginId = ship.getPlayer().getLogin().getId();

		Player enemyPlayer = playerRepository.findByGameId(1L).stream().filter(p -> p.getLogin().getId() != shipLoginId).findFirst().get();

		boolean loginOwnsShip = shipRepository.loginOwnsShip(ship.getId(), enemyPlayer.getLogin().getId());
		assertFalse(loginOwnsShip);
	}
}