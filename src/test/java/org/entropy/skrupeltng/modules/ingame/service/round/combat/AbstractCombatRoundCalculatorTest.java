package org.entropy.skrupeltng.modules.ingame.service.round.combat;

import org.entropy.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.entropy.skrupeltng.modules.masterdata.database.ShipTemplate;
import org.entropy.skrupeltng.modules.masterdata.database.WeaponTemplate;

public class AbstractCombatRoundCalculatorTest {

	protected Ship createShip(int shield, int mass, int energyWeapons, int projectileWeapons, int crew) {
		Ship ship = new Ship();
		ship.setId(3L);
		ship.setShield(shield);
		ship.setProjectiles(10);
		ship.setName("test");
		ship.setCrew(crew);

		ShipTemplate shipTemplate = new ShipTemplate();
		shipTemplate.setEnergyWeaponsCount(energyWeapons);
		shipTemplate.setProjectileWeaponsCount(projectileWeapons);
		shipTemplate.setHangarCapacity(3);
		shipTemplate.setMass(mass);
		shipTemplate.setCrew(crew);
		ship.setShipTemplate(shipTemplate);

		WeaponTemplate energyWeaponTemplate = new WeaponTemplate();
		energyWeaponTemplate.setTechLevel(4);
		energyWeaponTemplate.setUsesProjectiles(false);
		ship.setEnergyWeaponTemplate(energyWeaponTemplate);

		WeaponTemplate projectileWeaponTemplate = new WeaponTemplate();
		projectileWeaponTemplate.setTechLevel(2);
		projectileWeaponTemplate.setUsesProjectiles(true);
		ship.setProjectileWeaponTemplate(projectileWeaponTemplate);

		return ship;
	}
}