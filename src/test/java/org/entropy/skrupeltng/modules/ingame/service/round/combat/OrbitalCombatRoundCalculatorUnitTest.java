package org.entropy.skrupeltng.modules.ingame.service.round.combat;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.entropy.skrupeltng.modules.ingame.database.Player;
import org.entropy.skrupeltng.modules.ingame.modules.overview.service.NewsService;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.Planet;
import org.entropy.skrupeltng.modules.ingame.modules.planet.database.PlanetRepository;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.Ship;
import org.entropy.skrupeltng.modules.ingame.modules.ship.database.ShipRepository;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.Starbase;
import org.entropy.skrupeltng.modules.ingame.modules.starbase.database.StarbaseType;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class OrbitalCombatRoundCalculatorUnitTest extends AbstractCombatRoundCalculatorTest {

	private OrbitalCombatRoundCalculator calculator;
	private ShipRepository shipRepository;
	private PlanetRepository planetRepository;
	private NewsService newsService;

	@Before
	public void setup() {
		calculator = Mockito.spy(new OrbitalCombatRoundCalculator());

		shipRepository = Mockito.mock(ShipRepository.class);
		calculator.setShipRepository(shipRepository);

		planetRepository = Mockito.mock(PlanetRepository.class);
		calculator.setPlanetRepository(planetRepository);

		newsService = Mockito.mock(NewsService.class);
		calculator.setNewsService(newsService);
	}

	@Test
	public void shouldDestroyAttackingShip() {
		Set<ShipDestructionData> destroyedShips = new HashSet<>(1);

		Planet planet = new Planet();
		planet.setId(1L);
		planet.setColonists(50000);
		planet.setMoney(10000);
		planet.setSupplies(50);
		planet.setPlanetaryDefense(50);

		Starbase starbase = new Starbase();
		starbase.setId(2L);
		starbase.setDefense(30);
		starbase.setType(StarbaseType.STAR_BASE);
		starbase.setPlanet(planet);
		planet.setStarbase(starbase);

		Ship ship = createShip(100, 1000, 2, 4, 50);
		ship.setPlanet(planet);

		Player shipPlayer = new Player();
		shipPlayer.setId(4L);
		ship.setPlayer(shipPlayer);

		Player planetPlayer = new Player();
		planetPlayer.setId(5L);
		planet.setPlayer(planetPlayer);

		calculator.processShip(destroyedShips, ship);

		Mockito.verify(shipRepository, Mockito.never()).save(Mockito.any());
		assertEquals(1, destroyedShips.size());
		assertEquals(ship, destroyedShips.iterator().next().getShip());

		assertEquals(planetPlayer, planet.getPlayer());
		Mockito.verify(planetRepository).save(planet);
	}

	@Test
	public void shouldConquerPlanet() {
		Set<ShipDestructionData> destroyedShips = new HashSet<>(1);

		Planet planet = new Planet();
		planet.setId(1L);
		planet.setColonists(50000);
		planet.setMoney(10000);
		planet.setSupplies(50);
		planet.setPlanetaryDefense(50);

		Starbase starbase = new Starbase();
		starbase.setId(2L);
		starbase.setDefense(10);
		starbase.setType(StarbaseType.STAR_BASE);
		starbase.setPlanet(planet);
		planet.setStarbase(starbase);

		Ship ship = createShip(100, 2000, 10, 10, 100);
		ship.setPlanet(planet);

		Player shipPlayer = new Player();
		shipPlayer.setId(4L);
		ship.setPlayer(shipPlayer);

		Player planetPlayer = new Player();
		planetPlayer.setId(5L);
		planet.setPlayer(planetPlayer);

		calculator.processShip(destroyedShips, ship);

		Mockito.verify(shipRepository).save(ship);
		assertEquals(0, destroyedShips.size());

		assertEquals(shipPlayer, planet.getPlayer());
		Mockito.verify(planetRepository).save(planet);
	}
}