package org.entropy.skrupeltng.selenium;

import static org.junit.Assert.assertEquals;

import org.entropy.skrupeltng.selenium.pages.InitSetupPage;
import org.entropy.skrupeltng.selenium.pages.dashboard.CreateNewGamePage;
import org.entropy.skrupeltng.selenium.pages.dashboard.ExistingGamesPage;
import org.entropy.skrupeltng.selenium.pages.dashboard.GameDetailsPage;
import org.entropy.skrupeltng.selenium.pages.ingame.GalaxyMapPage;
import org.entropy.skrupeltng.selenium.pages.ingame.IngameMainPage;
import org.entropy.skrupeltng.selenium.pages.ingame.PlanetOverviewPage;
import org.entropy.skrupeltng.selenium.pages.ingame.PlanetPage;
import org.entropy.skrupeltng.selenium.pages.ingame.ShipDetailsPage;
import org.entropy.skrupeltng.selenium.pages.ingame.ShipOverviewPage;
import org.entropy.skrupeltng.selenium.pages.ingame.StarbaseOverviewPage;
import org.entropy.skrupeltng.selenium.pages.ingame.StarbasePage;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SeleniumTest {

	private WebDriver driver;

	@Before
	public void setup() {
		System.setProperty("webdriver.chrome.driver", "libs/chromedriver");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--window-size=1500,1000");
		options.setHeadless(true);
		driver = new ChromeDriver(options);
	}

	@After
	public void tearDown() {
		if (driver != null) {
			driver.quit();
		}
	}

	@Test
	public void testAInitSetup() {
		driver.get("http://localhost:8080");
		assertEquals("Skrupel TNG", driver.getTitle());

		InitSetupPage initSetupPage = new InitSetupPage(driver);
		initSetupPage.init();

		ExistingGamesPage existingGamesPage = new ExistingGamesPage(driver);
		existingGamesPage.showGameDetails();

		GameDetailsPage gameDetailsPage = new GameDetailsPage(driver);
		gameDetailsPage.checkGameDetails();
		gameDetailsPage.playGame();
	}

	@Test
	public void testBPlanet() {
		IngameMainPage ingameOverviewPage = new IngameMainPage(driver);
		ingameOverviewPage.login("/ingame/game?id=1");

		PlanetOverviewPage planetOverviewPage = new PlanetOverviewPage(driver);
		planetOverviewPage.showFirstPlanet();

		PlanetPage ingamePlanetPage = new PlanetPage(driver);
		ingamePlanetPage.autobuildMines();
		ingamePlanetPage.autobuildFactories();
		ingamePlanetPage.autobuildPlanetaryDefense();
	}

	@Test
	public void testCStarbase() {
		IngameMainPage overviewPage = new IngameMainPage(driver);
		overviewPage.login("/ingame/game?id=1");

		GalaxyMapPage galaxyMapPage = new GalaxyMapPage(driver);
		galaxyMapPage.selectShipOnPlanet();

		StarbaseOverviewPage starbaseOverviewPage = new StarbaseOverviewPage(driver);
		starbaseOverviewPage.showFirstStarbase();

		StarbasePage starbasePage = new StarbasePage(driver);
		starbasePage.upgradeTechlevel();
		starbasePage.productShipParts();
		starbasePage.buildShip();

		overviewPage.endTurn();
	}

	@Test
	public void testDShip() {
		IngameMainPage overviewPage = new IngameMainPage(driver);
		overviewPage.login("/ingame/game?id=1");

		StarbaseOverviewPage starbaseOverviewPage = new StarbaseOverviewPage(driver);
		starbaseOverviewPage.showFirstStarbase();

		StarbasePage starbasePage = new StarbasePage(driver);
		starbasePage.productShipParts();
		starbasePage.buildShip();

		ShipOverviewPage shipOverviewPage = new ShipOverviewPage(driver);
		shipOverviewPage.showFirstShip();

		ShipDetailsPage shipPage = new ShipDetailsPage(driver);
		shipPage.transportFuelOntoShip();
		shipPage.navigateShipToNearestPlanet();

		overviewPage.endTurn();
	}

	@Test
	public void testEGalaxyMapClicks() {
		IngameMainPage overviewPage = new IngameMainPage(driver);
		overviewPage.login("/ingame/game?id=1");

		GalaxyMapPage galaxyMapPage = new GalaxyMapPage(driver);
		galaxyMapPage.selectShipInSpace();
		galaxyMapPage.selectHomePlanet();
		galaxyMapPage.selectHomeStarbase();

		StarbasePage starbasePage = new StarbasePage(driver);
		starbasePage.productShipParts();
		starbasePage.buildShip();

		overviewPage.endTurn();
		overviewPage.closePopup();

		galaxyMapPage.selectMultipleShipOnPlanet();
	}

	@Test
	public void testFNewGame() {
		ExistingGamesPage existingGamesPage = new ExistingGamesPage(driver);
		existingGamesPage.login("/");
		existingGamesPage.showCreateNewGame();

		CreateNewGamePage createNewGamePage = new CreateNewGamePage(driver);
		createNewGamePage.createNewGame(2L);

		GameDetailsPage gameDetailsPage = new GameDetailsPage(driver);
		gameDetailsPage.selectRaceForPlayer();
		gameDetailsPage.startGame();

		IngameMainPage overviewPage = new IngameMainPage(driver);
		overviewPage.closePopup();
		overviewPage.returnToDashboard();
		existingGamesPage.showCreateNewGame();

		createNewGamePage.createNewGame(3L);
	}
}