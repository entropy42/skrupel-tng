package org.entropy.skrupeltng.selenium.pages.ingame;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.entropy.skrupeltng.modules.ingame.CoordinateImpl;
import org.entropy.skrupeltng.modules.ingame.service.CoordHelper;
import org.entropy.skrupeltng.selenium.pages.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ShipDetailsPage extends AbstractPage {

	public ShipDetailsPage(WebDriver driver) {
		super(driver);
	}

	public void transportFuelOntoShip() {
		driver.findElement(By.id("skr-ingame-ship-transporter-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains(";transporter"));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("slider-fuel")));

		WebElement slider = driver.findElement(By.id("slider-fuel"));

		Actions move = new Actions(driver);
		Action action = move.dragAndDropBy(slider, 30, 0).build();
		action.perform();

		waitShortly();

		scrollToBottom();

		driver.findElement(By.id("skr-ingame-ship-finish-transport-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-ship-transport-success")));
	}

	public void navigateShipToNearestPlanet() {
		driver.findElement(By.id("skr-ingame-ship-navigation-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains(";navigation"));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ship-courseselect-checkbox")));

		driver.findElement(By.id("skr-ship-courseselect-checkbox")).click();

		CoordinateImpl shipCoords = getCoords(By.className("ship"));
		List<WebElement> planets = driver.findElements(By.className("skr-game-planet-mouse"));

		double bestDistance = Double.MAX_VALUE;
		WebElement nearestPlanet = null;

		for (WebElement planet : planets) {
			CoordinateImpl planetCoords = getCoordsOfParent(planet);

			if (planetCoords.equals(shipCoords)) {
				continue;
			}

			double distance = CoordHelper.getDistance(planetCoords, shipCoords);

			if (distance < bestDistance) {
				nearestPlanet = planet;
				bestDistance = distance;
			}
		}

		assertNotNull(nearestPlanet);
		nearestPlanet.click();

		waitShortly();

		driver.findElement(By.id("skr-ingame-ship-save-course-button")).click();
		waitShortly();

		String coordsText = driver.findElement(By.id("skr-ship-courseselect-coordinates")).getText();
		assertTrue(!coordsText.contains("-"));

		String distanceText = driver.findElement(By.id("skr-ship-courseselect-distance")).getText();
		assertTrue(!distanceText.contains("-"));

		String targetText = driver.findElement(By.id("skr-ship-courseselect-name")).getText();
		assertTrue(!targetText.contains("-"));

		String durationText = driver.findElement(By.id("skr-ship-courseselect-duration")).getText();
		assertTrue(!durationText.contains("-"));

		String fuelConsumptionText = driver.findElement(By.id("skr-ship-courseselect-fuelconsumption")).getText();
		assertFalse(fuelConsumptionText.equals("0"));

		assertTrue(driver.findElement(By.id("skr-ship-courseselect-delete-course")).isEnabled());
	}
}