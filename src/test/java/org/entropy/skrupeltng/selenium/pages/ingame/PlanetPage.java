package org.entropy.skrupeltng.selenium.pages.ingame;

import org.entropy.skrupeltng.selenium.pages.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PlanetPage extends AbstractPage {

	public PlanetPage(WebDriver driver) {
		super(driver);
	}

	public void autobuildMines() {
		driver.findElement(By.id("skr-ingame-planet-mines-button")).click();

		System.out.println("Loading mines view...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains(";mines"));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-planet-mines-build-mines")));

		driver.findElement(By.id("skr-planet-mines-build-mines")).click();

		System.out.println("Building mines...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.invisibilityOfElementLocated(By.id("skr-planet-mines-build-mines")));

		System.out.println("Setting mines to autobuild...");
		driver.findElement(By.id("skr-planet-mines-automated-checkbox")).click();
	}

	public void autobuildFactories() {
		driver.findElement(By.id("skr-ingame-planet-factories-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains(";factories"));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.invisibilityOfElementLocated(By.id("skr-planet-factories-build-factories")));

		System.out.println("Setting factories to autobuild...");

		driver.findElement(By.id("skr-planet-factories-automated-checkbox")).click();
	}

	public void autobuildPlanetaryDefense() {
		driver.findElement(By.id("skr-ingame-planet-planetarydefense-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains(";planetary-defense"));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.invisibilityOfElementLocated(By.id("skr-planet-planetarydefense-build-planetarydefense")));

		System.out.println("Setting planetary defense to autobuild...");

		driver.findElement(By.id("skr-planet-planetarydefense-automated-checkbox")).click();
	}

	public void showStarbase() {
		driver.findElement(By.id("skr-ingame-planet-starbase-button")).click();

		System.out.println("Loading starbase of planet...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("#starbase="));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-starbase-upgrade-button")));
	}
}