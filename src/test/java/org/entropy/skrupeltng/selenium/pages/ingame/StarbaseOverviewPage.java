package org.entropy.skrupeltng.selenium.pages.ingame;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.entropy.skrupeltng.selenium.pages.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StarbaseOverviewPage extends AbstractPage {

	public StarbaseOverviewPage(WebDriver driver) {
		super(driver);
	}

	public void showFirstStarbase() {
		driver.findElement(By.id("skr-ingame-starbases-button")).click();

		System.out.println("Loading starbases list...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("ingame/game?id=1#starbase-selection"));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.className("skr-ingame-starbase-selection-item")));

		List<WebElement> planetItems = driver.findElements(By.className("skr-ingame-starbase-selection-item"));
		assertEquals(1, planetItems.size());
		planetItems.get(0).click();

		System.out.println("Loading starbase view...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("ingame/game?id=1#starbase="));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-starbase-upgrade-button")));
	}
}