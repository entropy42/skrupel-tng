package org.entropy.skrupeltng.selenium.pages.ingame;

import org.entropy.skrupeltng.selenium.pages.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class IngameMainPage extends AbstractPage {

	public IngameMainPage(WebDriver driver) {
		super(driver);
	}

	public void closePopup() {
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("ingame/game?id="));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-overview-modal")));

		driver.findElement(By.id("skr-close-overview-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.invisibilityOfElementLocated(By.id("skr-ingame-overview-modal")));
	}

	public void endTurn() {
		System.out.println("Ending turn...");

		driver.findElement(By.id("skr-dashboard-user-menu-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.elementToBeClickable(By.id("skr-ingame-end-turn-button")));

		driver.findElement(By.id("skr-ingame-end-turn-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.visibilityOfElementLocated(By.id("skr-ingame-overview-modal")));
	}

	public void returnToDashboard() {
		System.out.println("Returning to dashboard...");

		driver.findElement(By.id("skr-ingame-to-dashboard")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("dashboard/existing-games"));
	}
}