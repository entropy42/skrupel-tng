package org.entropy.skrupeltng.selenium.pages.ingame;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.entropy.skrupeltng.selenium.pages.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PlanetOverviewPage extends AbstractPage {

	public PlanetOverviewPage(WebDriver driver) {
		super(driver);
	}

	public void showFirstPlanet() {
		driver.findElement(By.id("skr-ingame-planets-button")).click();

		System.out.println("Loading planets list...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("ingame/game?id=1#planet-selection"));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.className("skr-ingame-planet-selection-item")));

		List<WebElement> planetItems = driver.findElements(By.className("skr-ingame-planet-selection-item"));
		assertEquals(1, planetItems.size());
		planetItems.get(0).click();

		System.out.println("Loading planet view...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("ingame/game?id=1#planet="));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-planet-mines-button")));
	}
}