package org.entropy.skrupeltng.selenium.pages;

import org.entropy.skrupeltng.modules.ingame.CoordinateImpl;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public abstract class AbstractPage {

	protected static final int STANDARD_TIMEOUT = 8;
	protected static final String USERNAME = "admin";
	protected static final String PASSWORD = "test";

	protected final WebDriver driver;

	public AbstractPage(WebDriver driver) {
		this.driver = driver;
	}

	protected void waitShortly() {
		try {
			Thread.sleep(500L);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void login(String path) {
		driver.get("http://localhost:8080" + path);

		driver.findElement(By.id("username")).sendKeys(USERNAME);
		driver.findElement(By.id("password")).sendKeys(PASSWORD);
		driver.findElement(By.id("skr-login-button")).submit();

		System.out.println("Logging in to " + path);
	}

	protected void scrollToBottom() {
		((JavascriptExecutor)driver)
				.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		waitShortly();
	}

	protected void scrollToElement(By by) {
		WebElement element = driver.findElement(by);
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", element);
		waitShortly();
	}

	protected CoordinateImpl getCoords(By by) {
		return getCoordsOfParent(driver.findElement(by));
	}

	protected CoordinateImpl getCoordsOfParent(WebElement elem) {
		WebElement shipParent = elem.findElement(By.xpath(".."));
		return getCoords(shipParent);
	}

	protected CoordinateImpl getCoords(WebElement elem) {
		Integer x = Integer.valueOf(elem.getCssValue("left").replace("px", ""));
		Integer y = Integer.valueOf(elem.getCssValue("top").replace("px", ""));
		return new CoordinateImpl(x, y);
	}
}