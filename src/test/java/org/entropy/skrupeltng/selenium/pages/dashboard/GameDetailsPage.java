package org.entropy.skrupeltng.selenium.pages.dashboard;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.entropy.skrupeltng.selenium.pages.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GameDetailsPage extends AbstractPage {

	public GameDetailsPage(WebDriver driver) {
		super(driver);
	}

	public void checkGameDetails() {
		System.out.println("Checking game details..");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-play-button")));

		List<WebElement> playerNameElements = driver.findElements(By.className("skr-player-name"));
		boolean hasAdminPlayer = playerNameElements.stream().anyMatch(w -> w.getText().equals(USERNAME));
		assertTrue(hasAdminPlayer);
	}

	public void startGame() {
		waitShortly();
		driver.findElement(By.id("skr-start-game-button")).click();
	}

	public void playGame() {
		waitShortly();
		driver.findElement(By.id("skr-play-button")).click();
	}

	public void selectRaceForPlayer() {
		new Select(driver.findElement(By.id("skr-game-race-select-10"))).selectByIndex(1);
	}
}