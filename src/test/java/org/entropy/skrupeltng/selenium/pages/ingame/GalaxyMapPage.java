package org.entropy.skrupeltng.selenium.pages.ingame;

import org.entropy.skrupeltng.selenium.pages.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GalaxyMapPage extends AbstractPage {

	public GalaxyMapPage(WebDriver driver) {
		super(driver);
	}

	public void selectHomePlanet() {
		By owned = By.className("owned");
		scrollToElement(owned);

		driver.findElement(owned).click();

		System.out.println("Selecting home planet from galaxy map...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("ingame/game?id=1#planet="));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-planet-mines-button")));
	}

	public void selectShipInSpace() {
		By owned = By.className("ship-in-space");
		scrollToElement(owned);

		driver.findElement(owned).click();

		System.out.println("Selecting ship in space from galaxy map...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("ingame/game?id=1#ship="));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-ship-transporter-button")));
	}

	public void selectHomeStarbase() {
		By owned = By.className("starbase");
		scrollToElement(owned);

		new Actions(driver).moveToElement(driver.findElement(owned), 5, 5).click().build().perform();

		System.out.println("Selecting home starbase from galaxy map...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("ingame/game?id=1#starbase="));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-starbase-upgrade-button")));
	}

	public void selectShipOnPlanet() {
		By owned = By.className("ship");
		scrollToElement(owned);

		new Actions(driver).moveToElement(driver.findElement(owned), 2, 2).click().build().perform();

		System.out.println("Selecting ship on planet from galaxy map...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("ingame/game?id=1#ship="));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-ship-transporter-button")));
	}

	public void selectMultipleShipOnPlanet() {
		By owned = By.className("ships-around-planet");
		scrollToElement(owned);

		new Actions(driver).moveToElement(driver.findElement(owned), 2, 2).click().build().perform();

		System.out.println("Selecting multiple ship on planet from galaxy map...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("ingame/game?id=1#ship-selection"));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.className("skr-ingame-ship-selection-item")));
	}
}