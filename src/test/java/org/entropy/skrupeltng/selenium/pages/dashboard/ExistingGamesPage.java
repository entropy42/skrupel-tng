package org.entropy.skrupeltng.selenium.pages.dashboard;

import static org.junit.Assert.assertEquals;

import org.entropy.skrupeltng.selenium.pages.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ExistingGamesPage extends AbstractPage {

	public ExistingGamesPage(WebDriver driver) {
		super(driver);
	}

	public void showGameDetails() {
		driver.findElement(By.className("skr-game-details-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("game?id=1"));
	}

	public void showCreateNewGame() {
		driver.findElement(By.id("skr-dashboard-new-game-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("new-game"));

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-dashboard-newgame-create-button")));
	}

	public void checkPlayButtons() {
		int playButtonCount = driver.findElements(By.className("skr-play-button")).size();

		assertEquals(2, playButtonCount);
	}
}