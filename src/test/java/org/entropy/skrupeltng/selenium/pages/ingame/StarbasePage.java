package org.entropy.skrupeltng.selenium.pages.ingame;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.entropy.skrupeltng.selenium.pages.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StarbasePage extends AbstractPage {

	public StarbasePage(WebDriver driver) {
		super(driver);
	}

	public void upgradeTechlevel() {
		driver.findElement(By.id("skr-ingame-starbase-upgrade-button")).click();

		System.out.println("Loading starbase upgrade view...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains(";upgrade"));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-starbase-performupgrade-button")));

		System.out.println("Upgrading starbase tech levels...");

		new Select(driver.findElement(By.id("skr-starbase-upgrade-hull-select"))).selectByIndex(5);
		new Select(driver.findElement(By.id("skr-starbase-upgrade-propulsion-select"))).selectByIndex(8);
		new Select(driver.findElement(By.id("skr-starbase-upgrade-energy-select"))).selectByIndex(1);
		new Select(driver.findElement(By.id("skr-starbase-upgrade-projectile-select"))).selectByIndex(1);

		driver.findElement(By.id("skr-ingame-starbase-performupgrade-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-starbase-hullproduction-button")));

		waitShortly();
	}

	public void productShipParts() {
		productShipPart("hull", "Trova Frachter", 18, 1);
		productShipPart("propulsion", "Tachion-Flux", 9, 2);
		productShipPart("energyweapon", "Laser", 10, 2);
		productShipPart("projectileweapon", "Fusionsraketen", 10, 2);
	}

	public void productShipPart(String type, String productName, int expectedItems, int quantity) {
		System.out.println("Loading starbase " + type + " production view...");

		waitShortly();

		driver.findElement(By.id("skr-ingame-starbase-" + type + "production-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains(";production-" + type));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.className("skr-ingame-starbase-production-item")));

		System.out.println("Producing " + type + "...");

		if (type.equals("propulsion")) {
			scrollToBottom();
		}

		List<WebElement> items = driver.findElements(By.className("skr-ingame-starbase-production-item"));
		assertEquals(expectedItems, items.size());

		WebElement constructButton = null;

		for (WebElement item : items) {
			String text = item.findElement(By.className("skr-ingame-starbase-production-item-name")).getText();

			if (text.equals(productName)) {
				new Select(item.findElement(By.className("skr-starbase-production-quantity-select"))).selectByValue("" + quantity);
				constructButton = item.findElement(By.className("skr-ingame-starbase-produce-button"));
				break;
			}
		}

		assertNotNull(constructButton);

		constructButton.click();
	}

	public void buildShip() {
		waitShortly();

		driver.findElement(By.id("skr-ingame-starbase-shipconstruction-button")).click();

		System.out.println("Loading ship construction view...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains(";shipconstruction"));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-starbase-shipconstruction-hull-select-button")));

		driver.findElement(By.id("skr-ingame-starbase-shipconstruction-hull-select-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-starbase-shipconstruction-shipname-input")));

		driver.findElement(By.id("skr-starbase-shipconstruction-shipname-input")).sendKeys("test");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(new ExpectedCondition<Boolean>() {
					@Override
					public Boolean apply(WebDriver input) {
						return input.findElement(By.id("skr-starbase-shipconstruction-build-button")).isEnabled();
					}
				});

		driver.findElement(By.id("skr-starbase-shipconstruction-build-button")).click();

		System.out.println("Building ship...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-starbase-shipconstruction-success-message")));
	}
}
