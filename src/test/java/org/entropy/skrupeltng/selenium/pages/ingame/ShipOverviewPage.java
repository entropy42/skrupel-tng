package org.entropy.skrupeltng.selenium.pages.ingame;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.entropy.skrupeltng.selenium.pages.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ShipOverviewPage extends AbstractPage {

	public ShipOverviewPage(WebDriver driver) {
		super(driver);
	}

	public void showFirstShip() {
		driver.findElement(By.id("skr-ingame-ships-button")).click();

		System.out.println("Loading ships list...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("ingame/game?id=1#ship-selection"));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.className("skr-ingame-ship-selection-item")));

		List<WebElement> planetItems = driver.findElements(By.className("skr-ingame-ship-selection-item"));
		assertEquals(2, planetItems.size());
		planetItems.get(0).click();

		System.out.println("Loading ship view...");

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("ingame/game?id=1#ship="));
		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.presenceOfElementLocated(By.id("skr-ingame-ship-transporter-button")));
	}
}