package org.entropy.skrupeltng.selenium.pages.dashboard;

import org.entropy.skrupeltng.selenium.pages.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreateNewGamePage extends AbstractPage {

	public CreateNewGamePage(WebDriver driver) {
		super(driver);
	}

	public void createNewGame(long expectedGameId) {
		System.out.println("Creating new game...");
		driver.findElement(By.id("name")).sendKeys("2");

		driver.findElement(By.id("gala_1")).click();

		driver.findElement(By.id("skr-dashboard-newgame-create-button")).click();

		(new WebDriverWait(driver, STANDARD_TIMEOUT))
				.until(ExpectedConditions.urlContains("game?id=" + expectedGameId));
	}
}