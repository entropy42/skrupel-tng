--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6 (Ubuntu 10.6-0ubuntu0.18.10.1)
-- Dumped by pg_dump version 10.6 (Ubuntu 10.6-0ubuntu0.18.10.1)

-- Started on 2019-03-10 16:48:50 CET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3133 (class 0 OID 97095)
-- Dependencies: 199
-- Data for Name: login; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

SET SESSION AUTHORIZATION DEFAULT;

ALTER TABLE public.login DISABLE TRIGGER ALL;

INSERT INTO public.login VALUES (1, 'admin', '$2a$10$ZkN/rSX.ophBRE.tZlTLaOxLZ1j15zE7XykX27jAITwWwWHvsNu6W');
INSERT INTO public.login VALUES (2, 'EASY', '$2a$10$aunuaMa2bTX7lQmUd4VgVul4/q7qF4pYLfQvRT0lVOM5DilpQmu2a');
INSERT INTO public.login VALUES (3, 'MEDIUM', '$2a$10$CJCzSuUlv2PaXHW6rP5LxOg7VB7stzxfzxQZ2StPI7/v3SJzW0Gje');
INSERT INTO public.login VALUES (4, 'HARD', '$2a$10$T2Tkh3/p8f7qeWHOg0xVZelgN0I8RJ2bFEstaoCnNIa5EC6CL7eN.');


ALTER TABLE public.login ENABLE TRIGGER ALL;

--
-- TOC entry 3139 (class 0 OID 97132)
-- Dependencies: 206
-- Data for Name: game; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.game DISABLE TRIGGER ALL;

INSERT INTO public.game VALUES (1, '2019-03-10 16:47:21.931', 1, 'Debug', 'SURVIVE', true, false, 4, '2019-03-10 16:47:21.931', true, true, true, false, 'EQUAL_DISTANCE', '1', 'LOSE_HOME_PLANET', 8, 1000, 200, 15000, 0.25, '1', '1', 0, NULL, 'gala_0', 0, 0, 3, 'LONG_RANGE_SENSORS');
INSERT INTO public.game VALUES (2, '2019-03-10 16:48:25.89', 1, '2', 'SURVIVE', true, false, 1, '2019-03-10 16:48:25.89', true, true, true, false, 'EQUAL_DISTANCE', '1', 'LOSE_HOME_PLANET', 2, 1000, 200, 15000, 0.25, '1', '1', 0, '0', 'gala_1', 0, 0, 3, 'LONG_RANGE_SENSORS');
INSERT INTO public.game VALUES (3, '2019-03-10 16:48:28.016', 1, '2', 'SURVIVE', false, false, 1, '2019-03-10 16:48:28.016', true, true, true, false, 'EQUAL_DISTANCE', '1', 'LOSE_HOME_PLANET', 2, 1000, 200, 15000, 0.25, '1', '1', 0, '0', 'gala_1', 0, 0, 3, 'LONG_RANGE_SENSORS');


ALTER TABLE public.game ENABLE TRIGGER ALL;

--
-- TOC entry 3135 (class 0 OID 97105)
-- Dependencies: 201
-- Data for Name: login_role; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.login_role DISABLE TRIGGER ALL;

INSERT INTO public.login_role VALUES (1, 1, 'ROLE_ADMIN');
INSERT INTO public.login_role VALUES (2, 1, 'ROLE_PLAYER');
INSERT INTO public.login_role VALUES (3, 2, 'ROLE_PLAYER');
INSERT INTO public.login_role VALUES (4, 2, 'ROLE_AI');
INSERT INTO public.login_role VALUES (5, 3, 'ROLE_PLAYER');
INSERT INTO public.login_role VALUES (6, 3, 'ROLE_AI');
INSERT INTO public.login_role VALUES (7, 4, 'ROLE_PLAYER');
INSERT INTO public.login_role VALUES (8, 4, 'ROLE_AI');


ALTER TABLE public.login_role ENABLE TRIGGER ALL;

--
-- TOC entry 3137 (class 0 OID 97119)
-- Dependencies: 203
-- Data for Name: native_species; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.native_species DISABLE TRIGGER ALL;



ALTER TABLE public.native_species ENABLE TRIGGER ALL;

--
-- TOC entry 3140 (class 0 OID 97154)
-- Dependencies: 207
-- Data for Name: race; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.race DISABLE TRIGGER ALL;

INSERT INTO public.race VALUES ('Orion Konglomerat', 'orion', NULL, NULL, 'I', 32, 1.35000002384185791, 1.04999995231628418, 1.14999997615814209, 0.75, 0.800000011920928955, 'Beteigeuze');


ALTER TABLE public.race ENABLE TRIGGER ALL;

--
-- TOC entry 3142 (class 0 OID 97164)
-- Dependencies: 209
-- Data for Name: player; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.player DISABLE TRIGGER ALL;

INSERT INTO public.player VALUES (3, true, 2, 'Orion Konglomerat', '0000ff', 1, 'EASY', 75, false, false);
INSERT INTO public.player VALUES (4, true, 2, 'Orion Konglomerat', '00aaff', 1, 'EASY', 106, false, false);
INSERT INTO public.player VALUES (6, true, 2, 'Orion Konglomerat', '00ff00', 1, 'EASY', 57, false, false);
INSERT INTO public.player VALUES (5, true, 2, 'Orion Konglomerat', '00ffaa', 1, 'EASY', 102, false, false);
INSERT INTO public.player VALUES (2, true, 2, 'Orion Konglomerat', 'aa00ff', 1, 'EASY', 17, false, false);
INSERT INTO public.player VALUES (7, true, 2, 'Orion Konglomerat', 'aaff00', 1, 'EASY', 148, false, false);
INSERT INTO public.player VALUES (9, true, 2, 'Orion Konglomerat', 'ff0000', 1, 'EASY', 130, false, false);
INSERT INTO public.player VALUES (8, true, 2, 'Orion Konglomerat', 'ffaa00', 1, 'EASY', 173, false, false);
INSERT INTO public.player VALUES (1, false, 1, 'Orion Konglomerat', 'ff00aa', 1, NULL, 43, false, true);
INSERT INTO public.player VALUES (10, false, 1, 'Orion Konglomerat', 'ff0000', 2, NULL, 213, false, true);
INSERT INTO public.player VALUES (11, false, 1, NULL, NULL, 3, NULL, NULL, false, false);


ALTER TABLE public.player ENABLE TRIGGER ALL;

--
-- TOC entry 3163 (class 0 OID 97461)
-- Dependencies: 234
-- Data for Name: news_entry; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.news_entry DISABLE TRIGGER ALL;

INSERT INTO public.news_entry VALUES (9, 1, 1, '/races/orion/bilder_schiffe/1.jpg', '2019-03-10 16:48:21.012', true, 'news_entry_ship_arrived', 'Debug ship');
INSERT INTO public.news_entry VALUES (10, 9, 1, '/races/orion/bilder_schiffe/6.jpg', '2019-03-10 16:48:21.013', true, 'news_entry_ship_arrived', 'Freighter');
INSERT INTO public.news_entry VALUES (11, 2, 1, '/races/orion/bilder_schiffe/1.jpg', '2019-03-10 16:48:21.015', true, 'news_entry_ship_arrived', 'Debug ship');
INSERT INTO public.news_entry VALUES (12, 4, 1, '/races/orion/bilder_schiffe/1.jpg', '2019-03-10 16:48:21.017', true, 'news_entry_ship_arrived', 'Debug ship');
INSERT INTO public.news_entry VALUES (13, 6, 1, '/races/orion/bilder_schiffe/1.jpg', '2019-03-10 16:48:21.019', true, 'news_entry_ship_arrived', 'Debug ship');
INSERT INTO public.news_entry VALUES (14, 7, 1, '/races/orion/bilder_schiffe/1.jpg', '2019-03-10 16:48:21.021', true, 'news_entry_ship_arrived', 'Debug ship');
INSERT INTO public.news_entry VALUES (15, 3, 1, '/races/orion/bilder_schiffe/1.jpg', '2019-03-10 16:48:21.022', true, 'news_entry_ship_arrived', 'Debug ship');
INSERT INTO public.news_entry VALUES (16, 9, 1, '/races/orion/bilder_schiffe/1.jpg', '2019-03-10 16:48:21.024', true, 'news_entry_ship_arrived', 'Debug ship');
INSERT INTO public.news_entry VALUES (17, 8, 1, '/races/orion/bilder_schiffe/1.jpg', '2019-03-10 16:48:21.025', true, 'news_entry_ship_arrived', 'Debug ship');
INSERT INTO public.news_entry VALUES (18, 5, 1, '/races/orion/bilder_schiffe/1.jpg', '2019-03-10 16:48:21.027', true, 'news_entry_ship_arrived', 'Debug ship');
INSERT INTO public.news_entry VALUES (19, 1, 1, '/races/orion/bilder_schiffe/2.jpg', '2019-03-10 16:48:21.07', true, 'news_entry_ship_constructed', 'test');


ALTER TABLE public.news_entry ENABLE TRIGGER ALL;

--
-- TOC entry 3144 (class 0 OID 97193)
-- Dependencies: 211
-- Data for Name: starbase; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.starbase DISABLE TRIGGER ALL;

INSERT INTO public.starbase VALUES (1, 'STAR_BASE', 'Starbase 1', NULL, 0, 5, 8, 1, 1);
INSERT INTO public.starbase VALUES (3, 'STAR_BASE', 'Starbase 1', NULL, 0, 8, 8, 8, 8);
INSERT INTO public.starbase VALUES (4, 'STAR_BASE', 'Starbase 1', NULL, 0, 8, 8, 8, 8);
INSERT INTO public.starbase VALUES (6, 'STAR_BASE', 'Starbase 1', NULL, 0, 8, 8, 8, 8);
INSERT INTO public.starbase VALUES (5, 'STAR_BASE', 'Starbase 1', NULL, 0, 8, 8, 8, 8);
INSERT INTO public.starbase VALUES (2, 'STAR_BASE', 'Starbase 1', NULL, 0, 8, 8, 8, 8);
INSERT INTO public.starbase VALUES (7, 'STAR_BASE', 'Starbase 1', NULL, 0, 8, 8, 8, 8);
INSERT INTO public.starbase VALUES (9, 'STAR_BASE', 'Starbase 1', NULL, 0, 8, 8, 8, 8);
INSERT INTO public.starbase VALUES (8, 'STAR_BASE', 'Starbase 1', NULL, 0, 8, 8, 8, 8);
INSERT INTO public.starbase VALUES (10, 'STAR_BASE', 'Starbase 1', NULL, 0, 0, 0, 0, 0);


ALTER TABLE public.starbase ENABLE TRIGGER ALL;

--
-- TOC entry 3146 (class 0 OID 97206)
-- Dependencies: 213
-- Data for Name: planet; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.planet DISABLE TRIGGER ALL;

INSERT INTO public.planet VALUES (1, 1, 'Peliar Zel', 969, 693, 'M', 82, 0, 43, 43, 57, 20, '1_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2776, 2332, 4710, 1699, 1, 1, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (2, 1, 'Benecia', 742, 965, 'L', 81, 0, 46, 60, 21, 56, '4_12', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1234, 4215, 1660, 2877, 4, 4, 4, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (3, 1, 'Benzar', 53, 596, 'L', 120, 0, 70, 15, 5, 51, '4_11', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2751, 4086, 4883, 4998, 6, 2, 1, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (4, 1, 'Kenda II', 579, 216, 'G', 108, 0, 43, 32, 32, 8, '5_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3088, 2762, 4441, 2108, 10, 1, 1, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (5, 1, 'Delta Rana', 190, 917, 'J', 0, 0, 6, 12, 32, 27, '3_11', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2450, 2474, 4032, 2294, 10, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (7, 1, 'Klavdia III', 439, 854, 'J', 1, 0, 16, 6, 57, 25, '3_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3491, 3416, 4836, 888, 2, 6, 4, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (8, 1, 'Nel Bato', 819, 635, 'G', 180, 0, 14, 68, 62, 56, '5_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3957, 3664, 907, 4557, 1, 6, 4, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (9, 1, 'Endor', 666, 711, 'F', 30, 0, 39, 23, 26, 57, '9_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2170, 3570, 1229, 4495, 2, 10, 10, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (10, 1, 'Pspopta', 138, 887, 'C', 117, 0, 62, 51, 19, 4, '7_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1405, 1232, 3381, 1315, 1, 6, 6, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (11, 1, 'Minara', 678, 542, 'G', 103, 0, 56, 65, 2, 37, '5_11', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4979, 1068, 3298, 883, 2, 1, 10, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (12, 1, 'Duro', 789, 552, 'M', 50, 0, 12, 39, 67, 6, '1_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2550, 4720, 4146, 3219, 1, 2, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (13, 1, 'Mintaka', 839, 578, 'N', 78, 0, 3, 17, 68, 20, '2_15', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2511, 4909, 1698, 2848, 1, 6, 4, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (14, 1, 'Galvin', 505, 174, 'N', 31, 0, 19, 8, 17, 60, '2_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2703, 4642, 4420, 4451, 2, 10, 10, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (15, 1, 'Sluis Van', 849, 66, 'C', 146, 0, 24, 66, 40, 57, '7_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1158, 2641, 1208, 2364, 2, 6, 6, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (16, 1, 'CetiAlpha V', 783, 181, 'K', 40, 0, 18, 26, 52, 11, '8_30', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 844, 4013, 3895, 2324, 1, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (18, 1, 'Morska', 414, 445, 'F', 68, 0, 50, 17, 65, 49, '9_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 962, 2303, 4567, 2963, 10, 1, 1, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (19, 1, 'Otar II', 313, 877, 'G', 168, 0, 26, 46, 20, 30, '5_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2900, 4456, 1875, 4532, 4, 10, 4, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (20, 1, 'AlphaCentauri', 948, 327, 'L', 107, 0, 10, 41, 30, 35, '4_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3723, 2304, 1436, 3333, 2, 1, 10, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (21, 1, 'Garon', 5, 340, 'L', 111, 0, 42, 20, 55, 19, '4_14', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1093, 4361, 3440, 4854, 6, 1, 4, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (22, 1, 'Zentrifaal', 569, 514, 'M', 95, 0, 69, 14, 5, 29, '1_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3763, 1773, 1187, 1518, 2, 1, 4, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (23, 1, 'Daran', 936, 535, 'N', 54, 0, 28, 35, 22, 23, '2_24', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4241, 2914, 4517, 2700, 4, 10, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (24, 1, 'Makus', 190, 55, 'L', 117, 0, 62, 60, 31, 24, '4_13', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4668, 3243, 1971, 4525, 1, 1, 10, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (25, 1, 'Borka', 268, 708, 'C', 103, 0, 44, 3, 28, 6, '7_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1789, 4387, 4644, 4238, 10, 4, 6, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (26, 1, 'Troyius', 151, 201, 'M', 78, 0, 55, 15, 17, 38, '1_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4216, 4430, 4059, 4709, 2, 4, 6, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (27, 1, 'Pentarus', 406, 610, 'I', 144, 0, 7, 60, 40, 66, '6_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 839, 3483, 1125, 3450, 2, 2, 10, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (28, 1, 'Lamuu', 643, 336, 'J', 9, 0, 62, 39, 40, 17, '3_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2103, 1227, 3275, 3228, 4, 4, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (29, 1, 'Nequencia', 549, 752, 'G', 162, 0, 1, 29, 22, 69, '5_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2053, 987, 2818, 1246, 10, 2, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (30, 1, 'Andevian', 728, 730, 'L', 58, 0, 14, 37, 54, 42, '4_11', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2841, 4188, 4857, 3475, 10, 1, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (31, 1, 'Archer', 498, 923, 'J', 0, 0, 15, 7, 13, 67, '3_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4408, 3047, 2825, 2271, 10, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (32, 1, 'AlphaMajoris', 459, 968, 'F', 39, 0, 33, 62, 46, 1, '9_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 960, 1494, 968, 4967, 10, 6, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (33, 1, 'Cygnet', 341, 806, 'C', 161, 0, 50, 18, 66, 21, '7_11', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4720, 4326, 2564, 3201, 4, 1, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (34, 1, 'Talos', 487, 5, 'K', 45, 0, 44, 27, 28, 37, '8_23', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1360, 2223, 4770, 2853, 1, 2, 6, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (35, 1, 'Bajor', 798, 726, 'C', 119, 0, 56, 19, 37, 7, '7_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2524, 1709, 1780, 1199, 4, 2, 10, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (36, 1, 'Argelius', 460, 796, 'G', 171, 0, 26, 51, 66, 56, '5_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2612, 1429, 1260, 3611, 2, 2, 10, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (37, 1, 'Norpin', 11, 113, 'G', 111, 0, 53, 57, 47, 54, '5_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1977, 4935, 1792, 2076, 10, 4, 4, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (38, 1, 'Deneva', 693, 424, 'C', 99, 0, 67, 10, 49, 31, '7_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1199, 1273, 1169, 893, 1, 6, 6, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (39, 1, 'Dantooine', 830, 878, 'C', 93, 0, 11, 16, 3, 35, '7_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2538, 2933, 4308, 3738, 4, 6, 1, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (40, 1, 'Qualor', 999, 583, 'K', 22, 0, 10, 5, 54, 68, '8_29', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1803, 3649, 814, 870, 1, 1, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (41, 1, 'Borgolis', 375, 659, 'C', 138, 0, 11, 45, 40, 20, '7_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2364, 2191, 2958, 1556, 1, 4, 2, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (42, 1, 'Qualor', 659, 962, 'M', 60, 0, 63, 65, 57, 51, '1_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2520, 4002, 2741, 4195, 6, 2, 4, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (6, 1, 'Gamelan', 863, 807, 'N', 31, 375, 32, 0, 0, 0, '2_18', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4050, 1569, 4008, 1603, 10, 6, 6, 6, 1500, 0, 0, 9, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (45, 1, 'Melona', 321, 533, 'G', 145, 0, 22, 60, 2, 48, '5_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3933, 3597, 1288, 3416, 4, 6, 4, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (46, 1, 'Pspopta', 985, 768, 'M', 92, 0, 64, 22, 43, 45, '1_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1026, 3629, 3472, 1488, 1, 4, 1, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (47, 1, 'Mystery', 497, 635, 'L', 58, 0, 44, 36, 10, 44, '4_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1263, 4998, 3654, 4817, 4, 4, 10, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (48, 1, 'Zetar', 924, 380, 'C', 105, 0, 2, 49, 49, 49, '7_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2806, 4386, 4686, 4529, 1, 4, 1, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (49, 1, 'Loren III', 427, 150, 'J', 9, 0, 48, 11, 5, 46, '3_14', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4072, 3431, 3242, 4736, 1, 1, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (50, 1, 'QuadraSigma', 182, 727, 'I', 75, 0, 66, 54, 67, 46, '6_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 893, 987, 4626, 4514, 1, 1, 4, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (51, 1, 'Kuat', 932, 606, 'G', 184, 0, 7, 55, 17, 11, '5_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3592, 3555, 3781, 1037, 10, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (52, 1, 'Zentrifaal', 747, 786, 'K', 43, 0, 1, 30, 2, 70, '8_11', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4083, 1949, 1492, 4624, 10, 10, 6, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (53, 1, 'Anobis', 759, 1, 'K', 38, 0, 47, 51, 32, 35, '8_13', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1091, 4961, 4635, 2388, 2, 4, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (54, 1, 'Lixos', 557, 640, 'J', 4, 0, 46, 25, 44, 69, '3_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3367, 4593, 1968, 4374, 1, 4, 10, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (55, 1, 'Bilana', 594, 112, 'J', 5, 0, 29, 21, 1, 13, '3_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2190, 4885, 4050, 3276, 4, 10, 4, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (56, 1, 'Osarian', 425, 234, 'G', 98, 0, 68, 69, 70, 38, '5_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4220, 4803, 3650, 1539, 10, 10, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (58, 1, 'Gagarin', 461, 477, 'G', 158, 0, 23, 23, 7, 52, '5_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1465, 1133, 4520, 957, 2, 10, 4, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (59, 1, 'Devidia', 18, 852, 'G', 128, 0, 46, 25, 61, 23, '5_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1607, 2053, 3444, 3326, 4, 4, 4, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (60, 1, 'Cerebus', 76, 796, 'K', 36, 0, 60, 5, 13, 50, '8_14', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1915, 2949, 4845, 2527, 4, 1, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (61, 1, 'Tarsus', 139, 617, 'L', 55, 0, 12, 30, 20, 20, '4_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2472, 4031, 4907, 2796, 6, 6, 4, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (62, 1, 'Sullust', 313, 608, 'N', 73, 0, 27, 33, 64, 14, '2_16', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4278, 3587, 2254, 2443, 6, 2, 10, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (63, 1, 'Arkon', 47, 53, 'F', 58, 0, 43, 31, 54, 2, '9_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4647, 2209, 2555, 4679, 2, 2, 4, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (64, 1, 'Narenda', 264, 214, 'N', 38, 0, 23, 62, 6, 42, '2_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1159, 3062, 2658, 1023, 1, 4, 4, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (65, 1, 'BetaPortolan', 582, 691, 'I', 140, 0, 36, 45, 19, 51, '6_21', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3900, 4663, 4512, 3410, 6, 4, 10, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (66, 1, 'Deinonychus', 332, 367, 'J', 9, 0, 47, 8, 16, 19, '3_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1476, 4212, 4438, 2374, 6, 6, 1, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (67, 1, 'Ethernia', 221, 341, 'K', 31, 0, 47, 11, 53, 61, '8_24', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1448, 1654, 3259, 1788, 2, 6, 10, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (68, 1, 'SigmaDracon', 153, 413, 'G', 87, 0, 17, 50, 45, 68, '5_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2665, 1546, 1827, 2140, 1, 10, 10, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (70, 1, 'Thagarum', 190, 454, 'L', 121, 0, 37, 53, 58, 37, '4_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2366, 4769, 2239, 2035, 1, 4, 1, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (71, 1, 'Attalan', 556, 864, 'C', 161, 0, 2, 40, 49, 34, '7_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3330, 1174, 1181, 4818, 1, 1, 2, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (72, 1, 'Arkta', 498, 65, 'L', 110, 0, 10, 28, 50, 2, '4_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1893, 3251, 4118, 1471, 1, 4, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (73, 1, 'Betazed', 548, 950, 'M', 83, 0, 56, 66, 63, 66, '1_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2124, 3114, 3517, 2112, 10, 1, 2, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (74, 1, 'Arkta', 916, 36, 'L', 51, 0, 63, 70, 51, 8, '4_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 848, 1541, 3356, 4621, 10, 10, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (76, 1, 'AlphaCarinae', 267, 289, 'C', 157, 0, 52, 31, 13, 3, '7_11', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2544, 2921, 3422, 3482, 10, 2, 6, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (77, 1, 'Ekos', 998, 992, 'G', 117, 0, 18, 27, 57, 47, '5_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2262, 3067, 3859, 2576, 10, 10, 10, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (78, 1, 'Taurus', 127, 95, 'I', 121, 0, 38, 30, 13, 14, '6_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3803, 2815, 1550, 2942, 1, 2, 6, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (80, 1, 'Agamar', 993, 247, 'I', 113, 0, 28, 65, 68, 63, '6_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3228, 1617, 4696, 2099, 10, 10, 6, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (81, 1, 'Sirius', 569, 341, 'C', 104, 0, 34, 7, 60, 44, '7_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 810, 2737, 1398, 4609, 1, 2, 10, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (82, 1, 'Gammor', 837, 139, 'G', 146, 0, 5, 25, 18, 43, '5_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1670, 3405, 4234, 2654, 1, 1, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (83, 1, 'Ertrus', 127, 477, 'G', 93, 0, 1, 23, 14, 55, '5_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4327, 4221, 800, 2935, 1, 2, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (84, 1, 'Icor', 975, 893, 'C', 97, 0, 23, 19, 36, 12, '7_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4383, 1760, 2724, 3634, 6, 2, 2, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (44, 1, 'Sempidal', 239, 518, 'I', 126, 339, 11, 57, 21, 26, '6_4', NULL, NULL, NULL, 0, 7, 0, false, 0, false, false, 0, false, 0, 0, 4323, 2482, 2863, 2563, 1, 6, 1, 1, 1500, 0, 0, 4, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (79, 1, 'Arneb', 624, 448, 'N', 57, 339, 9, 0, 0, 38, '2_24', NULL, NULL, NULL, 0, 7, 0, false, 0, false, false, 0, false, 0, 0, 3631, 2700, 3154, 896, 4, 6, 1, 4, 1500, 0, 0, 5, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (69, 1, 'Tiburon', 629, 830, 'M', 55, 339, 37, 0, 16, 2, '1_1', NULL, NULL, NULL, 0, 7, 0, false, 0, false, false, 0, false, 0, 0, 3530, 1597, 4435, 2736, 2, 1, 2, 6, 1500, 0, 0, 7, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (85, 1, 'Zentrifaal', 368, 951, 'M', 92, 0, 40, 2, 23, 2, '1_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2202, 4223, 1089, 895, 1, 1, 1, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (86, 1, 'Fabrina', 851, 474, 'I', 95, 0, 26, 24, 9, 17, '6_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3731, 2940, 4020, 2509, 1, 10, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (87, 1, 'Bynaus', 248, 787, 'I', 82, 0, 19, 5, 20, 52, '6_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1789, 2650, 3937, 4814, 10, 4, 10, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (88, 1, 'Arneb', 432, 64, 'C', 99, 0, 9, 61, 23, 70, '7_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3262, 1238, 2461, 4007, 2, 2, 2, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (89, 1, 'Ryloth', 979, 1, 'I', 137, 0, 42, 1, 53, 17, '6_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4666, 4209, 1975, 1677, 2, 4, 4, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (90, 1, 'Borka', 910, 193, 'I', 95, 0, 25, 38, 4, 7, '6_18', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4152, 3836, 3436, 897, 4, 6, 10, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (91, 1, 'Kuat', 746, 274, 'N', 45, 0, 54, 44, 39, 45, '2_18', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4424, 4257, 3958, 3110, 10, 6, 4, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (92, 1, 'Icor', 620, 631, 'L', 78, 0, 8, 18, 55, 37, '4_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3320, 1144, 2919, 831, 1, 1, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (93, 1, 'Corulag', 781, 454, 'K', 46, 0, 68, 16, 6, 39, '8_14', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2215, 3529, 1536, 1430, 10, 6, 6, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (94, 1, 'Halut', 734, 594, 'K', 28, 0, 28, 49, 33, 32, '8_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4770, 985, 4368, 2315, 1, 1, 2, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (95, 1, 'Honoghr', 591, 280, 'J', 7, 0, 2, 64, 60, 50, '3_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3464, 2150, 4383, 4185, 10, 2, 4, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (96, 1, 'Aolian', 578, 26, 'C', 125, 0, 61, 63, 65, 69, '7_11', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2615, 3317, 3182, 2567, 4, 1, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (97, 1, 'Gatas', 306, 14, 'I', 131, 0, 35, 8, 53, 21, '6_13', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3493, 4337, 2861, 2584, 6, 4, 6, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (98, 1, 'Zophengorn', 563, 412, 'G', 147, 0, 70, 55, 31, 53, '5_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1643, 1631, 2886, 833, 1, 2, 10, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (99, 1, 'Aralon', 205, 247, 'C', 135, 0, 20, 59, 29, 58, '7_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4824, 3464, 4354, 4262, 4, 1, 2, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (101, 1, 'Zorkum', 442, 372, 'L', 74, 0, 2, 18, 24, 35, '4_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4459, 3809, 4224, 3415, 6, 2, 4, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (103, 1, 'Moha', 740, 345, 'L', 60, 0, 39, 59, 64, 51, '4_14', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4393, 2042, 1472, 1209, 10, 6, 6, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (104, 1, 'Ophicus', 887, 699, 'J', 9, 0, 50, 14, 34, 49, '3_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2243, 3840, 4871, 4485, 1, 1, 4, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (105, 1, 'Orllyndie', 235, 146, 'N', 39, 0, 47, 61, 43, 3, '2_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 938, 2979, 4249, 4210, 6, 2, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (107, 1, 'Alfa', 699, 843, 'N', 42, 0, 47, 22, 70, 57, '2_18', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 859, 1145, 1768, 3123, 2, 2, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (108, 1, 'Delb', 719, 657, 'I', 89, 0, 52, 29, 48, 33, '6_11', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2437, 2839, 1377, 4281, 2, 4, 10, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (109, 1, 'Gäa', 827, 969, 'I', 116, 0, 31, 25, 25, 19, '6_20', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1844, 1387, 2124, 3052, 2, 6, 1, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (110, 1, 'Kostolain', 129, 816, 'J', 2, 0, 50, 62, 34, 47, '3_12', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2155, 3507, 1574, 2449, 2, 1, 2, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (111, 1, 'Danula', 64, 131, 'J', 5, 0, 22, 70, 46, 8, '3_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 905, 1147, 2561, 4909, 2, 2, 1, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (112, 1, 'L370', 576, 574, 'N', 67, 0, 66, 50, 63, 13, '2_22', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1651, 2796, 1127, 2147, 1, 2, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (113, 1, 'QuadraSigma', 435, 311, 'G', 92, 0, 65, 3, 26, 24, '5_11', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3124, 1092, 4262, 1791, 10, 1, 4, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (114, 1, 'Kurkos', 529, 456, 'C', 160, 0, 39, 56, 29, 7, '7_12', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2894, 4277, 2926, 4108, 4, 10, 10, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (115, 1, 'Bajor', 239, 91, 'N', 73, 0, 65, 32, 30, 54, '2_12', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3898, 3337, 2845, 1987, 6, 1, 4, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (116, 1, 'Elas', 276, 955, 'K', 21, 0, 70, 62, 45, 14, '8_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4248, 3738, 1349, 1066, 2, 10, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (117, 1, 'Ethernia', 996, 178, 'F', 26, 0, 63, 69, 67, 26, '9_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4929, 920, 2900, 4421, 4, 1, 6, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (118, 1, 'Icor', 889, 948, 'L', 92, 0, 4, 63, 47, 16, '4_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2993, 4862, 2420, 2299, 6, 10, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (119, 1, 'Ord Mantell', 3, 462, 'C', 83, 0, 62, 48, 29, 15, '7_12', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4750, 1406, 4002, 3866, 2, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (121, 1, 'Tycho', 738, 114, 'K', 21, 0, 25, 11, 31, 4, '8_23', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2727, 990, 2198, 1280, 10, 2, 10, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (122, 1, 'Topsid', 483, 422, 'C', 131, 0, 56, 2, 51, 33, '7_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1639, 3399, 2379, 1870, 6, 6, 6, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (123, 1, 'Gyndine', 272, 405, 'I', 94, 0, 15, 63, 67, 44, '6_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2387, 4459, 3517, 3043, 2, 2, 6, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (124, 1, 'Gagarin', 72, 391, 'C', 92, 0, 42, 18, 30, 47, '7_12', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4427, 2177, 4714, 1488, 6, 4, 10, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (125, 1, 'Peliar Zel', 935, 471, 'C', 135, 0, 66, 51, 60, 31, '7_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2619, 2038, 3401, 4046, 1, 2, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (126, 1, 'Antica', 833, 390, 'L', 87, 0, 54, 39, 56, 57, '4_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4515, 1740, 3433, 3771, 4, 6, 1, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (120, 1, 'Jaros II', 181, 976, 'M', 96, 339, 53, 57, 23, 52, '1_1', NULL, NULL, NULL, 0, 7, 0, false, 0, false, false, 0, false, 0, 0, 4697, 4239, 3409, 4588, 6, 4, 2, 1, 1500, 0, 0, 6, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (100, 1, 'Aolian', 933, 850, 'K', 20, 339, 18, 0, 0, 31, '8_11', NULL, NULL, NULL, 0, 7, 0, false, 0, false, false, 0, false, 0, 0, 2235, 3635, 4987, 2731, 1, 2, 4, 10, 1500, 0, 0, 9, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (127, 1, 'Khitomer', 928, 995, 'L', 73, 0, 43, 19, 70, 12, '4_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2141, 1249, 2420, 1335, 1, 1, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (128, 1, 'Omicron Ceti', 19, 1, 'M', 40, 0, 62, 18, 1, 16, '1_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1103, 3427, 2586, 2475, 10, 1, 6, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (129, 1, 'Eden', 523, 820, 'I', 133, 0, 59, 33, 37, 12, '6_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2190, 3127, 4100, 3768, 10, 6, 4, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (131, 1, 'Organia', 833, 278, 'N', 50, 0, 57, 59, 33, 47, '2_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4407, 3401, 1837, 3131, 4, 4, 6, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (132, 1, 'Kaldra', 104, 745, 'I', 157, 0, 68, 40, 61, 39, '6_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4691, 4217, 4010, 4523, 2, 6, 4, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (133, 1, 'Ingraham B', 328, 187, 'C', 128, 0, 45, 52, 41, 26, '7_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1198, 4403, 3205, 4181, 10, 1, 6, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (134, 1, 'Koris', 326, 110, 'K', 29, 0, 4, 43, 25, 53, '8_29', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3412, 3293, 2037, 4159, 4, 10, 6, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (135, 1, 'AlphaCarinae', 105, 170, 'F', 37, 0, 50, 42, 27, 61, '9_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4187, 1357, 2734, 2130, 4, 4, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (136, 1, 'Sphinx', 11, 769, 'N', 51, 0, 53, 50, 62, 44, '2_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4098, 3766, 1292, 990, 4, 1, 6, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (137, 1, 'Pegos', 906, 122, 'G', 87, 0, 40, 57, 11, 15, '5_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3577, 4716, 4225, 2002, 4, 6, 4, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (139, 1, 'Nimbus III', 161, 6, 'I', 97, 0, 51, 6, 13, 24, '6_22', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1632, 2931, 4286, 2893, 4, 10, 10, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (140, 1, 'Cirrus', 197, 598, 'L', 124, 0, 55, 70, 61, 51, '4_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3950, 1469, 4510, 2681, 4, 6, 1, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (141, 1, 'Ord Mantell', 684, 92, 'C', 125, 0, 21, 41, 45, 47, '7_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 814, 3016, 2025, 1975, 4, 4, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (142, 1, 'Balosnee', 918, 256, 'N', 49, 0, 48, 40, 58, 54, '2_15', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2447, 841, 1510, 3521, 4, 10, 1, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (143, 1, 'Makus', 998, 302, 'N', 67, 0, 25, 14, 44, 11, '2_12', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4918, 2232, 2606, 1404, 10, 4, 4, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (144, 1, 'Ophicus', 485, 536, 'L', 51, 0, 1, 65, 5, 12, '4_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2423, 3353, 2242, 1804, 6, 4, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (145, 1, 'Arkon', 752, 862, 'N', 63, 0, 18, 26, 64, 64, '2_19', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2634, 1895, 2055, 1799, 1, 10, 4, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (146, 1, 'Triadus', 439, 693, 'L', 112, 0, 28, 28, 19, 30, '4_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4464, 4926, 2593, 1763, 6, 4, 1, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (147, 1, 'Deriben', 4, 545, 'C', 111, 0, 51, 4, 20, 15, '7_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3218, 1891, 2907, 2044, 1, 6, 2, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (149, 1, 'Lixos', 789, 51, 'L', 63, 0, 32, 47, 48, 34, '4_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1110, 972, 3525, 3781, 1, 10, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (150, 1, 'Talus', 2, 925, 'G', 107, 0, 3, 67, 47, 16, '5_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1511, 4784, 4333, 2322, 10, 4, 4, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (151, 1, 'Tynna', 964, 95, 'C', 144, 0, 47, 5, 4, 23, '7_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3058, 1762, 1883, 3179, 10, 1, 6, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (152, 1, 'Phönix', 978, 404, 'C', 100, 0, 30, 42, 12, 70, '7_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1025, 1176, 2351, 4994, 4, 10, 10, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (153, 1, 'Danula', 685, 771, 'L', 106, 0, 22, 4, 39, 33, '4_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2263, 1207, 1520, 2376, 10, 6, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (154, 1, 'Lemma II', 88, 254, 'C', 81, 0, 65, 25, 52, 25, '7_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4913, 3503, 882, 4703, 4, 4, 6, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (155, 1, 'Acamar', 402, 535, 'M', 41, 0, 1, 13, 70, 51, '1_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1937, 1270, 2322, 1186, 1, 2, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (156, 1, 'Danula', 26, 978, 'G', 91, 0, 67, 52, 26, 4, '5_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2180, 4495, 1605, 4205, 6, 2, 6, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (157, 1, 'Kea IV', 55, 895, 'L', 54, 0, 4, 16, 6, 66, '4_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2664, 4452, 3310, 1751, 6, 6, 6, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (158, 1, 'Holberg 917-G', 342, 456, 'L', 111, 0, 31, 22, 11, 11, '4_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1008, 4731, 2089, 3721, 2, 4, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (159, 1, 'Carillon', 19, 648, 'F', 37, 0, 15, 68, 36, 54, '9_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3691, 4152, 3432, 2021, 4, 10, 10, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (161, 1, 'Honoghr', 707, 491, 'J', 9, 0, 27, 12, 39, 53, '3_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2903, 3135, 4131, 3995, 1, 6, 1, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (162, 1, 'Aldebaran III', 430, 911, 'G', 129, 0, 53, 55, 30, 10, '5_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1913, 3908, 2302, 1384, 10, 2, 1, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (163, 1, 'Bringloid', 674, 227, 'C', 154, 0, 63, 37, 69, 21, '7_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1211, 4625, 1295, 3338, 1, 1, 6, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (164, 1, 'Pspopta', 711, 183, 'M', 85, 0, 24, 41, 19, 38, '1_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1522, 3691, 1731, 1923, 1, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (165, 1, 'Arkta', 870, 532, 'N', 35, 0, 54, 38, 25, 16, '2_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3306, 1717, 819, 4882, 6, 10, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (166, 1, 'YagDul', 255, 616, 'L', 94, 0, 57, 67, 57, 29, '4_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2391, 4409, 2458, 1342, 1, 1, 10, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (167, 1, 'Kataan', 612, 914, 'M', 95, 0, 12, 62, 56, 36, '1_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2003, 1789, 2309, 3181, 4, 4, 4, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (168, 1, 'Breel', 494, 321, 'K', 38, 0, 54, 28, 51, 37, '8_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2518, 4234, 4687, 3290, 6, 4, 10, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (138, 1, 'Theta Cygni', 60, 316, 'M', 51, 339, 31, 41, 40, 21, '1_7', NULL, NULL, NULL, 0, 7, 0, false, 0, false, false, 0, false, 0, 0, 4729, 4498, 4277, 4496, 6, 6, 2, 2, 1500, 0, 0, 2, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (160, 1, 'Nimbus III', 649, 46, 'C', 87, 339, 24, 45, 22, 40, '7_10', NULL, NULL, NULL, 0, 7, 0, false, 0, false, false, 0, false, 0, 0, 809, 2136, 3638, 2348, 6, 4, 4, 1, 1500, 0, 0, 8, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (169, 1, 'Fondor', 666, 901, 'G', 162, 0, 50, 55, 11, 30, '5_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3969, 4603, 3100, 2780, 10, 4, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (170, 1, 'Nemptox', 479, 734, 'C', 143, 0, 58, 68, 24, 50, '7_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3310, 1606, 2914, 1681, 1, 10, 1, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (171, 1, 'Gaspar', 525, 259, 'L', 53, 0, 63, 4, 2, 7, '4_11', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3779, 2792, 2926, 1344, 2, 4, 4, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (172, 1, 'Reese', 195, 853, 'J', 8, 0, 1, 62, 46, 5, '3_13', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4846, 4919, 2867, 2798, 1, 6, 2, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (174, 1, 'Tarsus', 162, 139, 'K', 46, 0, 44, 47, 54, 21, '8_26', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3230, 939, 4923, 2872, 10, 10, 10, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (175, 1, 'Nel Bato', 401, 17, 'N', 31, 0, 1, 64, 32, 15, '2_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1880, 1014, 1673, 4640, 4, 2, 10, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (176, 1, 'Carraya', 384, 861, 'N', 46, 0, 23, 29, 28, 57, '2_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2513, 4490, 1906, 4736, 6, 2, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (177, 1, 'Barzan', 182, 526, 'I', 139, 0, 36, 60, 5, 55, '6_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2537, 891, 4449, 3344, 4, 1, 6, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (178, 1, 'Lepso', 58, 714, 'I', 155, 0, 66, 22, 52, 52, '6_12', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 893, 2290, 3282, 1224, 2, 4, 1, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (179, 1, 'Bringloid', 357, 305, 'M', 94, 0, 70, 46, 33, 5, '1_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2912, 3733, 3334, 4073, 2, 10, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (180, 1, 'Celtis', 396, 787, 'L', 116, 0, 34, 18, 61, 59, '4_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4714, 4778, 3653, 3197, 10, 2, 4, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (181, 1, 'Beta V', 1, 205, 'C', 116, 0, 8, 35, 22, 25, '7_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2708, 3261, 2609, 1041, 10, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (182, 1, 'Cheron', 256, 875, 'M', 84, 0, 35, 20, 54, 43, '1_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2025, 3098, 1099, 1454, 6, 2, 4, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (183, 1, 'Beta V', 886, 425, 'K', 49, 0, 32, 40, 54, 37, '8_18', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4233, 3179, 1555, 1311, 6, 6, 10, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (185, 1, 'Cestus', 691, 294, 'M', 63, 0, 62, 39, 57, 13, '1_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1568, 4667, 2495, 2737, 10, 4, 2, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (186, 1, 'Kessel', 994, 483, 'K', 46, 0, 25, 35, 50, 41, '8_31', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2726, 4520, 4335, 829, 2, 10, 1, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (187, 1, 'Mariah', 219, 678, 'M', 96, 0, 64, 57, 34, 36, '1_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2383, 2139, 1884, 1728, 2, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (184, 1, 'Ethernia', 316, 745, 'K', 47, 339, 52, 25, 2, 9, '8_12', NULL, NULL, NULL, 0, 7, 0, false, 0, false, false, 0, false, 0, 0, 2302, 3598, 1424, 4240, 10, 4, 4, 6, 1500, 0, 0, 3, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (75, 1, 'Beteigeuze', 371, 728, 'I', 32, 3920, 17, 6, 13, 2, '6_10', 3, NULL, 3, 44133, 0, 5, false, 21, false, false, 5, false, 0, 0, 2083, 2080, 2122, 1885, 2, 2, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (106, 1, 'Beteigeuze', 279, 469, 'I', 32, 4412, 45, 12, 26, 0, '6_17', 4, NULL, 4, 49147, 0, 5, false, 43, false, false, 5, false, 0, 0, 1777, 2437, 1563, 2374, 2, 2, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (57, 1, 'Beteigeuze', 120, 964, 'I', 32, 3817, 21, 9, 29, 14, '6_14', 6, NULL, 6, 42948, 0, 5, false, 43, false, false, 5, false, 0, 0, 2474, 1851, 1988, 2253, 1, 1, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (43, 1, 'Beteigeuze', 75, 527, 'I', 32, 6668, 41, 42, 50, 24, '6_6', 1, NULL, 1, 54499, 0, 10, true, 43, true, false, 5, true, 0, 0, 2021, 1735, 1699, 1643, 2, 1, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (148, 1, 'Beteigeuze', 607, 774, 'I', 32, 4447, 48, 3, 21, 6, '6_3', 7, NULL, 7, 50389, 0, 5, false, 43, false, false, 5, false, 0, 0, 1566, 1563, 1984, 1900, 2, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (130, 1, 'Beteigeuze', 921, 795, 'I', 32, 3807, 16, 6, 29, 2, '6_8', 9, NULL, 9, 43250, 0, 5, false, 43, false, false, 5, false, 0, 0, 2074, 1977, 2360, 2328, 1, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (173, 1, 'Beteigeuze', 705, 37, 'I', 32, 3995, 21, 8, 13, 3, '6_2', 8, NULL, 8, 46242, 0, 5, false, 21, false, false, 5, false, 0, 0, 2388, 1807, 1602, 2173, 1, 1, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (188, 2, 'Carillon', 662, 370, 'G', 146, 0, 9, 70, 27, 41, '5_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2005, 4251, 3106, 4772, 2, 1, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (189, 2, 'Rodia', 238, 263, 'F', 29, 0, 36, 43, 58, 62, '9_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1027, 3432, 841, 4772, 1, 6, 2, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (190, 2, 'Ethernia', 685, 231, 'I', 79, 0, 34, 19, 56, 6, '6_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 830, 2987, 2529, 928, 4, 1, 10, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (191, 2, 'Lixos', 361, 348, 'F', 60, 0, 35, 5, 44, 70, '9_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2198, 4850, 932, 2023, 1, 10, 6, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (192, 2, 'Melona', 457, 328, 'K', 36, 0, 69, 61, 60, 3, '8_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4420, 3499, 4394, 2784, 10, 6, 6, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (193, 2, 'Dantooine', 578, 694, 'C', 144, 0, 30, 5, 66, 31, '7_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3332, 1662, 3115, 4356, 4, 2, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (194, 2, 'Sluis Van', 390, 267, 'M', 98, 0, 60, 21, 34, 16, '1_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1181, 3001, 4936, 2889, 1, 1, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (195, 2, 'AlphaMajoris', 732, 447, 'N', 51, 0, 42, 36, 53, 53, '2_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3795, 3277, 4425, 2304, 2, 10, 6, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (196, 2, 'Ogus', 465, 962, 'N', 49, 0, 67, 51, 60, 24, '2_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4342, 4983, 1149, 1324, 4, 10, 1, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (213, 2, 'Beteigeuze', 573, 446, 'I', 32, 15000, 64, 55, 53, 63, '6_20', 10, NULL, 10, 53624, 5, 5, false, 5, false, false, 5, false, 0, 0, 1907, 2239, 2127, 2047, 2, 1, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (197, 2, 'Mimas', 265, 610, 'I', 130, 0, 44, 13, 15, 54, '6_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3847, 2271, 4373, 1881, 4, 4, 10, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (198, 2, 'Talos', 359, 606, 'M', 56, 0, 31, 10, 27, 25, '1_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1720, 3170, 1432, 1788, 10, 2, 1, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (199, 2, 'Zorkum', 244, 431, 'C', 122, 0, 1, 60, 26, 21, '7_13', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4418, 3136, 2901, 3742, 1, 1, 4, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (200, 2, 'Kobol', 340, 479, 'I', 124, 0, 8, 26, 4, 24, '6_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4975, 1378, 4125, 3611, 1, 10, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (201, 2, 'Armus', 779, 300, 'N', 32, 0, 9, 58, 29, 37, '2_16', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2922, 1658, 2285, 3643, 6, 4, 10, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (202, 2, 'Mizar', 168, 378, 'J', 2, 0, 64, 56, 70, 52, '3_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1151, 3200, 3931, 2434, 4, 10, 10, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (203, 2, 'Corulag', 826, 668, 'L', 80, 0, 59, 21, 17, 43, '4_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3960, 4500, 4946, 3722, 2, 2, 10, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (204, 2, 'Corulag', 709, 536, 'K', 49, 0, 16, 22, 40, 50, '8_17', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3747, 3300, 1776, 4007, 10, 6, 4, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (205, 2, 'Helska', 740, 731, 'I', 125, 0, 52, 45, 37, 32, '6_12', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4682, 4940, 1708, 1836, 2, 6, 4, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (206, 2, 'Phelan', 662, 481, 'I', 112, 0, 43, 37, 53, 61, '6_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3205, 1527, 3947, 2736, 4, 1, 4, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (207, 2, 'Argolis', 401, 656, 'G', 131, 0, 46, 40, 43, 65, '5_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4845, 4222, 2053, 2432, 1, 4, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (208, 2, 'Zhar', 476, 641, 'G', 169, 0, 61, 43, 30, 20, '5_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3168, 4511, 966, 3634, 4, 2, 4, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (209, 2, 'Bersallis', 879, 628, 'L', 71, 0, 29, 48, 21, 55, '4_12', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4895, 4235, 3782, 2827, 2, 6, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (210, 2, 'Kora II', 470, 530, 'L', 70, 0, 58, 3, 33, 33, '4_12', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1455, 3699, 1843, 1354, 4, 4, 4, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (211, 2, 'Breel', 563, 109, 'J', 9, 0, 68, 49, 57, 35, '3_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1373, 2309, 1733, 2528, 4, 4, 4, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (212, 2, 'Melina', 999, 491, 'M', 81, 0, 51, 29, 59, 53, '1_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1075, 4428, 1813, 1236, 10, 1, 2, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (102, 1, 'Beteigeuze', 637, 503, 'I', 32, 3982, 5, 2, 14, 2, '6_5', 5, NULL, 5, 46510, 0, 5, false, 21, false, false, 5, false, 0, 0, 1581, 2284, 2481, 2455, 2, 2, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (17, 1, 'Beteigeuze', 126, 323, 'I', 32, 3899, 3, 5, 25, 10, '6_6', 2, NULL, 2, 43524, 0, 5, false, 21, false, false, 5, false, 0, 0, 1756, 1680, 1517, 2228, 1, 2, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (214, 2, 'Lixos', 534, 600, 'I', 118, 0, 16, 8, 17, 9, '6_11', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1142, 4199, 2299, 1304, 2, 2, 4, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (215, 2, 'Psi2000', 652, 572, 'M', 40, 0, 15, 47, 15, 38, '1_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2209, 3388, 2220, 2768, 2, 10, 10, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (216, 2, 'Narenda', 515, 408, 'N', 33, 0, 16, 9, 68, 56, '2_13', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1722, 2737, 1856, 4149, 6, 10, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (217, 2, 'Nelvana', 581, 514, 'N', 78, 0, 70, 60, 63, 14, '2_18', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4936, 3038, 1224, 3313, 2, 10, 4, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (218, 2, 'Alawanir', 919, 556, 'J', 7, 0, 51, 39, 8, 26, '3_14', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1652, 3444, 4798, 4328, 10, 2, 6, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (219, 2, 'Morikin', 2, 489, 'K', 54, 0, 35, 25, 26, 48, '8_30', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4621, 1053, 4584, 2150, 10, 10, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (220, 2, 'Hanolin', 288, 731, 'M', 46, 0, 26, 60, 1, 65, '1_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4768, 2677, 4211, 2922, 6, 1, 2, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (221, 2, 'Cestus', 597, 576, 'L', 53, 0, 48, 16, 33, 41, '4_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1541, 2294, 4976, 2179, 2, 10, 6, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (222, 2, 'Topsid', 489, 776, 'N', 46, 0, 27, 65, 23, 6, '2_17', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4933, 3946, 2681, 3124, 6, 4, 2, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (223, 2, 'Yaga Minor', 102, 371, 'L', 55, 0, 9, 54, 57, 10, '4_14', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1375, 1169, 3960, 1014, 4, 6, 10, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (224, 2, 'Sluis Van', 795, 424, 'M', 88, 0, 9, 22, 30, 50, '1_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4536, 3452, 3180, 2170, 2, 10, 2, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (225, 2, 'Gäa', 645, 775, 'N', 31, 0, 19, 45, 30, 37, '2_19', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4339, 4942, 4197, 2303, 10, 2, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (226, 2, 'Brental', 591, 346, 'F', 27, 0, 16, 17, 37, 30, '9_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3566, 818, 3243, 1674, 10, 4, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (227, 2, 'Talos', 312, 397, 'C', 121, 0, 21, 70, 65, 61, '7_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4321, 1843, 2162, 4511, 2, 10, 6, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (228, 2, 'Talos', 426, 417, 'C', 129, 0, 19, 42, 63, 14, '7_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3604, 2549, 3722, 2395, 1, 6, 4, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (229, 2, 'Tau Ceti', 375, 820, 'K', 40, 0, 41, 4, 14, 24, '8_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2044, 1071, 4097, 1748, 10, 1, 4, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (230, 2, 'Deneb', 775, 368, 'C', 158, 0, 35, 21, 12, 38, '7_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1419, 4906, 1750, 3027, 6, 4, 6, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (231, 2, 'Taurus', 492, 222, 'I', 78, 0, 20, 12, 24, 61, '6_14', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1699, 4069, 3549, 2385, 4, 1, 1, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (232, 2, 'Coridan', 260, 681, 'F', 38, 0, 23, 59, 63, 69, '9_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4189, 1881, 4381, 4434, 2, 1, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (233, 2, 'Kraus IV', 385, 515, 'I', 85, 0, 61, 7, 13, 10, '6_20', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3659, 2331, 2096, 2438, 6, 6, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (234, 2, 'Ophicus', 264, 516, 'M', 50, 0, 14, 57, 17, 29, '1_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3016, 3516, 2609, 4904, 10, 1, 6, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (235, 2, 'Mizar', 628, 417, 'J', 1, 0, 7, 5, 42, 24, '3_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1508, 2551, 1220, 4321, 1, 1, 2, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (236, 2, 'Cardassia', 637, 148, 'G', 162, 0, 61, 61, 58, 17, '5_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1789, 4918, 2303, 1985, 10, 10, 10, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (237, 2, 'Westland', 721, 307, 'M', 77, 0, 8, 12, 14, 36, '1_5', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1375, 1293, 3057, 826, 4, 4, 2, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (238, 2, 'Lixos', 606, 635, 'M', 44, 0, 30, 12, 32, 18, '1_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3284, 3674, 3419, 4681, 2, 4, 1, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (239, 2, 'Sahndara', 479, 718, 'J', 0, 0, 23, 22, 69, 42, '3_10', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4968, 3839, 1276, 4224, 10, 6, 6, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (240, 2, 'Metaline', 692, 637, 'M', 86, 0, 61, 53, 45, 11, '1_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2018, 3534, 3594, 4792, 6, 10, 10, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (241, 2, 'Lambdo Paz', 79, 446, 'F', 25, 0, 62, 21, 61, 1, '9_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3388, 4274, 2568, 4297, 2, 10, 10, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (242, 2, 'Eden', 302, 255, 'C', 100, 0, 57, 37, 11, 11, '7_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3653, 4663, 1128, 4510, 4, 10, 2, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (243, 2, 'Boreth', 421, 883, 'I', 106, 0, 21, 30, 30, 56, '6_20', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4166, 2411, 1395, 2598, 6, 1, 4, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (244, 2, 'Lantar', 496, 472, 'N', 53, 0, 38, 56, 6, 18, '2_24', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3705, 2586, 1219, 1771, 2, 4, 6, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (245, 2, 'Ekos', 414, 733, 'L', 100, 0, 50, 18, 68, 13, '4_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4351, 833, 1538, 3171, 10, 10, 6, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (246, 2, 'Carraya', 489, 25, 'C', 131, 0, 27, 70, 5, 32, '7_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4591, 4992, 992, 3556, 4, 4, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (247, 2, 'Rhommanool', 148, 322, 'G', 91, 0, 11, 52, 10, 21, '5_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4236, 4764, 2468, 1010, 2, 2, 2, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (248, 2, 'Nahmi', 525, 280, 'M', 43, 0, 8, 42, 29, 24, '1_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2534, 2499, 1932, 4165, 6, 4, 10, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (249, 2, 'Babel', 305, 563, 'L', 122, 0, 13, 60, 63, 17, '4_13', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1615, 1432, 4315, 1461, 6, 2, 10, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (250, 2, 'KrePain', 426, 602, 'I', 120, 0, 61, 15, 60, 24, '6_15', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4288, 3826, 1992, 3271, 1, 1, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (251, 2, 'Tiburon', 422, 220, 'C', 95, 0, 28, 20, 58, 25, '7_13', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4573, 2450, 3577, 4005, 4, 10, 1, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (252, 2, 'Jouret', 530, 344, 'K', 20, 0, 56, 39, 58, 28, '8_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4604, 3056, 1901, 3021, 4, 2, 6, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (253, 2, 'Kenda II', 208, 314, 'F', 65, 0, 3, 5, 50, 50, '9_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4608, 3647, 4926, 2067, 4, 1, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (254, 2, 'Hanolin', 212, 566, 'J', 5, 0, 7, 62, 41, 54, '3_13', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2081, 1351, 1939, 1336, 4, 1, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (255, 2, 'Earth2', 309, 821, 'N', 75, 0, 29, 14, 57, 34, '2_21', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3694, 1973, 1252, 877, 10, 6, 10, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (256, 2, 'Thalotin', 525, 677, 'C', 119, 0, 5, 31, 1, 25, '7_11', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2042, 924, 1508, 4669, 6, 10, 1, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (257, 2, 'BetaAurigae', 631, 305, 'K', 45, 0, 16, 47, 2, 35, '8_17', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3815, 922, 3627, 1608, 10, 2, 10, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (258, 2, 'BetaNiobe', 563, 757, 'K', 41, 0, 23, 19, 63, 42, '8_14', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1651, 2902, 2715, 2897, 4, 6, 1, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (259, 2, 'Balosnee', 769, 489, 'K', 33, 0, 58, 23, 17, 22, '8_7', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3749, 4101, 2679, 4498, 6, 10, 4, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (260, 2, 'Marlonia', 450, 269, 'F', 68, 0, 4, 44, 62, 60, '9_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4314, 2932, 3788, 2484, 10, 6, 2, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (261, 2, 'Archer', 350, 877, 'F', 63, 0, 24, 25, 41, 68, '9_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2337, 1391, 4880, 4499, 6, 4, 2, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (262, 2, 'Mempa', 724, 392, 'C', 88, 0, 36, 24, 20, 5, '7_12', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3508, 1718, 2179, 3660, 1, 6, 10, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (263, 2, 'Tarsus', 342, 672, 'K', 41, 0, 18, 13, 38, 50, '8_2', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1810, 2246, 2426, 2005, 4, 1, 10, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (264, 2, 'Minara', 730, 592, 'N', 61, 0, 18, 38, 5, 44, '2_1', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2652, 2215, 1103, 3482, 6, 2, 10, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (265, 2, 'Hurada', 698, 172, 'F', 48, 0, 20, 17, 9, 18, '9_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3456, 1551, 2904, 3420, 10, 6, 4, 6, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (266, 2, 'Arcybite', 739, 245, 'N', 51, 0, 66, 2, 62, 22, '2_3', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3123, 4042, 3157, 4510, 4, 6, 6, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (267, 2, 'Yavin 4', 685, 735, 'G', 158, 0, 68, 22, 62, 17, '5_4', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4873, 3223, 2016, 1774, 4, 4, 4, 2, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (268, 2, 'Ardana', 557, 43, 'J', 1, 0, 45, 43, 59, 69, '3_11', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1156, 2728, 1511, 3787, 4, 10, 2, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (269, 2, 'Kelrabi', 792, 712, 'N', 74, 0, 32, 58, 11, 38, '2_19', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 1842, 4737, 1528, 3335, 10, 1, 2, 10, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (270, 2, 'Gamma II', 440, 480, 'I', 80, 0, 15, 60, 34, 54, '6_6', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4988, 4986, 1492, 1547, 4, 4, 1, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (271, 2, 'Deneb', 567, 240, 'K', 32, 0, 42, 70, 10, 47, '8_32', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 3061, 1958, 1347, 3839, 6, 2, 6, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (272, 2, 'Chalna', 650, 679, 'N', 42, 0, 69, 11, 18, 53, '2_9', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 4482, 4732, 1748, 1819, 4, 10, 2, 4, 0, 0, 0, NULL, NULL, 0, NULL, NULL);
INSERT INTO public.planet VALUES (273, 2, 'Quinto-Center', 371, 421, 'G', 123, 0, 5, 59, 18, 10, '5_8', NULL, NULL, NULL, 0, 0, 0, false, 0, false, false, 0, false, 0, 0, 2314, 1613, 2514, 3689, 6, 2, 2, 1, 0, 0, 0, NULL, NULL, 0, NULL, NULL);


ALTER TABLE public.planet ENABLE TRIGGER ALL;

--
-- TOC entry 3161 (class 0 OID 97447)
-- Dependencies: 232
-- Data for Name: orbital_system; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.orbital_system DISABLE TRIGGER ALL;

INSERT INTO public.orbital_system VALUES (1, 1, NULL);
INSERT INTO public.orbital_system VALUES (2, 1, NULL);
INSERT INTO public.orbital_system VALUES (3, 1, NULL);
INSERT INTO public.orbital_system VALUES (4, 2, NULL);
INSERT INTO public.orbital_system VALUES (5, 3, NULL);
INSERT INTO public.orbital_system VALUES (6, 3, NULL);
INSERT INTO public.orbital_system VALUES (7, 3, NULL);
INSERT INTO public.orbital_system VALUES (8, 3, NULL);
INSERT INTO public.orbital_system VALUES (9, 3, NULL);
INSERT INTO public.orbital_system VALUES (10, 3, NULL);
INSERT INTO public.orbital_system VALUES (11, 4, NULL);
INSERT INTO public.orbital_system VALUES (12, 4, NULL);
INSERT INTO public.orbital_system VALUES (13, 4, NULL);
INSERT INTO public.orbital_system VALUES (14, 4, NULL);
INSERT INTO public.orbital_system VALUES (15, 5, NULL);
INSERT INTO public.orbital_system VALUES (16, 5, NULL);
INSERT INTO public.orbital_system VALUES (17, 5, NULL);
INSERT INTO public.orbital_system VALUES (18, 5, NULL);
INSERT INTO public.orbital_system VALUES (19, 6, NULL);
INSERT INTO public.orbital_system VALUES (20, 6, NULL);
INSERT INTO public.orbital_system VALUES (21, 6, NULL);
INSERT INTO public.orbital_system VALUES (22, 6, NULL);
INSERT INTO public.orbital_system VALUES (23, 6, NULL);
INSERT INTO public.orbital_system VALUES (24, 7, NULL);
INSERT INTO public.orbital_system VALUES (25, 7, NULL);
INSERT INTO public.orbital_system VALUES (26, 7, NULL);
INSERT INTO public.orbital_system VALUES (27, 8, NULL);
INSERT INTO public.orbital_system VALUES (28, 8, NULL);
INSERT INTO public.orbital_system VALUES (29, 8, NULL);
INSERT INTO public.orbital_system VALUES (30, 8, NULL);
INSERT INTO public.orbital_system VALUES (31, 9, NULL);
INSERT INTO public.orbital_system VALUES (32, 9, NULL);
INSERT INTO public.orbital_system VALUES (33, 10, NULL);
INSERT INTO public.orbital_system VALUES (34, 10, NULL);
INSERT INTO public.orbital_system VALUES (35, 10, NULL);
INSERT INTO public.orbital_system VALUES (36, 10, NULL);
INSERT INTO public.orbital_system VALUES (37, 10, NULL);
INSERT INTO public.orbital_system VALUES (38, 10, NULL);
INSERT INTO public.orbital_system VALUES (39, 11, NULL);
INSERT INTO public.orbital_system VALUES (40, 11, NULL);
INSERT INTO public.orbital_system VALUES (41, 11, NULL);
INSERT INTO public.orbital_system VALUES (42, 11, NULL);
INSERT INTO public.orbital_system VALUES (43, 12, NULL);
INSERT INTO public.orbital_system VALUES (44, 12, NULL);
INSERT INTO public.orbital_system VALUES (45, 12, NULL);
INSERT INTO public.orbital_system VALUES (46, 12, NULL);
INSERT INTO public.orbital_system VALUES (47, 12, NULL);
INSERT INTO public.orbital_system VALUES (48, 13, NULL);
INSERT INTO public.orbital_system VALUES (49, 13, NULL);
INSERT INTO public.orbital_system VALUES (50, 13, NULL);
INSERT INTO public.orbital_system VALUES (51, 13, NULL);
INSERT INTO public.orbital_system VALUES (52, 13, NULL);
INSERT INTO public.orbital_system VALUES (53, 14, NULL);
INSERT INTO public.orbital_system VALUES (54, 14, NULL);
INSERT INTO public.orbital_system VALUES (55, 14, NULL);
INSERT INTO public.orbital_system VALUES (56, 14, NULL);
INSERT INTO public.orbital_system VALUES (57, 14, NULL);
INSERT INTO public.orbital_system VALUES (58, 14, NULL);
INSERT INTO public.orbital_system VALUES (59, 15, NULL);
INSERT INTO public.orbital_system VALUES (60, 15, NULL);
INSERT INTO public.orbital_system VALUES (61, 15, NULL);
INSERT INTO public.orbital_system VALUES (62, 15, NULL);
INSERT INTO public.orbital_system VALUES (63, 15, NULL);
INSERT INTO public.orbital_system VALUES (64, 16, NULL);
INSERT INTO public.orbital_system VALUES (65, 16, NULL);
INSERT INTO public.orbital_system VALUES (66, 16, NULL);
INSERT INTO public.orbital_system VALUES (67, 16, NULL);
INSERT INTO public.orbital_system VALUES (71, 18, NULL);
INSERT INTO public.orbital_system VALUES (72, 18, NULL);
INSERT INTO public.orbital_system VALUES (73, 18, NULL);
INSERT INTO public.orbital_system VALUES (74, 19, NULL);
INSERT INTO public.orbital_system VALUES (75, 19, NULL);
INSERT INTO public.orbital_system VALUES (76, 19, NULL);
INSERT INTO public.orbital_system VALUES (77, 19, NULL);
INSERT INTO public.orbital_system VALUES (78, 20, NULL);
INSERT INTO public.orbital_system VALUES (79, 20, NULL);
INSERT INTO public.orbital_system VALUES (80, 20, NULL);
INSERT INTO public.orbital_system VALUES (81, 21, NULL);
INSERT INTO public.orbital_system VALUES (82, 21, NULL);
INSERT INTO public.orbital_system VALUES (83, 21, NULL);
INSERT INTO public.orbital_system VALUES (84, 21, NULL);
INSERT INTO public.orbital_system VALUES (85, 21, NULL);
INSERT INTO public.orbital_system VALUES (86, 22, NULL);
INSERT INTO public.orbital_system VALUES (87, 22, NULL);
INSERT INTO public.orbital_system VALUES (88, 22, NULL);
INSERT INTO public.orbital_system VALUES (89, 23, NULL);
INSERT INTO public.orbital_system VALUES (90, 23, NULL);
INSERT INTO public.orbital_system VALUES (91, 23, NULL);
INSERT INTO public.orbital_system VALUES (92, 23, NULL);
INSERT INTO public.orbital_system VALUES (93, 24, NULL);
INSERT INTO public.orbital_system VALUES (94, 24, NULL);
INSERT INTO public.orbital_system VALUES (95, 24, NULL);
INSERT INTO public.orbital_system VALUES (96, 25, NULL);
INSERT INTO public.orbital_system VALUES (97, 25, NULL);
INSERT INTO public.orbital_system VALUES (98, 25, NULL);
INSERT INTO public.orbital_system VALUES (99, 25, NULL);
INSERT INTO public.orbital_system VALUES (100, 25, NULL);
INSERT INTO public.orbital_system VALUES (101, 25, NULL);
INSERT INTO public.orbital_system VALUES (102, 26, NULL);
INSERT INTO public.orbital_system VALUES (103, 26, NULL);
INSERT INTO public.orbital_system VALUES (104, 26, NULL);
INSERT INTO public.orbital_system VALUES (105, 27, NULL);
INSERT INTO public.orbital_system VALUES (106, 27, NULL);
INSERT INTO public.orbital_system VALUES (107, 27, NULL);
INSERT INTO public.orbital_system VALUES (108, 28, NULL);
INSERT INTO public.orbital_system VALUES (109, 29, NULL);
INSERT INTO public.orbital_system VALUES (110, 29, NULL);
INSERT INTO public.orbital_system VALUES (111, 29, NULL);
INSERT INTO public.orbital_system VALUES (112, 30, NULL);
INSERT INTO public.orbital_system VALUES (113, 30, NULL);
INSERT INTO public.orbital_system VALUES (114, 31, NULL);
INSERT INTO public.orbital_system VALUES (115, 32, NULL);
INSERT INTO public.orbital_system VALUES (116, 32, NULL);
INSERT INTO public.orbital_system VALUES (117, 33, NULL);
INSERT INTO public.orbital_system VALUES (118, 33, NULL);
INSERT INTO public.orbital_system VALUES (119, 34, NULL);
INSERT INTO public.orbital_system VALUES (120, 35, NULL);
INSERT INTO public.orbital_system VALUES (121, 36, NULL);
INSERT INTO public.orbital_system VALUES (122, 36, NULL);
INSERT INTO public.orbital_system VALUES (123, 36, NULL);
INSERT INTO public.orbital_system VALUES (124, 36, NULL);
INSERT INTO public.orbital_system VALUES (125, 37, NULL);
INSERT INTO public.orbital_system VALUES (126, 37, NULL);
INSERT INTO public.orbital_system VALUES (127, 37, NULL);
INSERT INTO public.orbital_system VALUES (128, 37, NULL);
INSERT INTO public.orbital_system VALUES (129, 37, NULL);
INSERT INTO public.orbital_system VALUES (130, 38, NULL);
INSERT INTO public.orbital_system VALUES (131, 38, NULL);
INSERT INTO public.orbital_system VALUES (132, 38, NULL);
INSERT INTO public.orbital_system VALUES (133, 38, NULL);
INSERT INTO public.orbital_system VALUES (134, 38, NULL);
INSERT INTO public.orbital_system VALUES (135, 39, NULL);
INSERT INTO public.orbital_system VALUES (136, 39, NULL);
INSERT INTO public.orbital_system VALUES (137, 39, NULL);
INSERT INTO public.orbital_system VALUES (138, 39, NULL);
INSERT INTO public.orbital_system VALUES (139, 39, NULL);
INSERT INTO public.orbital_system VALUES (140, 39, NULL);
INSERT INTO public.orbital_system VALUES (141, 40, NULL);
INSERT INTO public.orbital_system VALUES (142, 40, NULL);
INSERT INTO public.orbital_system VALUES (143, 40, NULL);
INSERT INTO public.orbital_system VALUES (144, 40, NULL);
INSERT INTO public.orbital_system VALUES (145, 40, NULL);
INSERT INTO public.orbital_system VALUES (146, 40, NULL);
INSERT INTO public.orbital_system VALUES (147, 41, NULL);
INSERT INTO public.orbital_system VALUES (148, 41, NULL);
INSERT INTO public.orbital_system VALUES (149, 41, NULL);
INSERT INTO public.orbital_system VALUES (150, 41, NULL);
INSERT INTO public.orbital_system VALUES (151, 41, NULL);
INSERT INTO public.orbital_system VALUES (152, 41, NULL);
INSERT INTO public.orbital_system VALUES (153, 42, NULL);
INSERT INTO public.orbital_system VALUES (155, 44, NULL);
INSERT INTO public.orbital_system VALUES (156, 45, NULL);
INSERT INTO public.orbital_system VALUES (157, 45, NULL);
INSERT INTO public.orbital_system VALUES (158, 46, NULL);
INSERT INTO public.orbital_system VALUES (159, 46, NULL);
INSERT INTO public.orbital_system VALUES (160, 46, NULL);
INSERT INTO public.orbital_system VALUES (161, 47, NULL);
INSERT INTO public.orbital_system VALUES (162, 48, NULL);
INSERT INTO public.orbital_system VALUES (163, 48, NULL);
INSERT INTO public.orbital_system VALUES (164, 48, NULL);
INSERT INTO public.orbital_system VALUES (165, 48, NULL);
INSERT INTO public.orbital_system VALUES (166, 49, NULL);
INSERT INTO public.orbital_system VALUES (167, 49, NULL);
INSERT INTO public.orbital_system VALUES (168, 49, NULL);
INSERT INTO public.orbital_system VALUES (169, 50, NULL);
INSERT INTO public.orbital_system VALUES (170, 50, NULL);
INSERT INTO public.orbital_system VALUES (171, 50, NULL);
INSERT INTO public.orbital_system VALUES (172, 50, NULL);
INSERT INTO public.orbital_system VALUES (173, 51, NULL);
INSERT INTO public.orbital_system VALUES (174, 51, NULL);
INSERT INTO public.orbital_system VALUES (175, 51, NULL);
INSERT INTO public.orbital_system VALUES (176, 51, NULL);
INSERT INTO public.orbital_system VALUES (177, 52, NULL);
INSERT INTO public.orbital_system VALUES (178, 52, NULL);
INSERT INTO public.orbital_system VALUES (179, 52, NULL);
INSERT INTO public.orbital_system VALUES (180, 52, NULL);
INSERT INTO public.orbital_system VALUES (181, 52, NULL);
INSERT INTO public.orbital_system VALUES (182, 52, NULL);
INSERT INTO public.orbital_system VALUES (183, 53, NULL);
INSERT INTO public.orbital_system VALUES (184, 54, NULL);
INSERT INTO public.orbital_system VALUES (185, 54, NULL);
INSERT INTO public.orbital_system VALUES (186, 54, NULL);
INSERT INTO public.orbital_system VALUES (187, 54, NULL);
INSERT INTO public.orbital_system VALUES (188, 55, NULL);
INSERT INTO public.orbital_system VALUES (189, 55, NULL);
INSERT INTO public.orbital_system VALUES (190, 55, NULL);
INSERT INTO public.orbital_system VALUES (191, 55, NULL);
INSERT INTO public.orbital_system VALUES (192, 55, NULL);
INSERT INTO public.orbital_system VALUES (193, 55, NULL);
INSERT INTO public.orbital_system VALUES (194, 56, NULL);
INSERT INTO public.orbital_system VALUES (195, 56, NULL);
INSERT INTO public.orbital_system VALUES (196, 56, NULL);
INSERT INTO public.orbital_system VALUES (197, 56, NULL);
INSERT INTO public.orbital_system VALUES (198, 56, NULL);
INSERT INTO public.orbital_system VALUES (199, 56, NULL);
INSERT INTO public.orbital_system VALUES (206, 58, NULL);
INSERT INTO public.orbital_system VALUES (207, 58, NULL);
INSERT INTO public.orbital_system VALUES (208, 58, NULL);
INSERT INTO public.orbital_system VALUES (209, 58, NULL);
INSERT INTO public.orbital_system VALUES (210, 58, NULL);
INSERT INTO public.orbital_system VALUES (211, 59, NULL);
INSERT INTO public.orbital_system VALUES (212, 60, NULL);
INSERT INTO public.orbital_system VALUES (213, 60, NULL);
INSERT INTO public.orbital_system VALUES (214, 60, NULL);
INSERT INTO public.orbital_system VALUES (215, 60, NULL);
INSERT INTO public.orbital_system VALUES (216, 60, NULL);
INSERT INTO public.orbital_system VALUES (217, 60, NULL);
INSERT INTO public.orbital_system VALUES (218, 61, NULL);
INSERT INTO public.orbital_system VALUES (219, 62, NULL);
INSERT INTO public.orbital_system VALUES (220, 62, NULL);
INSERT INTO public.orbital_system VALUES (221, 62, NULL);
INSERT INTO public.orbital_system VALUES (222, 63, NULL);
INSERT INTO public.orbital_system VALUES (223, 64, NULL);
INSERT INTO public.orbital_system VALUES (224, 64, NULL);
INSERT INTO public.orbital_system VALUES (225, 64, NULL);
INSERT INTO public.orbital_system VALUES (226, 65, NULL);
INSERT INTO public.orbital_system VALUES (227, 65, NULL);
INSERT INTO public.orbital_system VALUES (228, 66, NULL);
INSERT INTO public.orbital_system VALUES (229, 66, NULL);
INSERT INTO public.orbital_system VALUES (230, 66, NULL);
INSERT INTO public.orbital_system VALUES (231, 66, NULL);
INSERT INTO public.orbital_system VALUES (232, 67, NULL);
INSERT INTO public.orbital_system VALUES (233, 67, NULL);
INSERT INTO public.orbital_system VALUES (234, 68, NULL);
INSERT INTO public.orbital_system VALUES (235, 68, NULL);
INSERT INTO public.orbital_system VALUES (236, 68, NULL);
INSERT INTO public.orbital_system VALUES (237, 68, NULL);
INSERT INTO public.orbital_system VALUES (238, 68, NULL);
INSERT INTO public.orbital_system VALUES (239, 69, NULL);
INSERT INTO public.orbital_system VALUES (240, 69, NULL);
INSERT INTO public.orbital_system VALUES (241, 69, NULL);
INSERT INTO public.orbital_system VALUES (242, 69, NULL);
INSERT INTO public.orbital_system VALUES (243, 69, NULL);
INSERT INTO public.orbital_system VALUES (244, 69, NULL);
INSERT INTO public.orbital_system VALUES (245, 70, NULL);
INSERT INTO public.orbital_system VALUES (246, 70, NULL);
INSERT INTO public.orbital_system VALUES (247, 70, NULL);
INSERT INTO public.orbital_system VALUES (248, 71, NULL);
INSERT INTO public.orbital_system VALUES (249, 72, NULL);
INSERT INTO public.orbital_system VALUES (250, 72, NULL);
INSERT INTO public.orbital_system VALUES (251, 72, NULL);
INSERT INTO public.orbital_system VALUES (252, 73, NULL);
INSERT INTO public.orbital_system VALUES (253, 73, NULL);
INSERT INTO public.orbital_system VALUES (254, 73, NULL);
INSERT INTO public.orbital_system VALUES (255, 73, NULL);
INSERT INTO public.orbital_system VALUES (256, 74, NULL);
INSERT INTO public.orbital_system VALUES (257, 74, NULL);
INSERT INTO public.orbital_system VALUES (258, 74, NULL);
INSERT INTO public.orbital_system VALUES (259, 74, NULL);
INSERT INTO public.orbital_system VALUES (260, 74, NULL);
INSERT INTO public.orbital_system VALUES (263, 76, NULL);
INSERT INTO public.orbital_system VALUES (264, 76, NULL);
INSERT INTO public.orbital_system VALUES (265, 77, NULL);
INSERT INTO public.orbital_system VALUES (266, 77, NULL);
INSERT INTO public.orbital_system VALUES (267, 77, NULL);
INSERT INTO public.orbital_system VALUES (268, 77, NULL);
INSERT INTO public.orbital_system VALUES (269, 77, NULL);
INSERT INTO public.orbital_system VALUES (270, 77, NULL);
INSERT INTO public.orbital_system VALUES (271, 78, NULL);
INSERT INTO public.orbital_system VALUES (272, 78, NULL);
INSERT INTO public.orbital_system VALUES (273, 78, NULL);
INSERT INTO public.orbital_system VALUES (274, 78, NULL);
INSERT INTO public.orbital_system VALUES (275, 78, NULL);
INSERT INTO public.orbital_system VALUES (276, 78, NULL);
INSERT INTO public.orbital_system VALUES (277, 79, NULL);
INSERT INTO public.orbital_system VALUES (278, 79, NULL);
INSERT INTO public.orbital_system VALUES (279, 80, NULL);
INSERT INTO public.orbital_system VALUES (280, 80, NULL);
INSERT INTO public.orbital_system VALUES (281, 80, NULL);
INSERT INTO public.orbital_system VALUES (282, 80, NULL);
INSERT INTO public.orbital_system VALUES (283, 81, NULL);
INSERT INTO public.orbital_system VALUES (284, 81, NULL);
INSERT INTO public.orbital_system VALUES (285, 82, NULL);
INSERT INTO public.orbital_system VALUES (286, 82, NULL);
INSERT INTO public.orbital_system VALUES (287, 82, NULL);
INSERT INTO public.orbital_system VALUES (288, 82, NULL);
INSERT INTO public.orbital_system VALUES (289, 82, NULL);
INSERT INTO public.orbital_system VALUES (290, 82, NULL);
INSERT INTO public.orbital_system VALUES (291, 83, NULL);
INSERT INTO public.orbital_system VALUES (292, 83, NULL);
INSERT INTO public.orbital_system VALUES (293, 83, NULL);
INSERT INTO public.orbital_system VALUES (294, 83, NULL);
INSERT INTO public.orbital_system VALUES (295, 84, NULL);
INSERT INTO public.orbital_system VALUES (296, 85, NULL);
INSERT INTO public.orbital_system VALUES (297, 85, NULL);
INSERT INTO public.orbital_system VALUES (298, 85, NULL);
INSERT INTO public.orbital_system VALUES (299, 85, NULL);
INSERT INTO public.orbital_system VALUES (300, 85, NULL);
INSERT INTO public.orbital_system VALUES (301, 85, NULL);
INSERT INTO public.orbital_system VALUES (302, 86, NULL);
INSERT INTO public.orbital_system VALUES (303, 87, NULL);
INSERT INTO public.orbital_system VALUES (304, 87, NULL);
INSERT INTO public.orbital_system VALUES (305, 88, NULL);
INSERT INTO public.orbital_system VALUES (306, 89, NULL);
INSERT INTO public.orbital_system VALUES (307, 89, NULL);
INSERT INTO public.orbital_system VALUES (308, 89, NULL);
INSERT INTO public.orbital_system VALUES (309, 89, NULL);
INSERT INTO public.orbital_system VALUES (310, 89, NULL);
INSERT INTO public.orbital_system VALUES (311, 89, NULL);
INSERT INTO public.orbital_system VALUES (312, 90, NULL);
INSERT INTO public.orbital_system VALUES (313, 90, NULL);
INSERT INTO public.orbital_system VALUES (314, 90, NULL);
INSERT INTO public.orbital_system VALUES (315, 90, NULL);
INSERT INTO public.orbital_system VALUES (316, 90, NULL);
INSERT INTO public.orbital_system VALUES (317, 90, NULL);
INSERT INTO public.orbital_system VALUES (318, 91, NULL);
INSERT INTO public.orbital_system VALUES (319, 91, NULL);
INSERT INTO public.orbital_system VALUES (320, 91, NULL);
INSERT INTO public.orbital_system VALUES (321, 91, NULL);
INSERT INTO public.orbital_system VALUES (322, 91, NULL);
INSERT INTO public.orbital_system VALUES (323, 91, NULL);
INSERT INTO public.orbital_system VALUES (324, 92, NULL);
INSERT INTO public.orbital_system VALUES (325, 93, NULL);
INSERT INTO public.orbital_system VALUES (326, 93, NULL);
INSERT INTO public.orbital_system VALUES (327, 93, NULL);
INSERT INTO public.orbital_system VALUES (328, 93, NULL);
INSERT INTO public.orbital_system VALUES (329, 93, NULL);
INSERT INTO public.orbital_system VALUES (330, 94, NULL);
INSERT INTO public.orbital_system VALUES (331, 95, NULL);
INSERT INTO public.orbital_system VALUES (332, 95, NULL);
INSERT INTO public.orbital_system VALUES (333, 95, NULL);
INSERT INTO public.orbital_system VALUES (334, 95, NULL);
INSERT INTO public.orbital_system VALUES (335, 95, NULL);
INSERT INTO public.orbital_system VALUES (336, 96, NULL);
INSERT INTO public.orbital_system VALUES (337, 96, NULL);
INSERT INTO public.orbital_system VALUES (338, 96, NULL);
INSERT INTO public.orbital_system VALUES (339, 96, NULL);
INSERT INTO public.orbital_system VALUES (340, 97, NULL);
INSERT INTO public.orbital_system VALUES (341, 97, NULL);
INSERT INTO public.orbital_system VALUES (342, 97, NULL);
INSERT INTO public.orbital_system VALUES (343, 97, NULL);
INSERT INTO public.orbital_system VALUES (344, 97, NULL);
INSERT INTO public.orbital_system VALUES (345, 98, NULL);
INSERT INTO public.orbital_system VALUES (346, 98, NULL);
INSERT INTO public.orbital_system VALUES (347, 99, NULL);
INSERT INTO public.orbital_system VALUES (348, 100, NULL);
INSERT INTO public.orbital_system VALUES (349, 100, NULL);
INSERT INTO public.orbital_system VALUES (350, 101, NULL);
INSERT INTO public.orbital_system VALUES (351, 101, NULL);
INSERT INTO public.orbital_system VALUES (352, 101, NULL);
INSERT INTO public.orbital_system VALUES (353, 101, NULL);
INSERT INTO public.orbital_system VALUES (360, 103, NULL);
INSERT INTO public.orbital_system VALUES (361, 103, NULL);
INSERT INTO public.orbital_system VALUES (362, 103, NULL);
INSERT INTO public.orbital_system VALUES (363, 103, NULL);
INSERT INTO public.orbital_system VALUES (364, 103, NULL);
INSERT INTO public.orbital_system VALUES (365, 103, NULL);
INSERT INTO public.orbital_system VALUES (366, 104, NULL);
INSERT INTO public.orbital_system VALUES (367, 104, NULL);
INSERT INTO public.orbital_system VALUES (368, 104, NULL);
INSERT INTO public.orbital_system VALUES (369, 105, NULL);
INSERT INTO public.orbital_system VALUES (370, 105, NULL);
INSERT INTO public.orbital_system VALUES (371, 105, NULL);
INSERT INTO public.orbital_system VALUES (372, 105, NULL);
INSERT INTO public.orbital_system VALUES (377, 107, NULL);
INSERT INTO public.orbital_system VALUES (378, 108, NULL);
INSERT INTO public.orbital_system VALUES (379, 108, NULL);
INSERT INTO public.orbital_system VALUES (380, 108, NULL);
INSERT INTO public.orbital_system VALUES (381, 108, NULL);
INSERT INTO public.orbital_system VALUES (382, 108, NULL);
INSERT INTO public.orbital_system VALUES (383, 109, NULL);
INSERT INTO public.orbital_system VALUES (384, 109, NULL);
INSERT INTO public.orbital_system VALUES (385, 109, NULL);
INSERT INTO public.orbital_system VALUES (386, 110, NULL);
INSERT INTO public.orbital_system VALUES (387, 110, NULL);
INSERT INTO public.orbital_system VALUES (388, 110, NULL);
INSERT INTO public.orbital_system VALUES (389, 110, NULL);
INSERT INTO public.orbital_system VALUES (390, 110, NULL);
INSERT INTO public.orbital_system VALUES (391, 111, NULL);
INSERT INTO public.orbital_system VALUES (392, 111, NULL);
INSERT INTO public.orbital_system VALUES (393, 111, NULL);
INSERT INTO public.orbital_system VALUES (394, 111, NULL);
INSERT INTO public.orbital_system VALUES (395, 111, NULL);
INSERT INTO public.orbital_system VALUES (396, 111, NULL);
INSERT INTO public.orbital_system VALUES (397, 112, NULL);
INSERT INTO public.orbital_system VALUES (398, 112, NULL);
INSERT INTO public.orbital_system VALUES (399, 112, NULL);
INSERT INTO public.orbital_system VALUES (400, 112, NULL);
INSERT INTO public.orbital_system VALUES (401, 113, NULL);
INSERT INTO public.orbital_system VALUES (402, 113, NULL);
INSERT INTO public.orbital_system VALUES (403, 113, NULL);
INSERT INTO public.orbital_system VALUES (404, 114, NULL);
INSERT INTO public.orbital_system VALUES (405, 114, NULL);
INSERT INTO public.orbital_system VALUES (406, 114, NULL);
INSERT INTO public.orbital_system VALUES (407, 114, NULL);
INSERT INTO public.orbital_system VALUES (408, 115, NULL);
INSERT INTO public.orbital_system VALUES (409, 115, NULL);
INSERT INTO public.orbital_system VALUES (410, 115, NULL);
INSERT INTO public.orbital_system VALUES (411, 115, NULL);
INSERT INTO public.orbital_system VALUES (412, 115, NULL);
INSERT INTO public.orbital_system VALUES (413, 115, NULL);
INSERT INTO public.orbital_system VALUES (414, 116, NULL);
INSERT INTO public.orbital_system VALUES (415, 116, NULL);
INSERT INTO public.orbital_system VALUES (416, 116, NULL);
INSERT INTO public.orbital_system VALUES (417, 117, NULL);
INSERT INTO public.orbital_system VALUES (418, 117, NULL);
INSERT INTO public.orbital_system VALUES (419, 117, NULL);
INSERT INTO public.orbital_system VALUES (420, 117, NULL);
INSERT INTO public.orbital_system VALUES (421, 117, NULL);
INSERT INTO public.orbital_system VALUES (422, 117, NULL);
INSERT INTO public.orbital_system VALUES (423, 118, NULL);
INSERT INTO public.orbital_system VALUES (424, 119, NULL);
INSERT INTO public.orbital_system VALUES (425, 119, NULL);
INSERT INTO public.orbital_system VALUES (426, 120, NULL);
INSERT INTO public.orbital_system VALUES (427, 121, NULL);
INSERT INTO public.orbital_system VALUES (428, 121, NULL);
INSERT INTO public.orbital_system VALUES (429, 121, NULL);
INSERT INTO public.orbital_system VALUES (430, 121, NULL);
INSERT INTO public.orbital_system VALUES (431, 121, NULL);
INSERT INTO public.orbital_system VALUES (432, 122, NULL);
INSERT INTO public.orbital_system VALUES (433, 123, NULL);
INSERT INTO public.orbital_system VALUES (434, 123, NULL);
INSERT INTO public.orbital_system VALUES (435, 123, NULL);
INSERT INTO public.orbital_system VALUES (436, 123, NULL);
INSERT INTO public.orbital_system VALUES (437, 123, NULL);
INSERT INTO public.orbital_system VALUES (438, 124, NULL);
INSERT INTO public.orbital_system VALUES (439, 124, NULL);
INSERT INTO public.orbital_system VALUES (440, 125, NULL);
INSERT INTO public.orbital_system VALUES (441, 126, NULL);
INSERT INTO public.orbital_system VALUES (442, 126, NULL);
INSERT INTO public.orbital_system VALUES (443, 126, NULL);
INSERT INTO public.orbital_system VALUES (444, 127, NULL);
INSERT INTO public.orbital_system VALUES (445, 127, NULL);
INSERT INTO public.orbital_system VALUES (446, 127, NULL);
INSERT INTO public.orbital_system VALUES (447, 127, NULL);
INSERT INTO public.orbital_system VALUES (448, 127, NULL);
INSERT INTO public.orbital_system VALUES (449, 127, NULL);
INSERT INTO public.orbital_system VALUES (450, 128, NULL);
INSERT INTO public.orbital_system VALUES (451, 128, NULL);
INSERT INTO public.orbital_system VALUES (452, 128, NULL);
INSERT INTO public.orbital_system VALUES (453, 128, NULL);
INSERT INTO public.orbital_system VALUES (454, 128, NULL);
INSERT INTO public.orbital_system VALUES (455, 128, NULL);
INSERT INTO public.orbital_system VALUES (456, 129, NULL);
INSERT INTO public.orbital_system VALUES (462, 131, NULL);
INSERT INTO public.orbital_system VALUES (463, 131, NULL);
INSERT INTO public.orbital_system VALUES (464, 131, NULL);
INSERT INTO public.orbital_system VALUES (465, 131, NULL);
INSERT INTO public.orbital_system VALUES (466, 131, NULL);
INSERT INTO public.orbital_system VALUES (467, 132, NULL);
INSERT INTO public.orbital_system VALUES (468, 132, NULL);
INSERT INTO public.orbital_system VALUES (469, 133, NULL);
INSERT INTO public.orbital_system VALUES (470, 133, NULL);
INSERT INTO public.orbital_system VALUES (471, 133, NULL);
INSERT INTO public.orbital_system VALUES (472, 134, NULL);
INSERT INTO public.orbital_system VALUES (473, 134, NULL);
INSERT INTO public.orbital_system VALUES (474, 134, NULL);
INSERT INTO public.orbital_system VALUES (475, 134, NULL);
INSERT INTO public.orbital_system VALUES (476, 134, NULL);
INSERT INTO public.orbital_system VALUES (477, 135, NULL);
INSERT INTO public.orbital_system VALUES (478, 135, NULL);
INSERT INTO public.orbital_system VALUES (479, 136, NULL);
INSERT INTO public.orbital_system VALUES (480, 137, NULL);
INSERT INTO public.orbital_system VALUES (481, 137, NULL);
INSERT INTO public.orbital_system VALUES (482, 137, NULL);
INSERT INTO public.orbital_system VALUES (483, 137, NULL);
INSERT INTO public.orbital_system VALUES (484, 137, NULL);
INSERT INTO public.orbital_system VALUES (485, 138, NULL);
INSERT INTO public.orbital_system VALUES (486, 138, NULL);
INSERT INTO public.orbital_system VALUES (487, 138, NULL);
INSERT INTO public.orbital_system VALUES (488, 138, NULL);
INSERT INTO public.orbital_system VALUES (489, 138, NULL);
INSERT INTO public.orbital_system VALUES (490, 139, NULL);
INSERT INTO public.orbital_system VALUES (491, 139, NULL);
INSERT INTO public.orbital_system VALUES (492, 140, NULL);
INSERT INTO public.orbital_system VALUES (493, 140, NULL);
INSERT INTO public.orbital_system VALUES (494, 140, NULL);
INSERT INTO public.orbital_system VALUES (495, 141, NULL);
INSERT INTO public.orbital_system VALUES (496, 141, NULL);
INSERT INTO public.orbital_system VALUES (497, 142, NULL);
INSERT INTO public.orbital_system VALUES (498, 142, NULL);
INSERT INTO public.orbital_system VALUES (499, 142, NULL);
INSERT INTO public.orbital_system VALUES (500, 143, NULL);
INSERT INTO public.orbital_system VALUES (501, 143, NULL);
INSERT INTO public.orbital_system VALUES (502, 143, NULL);
INSERT INTO public.orbital_system VALUES (503, 144, NULL);
INSERT INTO public.orbital_system VALUES (504, 145, NULL);
INSERT INTO public.orbital_system VALUES (505, 145, NULL);
INSERT INTO public.orbital_system VALUES (506, 145, NULL);
INSERT INTO public.orbital_system VALUES (507, 145, NULL);
INSERT INTO public.orbital_system VALUES (508, 146, NULL);
INSERT INTO public.orbital_system VALUES (509, 146, NULL);
INSERT INTO public.orbital_system VALUES (510, 147, NULL);
INSERT INTO public.orbital_system VALUES (511, 147, NULL);
INSERT INTO public.orbital_system VALUES (518, 149, NULL);
INSERT INTO public.orbital_system VALUES (519, 149, NULL);
INSERT INTO public.orbital_system VALUES (520, 149, NULL);
INSERT INTO public.orbital_system VALUES (521, 150, NULL);
INSERT INTO public.orbital_system VALUES (522, 150, NULL);
INSERT INTO public.orbital_system VALUES (523, 151, NULL);
INSERT INTO public.orbital_system VALUES (524, 152, NULL);
INSERT INTO public.orbital_system VALUES (525, 152, NULL);
INSERT INTO public.orbital_system VALUES (526, 152, NULL);
INSERT INTO public.orbital_system VALUES (527, 152, NULL);
INSERT INTO public.orbital_system VALUES (528, 152, NULL);
INSERT INTO public.orbital_system VALUES (529, 152, NULL);
INSERT INTO public.orbital_system VALUES (530, 153, NULL);
INSERT INTO public.orbital_system VALUES (531, 153, NULL);
INSERT INTO public.orbital_system VALUES (532, 153, NULL);
INSERT INTO public.orbital_system VALUES (533, 153, NULL);
INSERT INTO public.orbital_system VALUES (534, 154, NULL);
INSERT INTO public.orbital_system VALUES (535, 155, NULL);
INSERT INTO public.orbital_system VALUES (536, 155, NULL);
INSERT INTO public.orbital_system VALUES (537, 155, NULL);
INSERT INTO public.orbital_system VALUES (538, 155, NULL);
INSERT INTO public.orbital_system VALUES (539, 155, NULL);
INSERT INTO public.orbital_system VALUES (540, 155, NULL);
INSERT INTO public.orbital_system VALUES (541, 156, NULL);
INSERT INTO public.orbital_system VALUES (542, 156, NULL);
INSERT INTO public.orbital_system VALUES (543, 156, NULL);
INSERT INTO public.orbital_system VALUES (544, 157, NULL);
INSERT INTO public.orbital_system VALUES (545, 158, NULL);
INSERT INTO public.orbital_system VALUES (546, 158, NULL);
INSERT INTO public.orbital_system VALUES (547, 158, NULL);
INSERT INTO public.orbital_system VALUES (548, 159, NULL);
INSERT INTO public.orbital_system VALUES (549, 159, NULL);
INSERT INTO public.orbital_system VALUES (550, 159, NULL);
INSERT INTO public.orbital_system VALUES (551, 159, NULL);
INSERT INTO public.orbital_system VALUES (552, 160, NULL);
INSERT INTO public.orbital_system VALUES (553, 160, NULL);
INSERT INTO public.orbital_system VALUES (554, 160, NULL);
INSERT INTO public.orbital_system VALUES (555, 160, NULL);
INSERT INTO public.orbital_system VALUES (556, 161, NULL);
INSERT INTO public.orbital_system VALUES (557, 161, NULL);
INSERT INTO public.orbital_system VALUES (558, 161, NULL);
INSERT INTO public.orbital_system VALUES (559, 161, NULL);
INSERT INTO public.orbital_system VALUES (560, 162, NULL);
INSERT INTO public.orbital_system VALUES (561, 162, NULL);
INSERT INTO public.orbital_system VALUES (562, 162, NULL);
INSERT INTO public.orbital_system VALUES (563, 162, NULL);
INSERT INTO public.orbital_system VALUES (564, 162, NULL);
INSERT INTO public.orbital_system VALUES (565, 163, NULL);
INSERT INTO public.orbital_system VALUES (566, 163, NULL);
INSERT INTO public.orbital_system VALUES (567, 163, NULL);
INSERT INTO public.orbital_system VALUES (568, 163, NULL);
INSERT INTO public.orbital_system VALUES (569, 164, NULL);
INSERT INTO public.orbital_system VALUES (570, 164, NULL);
INSERT INTO public.orbital_system VALUES (571, 165, NULL);
INSERT INTO public.orbital_system VALUES (572, 165, NULL);
INSERT INTO public.orbital_system VALUES (573, 166, NULL);
INSERT INTO public.orbital_system VALUES (574, 167, NULL);
INSERT INTO public.orbital_system VALUES (575, 167, NULL);
INSERT INTO public.orbital_system VALUES (576, 167, NULL);
INSERT INTO public.orbital_system VALUES (577, 168, NULL);
INSERT INTO public.orbital_system VALUES (578, 168, NULL);
INSERT INTO public.orbital_system VALUES (579, 168, NULL);
INSERT INTO public.orbital_system VALUES (580, 169, NULL);
INSERT INTO public.orbital_system VALUES (581, 169, NULL);
INSERT INTO public.orbital_system VALUES (582, 169, NULL);
INSERT INTO public.orbital_system VALUES (583, 169, NULL);
INSERT INTO public.orbital_system VALUES (584, 170, NULL);
INSERT INTO public.orbital_system VALUES (585, 171, NULL);
INSERT INTO public.orbital_system VALUES (586, 171, NULL);
INSERT INTO public.orbital_system VALUES (587, 171, NULL);
INSERT INTO public.orbital_system VALUES (588, 171, NULL);
INSERT INTO public.orbital_system VALUES (589, 172, NULL);
INSERT INTO public.orbital_system VALUES (595, 174, NULL);
INSERT INTO public.orbital_system VALUES (596, 174, NULL);
INSERT INTO public.orbital_system VALUES (597, 174, NULL);
INSERT INTO public.orbital_system VALUES (598, 174, NULL);
INSERT INTO public.orbital_system VALUES (599, 174, NULL);
INSERT INTO public.orbital_system VALUES (600, 174, NULL);
INSERT INTO public.orbital_system VALUES (601, 175, NULL);
INSERT INTO public.orbital_system VALUES (602, 175, NULL);
INSERT INTO public.orbital_system VALUES (603, 175, NULL);
INSERT INTO public.orbital_system VALUES (604, 175, NULL);
INSERT INTO public.orbital_system VALUES (605, 175, NULL);
INSERT INTO public.orbital_system VALUES (606, 175, NULL);
INSERT INTO public.orbital_system VALUES (607, 176, NULL);
INSERT INTO public.orbital_system VALUES (608, 176, NULL);
INSERT INTO public.orbital_system VALUES (609, 176, NULL);
INSERT INTO public.orbital_system VALUES (610, 176, NULL);
INSERT INTO public.orbital_system VALUES (611, 177, NULL);
INSERT INTO public.orbital_system VALUES (612, 177, NULL);
INSERT INTO public.orbital_system VALUES (613, 177, NULL);
INSERT INTO public.orbital_system VALUES (614, 177, NULL);
INSERT INTO public.orbital_system VALUES (615, 177, NULL);
INSERT INTO public.orbital_system VALUES (616, 177, NULL);
INSERT INTO public.orbital_system VALUES (617, 178, NULL);
INSERT INTO public.orbital_system VALUES (618, 178, NULL);
INSERT INTO public.orbital_system VALUES (619, 178, NULL);
INSERT INTO public.orbital_system VALUES (620, 178, NULL);
INSERT INTO public.orbital_system VALUES (621, 178, NULL);
INSERT INTO public.orbital_system VALUES (622, 179, NULL);
INSERT INTO public.orbital_system VALUES (623, 179, NULL);
INSERT INTO public.orbital_system VALUES (624, 180, NULL);
INSERT INTO public.orbital_system VALUES (625, 181, NULL);
INSERT INTO public.orbital_system VALUES (626, 181, NULL);
INSERT INTO public.orbital_system VALUES (627, 181, NULL);
INSERT INTO public.orbital_system VALUES (628, 181, NULL);
INSERT INTO public.orbital_system VALUES (629, 181, NULL);
INSERT INTO public.orbital_system VALUES (630, 182, NULL);
INSERT INTO public.orbital_system VALUES (631, 182, NULL);
INSERT INTO public.orbital_system VALUES (632, 182, NULL);
INSERT INTO public.orbital_system VALUES (633, 182, NULL);
INSERT INTO public.orbital_system VALUES (634, 182, NULL);
INSERT INTO public.orbital_system VALUES (635, 182, NULL);
INSERT INTO public.orbital_system VALUES (636, 183, NULL);
INSERT INTO public.orbital_system VALUES (637, 183, NULL);
INSERT INTO public.orbital_system VALUES (638, 183, NULL);
INSERT INTO public.orbital_system VALUES (639, 183, NULL);
INSERT INTO public.orbital_system VALUES (640, 183, NULL);
INSERT INTO public.orbital_system VALUES (641, 184, NULL);
INSERT INTO public.orbital_system VALUES (642, 184, NULL);
INSERT INTO public.orbital_system VALUES (643, 184, NULL);
INSERT INTO public.orbital_system VALUES (644, 185, NULL);
INSERT INTO public.orbital_system VALUES (645, 185, NULL);
INSERT INTO public.orbital_system VALUES (646, 185, NULL);
INSERT INTO public.orbital_system VALUES (647, 185, NULL);
INSERT INTO public.orbital_system VALUES (648, 185, NULL);
INSERT INTO public.orbital_system VALUES (649, 185, NULL);
INSERT INTO public.orbital_system VALUES (650, 186, NULL);
INSERT INTO public.orbital_system VALUES (651, 186, NULL);
INSERT INTO public.orbital_system VALUES (652, 186, NULL);
INSERT INTO public.orbital_system VALUES (653, 186, NULL);
INSERT INTO public.orbital_system VALUES (654, 187, NULL);
INSERT INTO public.orbital_system VALUES (655, 43, NULL);
INSERT INTO public.orbital_system VALUES (656, 43, NULL);
INSERT INTO public.orbital_system VALUES (657, 43, NULL);
INSERT INTO public.orbital_system VALUES (658, 43, NULL);
INSERT INTO public.orbital_system VALUES (659, 17, NULL);
INSERT INTO public.orbital_system VALUES (660, 17, NULL);
INSERT INTO public.orbital_system VALUES (661, 17, NULL);
INSERT INTO public.orbital_system VALUES (662, 17, NULL);
INSERT INTO public.orbital_system VALUES (663, 75, NULL);
INSERT INTO public.orbital_system VALUES (664, 75, NULL);
INSERT INTO public.orbital_system VALUES (665, 75, NULL);
INSERT INTO public.orbital_system VALUES (666, 75, NULL);
INSERT INTO public.orbital_system VALUES (667, 106, NULL);
INSERT INTO public.orbital_system VALUES (668, 106, NULL);
INSERT INTO public.orbital_system VALUES (669, 106, NULL);
INSERT INTO public.orbital_system VALUES (670, 106, NULL);
INSERT INTO public.orbital_system VALUES (671, 102, NULL);
INSERT INTO public.orbital_system VALUES (672, 102, NULL);
INSERT INTO public.orbital_system VALUES (673, 102, NULL);
INSERT INTO public.orbital_system VALUES (674, 102, NULL);
INSERT INTO public.orbital_system VALUES (675, 57, NULL);
INSERT INTO public.orbital_system VALUES (676, 57, NULL);
INSERT INTO public.orbital_system VALUES (677, 57, NULL);
INSERT INTO public.orbital_system VALUES (678, 57, NULL);
INSERT INTO public.orbital_system VALUES (679, 148, NULL);
INSERT INTO public.orbital_system VALUES (680, 148, NULL);
INSERT INTO public.orbital_system VALUES (681, 148, NULL);
INSERT INTO public.orbital_system VALUES (682, 148, NULL);
INSERT INTO public.orbital_system VALUES (683, 173, NULL);
INSERT INTO public.orbital_system VALUES (684, 173, NULL);
INSERT INTO public.orbital_system VALUES (685, 173, NULL);
INSERT INTO public.orbital_system VALUES (686, 173, NULL);
INSERT INTO public.orbital_system VALUES (687, 130, NULL);
INSERT INTO public.orbital_system VALUES (688, 130, NULL);
INSERT INTO public.orbital_system VALUES (689, 130, NULL);
INSERT INTO public.orbital_system VALUES (690, 130, NULL);
INSERT INTO public.orbital_system VALUES (691, 188, NULL);
INSERT INTO public.orbital_system VALUES (692, 188, NULL);
INSERT INTO public.orbital_system VALUES (693, 189, NULL);
INSERT INTO public.orbital_system VALUES (694, 189, NULL);
INSERT INTO public.orbital_system VALUES (695, 189, NULL);
INSERT INTO public.orbital_system VALUES (696, 189, NULL);
INSERT INTO public.orbital_system VALUES (697, 189, NULL);
INSERT INTO public.orbital_system VALUES (698, 190, NULL);
INSERT INTO public.orbital_system VALUES (699, 191, NULL);
INSERT INTO public.orbital_system VALUES (700, 192, NULL);
INSERT INTO public.orbital_system VALUES (701, 192, NULL);
INSERT INTO public.orbital_system VALUES (702, 192, NULL);
INSERT INTO public.orbital_system VALUES (703, 192, NULL);
INSERT INTO public.orbital_system VALUES (704, 192, NULL);
INSERT INTO public.orbital_system VALUES (705, 193, NULL);
INSERT INTO public.orbital_system VALUES (706, 193, NULL);
INSERT INTO public.orbital_system VALUES (707, 193, NULL);
INSERT INTO public.orbital_system VALUES (708, 193, NULL);
INSERT INTO public.orbital_system VALUES (709, 193, NULL);
INSERT INTO public.orbital_system VALUES (710, 193, NULL);
INSERT INTO public.orbital_system VALUES (711, 194, NULL);
INSERT INTO public.orbital_system VALUES (712, 194, NULL);
INSERT INTO public.orbital_system VALUES (713, 194, NULL);
INSERT INTO public.orbital_system VALUES (714, 194, NULL);
INSERT INTO public.orbital_system VALUES (715, 194, NULL);
INSERT INTO public.orbital_system VALUES (716, 195, NULL);
INSERT INTO public.orbital_system VALUES (717, 195, NULL);
INSERT INTO public.orbital_system VALUES (718, 196, NULL);
INSERT INTO public.orbital_system VALUES (719, 196, NULL);
INSERT INTO public.orbital_system VALUES (720, 196, NULL);
INSERT INTO public.orbital_system VALUES (721, 196, NULL);
INSERT INTO public.orbital_system VALUES (722, 196, NULL);
INSERT INTO public.orbital_system VALUES (723, 197, NULL);
INSERT INTO public.orbital_system VALUES (724, 198, NULL);
INSERT INTO public.orbital_system VALUES (725, 199, NULL);
INSERT INTO public.orbital_system VALUES (726, 199, NULL);
INSERT INTO public.orbital_system VALUES (727, 199, NULL);
INSERT INTO public.orbital_system VALUES (728, 199, NULL);
INSERT INTO public.orbital_system VALUES (729, 200, NULL);
INSERT INTO public.orbital_system VALUES (730, 201, NULL);
INSERT INTO public.orbital_system VALUES (731, 201, NULL);
INSERT INTO public.orbital_system VALUES (732, 201, NULL);
INSERT INTO public.orbital_system VALUES (733, 202, NULL);
INSERT INTO public.orbital_system VALUES (734, 202, NULL);
INSERT INTO public.orbital_system VALUES (735, 202, NULL);
INSERT INTO public.orbital_system VALUES (736, 202, NULL);
INSERT INTO public.orbital_system VALUES (737, 203, NULL);
INSERT INTO public.orbital_system VALUES (738, 203, NULL);
INSERT INTO public.orbital_system VALUES (739, 203, NULL);
INSERT INTO public.orbital_system VALUES (740, 204, NULL);
INSERT INTO public.orbital_system VALUES (741, 204, NULL);
INSERT INTO public.orbital_system VALUES (742, 204, NULL);
INSERT INTO public.orbital_system VALUES (743, 204, NULL);
INSERT INTO public.orbital_system VALUES (744, 205, NULL);
INSERT INTO public.orbital_system VALUES (745, 205, NULL);
INSERT INTO public.orbital_system VALUES (746, 206, NULL);
INSERT INTO public.orbital_system VALUES (747, 206, NULL);
INSERT INTO public.orbital_system VALUES (748, 207, NULL);
INSERT INTO public.orbital_system VALUES (749, 207, NULL);
INSERT INTO public.orbital_system VALUES (750, 207, NULL);
INSERT INTO public.orbital_system VALUES (751, 207, NULL);
INSERT INTO public.orbital_system VALUES (752, 207, NULL);
INSERT INTO public.orbital_system VALUES (753, 208, NULL);
INSERT INTO public.orbital_system VALUES (754, 208, NULL);
INSERT INTO public.orbital_system VALUES (755, 208, NULL);
INSERT INTO public.orbital_system VALUES (756, 208, NULL);
INSERT INTO public.orbital_system VALUES (757, 209, NULL);
INSERT INTO public.orbital_system VALUES (758, 209, NULL);
INSERT INTO public.orbital_system VALUES (759, 209, NULL);
INSERT INTO public.orbital_system VALUES (760, 210, NULL);
INSERT INTO public.orbital_system VALUES (761, 210, NULL);
INSERT INTO public.orbital_system VALUES (762, 210, NULL);
INSERT INTO public.orbital_system VALUES (763, 211, NULL);
INSERT INTO public.orbital_system VALUES (764, 211, NULL);
INSERT INTO public.orbital_system VALUES (765, 211, NULL);
INSERT INTO public.orbital_system VALUES (766, 211, NULL);
INSERT INTO public.orbital_system VALUES (767, 211, NULL);
INSERT INTO public.orbital_system VALUES (768, 212, NULL);
INSERT INTO public.orbital_system VALUES (769, 212, NULL);
INSERT INTO public.orbital_system VALUES (770, 212, NULL);
INSERT INTO public.orbital_system VALUES (771, 212, NULL);
INSERT INTO public.orbital_system VALUES (772, 212, NULL);
INSERT INTO public.orbital_system VALUES (773, 212, NULL);
INSERT INTO public.orbital_system VALUES (780, 214, NULL);
INSERT INTO public.orbital_system VALUES (781, 214, NULL);
INSERT INTO public.orbital_system VALUES (782, 214, NULL);
INSERT INTO public.orbital_system VALUES (783, 215, NULL);
INSERT INTO public.orbital_system VALUES (784, 215, NULL);
INSERT INTO public.orbital_system VALUES (785, 216, NULL);
INSERT INTO public.orbital_system VALUES (786, 216, NULL);
INSERT INTO public.orbital_system VALUES (787, 216, NULL);
INSERT INTO public.orbital_system VALUES (788, 217, NULL);
INSERT INTO public.orbital_system VALUES (789, 217, NULL);
INSERT INTO public.orbital_system VALUES (790, 217, NULL);
INSERT INTO public.orbital_system VALUES (791, 217, NULL);
INSERT INTO public.orbital_system VALUES (792, 217, NULL);
INSERT INTO public.orbital_system VALUES (793, 218, NULL);
INSERT INTO public.orbital_system VALUES (794, 218, NULL);
INSERT INTO public.orbital_system VALUES (795, 219, NULL);
INSERT INTO public.orbital_system VALUES (796, 219, NULL);
INSERT INTO public.orbital_system VALUES (797, 219, NULL);
INSERT INTO public.orbital_system VALUES (798, 219, NULL);
INSERT INTO public.orbital_system VALUES (799, 219, NULL);
INSERT INTO public.orbital_system VALUES (800, 220, NULL);
INSERT INTO public.orbital_system VALUES (801, 220, NULL);
INSERT INTO public.orbital_system VALUES (802, 220, NULL);
INSERT INTO public.orbital_system VALUES (803, 220, NULL);
INSERT INTO public.orbital_system VALUES (804, 220, NULL);
INSERT INTO public.orbital_system VALUES (805, 221, NULL);
INSERT INTO public.orbital_system VALUES (806, 221, NULL);
INSERT INTO public.orbital_system VALUES (807, 221, NULL);
INSERT INTO public.orbital_system VALUES (808, 221, NULL);
INSERT INTO public.orbital_system VALUES (809, 222, NULL);
INSERT INTO public.orbital_system VALUES (810, 222, NULL);
INSERT INTO public.orbital_system VALUES (811, 222, NULL);
INSERT INTO public.orbital_system VALUES (812, 222, NULL);
INSERT INTO public.orbital_system VALUES (813, 223, NULL);
INSERT INTO public.orbital_system VALUES (814, 223, NULL);
INSERT INTO public.orbital_system VALUES (815, 223, NULL);
INSERT INTO public.orbital_system VALUES (816, 223, NULL);
INSERT INTO public.orbital_system VALUES (817, 223, NULL);
INSERT INTO public.orbital_system VALUES (818, 223, NULL);
INSERT INTO public.orbital_system VALUES (819, 224, NULL);
INSERT INTO public.orbital_system VALUES (820, 224, NULL);
INSERT INTO public.orbital_system VALUES (821, 224, NULL);
INSERT INTO public.orbital_system VALUES (822, 224, NULL);
INSERT INTO public.orbital_system VALUES (823, 224, NULL);
INSERT INTO public.orbital_system VALUES (824, 225, NULL);
INSERT INTO public.orbital_system VALUES (825, 225, NULL);
INSERT INTO public.orbital_system VALUES (826, 225, NULL);
INSERT INTO public.orbital_system VALUES (827, 226, NULL);
INSERT INTO public.orbital_system VALUES (828, 227, NULL);
INSERT INTO public.orbital_system VALUES (829, 227, NULL);
INSERT INTO public.orbital_system VALUES (830, 227, NULL);
INSERT INTO public.orbital_system VALUES (831, 227, NULL);
INSERT INTO public.orbital_system VALUES (832, 228, NULL);
INSERT INTO public.orbital_system VALUES (833, 228, NULL);
INSERT INTO public.orbital_system VALUES (834, 228, NULL);
INSERT INTO public.orbital_system VALUES (835, 228, NULL);
INSERT INTO public.orbital_system VALUES (836, 228, NULL);
INSERT INTO public.orbital_system VALUES (837, 228, NULL);
INSERT INTO public.orbital_system VALUES (838, 229, NULL);
INSERT INTO public.orbital_system VALUES (839, 229, NULL);
INSERT INTO public.orbital_system VALUES (840, 229, NULL);
INSERT INTO public.orbital_system VALUES (841, 229, NULL);
INSERT INTO public.orbital_system VALUES (842, 229, NULL);
INSERT INTO public.orbital_system VALUES (843, 230, NULL);
INSERT INTO public.orbital_system VALUES (844, 230, NULL);
INSERT INTO public.orbital_system VALUES (845, 230, NULL);
INSERT INTO public.orbital_system VALUES (846, 230, NULL);
INSERT INTO public.orbital_system VALUES (847, 230, NULL);
INSERT INTO public.orbital_system VALUES (848, 231, NULL);
INSERT INTO public.orbital_system VALUES (849, 232, NULL);
INSERT INTO public.orbital_system VALUES (850, 232, NULL);
INSERT INTO public.orbital_system VALUES (851, 232, NULL);
INSERT INTO public.orbital_system VALUES (852, 232, NULL);
INSERT INTO public.orbital_system VALUES (853, 232, NULL);
INSERT INTO public.orbital_system VALUES (854, 232, NULL);
INSERT INTO public.orbital_system VALUES (855, 233, NULL);
INSERT INTO public.orbital_system VALUES (856, 233, NULL);
INSERT INTO public.orbital_system VALUES (857, 233, NULL);
INSERT INTO public.orbital_system VALUES (858, 234, NULL);
INSERT INTO public.orbital_system VALUES (859, 234, NULL);
INSERT INTO public.orbital_system VALUES (860, 234, NULL);
INSERT INTO public.orbital_system VALUES (861, 234, NULL);
INSERT INTO public.orbital_system VALUES (862, 234, NULL);
INSERT INTO public.orbital_system VALUES (863, 235, NULL);
INSERT INTO public.orbital_system VALUES (864, 235, NULL);
INSERT INTO public.orbital_system VALUES (865, 235, NULL);
INSERT INTO public.orbital_system VALUES (866, 235, NULL);
INSERT INTO public.orbital_system VALUES (867, 235, NULL);
INSERT INTO public.orbital_system VALUES (868, 236, NULL);
INSERT INTO public.orbital_system VALUES (869, 237, NULL);
INSERT INTO public.orbital_system VALUES (870, 237, NULL);
INSERT INTO public.orbital_system VALUES (871, 237, NULL);
INSERT INTO public.orbital_system VALUES (872, 237, NULL);
INSERT INTO public.orbital_system VALUES (873, 237, NULL);
INSERT INTO public.orbital_system VALUES (874, 237, NULL);
INSERT INTO public.orbital_system VALUES (875, 238, NULL);
INSERT INTO public.orbital_system VALUES (876, 238, NULL);
INSERT INTO public.orbital_system VALUES (877, 239, NULL);
INSERT INTO public.orbital_system VALUES (878, 239, NULL);
INSERT INTO public.orbital_system VALUES (879, 240, NULL);
INSERT INTO public.orbital_system VALUES (880, 240, NULL);
INSERT INTO public.orbital_system VALUES (881, 240, NULL);
INSERT INTO public.orbital_system VALUES (882, 240, NULL);
INSERT INTO public.orbital_system VALUES (883, 240, NULL);
INSERT INTO public.orbital_system VALUES (884, 240, NULL);
INSERT INTO public.orbital_system VALUES (885, 241, NULL);
INSERT INTO public.orbital_system VALUES (886, 241, NULL);
INSERT INTO public.orbital_system VALUES (887, 241, NULL);
INSERT INTO public.orbital_system VALUES (888, 241, NULL);
INSERT INTO public.orbital_system VALUES (889, 242, NULL);
INSERT INTO public.orbital_system VALUES (890, 243, NULL);
INSERT INTO public.orbital_system VALUES (891, 243, NULL);
INSERT INTO public.orbital_system VALUES (892, 243, NULL);
INSERT INTO public.orbital_system VALUES (893, 243, NULL);
INSERT INTO public.orbital_system VALUES (894, 243, NULL);
INSERT INTO public.orbital_system VALUES (895, 243, NULL);
INSERT INTO public.orbital_system VALUES (896, 244, NULL);
INSERT INTO public.orbital_system VALUES (897, 244, NULL);
INSERT INTO public.orbital_system VALUES (898, 244, NULL);
INSERT INTO public.orbital_system VALUES (899, 244, NULL);
INSERT INTO public.orbital_system VALUES (900, 245, NULL);
INSERT INTO public.orbital_system VALUES (901, 245, NULL);
INSERT INTO public.orbital_system VALUES (902, 246, NULL);
INSERT INTO public.orbital_system VALUES (903, 246, NULL);
INSERT INTO public.orbital_system VALUES (904, 246, NULL);
INSERT INTO public.orbital_system VALUES (905, 246, NULL);
INSERT INTO public.orbital_system VALUES (906, 246, NULL);
INSERT INTO public.orbital_system VALUES (907, 246, NULL);
INSERT INTO public.orbital_system VALUES (908, 247, NULL);
INSERT INTO public.orbital_system VALUES (909, 247, NULL);
INSERT INTO public.orbital_system VALUES (910, 247, NULL);
INSERT INTO public.orbital_system VALUES (911, 247, NULL);
INSERT INTO public.orbital_system VALUES (912, 248, NULL);
INSERT INTO public.orbital_system VALUES (913, 248, NULL);
INSERT INTO public.orbital_system VALUES (914, 249, NULL);
INSERT INTO public.orbital_system VALUES (915, 249, NULL);
INSERT INTO public.orbital_system VALUES (916, 249, NULL);
INSERT INTO public.orbital_system VALUES (917, 249, NULL);
INSERT INTO public.orbital_system VALUES (918, 250, NULL);
INSERT INTO public.orbital_system VALUES (919, 250, NULL);
INSERT INTO public.orbital_system VALUES (920, 251, NULL);
INSERT INTO public.orbital_system VALUES (921, 251, NULL);
INSERT INTO public.orbital_system VALUES (922, 251, NULL);
INSERT INTO public.orbital_system VALUES (923, 251, NULL);
INSERT INTO public.orbital_system VALUES (924, 251, NULL);
INSERT INTO public.orbital_system VALUES (925, 252, NULL);
INSERT INTO public.orbital_system VALUES (926, 252, NULL);
INSERT INTO public.orbital_system VALUES (927, 253, NULL);
INSERT INTO public.orbital_system VALUES (928, 253, NULL);
INSERT INTO public.orbital_system VALUES (929, 253, NULL);
INSERT INTO public.orbital_system VALUES (930, 253, NULL);
INSERT INTO public.orbital_system VALUES (931, 253, NULL);
INSERT INTO public.orbital_system VALUES (932, 254, NULL);
INSERT INTO public.orbital_system VALUES (933, 254, NULL);
INSERT INTO public.orbital_system VALUES (934, 254, NULL);
INSERT INTO public.orbital_system VALUES (935, 254, NULL);
INSERT INTO public.orbital_system VALUES (936, 255, NULL);
INSERT INTO public.orbital_system VALUES (937, 255, NULL);
INSERT INTO public.orbital_system VALUES (938, 256, NULL);
INSERT INTO public.orbital_system VALUES (939, 256, NULL);
INSERT INTO public.orbital_system VALUES (940, 256, NULL);
INSERT INTO public.orbital_system VALUES (941, 257, NULL);
INSERT INTO public.orbital_system VALUES (942, 258, NULL);
INSERT INTO public.orbital_system VALUES (943, 258, NULL);
INSERT INTO public.orbital_system VALUES (944, 258, NULL);
INSERT INTO public.orbital_system VALUES (945, 258, NULL);
INSERT INTO public.orbital_system VALUES (946, 258, NULL);
INSERT INTO public.orbital_system VALUES (947, 258, NULL);
INSERT INTO public.orbital_system VALUES (948, 259, NULL);
INSERT INTO public.orbital_system VALUES (949, 259, NULL);
INSERT INTO public.orbital_system VALUES (950, 259, NULL);
INSERT INTO public.orbital_system VALUES (951, 259, NULL);
INSERT INTO public.orbital_system VALUES (952, 259, NULL);
INSERT INTO public.orbital_system VALUES (953, 260, NULL);
INSERT INTO public.orbital_system VALUES (954, 260, NULL);
INSERT INTO public.orbital_system VALUES (955, 261, NULL);
INSERT INTO public.orbital_system VALUES (956, 261, NULL);
INSERT INTO public.orbital_system VALUES (957, 261, NULL);
INSERT INTO public.orbital_system VALUES (958, 261, NULL);
INSERT INTO public.orbital_system VALUES (959, 262, NULL);
INSERT INTO public.orbital_system VALUES (960, 262, NULL);
INSERT INTO public.orbital_system VALUES (961, 262, NULL);
INSERT INTO public.orbital_system VALUES (962, 262, NULL);
INSERT INTO public.orbital_system VALUES (963, 263, NULL);
INSERT INTO public.orbital_system VALUES (964, 263, NULL);
INSERT INTO public.orbital_system VALUES (965, 263, NULL);
INSERT INTO public.orbital_system VALUES (966, 263, NULL);
INSERT INTO public.orbital_system VALUES (967, 264, NULL);
INSERT INTO public.orbital_system VALUES (968, 264, NULL);
INSERT INTO public.orbital_system VALUES (969, 265, NULL);
INSERT INTO public.orbital_system VALUES (970, 265, NULL);
INSERT INTO public.orbital_system VALUES (971, 265, NULL);
INSERT INTO public.orbital_system VALUES (972, 266, NULL);
INSERT INTO public.orbital_system VALUES (973, 266, NULL);
INSERT INTO public.orbital_system VALUES (974, 266, NULL);
INSERT INTO public.orbital_system VALUES (975, 266, NULL);
INSERT INTO public.orbital_system VALUES (976, 266, NULL);
INSERT INTO public.orbital_system VALUES (977, 267, NULL);
INSERT INTO public.orbital_system VALUES (978, 267, NULL);
INSERT INTO public.orbital_system VALUES (979, 267, NULL);
INSERT INTO public.orbital_system VALUES (980, 268, NULL);
INSERT INTO public.orbital_system VALUES (981, 268, NULL);
INSERT INTO public.orbital_system VALUES (982, 269, NULL);
INSERT INTO public.orbital_system VALUES (983, 270, NULL);
INSERT INTO public.orbital_system VALUES (984, 270, NULL);
INSERT INTO public.orbital_system VALUES (985, 270, NULL);
INSERT INTO public.orbital_system VALUES (986, 270, NULL);
INSERT INTO public.orbital_system VALUES (987, 271, NULL);
INSERT INTO public.orbital_system VALUES (988, 272, NULL);
INSERT INTO public.orbital_system VALUES (989, 272, NULL);
INSERT INTO public.orbital_system VALUES (990, 272, NULL);
INSERT INTO public.orbital_system VALUES (991, 272, NULL);
INSERT INTO public.orbital_system VALUES (992, 272, NULL);
INSERT INTO public.orbital_system VALUES (993, 272, NULL);
INSERT INTO public.orbital_system VALUES (994, 273, NULL);
INSERT INTO public.orbital_system VALUES (995, 273, NULL);
INSERT INTO public.orbital_system VALUES (996, 273, NULL);
INSERT INTO public.orbital_system VALUES (997, 273, NULL);
INSERT INTO public.orbital_system VALUES (998, 273, NULL);
INSERT INTO public.orbital_system VALUES (999, 273, NULL);
INSERT INTO public.orbital_system VALUES (1000, 213, NULL);
INSERT INTO public.orbital_system VALUES (1001, 213, NULL);
INSERT INTO public.orbital_system VALUES (1002, 213, NULL);
INSERT INTO public.orbital_system VALUES (1003, 213, NULL);


ALTER TABLE public.orbital_system ENABLE TRIGGER ALL;

--
-- TOC entry 3159 (class 0 OID 97427)
-- Dependencies: 230
-- Data for Name: player_planet_scan_log; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.player_planet_scan_log DISABLE TRIGGER ALL;

INSERT INTO public.player_planet_scan_log VALUES (1, 1, 119, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (2, 1, 147, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (3, 1, 21, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (4, 1, 159, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (5, 1, 3, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (6, 1, 178, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (7, 1, 138, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (8, 1, 124, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (10, 1, 132, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (11, 1, 17, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (12, 1, 83, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (13, 1, 61, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (14, 1, 68, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (15, 1, 177, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (16, 1, 50, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (17, 1, 70, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (18, 1, 140, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (19, 1, 187, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (20, 1, 67, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (21, 1, 44, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (22, 1, 166, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (23, 1, 123, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (24, 1, 106, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (25, 1, 45, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (26, 6, 150, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (27, 6, 136, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (28, 6, 59, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (29, 6, 156, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (30, 6, 157, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (31, 6, 60, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (32, 6, 132, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (34, 6, 110, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (35, 6, 10, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (36, 6, 120, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (37, 6, 50, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (38, 6, 5, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (39, 6, 172, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (40, 6, 87, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (41, 6, 182, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (42, 6, 116, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (43, 6, 19, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (44, 6, 85, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (45, 2, 181, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (46, 2, 119, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (47, 2, 21, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (48, 2, 37, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (49, 2, 138, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (50, 2, 111, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (51, 2, 124, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (52, 2, 43, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (53, 2, 154, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (54, 2, 135, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (56, 2, 78, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (57, 2, 83, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (58, 2, 26, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (59, 2, 68, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (60, 2, 174, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (61, 2, 177, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (62, 2, 70, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (63, 2, 99, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (64, 2, 67, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (65, 2, 105, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (66, 2, 44, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (67, 2, 64, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (68, 2, 76, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (69, 2, 123, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (70, 2, 106, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (71, 2, 133, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (72, 2, 66, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (73, 2, 179, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (74, 4, 124, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (75, 4, 43, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (76, 4, 17, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (77, 4, 83, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (78, 4, 61, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (79, 4, 68, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (80, 4, 177, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (81, 4, 70, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (82, 4, 140, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (83, 4, 99, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (84, 4, 187, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (85, 4, 67, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (86, 4, 44, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (87, 4, 166, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (88, 4, 76, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (89, 4, 25, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (90, 4, 123, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (92, 4, 62, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (93, 4, 45, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (94, 4, 66, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (95, 4, 158, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (96, 4, 179, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (97, 4, 41, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (98, 4, 155, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (99, 4, 27, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (100, 4, 18, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (101, 4, 113, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (102, 4, 101, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (103, 4, 58, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (104, 4, 122, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (105, 4, 144, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (106, 3, 50, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (107, 3, 172, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (108, 3, 140, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (109, 3, 187, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (110, 3, 44, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (111, 3, 87, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (112, 3, 166, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (113, 3, 182, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (114, 3, 25, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (115, 3, 116, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (116, 3, 62, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (117, 3, 19, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (118, 3, 184, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (119, 3, 45, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (120, 3, 33, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (121, 3, 85, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (123, 3, 41, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (124, 3, 176, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (125, 3, 180, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (126, 3, 155, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (127, 3, 27, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (128, 3, 162, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (129, 3, 146, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (130, 3, 7, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (131, 3, 36, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (132, 3, 170, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (133, 3, 144, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (134, 3, 47, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (135, 3, 31, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (136, 3, 129, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (137, 3, 29, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (138, 3, 71, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (139, 3, 54, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (140, 3, 65, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (141, 3, 148, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (142, 7, 75, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (143, 7, 176, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (144, 7, 180, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (145, 7, 162, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (146, 7, 146, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (147, 7, 7, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (148, 7, 32, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (149, 7, 36, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (150, 7, 170, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (151, 7, 47, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (152, 7, 31, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (153, 7, 129, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (154, 7, 73, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (155, 7, 29, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (156, 7, 71, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (157, 7, 54, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (158, 7, 112, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (159, 7, 65, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (161, 7, 167, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (162, 7, 92, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (163, 7, 69, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (164, 7, 42, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (165, 7, 9, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (166, 7, 169, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (167, 7, 11, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (168, 7, 153, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (169, 7, 107, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (170, 7, 108, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (171, 7, 30, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (172, 7, 94, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (173, 7, 2, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (174, 7, 52, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (175, 7, 145, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (176, 7, 35, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (177, 7, 39, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (178, 5, 155, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (179, 5, 18, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (180, 5, 101, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (181, 5, 58, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (182, 5, 122, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (183, 5, 144, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (184, 5, 168, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (185, 5, 47, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (186, 5, 114, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (187, 5, 54, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (188, 5, 98, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (189, 5, 81, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (190, 5, 22, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (191, 5, 112, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (192, 5, 65, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (193, 5, 95, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (194, 5, 92, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (195, 5, 79, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (197, 5, 28, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (198, 5, 9, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (199, 5, 11, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (200, 5, 185, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (201, 5, 38, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (202, 5, 161, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (203, 5, 108, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (204, 5, 30, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (205, 5, 94, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (206, 5, 103, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (207, 5, 93, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (208, 5, 12, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (209, 5, 8, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (210, 5, 126, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (211, 5, 13, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (212, 5, 86, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (213, 5, 165, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (214, 8, 34, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (215, 8, 72, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (216, 8, 14, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (217, 8, 96, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (218, 8, 4, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (219, 8, 55, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (220, 8, 160, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (221, 8, 163, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (222, 8, 141, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (224, 8, 164, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (225, 8, 121, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (226, 8, 91, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (227, 8, 53, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (228, 8, 16, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (229, 8, 149, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (230, 8, 82, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (231, 8, 15, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (232, 8, 137, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (233, 8, 74, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (234, 9, 153, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (235, 9, 107, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (236, 9, 108, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (237, 9, 30, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (238, 9, 2, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (239, 9, 52, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (240, 9, 145, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (241, 9, 35, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (242, 9, 8, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (243, 9, 109, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (244, 9, 39, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (245, 9, 13, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (246, 9, 6, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (247, 9, 104, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (248, 9, 118, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (250, 9, 127, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (251, 9, 51, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (252, 9, 100, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (253, 9, 1, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (254, 9, 84, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (255, 9, 46, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (256, 9, 77, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (257, 9, 40, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (122, 3, 75, '0000ff', true);
INSERT INTO public.player_planet_scan_log VALUES (91, 4, 106, '00aaff', true);
INSERT INTO public.player_planet_scan_log VALUES (33, 6, 57, '00ff00', true);
INSERT INTO public.player_planet_scan_log VALUES (196, 5, 102, '00ffaa', true);
INSERT INTO public.player_planet_scan_log VALUES (55, 2, 17, 'aa00ff', true);
INSERT INTO public.player_planet_scan_log VALUES (160, 7, 148, 'aaff00', true);
INSERT INTO public.player_planet_scan_log VALUES (249, 9, 130, 'ff0000', true);
INSERT INTO public.player_planet_scan_log VALUES (9, 1, 43, 'ff00aa', true);
INSERT INTO public.player_planet_scan_log VALUES (223, 8, 173, 'ffaa00', true);
INSERT INTO public.player_planet_scan_log VALUES (258, 3, 5, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (259, 3, 61, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (260, 3, 10, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (261, 3, 110, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (262, 3, 132, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (263, 4, 119, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (264, 4, 3, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (265, 4, 184, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (266, 4, 147, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (267, 4, 50, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (268, 6, 178, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (269, 6, 176, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (270, 6, 33, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (271, 5, 91, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (272, 5, 163, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (273, 5, 171, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (274, 5, 4, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (275, 5, 113, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (276, 2, 147, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (277, 7, 6, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (278, 9, 169, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (279, 9, 9, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (280, 1, 154, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (281, 1, 158, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (282, 1, 62, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (283, 8, 88, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (284, 8, 95, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (285, 3, 60, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (286, 3, 73, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (287, 3, 32, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (288, 4, 75, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (289, 6, 25, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (290, 6, 184, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (291, 6, 187, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (292, 5, 183, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (293, 5, 35, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (294, 5, 153, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (295, 2, 63, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (296, 2, 115, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (297, 2, 24, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (298, 7, 109, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (299, 9, 94, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (300, 9, 69, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (301, 8, 175, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (302, 8, 49, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (303, 8, 171, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (304, 8, 185, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (305, 1, 99, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (306, 1, 66, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (307, 1, 76, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (308, 10, 213, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (309, 10, 273, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (310, 10, 272, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (311, 10, 271, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (312, 10, 270, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (313, 10, 264, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (314, 10, 262, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (315, 10, 260, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (316, 10, 259, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (317, 10, 257, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (318, 10, 256, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (319, 10, 252, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (320, 10, 250, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (321, 10, 248, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (322, 10, 244, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (323, 10, 240, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (324, 10, 238, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (325, 10, 237, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (326, 10, 235, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (327, 10, 233, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (328, 10, 231, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (329, 10, 230, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (330, 10, 228, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (331, 10, 226, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (332, 10, 224, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (333, 10, 221, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (334, 10, 217, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (335, 10, 216, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (336, 10, 215, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (337, 10, 214, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (338, 10, 210, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (339, 10, 208, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (340, 10, 206, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (341, 10, 204, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (342, 10, 200, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (343, 10, 195, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (344, 10, 193, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (345, 10, 192, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (346, 10, 191, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (347, 10, 190, NULL, false);
INSERT INTO public.player_planet_scan_log VALUES (348, 10, 188, NULL, false);


ALTER TABLE public.player_planet_scan_log ENABLE TRIGGER ALL;

--
-- TOC entry 3147 (class 0 OID 97263)
-- Dependencies: 216
-- Data for Name: ship_template; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.ship_template DISABLE TRIGGER ALL;

INSERT INTO public.ship_template VALUES ('Orion_Konglomerat_1', 'Drasid Frachter', 'Orion Konglomerat', 1, '1.jpg', 2, 30, 200, 70, 1, 0, 0, 0, 10, 2, 2, 3);
INSERT INTO public.ship_template VALUES ('Orion_Konglomerat_5', 'Sporiin Scout', 'Orion Konglomerat', 2, '5.jpg', 6, 30, 80, 15, 1, 2, 0, 0, 30, 12, 17, 7);
INSERT INTO public.ship_template VALUES ('Orion_Konglomerat_10', 'Hohinaf', 'Orion Konglomerat', 3, '10.jpg', 70, 32, 180, 30, 2, 2, 0, 0, 190, 10, 20, 3);
INSERT INTO public.ship_template VALUES ('Orion_Konglomerat_15', 'Seseal', 'Orion Konglomerat', 3, '15.jpg', 190, 90, 180, 50, 1, 4, 2, 0, 70, 25, 50, 7);
INSERT INTO public.ship_template VALUES ('Orion_Konglomerat_7', 'Quan Spion', 'Orion Konglomerat', 3, '7.jpg', 2, 80, 40, 60, 1, 0, 4, 0, 55, 25, 10, 20);
INSERT INTO public.ship_template VALUES ('Orion_Konglomerat_2', 'Trova Frachter', 'Orion Konglomerat', 3, '2.jpg', 6, 60, 250, 200, 1, 0, 0, 0, 65, 4, 4, 6);
INSERT INTO public.ship_template VALUES ('Orion_Konglomerat_16', 'Swidrol', 'Orion Konglomerat', 4, '16.jpg', 78, 35, 110, 30, 2, 2, 2, 0, 30, 4, 3, 13);
INSERT INTO public.ship_template VALUES ('Orion_Konglomerat_6', 'Xirclarod', 'Orion Konglomerat', 4, '6.jpg', 286, 115, 450, 250, 2, 4, 2, 0, 150, 32, 37, 23);
INSERT INTO public.ship_template VALUES ('Orion_Konglomerat_17', 'Drovoloon', 'Orion Konglomerat', 5, '17.jpg', 79, 100, 140, 30, 2, 4, 4, 0, 170, 12, 23, 57);
INSERT INTO public.ship_template VALUES ('Orion_Konglomerat_14', 'Guadrol', 'Orion Konglomerat', 5, '14.jpg', 162, 90, 140, 110, 2, 4, 0, 0, 100, 5, 45, 35);
INSERT INTO public.ship_template VALUES ('Orion_Konglomerat_18', 'Erakzil', 'Orion Konglomerat', 6, '18.jpg', 430, 180, 470, 350, 2, 4, 0, 4, 390, 42, 61, 73);
INSERT INTO public.ship_template VALUES ('Orion_Konglomerat_3', 'Xadron Frachter', 'Orion Konglomerat', 6, '3.jpg', 102, 130, 600, 1200, 2, 0, 0, 0, 160, 85, 7, 8);
INSERT INTO public.ship_template VALUES ('Orion_Konglomerat_13', 'Bundaram', 'Orion Konglomerat', 8, '13.jpg', 328, 210, 260, 170, 2, 7, 5, 0, 510, 120, 113, 105);
INSERT INTO public.ship_template VALUES ('Orion_Konglomerat_8', 'Zundrum', 'Orion Konglomerat', 9, '8.jpg', 190, 712, 800, 1050, 10, 6, 0, 0, 970, 125, 150, 527);
INSERT INTO public.ship_template VALUES ('Orion_Konglomerat_12', 'Frodury', 'Orion Konglomerat', 9, '12.jpg', 1870, 820, 1400, 300, 6, 4, 0, 9, 990, 422, 184, 165);
INSERT INTO public.ship_template VALUES ('Orion_Konglomerat_4', 'Niolan Frachter', 'Orion Konglomerat', 10, '4.jpg', 202, 160, 1200, 2600, 4, 0, 0, 0, 220, 125, 13, 18);
INSERT INTO public.ship_template VALUES ('Orion_Konglomerat_11', 'Ciroxyt', 'Orion Konglomerat', 10, '11.jpg', 1010, 650, 560, 320, 4, 10, 10, 0, 810, 240, 343, 350);
INSERT INTO public.ship_template VALUES ('Orion_Konglomerat_9', 'Woalytix', 'Orion Konglomerat', 10, '9.jpg', 120, 920, 450, 2700, 10, 8, 0, 0, 840, 625, 250, 134);


ALTER TABLE public.ship_template ENABLE TRIGGER ALL;

--
-- TOC entry 3157 (class 0 OID 97376)
-- Dependencies: 228
-- Data for Name: ship; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.ship DISABLE TRIGGER ALL;

INSERT INTO public.ship VALUES (12, 'test', 75, 527, 1, 43, 'Orion_Konglomerat_2', 'tachion_flux', NULL, NULL, 0, 0, 100, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL);
INSERT INTO public.ship VALUES (10, 'Freighter', 136, 893, 6, NULL, 'Orion_Konglomerat_6', 'micro_grav', 'desintegrator', 'mun_catapult', 0, 0, 100, 286, 32, 6000, 500, 0, 0, 0, 0, 0, 0, 0, 0, 138, 887, 128, 928, 6, NULL, NULL);
INSERT INTO public.ship VALUES (8, 'Debug ship', 127, 477, 1, 83, 'Orion_Konglomerat_1', 'solarisplasmotan', NULL, NULL, 0, 0, 0, 0, 25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 110, 493, 0, NULL, NULL);
INSERT INTO public.ship VALUES (14, 'Freighter', 663, 527, 5, NULL, 'Orion_Konglomerat_6', 'micro_grav', 'gravitractor', 'micromit_battery', 0, 0, 100, 286, 36, 6000, 500, 0, 0, 0, 11, 0, 0, 0, 0, 678, 542, 637, 503, 6, NULL, NULL);
INSERT INTO public.ship VALUES (15, 'Freighter', 108, 291, 2, NULL, 'Orion_Konglomerat_6', 'micro_grav', 'gravitractor', 'singularity_launcher', 0, 0, 100, 286, 36, 6000, 500, 0, 0, 0, 11, 0, 0, 0, 0, 88, 254, 126, 323, 6, NULL, NULL);
INSERT INTO public.ship VALUES (5, 'Debug ship', 60, 316, 2, 138, 'Orion_Konglomerat_1', 'solarisplasmotan', NULL, NULL, 0, 0, 0, 0, 10, 3166, 161, 11, 11, 13, 4, 0, 0, 0, 0, 5, 340, 77, 317, 7, NULL, NULL);
INSERT INTO public.ship VALUES (16, 'Freighter', 692, 70, 8, NULL, 'Orion_Konglomerat_6', 'micro_grav', 'gravitractor', 'mun_catapult', 0, 0, 100, 286, 36, 6000, 500, 0, 0, 0, 11, 0, 0, 0, 0, 684, 92, 705, 37, 6, NULL, NULL);
INSERT INTO public.ship VALUES (6, 'Debug ship', 629, 830, 7, 69, 'Orion_Konglomerat_1', 'solarisplasmotan', NULL, NULL, 0, 0, 0, 0, 10, 3166, 161, 3, 11, 13, 4, 0, 0, 0, 0, 699, 843, 624, 819, 7, NULL, NULL);
INSERT INTO public.ship VALUES (7, 'Debug ship', 933, 850, 9, 100, 'Orion_Konglomerat_1', 'solarisplasmotan', NULL, NULL, 0, 0, 0, 0, 10, 3166, 161, 7, 1, 13, 4, 0, 0, 0, 0, 975, 893, 931, 842, 7, NULL, NULL);
INSERT INTO public.ship VALUES (11, 'Freighter', 863, 807, 9, 6, 'Orion_Konglomerat_6', 'micro_grav', 'gravitractor', 'mun_catapult', 0, 0, 100, 286, 40, 4500, 125, 17, 38, 6, 0, 0, 0, 0, 0, 830, 878, 885, 802, 6, NULL, NULL);
INSERT INTO public.ship VALUES (9, 'Debug ship', 649, 46, 8, 160, 'Orion_Konglomerat_1', 'solarisplasmotan', NULL, NULL, 0, 0, 0, 0, 10, 3166, 161, 11, 11, 13, 4, 0, 0, 0, 0, 578, 26, 656, 44, 7, NULL, NULL);
INSERT INTO public.ship VALUES (13, 'Freighter', 385, 761, 3, NULL, 'Orion_Konglomerat_6', 'micro_grav', 'gravitractor', 'mun_catapult', 0, 0, 100, 286, 36, 6000, 500, 0, 0, 0, 11, 0, 0, 0, 0, 396, 787, 371, 728, 6, NULL, NULL);
INSERT INTO public.ship VALUES (1, 'Debug ship', 316, 745, 3, 184, 'Orion_Konglomerat_1', 'solarisplasmotan', NULL, NULL, 0, 0, 0, 0, 10, 3166, 161, 11, 11, 13, 4, 0, 0, 0, 0, 268, 708, 324, 742, 7, NULL, NULL);
INSERT INTO public.ship VALUES (2, 'Debug ship', 239, 518, 4, 44, 'Orion_Konglomerat_1', 'solarisplasmotan', NULL, NULL, 0, 0, 0, 0, 10, 3166, 161, 11, 11, 13, 4, 0, 0, 0, 0, 182, 526, 248, 506, 7, NULL, NULL);
INSERT INTO public.ship VALUES (3, 'Debug ship', 181, 976, 6, 120, 'Orion_Konglomerat_1', 'solarisplasmotan', NULL, NULL, 0, 0, 0, 0, 10, 3166, 161, 11, 11, 13, 4, 0, 0, 0, 0, 190, 917, 168, 973, 7, NULL, NULL);
INSERT INTO public.ship VALUES (4, 'Debug ship', 624, 448, 5, 79, 'Orion_Konglomerat_1', 'solarisplasmotan', NULL, NULL, 0, 0, 0, 0, 10, 3166, 161, 9, 6, 13, 4, 0, 0, 0, 0, 563, 412, 625, 455, 7, NULL, NULL);
INSERT INTO public.ship VALUES (17, 'test', 75, 527, 1, 43, 'Orion_Konglomerat_2', 'tachion_flux', NULL, NULL, 0, 0, 100, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL);
INSERT INTO public.ship VALUES (18, 'test', 75, 527, 1, 43, 'Orion_Konglomerat_2', 'tachion_flux', NULL, NULL, 0, 0, 100, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, -1, 0, 0, 0, NULL, NULL);


ALTER TABLE public.ship ENABLE TRIGGER ALL;

--
-- TOC entry 3149 (class 0 OID 97286)
-- Dependencies: 220
-- Data for Name: starbase_hull_stock; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.starbase_hull_stock DISABLE TRIGGER ALL;

INSERT INTO public.starbase_hull_stock VALUES (2, 4, 'Orion_Konglomerat_6', 1);
INSERT INTO public.starbase_hull_stock VALUES (6, 7, 'Orion_Konglomerat_6', 1);


ALTER TABLE public.starbase_hull_stock ENABLE TRIGGER ALL;

--
-- TOC entry 3153 (class 0 OID 97322)
-- Dependencies: 224
-- Data for Name: starbase_propulsion_stock; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.starbase_propulsion_stock DISABLE TRIGGER ALL;

INSERT INTO public.starbase_propulsion_stock VALUES (2, 4, 'micro_grav', 2);
INSERT INTO public.starbase_propulsion_stock VALUES (6, 7, 'micro_grav', 2);
INSERT INTO public.starbase_propulsion_stock VALUES (9, 1, 'tachion_flux', 3);


ALTER TABLE public.starbase_propulsion_stock ENABLE TRIGGER ALL;

--
-- TOC entry 3155 (class 0 OID 97340)
-- Dependencies: 226
-- Data for Name: starbase_ship_construction_job; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.starbase_ship_construction_job DISABLE TRIGGER ALL;



ALTER TABLE public.starbase_ship_construction_job ENABLE TRIGGER ALL;

--
-- TOC entry 3151 (class 0 OID 97304)
-- Dependencies: 222
-- Data for Name: starbase_weapon_stock; Type: TABLE DATA; Schema: public; Owner: skrupeladmin
--

ALTER TABLE public.starbase_weapon_stock DISABLE TRIGGER ALL;

INSERT INTO public.starbase_weapon_stock VALUES (2, 4, 'gravitractor', 4);
INSERT INTO public.starbase_weapon_stock VALUES (3, 4, 'desintegrator', 2);
INSERT INTO public.starbase_weapon_stock VALUES (7, 5, 'desintegrator', 2);
INSERT INTO public.starbase_weapon_stock VALUES (9, 2, 'desintegrator', 2);
INSERT INTO public.starbase_weapon_stock VALUES (10, 7, 'gravitractor', 4);
INSERT INTO public.starbase_weapon_stock VALUES (11, 7, 'desintegrator', 2);
INSERT INTO public.starbase_weapon_stock VALUES (15, 8, 'desintegrator', 2);
INSERT INTO public.starbase_weapon_stock VALUES (1, 3, 'gravitractor', 2);
INSERT INTO public.starbase_weapon_stock VALUES (16, 1, 'laser', 6);
INSERT INTO public.starbase_weapon_stock VALUES (17, 1, 'fusion_rockets', 6);


ALTER TABLE public.starbase_weapon_stock ENABLE TRIGGER ALL;

--
-- TOC entry 3169 (class 0 OID 0)
-- Dependencies: 205
-- Name: game_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.game_id_seq', 3, true);


--
-- TOC entry 3170 (class 0 OID 0)
-- Dependencies: 198
-- Name: login_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.login_id_seq', 4, true);


--
-- TOC entry 3171 (class 0 OID 0)
-- Dependencies: 200
-- Name: login_role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.login_role_id_seq', 8, true);


--
-- TOC entry 3172 (class 0 OID 0)
-- Dependencies: 202
-- Name: native_species_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.native_species_id_seq', 1, false);


--
-- TOC entry 3173 (class 0 OID 0)
-- Dependencies: 233
-- Name: news_entry_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.news_entry_id_seq', 19, true);


--
-- TOC entry 3174 (class 0 OID 0)
-- Dependencies: 231
-- Name: orbital_system_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.orbital_system_id_seq', 1003, true);


--
-- TOC entry 3175 (class 0 OID 0)
-- Dependencies: 212
-- Name: planet_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.planet_id_seq', 273, true);


--
-- TOC entry 3176 (class 0 OID 0)
-- Dependencies: 208
-- Name: player_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.player_id_seq', 11, true);


--
-- TOC entry 3177 (class 0 OID 0)
-- Dependencies: 229
-- Name: player_planet_scan_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.player_planet_scan_log_id_seq', 348, true);


--
-- TOC entry 3178 (class 0 OID 0)
-- Dependencies: 227
-- Name: ship_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.ship_id_seq', 18, true);


--
-- TOC entry 3179 (class 0 OID 0)
-- Dependencies: 219
-- Name: starbase_hull_stock_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.starbase_hull_stock_id_seq', 11, true);


--
-- TOC entry 3180 (class 0 OID 0)
-- Dependencies: 210
-- Name: starbase_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.starbase_id_seq', 10, true);


--
-- TOC entry 3181 (class 0 OID 0)
-- Dependencies: 223
-- Name: starbase_propulsion_stock_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.starbase_propulsion_stock_id_seq', 9, true);


--
-- TOC entry 3182 (class 0 OID 0)
-- Dependencies: 225
-- Name: starbase_ship_construction_job_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.starbase_ship_construction_job_id_seq', 9, true);


--
-- TOC entry 3183 (class 0 OID 0)
-- Dependencies: 221
-- Name: starbase_weapon_stock_id_seq; Type: SEQUENCE SET; Schema: public; Owner: skrupeladmin
--

SELECT pg_catalog.setval('public.starbase_weapon_stock_id_seq', 21, true);


-- Completed on 2019-03-10 16:48:51 CET

--
-- PostgreSQL database dump complete
--

